/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

    /*====================================*/
    /*  MODULE DE CALCUL DES COTISATIONS  */
    /*        programme th-0kCOC.C        */
    /*====================================*/


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif


    /*======*/
    /* MAIN */
    /*======*/

int th_0kcoc(s_coe1 * p_coe1 ,      /* pointeur sur structure entree          */
             s_coe2 * p_coe2 ,      /* pointeur sur structure entree          */
             s_cos  * p_cos)        /* pointeur sur structure sortie          */
{
    /* DECLARATIONS ET INITIALISATIONS */
    static s_signature signature;

    long bnc = 0;
    long bnq = 0;
    long bns = 0;
    long bnn = 0;
    long bng = 0;
    long bne = 0;
    long bnf = 0;
    long bnr = 0;

    int     ret=0;






    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_cos->signature = &signature;
    p_cos->versr = 'A' ;

    /* controle de la signature */



    ret = controle_signature(RKCOC,p_cos->versr, &(p_cos->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* THLV */
    /* Mise � jour des bases nettes */
    if (p_coe1->tyimp == 'T')
    {
        /* CA_THLV_0001 DETERMINATION BASE NETTE IMPOSEE */
        /*          base nette commune                           */

		if( p_coe1->indthlv == 'C' )
        {

			base_nettethlv(p_coe1,p_coe1->p_cols_c);
			if(p_coe2->tisyn != 0)
			{
			    /* base nette  syndicat */
				base_nettethlv(p_coe1,p_coe1->p_cols_s);
            }
		}

		/* base nette  intercommunalit� */
		if ( p_coe1->indthlv == 'I' )
        {
			base_nettethlv(p_coe1,p_coe1->p_cols_q);
		}

        /* CA_THLV_0009 DETERMINATION BASE NETTE IMPOSEE GEMAPI */
        /* Base nette GEMAPI qui peut s'adosser sur Commune/syndicat ou Interco en THLV */
        if ( p_coe2->timpe > 0  && p_coe1->indgem != ' ')
        {
            base_nettethlv(p_coe1,p_coe1->p_cols_e);
        }

        /* CA_THLV_0002 DETREMINATION DES COTISATIONS */
        p_coe1->p_cols_c->coti = cotisation(p_coe1->p_cols_c->bnimp , p_coe2->timpc);
        p_coe1->p_cols_q->coti = cotisation(p_coe1->p_cols_q->bnimp , p_coe2->timpq);
        p_coe1->p_cols_s->coti = cotisation(p_coe1->p_cols_s->bnimp , p_coe2->tisyn);

        /* CA_THLV_0011 DETERMINATION COTISATION GEMAPI */
        p_coe1->p_cols_e->coti = cotisation(p_coe1->p_cols_e->bnimp , p_coe2->timpe);

    }
    else
    {

        /* 1 */
        /* base nette des locaux principaux au niveau commune */

        /* DETERMINATION DE LA VL NETTE  CA_TH_0088 ou CA_TH_0136 ou CA_TH_0141*/
        if (p_coe1->tax == 'P')    /* Cas des articles THP  */
        {
            bnc = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_c));

            bnq = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_q));
            bns = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_s));
            bnf = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_f));
            bnr = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_r));

            bnn = vlNette (p_coe1->vlt,totAb(p_coe1->p_cols_n));
            bng = vlNette (p_coe1->vlt,totAb(p_coe1->p_cols_g));
            bne = vlNette (p_coe1->vlt,totAb(p_coe1->p_cols_e));

            if (bnc < 0){bnc =0;}
            if (bnq < 0){bnq =0;}
            if (bns < 0){bns =0;}
            if (bnf < 0){bnf =0;}
            if (bnr < 0){bnr =0;}

            if (bnn < 0){bnn =0;}
            if (bng < 0){bng =0;}
            if (bne < 0){bne =0;}
        }

        if ((p_coe1->tax == 'S') || (p_coe1->tax == 'E'))    /* Cas des articles THS ou THE  */
        {
            bnc = p_coe1->vl;
            bnq = p_coe1->vl;
            bns = p_coe1->vl;
            bnf = p_coe1->vl;
            bnr = p_coe1->vl;
            bnn = p_coe1->vlt;
            bng = p_coe1->vlt;
            bne = p_coe1->vlt;
        }

        /* DETERMINATION DE LA BASE NETTE EXONEREE  ET DE LA BASE NETTE IMPOSEE */
        /*  CA_TH_0089 ET CA_TH_0090 ou CA_TH_0137 et CA_TH_0138
        ou CA_TH_0142 et CA_TH_0143*/

        if (p_coe1->degex == 'F' || p_coe1->degex == 'I' || p_coe1->degex == 'A' || p_coe1->degex == 'D'
        || p_coe1->degex == 'V' || p_coe1->degex == 'S' || p_coe1->degex == 'W' || p_coe1->degex == 'K')
        {
            /* 2019MAJR08-01-01  */
            majBaseImpDansColsCQDFR(p_coe1->p_cols_c,0,bnc);
            majBaseImpDansColsCQDFR(p_coe1->p_cols_f,0,bnf);
            if (p_coe1->cocnq == 'C')
            {
                majBaseImpDansColsCQDFR(p_coe1->p_cols_q,0,bnq);
                majBaseImpDansColsCQDFR(p_coe1->p_cols_r,0,bnr);

            }


            majBaseImpDansCols(p_coe1->p_cols_s,0,bns, p_coe2->tisyn);
            majBaseImpDansCols(p_coe1->p_cols_n,0,bnn, p_coe2->titsn);
            majBaseImpDansCols(p_coe1->p_cols_g,0,bng, p_coe2->titgp);
            majBaseImpDansCols(p_coe1->p_cols_e,0,bne, p_coe2->timpe);
        }
        else
        {
            /* CA_G0085 exonerations HLM SEM */

            if ((p_coe1->permo =='5' || p_coe1->permo =='6') &&
                 (p_coe1->aff == 'H' || p_coe1->aff == 'M' || p_coe1->aff == 'F')
                 && (p_coe1->grrev == '2' || p_coe1->grrev == ' '))
            {
                majBaseImpDansColsCQDFR(p_coe1->p_cols_c,bnc,0);
                majBaseImpDansColsCQDFR(p_coe1->p_cols_f,bnf,0);
                if (p_coe1->cocnq == 'C')
                {
                    majBaseImpDansColsCQDFR(p_coe1->p_cols_q,bnq,0);
                    majBaseImpDansColsCQDFR(p_coe1->p_cols_r,bnr,0);
                }
                majBaseImpDansCols(p_coe1->p_cols_s,bns,0, p_coe2->tisyn);
                majBaseImpDansCols(p_coe1->p_cols_n,0,bnn, p_coe2->titsn);
                majBaseImpDansCols(p_coe1->p_cols_g,0,bng, p_coe2->titgp);
                majBaseImpDansCols(p_coe1->p_cols_e,0,bne, p_coe2->timpe);
             }
             else
             {
                majBaseImpDansColsCQDFR(p_coe1->p_cols_c,bnc,0);
                majBaseImpDansColsCQDFR(p_coe1->p_cols_f,bnf,0);

                if (p_coe1->cocnq == 'C')
                {
                        majBaseImpDansColsCQDFR(p_coe1->p_cols_q,bnq,0);
                        majBaseImpDansColsCQDFR(p_coe1->p_cols_r,bnr,0);
                }
                majBaseImpDansCols(p_coe1->p_cols_s,bns,0, p_coe2->tisyn);
                majBaseImpDansCols(p_coe1->p_cols_n,bnn,0, p_coe2->titsn);
                majBaseImpDansCols(p_coe1->p_cols_g,bng,0, p_coe2->titgp);
                majBaseImpDansCols(p_coe1->p_cols_e,bne,0, p_coe2->timpe);
             }
        }

        /* DETERMINATION DE LA COTISATION CA_TH_0091 ou CA_TH_0139 ou CA_TH_0144 */

        p_coe1->p_cols_c->coti = cotisation(p_coe1->p_cols_c->bnimp , p_coe2->timpc);
        p_coe1->p_cols_q->coti = cotisation(p_coe1->p_cols_q->bnimp , p_coe2->timpq);
        p_coe1->p_cols_s->coti = cotisation(p_coe1->p_cols_s->bnimp , p_coe2->tisyn);
        p_coe1->p_cols_f->coti = cotisation(p_coe1->p_cols_f->bnimp , p_coe2->timpc);
        p_coe1->p_cols_r->coti = cotisation(p_coe1->p_cols_r->bnimp , p_coe2->timpc);

        p_coe1->p_cols_n->coti = cotisation(p_coe1->p_cols_n->bnimp , p_coe2->titsn);
        p_coe1->p_cols_g->coti = cotisation(p_coe1->p_cols_g->bnimp , p_coe2->titgp);
        p_coe1->p_cols_e->coti = cotisation(p_coe1->p_cols_e->bnimp , p_coe2->timpe);
    }

    return (0);
}

long totAb(s_cols *p_cols)
{
    return (p_cols->abgen + p_cols->abpac + p_cols->abspe + p_cols->abhan);
}

long vlNette(long vl, long totAbat)
{
    return (vl - totAbat);
}

void majBaseImpDansColsCQDFR(s_cols *p_cols, long base, long vlnex)
{
    p_cols->bnimp = base;
    p_cols->vlnex = vlnex;
    p_cols->vlntt = base + vlnex; /* CA_TH_0089 l'une des deux est � 0 */
}

void majBaseImpDansCols(s_cols *p_cols, long base, long vlnex, double taux)
{
    p_cols->bnimp = 0;
    p_cols->vlnex = 0;
    p_cols->vlntt = 0; /* CA_G0077 */

    /* CA_G0077 Donnees renseignees ssi il existe un taux positif */
    if (taux > 0)
    {
        p_cols->bnimp = base;
        p_cols->vlnex = vlnex;
        p_cols->vlntt = base + vlnex; /* CA_TH_0089 l'une des deux est � 0 */
    }
}

void base_nettethlv( s_coe1 *p2, s_cols *p3)
{
    p3->vlntt=p2->vl;
    p3->bnimp=p2->vl;
}

long cotisation (long base, double taux)
{
    return (arrondi_euro_inf(base * taux + 0.5));
}
