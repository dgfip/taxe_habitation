/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/


#define ktos 'A'



/*----------------------------------------------------------------------------
   DIMENSION DU TABLEAU  DES LOCAUX PRO EN ENTREE LOPRE ET EN SORTIE LOPRS
  ----------------------------------------------------------------------------*/
#define DIM_LOCAUX_PRO 300




/*============================================================
           s_liberreur; structure lib erreur calculette th
============================================================*/
typedef struct {
     char    libelle[68];        /*zone libelle erreur calculette th   */
                 }
           s_liberreur  ;
/*- fin de liberreur    :structure lib erreur calculette th  --*/
/*============================================================
           s_erreur;    structure d'erreur  calculette th
============================================================*/
typedef struct {
     long    code;               /*code retour calculette th           */
     s_liberreur     liberreur_;      /*structure lib erreur calculette th  */
                 }
           s_erreur     ;
/*- fin de erreur       :structure d'erreur  calculette th   --*/
/*============================================================
           s_signature; structure signature  calculette th
============================================================*/
typedef struct {
     char    signature[32];      /*zone signature      calculette th   */
                 }
           s_signature  ;
/*- fin de signature    :structure signature  calculette th  --*/
/*============================================================
           s_e1;        entree local a actualiser
============================================================*/
typedef struct {
     char    csdi[04];           /*code dsf de gestion                 */
     char    dep[03];            /*code departement                    */
     char    cne[04];            /*code commune de l'aft               */
     short   antax;              /*annee campagne taxation (millesime) */
     char    aff;                /*code affectation d'un local         */
     long    vlb7l;              /*vl base 70 local en comp pour c     */
     long    vlb7c;              /*vl base 70 locaux communs en c      */
     char    cxloz[03];          /*nature local meuble exonere zrr     */
     short   txzrr;              /*taux d'exoneration local meuble zrr */
     char    tax;                /*P,S ou E en TH, ' ' en THLV ou TLV  */
                 }
           s_e1         ;
/*- fin de e1           :entree local a actualiser           --*/
/*============================================================
           s_s1;        sortie local actualise et revalorise
============================================================*/
typedef struct {
     short   anref;              /*annee ref calculette th (millesime) */
     char    versr;              /*lettre version d'un programme       */
     long    vlbll;              /*vl act/revalorisee du local en c    */
     long    vlblc;              /*vl act/reval des locaux communs en c*/
     long    vlblt;              /*vl act/reval. tot local en c imposee*/
     long    vlxm1;              /*vl exoneree zrr m1 du local en c    */
     long    vlxm2;              /*vl exoneree zrr m2 du local en c    */
     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_s1         ;
/*- fin de s1           :sortie local actualise et revalorise--*/
/*============================================================
           s_cols;      structure sortie collectivite cols
============================================================*/
typedef struct {
     long    abgen;              /*abatt general base  par collectivite*/
     long    abpac;              /*abatt total pac par collectivite    */
     long    abp12;              /*abatt pour pac1-2 par collectivite  */
     long    abspe;              /*abatt special base par collectivite */
     long    abhan;              /*abatt special handicape collectivite*/
     long    vlntt;              /*valeur locative nette  collectivite */
     long    vlnex;              /*vl nette exoneree par collectivite  */
     long    bnimp;              /*base nette d'imposition par collecti*/
     char    absps;              /*indic ASB applique VLM collect reportee */
     char    abspsa;             /*indic ASB applique VLM collect concernee*/
     long    coti;               /*cotisation (basenette imp x tx impot*/
                 }
           s_cols       ;
/*- fin de cols         :structure sortie collectivite cols  --*/
/*============================================================
           s_e2;        entree donnees individuelles th
============================================================*/
typedef struct {
     short   antax;              /*annee campagne taxation (millesime) */
     char    tyimp;              /*type d'impot                        */
     long    vlbpc;              /*vl actualisee pour locaux habitation*/
     long    vlbch;              /*vl brute locaux avec code affect = H*/
     long    vlbp7;              /*vl 70 actualisee pour LH  2017      */
     char    aff;                /*code affectation d'un local         */
     char    permo;              /*code type de personne morale        */
     char    grrev;              /*code groupe revision                */
     char    indaths;            /*Assujetissement majo THS (O ou blc) */
     char    tax;                /*code taxation du local              */
     short   nbpac;              /*nombre total pac th hors era        */
     short   nbera;              /*nombre enfants en residence alternee*/
     char    degex;              /*code degrevement art role th        */
     char    rgsor;              /*rang de sortie exon�ration droit W  */
     char    abatk;              /*code abattement th (K ou blanc)     */
     char    rgsork;             /*rang de sortie abatt (blc ou de 3 � 6)*/
     char    champ;              /*code champ / hors champ redevance tv*/
     char    qtvrt;              /*reponse quest tv foyer fiscal retenu*/
     char    degtv;              /*droit a deg redevance article th    */
     char    codni;              /*code non imposable ir pour la th    */
     char    codsh;              /*code abattement handicape applique  */
     char    cnat;               /*code nature fip                     */
     char    regul;              /*code de regularisation du role th   */
     long    napin;              /*montant du net a payer initial      */
     char    csdi[04];           /*code dsf de gestion                 */
     long    revffm;             /*revenu fiscal de REFERENCE total foyer*/
     long    seuilm1;            /*seuil revenus 1417 II bis I         */
     long    seuilm2;            /*seuil revenus 1417 II bis II        */
     char    imaisf;             /*indic deg Macron apres PeC ISF      */
     char    iman1;              /*Indic droit d�gmt Macron appliqu� N-1*/
     double  txdegm;             /*Taux de d�gmt Macron, effet cliquet */
                 }
           s_e2         ;
/*- fin de e2           :entree donnees individuelles th     --*/

/*=================================================================
           s_loprs;      structure sortie local professionnel
===================================================================*/
typedef struct {
   char indloc[19]; /* identifiant du local professionnel sur 18 caract�res */

   long qtprlc; /* quote part du pas de lissage commune N                   */
   long qtprls; /* quote part du pas de lissage syndicat N                  */
   long qtprlq; /* quote part du pas de lissage interco N                   */
   long qtprln; /* quote part du pas de lissage tse N                       */
   long qtprlg; /* quote part du pas de lissage tse autre N                 */
   long qtprle; /* quote part du pas de lissage gemapi N                    */

   long qtmc;  /* quote part modulee du pas de lissage commune N       */
   long qtms;  /* quote part modulee du pas de lissage syndicat N      */
   long qtmq;  /* quote part modulee du pas de lissage interco N       */
   long qtmn;  /* quote part modulee du pas de lissage tse N           */
   long qtmg;  /* quote part modulee du pas de lissage tse autre N     */
   long qtme;  /* quote part modulee du pas de lissage gemapi N        */

   long cotilpc; /* cotisation liss�e commune N         */
   long cotilps; /* cotisation liss�e syndicat N        */
   long cotilpq; /* cotisation liss�e interco N         */
   long cotilpn; /* cotisation liss�e tse N             */
   long cotilpg; /* cotisation liss�e tse autre N       */
   long cotilpe; /* cotisation liss�e gemapi N          */


     s_cols          cols_cr;          /*structure sortie commune r�vis�e N                           */
     s_cols          cols_qr;          /*structure sortie intercommunalite  r�vis�e N                 */
     s_cols          cols_sr;          /*structure sortie syndicat     r�vis�e N                      */
     s_cols          cols_nr;          /*structure sortie TSE          r�vis�e N                      */
     s_cols          cols_gr;          /*structure sortie TSE Autre     r�vis�e N                     */
     s_cols          cols_er;          /*structure sortie GEMAPI        r�vis�e N                     */
     s_cols          cols_fr;          /*structure sortie commune avant ajustement   r�vis�e N        */
     s_cols          cols_rr;          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/

     s_cols          cols_c7;          /*structure sortie commune 2017 base 70               */
     s_cols          cols_q7;          /*structure sortie intercommunalite 2017 base 70      */
     s_cols          cols_s7;          /*structure sortie syndicat 2017 base 70              */
     s_cols          cols_n7;          /*structure sortie TSE 2017 base 70                   */
     s_cols          cols_g7;          /*structure sortie TSE autre 2017 base 70             */
     s_cols          cols_e7;          /*structure sortie GEMAPI 2017 base 70                */

     s_cols          cols_cr7;         /*structure sortie commune 2017 revisee               */
     s_cols          cols_qr7;         /*structure sortie intercommunalite 2017 revisee      */
     s_cols          cols_sr7;         /*structure sortie syndicat 2017 revisee              */
     s_cols          cols_nr7;         /*structure sortie TSE 2017 revisee                   */
     s_cols          cols_gr7;         /*structure sortie TSE autre 2017 revisee             */
     s_cols          cols_er7;         /*structure sortie GEMAPI 2017 revisee                */

    long   mntpl                ; /*  Montant du planchonnement appliqu�                        */
    char   sgnpl                ; /*  Signe du planchonnement appliqu�                          */
    long   mntplt               ; /*  Montant du planchonnement appliqu� TSE                    */
    char   sgnplt               ; /*  Signe du planchonnement appliqu� TSE                      */
    long   vlrnp                ; /*  VL  r�vis�e neutralis�e et planchonn�e appliqu�e          */
    long   vlrnpt               ; /*  VL r�vis�e neutralis�e planchonn�e appliqu�e TSE�         */
    long   vlrnp7               ; /* VL r�vis�e neutralis�e et planchonn�e 2017 recalcul�e      */
    long   vlrnpt7              ; /* VL r�vis�e neutralis�e et planchonn�e TSE 2017 recalcul�e  */
     }      s_loprs       ;

/*- fin de loprs         :structure sortie local pro loprs  --*/


/*============================================================
           s_s2;        sortie contribuable calculette th
============================================================*/
typedef struct {
     short   anref;              /*annee ref calculette th (millesime) */
     char    versr;              /*lettre version d'un programme       */
     long    vlbri;              /*VL brute imposee (minoree abat K3)  */
     long    vlbni;              /*VL brute non imposee (abattement K3)*/
     long    vlbr;               /*VL brute revisee N                  */
     long    vlbrt;              /*VL brute revisee N TSE              */
     long    vlbr7;              /*VL brute revisee 2017               */
     long    vlbrt7;             /*VL brute revisee 2017 TSE           */
     long    vlb77;              /*VL brute base 70 2017               */
     long    vlbir;              /*VL brute imposee revisee N          */
     long    vlbirt;             /*VL brute imposee revisee N TSE      */
     long    vlbir7;             /*VL brute imposee revisee 2017       */
     long    vlbir7t;            /*VL brute imposee revisee 2017 TSE   */
     long    vlbi77;             /*VL brute imposee base 70 2017       */
     long    vlbnir;             /*VL brute non imposee revisee N      */
     long    vlbnirt;            /*VL brute non imposee revisee N TSE  */
     long    vlbnir7;            /*VL brute non imposee revisee 2017   */
     long    vlbnir7t;           /*VL brute non imposee revisee 2017TSE*/
     long    vlbni77;            /*VL brute non imposee base 70 2017   */
     long    cotic;              /*cotisation communale                */
     long    cotiq;              /*cotisation intercommunale           */
     long    cotis;              /*cotisation syndicale                */
     long    cotin;              /*cotisation TSE                      */
     long    cotig;              /*cotisation TSE Autre                */
     long    cotie;     		 /*Cotisation GEMAPI                   */

     long    cotilc;     		 /*Somme des cotisations communales lissees */
     long    cotilq;     		 /*Somme des cotisations interco lissees    */
     long    cotils;     		 /*Somme des cotisations syndicales lissees */
     long    cotiln;     		 /*Somme des cotisations TSE lissees        */
     long    cotilg;     		 /*Somme des cotisations TSE Autres lissees */
     long    cotile;     		 /*Somme des cotisations GEMAPI lissees     */
     long    palis;     		 /*Somme des pas de lissage loc pro N       */

     long    lissac;     		 /*Somme lissage applique commune           */
     long    lissaq;     		 /*Somme lissage applique intercommunalite  */
     long    lissas;     		 /*Somme lissage applique syndicat          */
     long    lissan;     		 /*Somme lissage applique TSE               */
     long    lissag;     		 /*Somme lissage applique TSE Autre         */
     long    lissae;     		 /*Somme lissage applique GEMAPI            */

     long    qtprltc;            /*Somme des quote-parts locaux pro commune */
     long    qtprltq;            /*Somme des quote-parts locaux pro EPCI    */
     long    qtprlts;            /*Somme des quote-parts locaux pro syndicat*/
     long    qtprltn;            /*Somme des quote-parts locaux pro TSE     */
     long    qtprltg;            /*Somme des quote-parts locaux pro TSEautre*/
     long    qtprlte;            /*Somme des quote-parts locaux pro GEMAPI  */
     long    frait;              /*total des frais de role             */
     long    frai4;              /*total des frais de role a 4,4 %     */
     long    prelt;              /*montant total des prelevements      */
     long    majths;			 /*Montant majoration THS              */
     long    limajths;           /*Partie du lissage sur la majo THS
                                 Bug 212403 amelioration */
     long    coticm;			 /*Cotisation communale major�e        */
     long    somrc;              /*total des sommes a recouvrer        */
     long    degpl;              /*montant degrevement ou plafonnement */
     long    totth;              /*montant du total th                 */
     long    netth;              /*montant du net a payer th           */
     long    somrp;              /* donn�e en instance de suppression remise ici pour correspondance cobol */
     long    pre02;              /*montant du prelevement a 0,2 %      */
     long    pre12;              /*montant du prelevement  a 1,2 %     */
     long    pre17;              /*montant du prelevement a 1,7 %      */
     char    codro[03];          /*code role th                        */
     long    cottv;              /*cotisation redevance tv             */
     long    fratv;              /*frais de role redevance tv          */
     long    somtv;              /*somme a recouvrer redevance tv      */
     long    mdgtv;              /*mont. degrevement redevance  tv     */
     long    nettv;              /*montant du net a payer tv           */
     char    roltv[04];          /*code role tv                        */
     long    netap;              /*montant du net a payer th+tv        */
     char    signe;              /*signe variation nap regularisation  */
     long    napdt;              /*valeur absolue delta net a payer    */
     long    tottaham;           /*cotisation avant degrevement reforme*/
     char    imap;               /*' ', M (degrev Macron), D (decote M)*/
     long    degm;               /*Degrevement ou decote Macron        */
     long    degmaso;            /*Degrev/decote Macron avt exam seuil */
     char    codegm[5];          /*Degrevement Macron                  */
     char    codecm;             /*code de la decote reforme Macron    */
     double  txrefm;             /*Taux r�forme Macron                 */
     long    preths;             /*prlvt additionnel resid secondaires */
     long    fgest_cq;           /*montant des fgest commune-interco   */
     long    fgest_s;            /*montant des fgest syndicat          */
     long    fgest_ng;           /*montant des fgest tse-tse gp        */
     long    fgest_e;			 /*Montant des FGEST GEMAPI            */
     long    far_cq;             /*montant des far commune-interco     */
     long    far_s;              /*montant des far syndicat            */
     long    far_ng;             /*montant des far tse-tse gp          */
     long    far_e;			     /*Montant des far GEMAPI              */

     /*Abattements applicables a la c�te*/
     /* NE PAS CHANGER L ORDRE ci-apres QUI EST IMPORTANT POUR LES OCCURRENCES COBOL Bug 215622 TH-NT */
     s_cols          cols_c;          /*01 structure sortie commune                            */
     s_cols          cols_q;          /*02 structure sortie intercommunalite                   */
     s_cols          cols_n;          /*04 structure sortie TSE                                */

     s_cols          cols_s;          /*10 structure sortie syndicat                           */
     s_cols          cols_f;          /*11 structure sortie commune avant ajustement           */

     s_cols          cols_g;          /*12 structure sortie TSE Autre                          */

     s_cols          cols_r;          /*14 structure sortie intercommunalite avant ajustement  */
     s_cols          cols_e;          /*15 structure sortie GEMAPI                             */

     /* NE PAS CHANGER L ORDRE ci-avant QUI EST IMPORTANT POUR LES OCCURRENCES COBOL */

     s_cols          cols_cr;          /*structure sortie commune r�vis�e N                           */
     s_cols          cols_qr;          /*structure sortie intercommunalite  r�vis�e N                 */
     s_cols          cols_sr;          /*structure sortie syndicat     r�vis�e N                      */
     s_cols          cols_nr;          /*structure sortie TSE          r�vis�e N                      */
     s_cols          cols_gr;          /*structure sortie TSE Autre     r�vis�e N                     */
     s_cols          cols_er;          /*structure sortie GEMAPI        r�vis�e N                     */
     s_cols          cols_fr;          /*structure sortie commune avant ajustement   r�vis�e N        */
     s_cols          cols_rr;          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/

     s_cols          cols_c7;          /*structure sortie commune 2017 base 70               */
     s_cols          cols_q7;          /*structure sortie intercommunalite 2017 base 70      */
     s_cols          cols_s7;          /*structure sortie syndicat 2017 base 70              */
     s_cols          cols_n7;          /*structure sortie TSE 2017 base 70                   */
     s_cols          cols_g7;          /*structure sortie TSE autre 2017 base 70             */
     s_cols          cols_e7;          /*structure sortie GEMAPI 2017 base 70                */

     s_cols          cols_cr7;         /*structure sortie commune 2017 revisee               */
     s_cols          cols_qr7;         /*structure sortie intercommunalite 2017 revisee      */
     s_cols          cols_sr7;         /*structure sortie syndicat 2017 revisee              */
     s_cols          cols_nr7;         /*structure sortie TSE 2017 revisee                   */
     s_cols          cols_gr7;         /*structure sortie TSE autre 2017 revisee             */
     s_cols          cols_er7;         /*structure sortie GEMAPI 2017 revisee                */

     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_s2         ;
/*- fin de s2           :sortie contribuable calculette th   --*/

/*============================================================
           s_cole;      structure entree collectivite cole
============================================================*/
typedef struct {
     char    txbas[03];          /*taux abattement general             */
     char    tpac1[03];          /*taux abattement pac 1 ou 2          */
     char    tpac3[03];          /*taux abattement pac 3 ou +          */
     char    txspe[03];          /*taux abattement special base        */
     char    txhan[03];          /*taux abattement special handicape   */
     long    vlmoy;              /*vl moyenne collectivite en comp     */
     long    abspe;              /*quotite abattement special en comp  */
     long    abbas;              /*quotite abattement general en comp  */
     long    apac1;              /*quotite abattement pac 1 / 2 en comp*/
     long    apac3;              /*quotite abattement pac 3 / + en comp*/
     long    abhan;              /*quotite abattement handicape en comp*/
                 }
           s_cole       ;
/*- fin de cole         :structure entree collectivite cole  --*/
/*============================================================
           s_e3;        entree donnees collectives th
============================================================*/
typedef struct {
     short   antax;              /*annee campagne taxation (millesime) */
     char    dep[03];            /*code departement                    */
     char    cne[04];            /*code commune de l'aft               */
     s_cole          cole_c;          /*structure entree commune                                         */
     s_cole          cole_q;          /*structure entree intercommunalite                                */
     s_cole          cole_f;          /*structure entree commune avant ajustement                        */
     s_cole          cole_r;          /*structure entree intercommunalite avant ajustement               */
     s_cole          cole_h;          /*structure entree commune 2017                                    */
     s_cole          cole_k;          /*structure entree commune 2017 avant ajustement                   */
     s_cole          cole_l;          /*structure entree intercommunalite 2017                           */
     char    cocnq;              /*code 1260 mt commune appartenant cu */
     long    abmos;              /*quotite minimale alsace-moselle c   */
     long    abmos7;             /*quotite minimale alsace-moselle 2017*/
     char    codef;              /*code 1260 al-mos rap norm           */
     char    codef7;             /*code 1260 al-mos rap norm  2017     */
     char    codeg;              /*code 1260 al-mos rang rap           */
     long    vmd89;              /*valeur locative moyenne dom 1989 c  */
     double  timpc;              /*taux imp commune/ifp ...            */
     double  timpc7;             /*taux imp commune/ifp ...  en 2017   */
     double  tisyn;              /*taux imp syndicat en c              */
     double  tisyn7;             /*taux imp syndicat en c    en 2017   */
     double  titsn;              /*somme taux imp tse en c             */
     double  titsn7;             /*somme taux imp tse en c   en 2017   */
     double  timpq;              /*taux imp groupement en c            */
     double  timpq7;             /*taux imp groupement en c  en 2017   */
     double  titgp;              /*taux d'imposition TSE Autre         */
     double  titgp7;             /*taux d'imposition TSE Autre en 2017 */
     double  timpe;				 /*taux d'imposition commune/ifp GEMAPI*/
     double  timpe7;			 /*taux d'imposition GEMAPI    en 2017 */
     char    indthlv;            /*code origine de la thlv             */
     char    indgem;             /*Indicateur d�lib�ration GEMAPI      */
     char    indmths;            /*Indicateur majoration THS           */
     double  timths;			 /*taux d'imposition majoration THS    */
                 }
           s_e3         ;
/*- fin de e3           :entree donnees collectives th       --*/

/*=================================================================
           s_lopre;      structure entree local professionnel
===================================================================*/
typedef struct {
    char   indloc[19]          ; /*Num invariant, passerelle ou seq du LP          */
    long   vlrnp               ; /*VL revisee, planchonnee du local A ou S         */
    long   vlrnpt              ; /*VL revisee, pl anchonnee du local A ou S pour TSE*/
    long   vlb70p7             ; /*VL70 actualisee en 2017 du local A ou S         */
    long   vlrnp7              ; /*VL revisee, planchonnee 2017 du local A ou S    */
    long   vlrnpt7             ; /*VL revisee, planchonnee 2017 du local A ou S TSE*/
    char   indlis              ; /*Indicateur de lissage O ou N                    */
    char   indrec               ; /*  Indicateur de recalcul du planchonnement et du lissage          */
    long   vlthr                ; /*  VL TH r�vis�e  N                                                */
    long   vlthr7               ; /*  VL r�vis�e 2017                                                 */
    double coefn                ; /*  Coefficient de neutralisation                                   */
    double coefnt               ; /*  Coefficient de neutralisation TSE                               */
    long   qtprlc               ; /*  Quote-part du pas de lissage Commune N                          */
    long   qtprls               ; /*  Quote-part du pas de lissage Syndicat N                         */
    long   qtprlq               ; /*  Quote-part du pas de lissage Intercommunalit� N                 */
    long   qtprln               ; /*  Quote-part du pas de lissage TSE N                              */
    long   qtprlg               ; /*  Quote-part du pas de lissage TSE �Autre� N                      */
    long   qtprle               ; /*  Quote-part du pas de lissage GEMAPI N                           */
    char   indqpls              ; /*  Indicateur de quote-part de lissage appliqu�e Syndicat          */
    char   indqplq              ; /*  Indicateur de quote-part de lissage appliqu�e Intercommunalit�  */
    char   indqpln              ; /*  Indicateur de quote-part de lissage appliqu�e TSE               */
    char   indqplg              ; /*  Indicateur de quote-part de lissage appliqu�e TSE �Autre�       */
    char   indqple              ; /*  Indicateur de quote-part de lissage appliqu�e GEMAPI            */
    long   mntpl                ; /*  Montant du planchonnement                                       */
    char   sgnpl                ; /*  Signe du planchonnement                                         */
    long   mntplt               ; /*  Montant du planchonnement TSE                                   */
    char   sgnplt               ; /*  Signe du planchonnement TSE                                     */

                 }
           s_lopre       ;
/*- fin de lopre         :structure entree local pro lopre  --*/


/*=======================================================================
           s_ea;       structure entree LP de la cote (300 occurrences)
=========================================================================*/
typedef struct {
        s_lopre           lopre[DIM_LOCAUX_PRO]     ; /*Description du local   */
                 }
           s_ea       ;
/*- fin de ea         :structure entree LP de la cote  --*/

/*=======================================================================
           s_sa;       structure sortie LP de la cote (300 occurrences)
=========================================================================*/
typedef struct {
        s_loprs           loprs[DIM_LOCAUX_PRO]     ; /*Description du local en sortie  */
                 }
           s_sa       ;
/*- fin de sa         :structure sortie LP de la cote  --*/

/*============================================================
           s_rede;      structure entree redevable rede
============================================================*/
typedef struct {
     long    revfe;              /*revenu imposable ir en franc ou euro*/
     short   nbpar;              /*nombre de parts ir calculette       */
     char    front;              /*code indicateur frontalier          */
     short   annir;              /*annee tax ir redevable th millesime */
                 }
           s_rede       ;
/*- fin de rede         :structure entree redevable rede     --*/
/*============================================================
           s_reds;      structure sortie redevable reds
============================================================*/
typedef struct {
     char    seuil;              /*code seuil th fonction du revenu ir */
     long    revim;              /*revenu imposable ir retenu pour th  */
     char    serfrc;             /*code seuil th 1414a 1417ii          */
     char    seuilb;             /*code seuil th article 1417-I bis    */
                 }
           s_reds       ;
/*- fin de reds         :structure sortie redevable reds     --*/
/*============================================================
           s_e4;        entree donnees seuil degrevement
============================================================*/
typedef struct {
     short   antax;              /*annee campagne taxation (millesime) */
     char    csdi[04];           /*code dsf de gestion                 */
     char    cnua;               /*code nature d'affectation red.th    */
     char    isfc;               /*indicateur ISF pour la cote         */
     long    revfenr;            /*RFR pour rattaches TH non retenus   */
     long    nbparnr;            /*Nbr parts rattaches TH non retenus  */
     s_rede          rede_1;          /*structure entree redevable rede     */
     s_rede          rede_2;          /*structure entree redevable rede     */
     s_rede          rede_r;          /*structure entree redevable rede     */
                 }
           s_e4         ;
/*- fin de e4           :entree donnees seuil degrevement    --*/
/*============================================================
           s_s4;        sortie seuil degrevement th
============================================================*/
typedef struct {
     short   anref;              /*annee ref calculette th (millesime) */
     char    versr;              /*lettre version d'un programme       */
     long    npaff;              /*nb parts ir foyer fiscal calculette */
     long    revffm;             /*revenu fiscal de REFERENCE total foyer*/
     long    npaffm;             /*nb parts TOTAL du foyer fiscal      */
     char    imhisf;             /*indic deg Macron avt PeC ISF        */
     char    imaisf;             /*indic deg Macron apres PeC ISF      */
     long    seuilm1;            /*seuil revenus 1417 II bis I         */
     long    seuilm2;            /*seuil revenus 1417 II bis II        */
     s_reds          reds_1;          /*structure sortie redevable reds     */
     s_reds          reds_2;          /*structure sortie redevable reds     */
     s_reds          reds_c;          /*structure sortie redevable reds     */
     s_reds          reds_f;          /*structure sortie redevable reds     */
     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_s4         ;
/*- fin de s4           :sortie seuil degrevement th         --*/
/*============================================================
           s_e6;        entree calcul base et taux tlv
============================================================*/
typedef struct {
     s_e1            e1_l;       /*entree local a actualiser           */
     short   atlvl;              /*annee reference tlv en comp         */
     short   atlvc;              /*annee app. tlv de la commune en comp*/
                 }
           s_e6         ;
/*- fin de e6           :entree calcul base et taux tlv      --*/
/*============================================================
           s_s6;        sortie calcul base et taux tlv
============================================================*/
typedef struct {
     short   anref;              /*annee ref calculette th (millesime) */
     char    versr;              /*lettre version d'un programme       */
     long    vlbll;              /*vl act/revalorisee du local en c    */
     long    vlblc;              /*vl act/reval des locaux communs en c*/
     long    vlblt;              /*vl act/reval. tot local en c imposee*/
     long    txtlv;              /*taux d'imposition tlv en comp       */
     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_s6         ;
/*- fin de s6           :sortie calcul base et taux tlv      --*/
/*============================================================
           s_e7;        entree calcul de la tlv
============================================================*/
typedef struct {
     short   antax;              /*annee campagne taxation (millesime) */
     long    btlv1;              /*base locaux tlv taxee taux 12,5% a compter de 2013) comp*/
     long    btlv2;              /*base locaux tlv taxee taux 25% a compter de 2013)   comp*/
                 }
           s_e7         ;
/*- fin de e7           :entree calcul de la tlv             --*/
/*============================================================
           s_s7;        sortie calcul de la tlv
============================================================*/
typedef struct {
     short   anref;              /*annee ref calculette th (millesime) */
     char    versr;              /*lettre version d'un programme       */
     long    cotv1;              /*mt cotis tlv taxee a 12,5%          */
     long    cotv2;              /*mt cotis tlv taxee a 25%            */
     long    cotitlv;            /*somme des cotisations tlv           */
     long    frait;              /*total des frais de role             */
     long    frai5;              /*total des frais de role a 5,4 %     */
     long    totlv;              /*montant du total tlv                */
     long    netap;              /*montant du net a payer th+tv        */
     char    codro[03];          /*code role th                        */
     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_s7         ;
/*- fin de s7           :sortie calcul de la tlv             --*/
/*============================================================
           s_colb;      structure sortie collectivite colb
============================================================*/
typedef struct {
     char    txbas[03];          /*taux abattement general             */
     char    tpac1[03];          /*taux abattement pac 1 ou 2          */
     char    tpac3[03];          /*taux abattement pac 3 ou +          */
     char    txspe[03];          /*taux abattement special base        */
     char    txhan[03];          /*taux abattement special handicape   */
     long    vlmoy;              /*vl moyenne collectivite en comp     */
     long    abspe;              /*quotite abattement special en comp  */
     long    abhan;              /*quotite abattement handicape en comp*/
     long    abbas;              /*quotite abattement general en comp  */
     long    apac1;              /*quotite abattement pac 1 / 2 en comp*/
     long    apac3;              /*quotite abattement pac 3 / + en comp*/
     long    abmos;              /*quotite minimale alsace-moselle c   */
     char    codef;              /*code 1260 al-mos rap norm           */
     char    codeg;              /*code 1260 al-mos rang rap           */
     long    vlmrf;              /*vl moyenne coll reference en comp   */
                 }
           s_colb       ;
/*- fin de colb         :structure sortie collectivite colb  --*/
/*============================================================
           s_e8;        entree determin double liquid ktac
============================================================*/
typedef struct {
     short   antax;              /*annee campagne taxation (millesime) */
     char    dep[03];            /*code departement                    */
     char    csdi[04];           /*code direction de gestion           */
     s_cole          cole_c;          /*structure entree commune                                         */
     s_cole          cole_q;          /*structure entree intercommunalite                                */
     s_cole          cole_f;          /*structure entree commune avant ajustement                        */
     s_cole          cole_r;          /*structure entree intercommunalite avant ajustement               */
     s_cole          cole_h;          /*structure entree commune 2017                                    */
     s_cole          cole_k;          /*structure entree commune 2017 avant ajustement                   */
     s_cole          cole_l;          /*structure entree intercommunalite 2017                           */
     double  tisyn;              /*taux imp syndicat en c              */
     char    cocnq;              /*code 1260 mt commune appartenant cu */
     double  titsn;              /*somme taux imp tse en c             */
     long    abmos;              /*quotite minimale alsace-moselle c   */
     long    abmos7;             /*quotite minimale alsace-moselle 2017*/
     char    codef;              /*code 1260 al-mos rap norm           */
     char    codef7;             /*code 1260 al-mos rap norm 2017      */
     char    codeg;              /*code 1260 al-mos rang rap           */
     long    vmd89;              /*valeur locative moyenne dom 1989 c  */
     double  titgp;              /*taux d'imposition TSE Autre         */
     double  timpe;              /*taux d'imposition commune/ifp GEMAPI*/
     char    indgem;             /*Indicateur d�lib�ration GEMAPI      */
     double  timpc7;             /*taux d'imposition commune 2017      */
     double  timpq7;             /*taux d'imposition interco 2017      */
     double  tisyn7;             /*taux d'imposition syndicat 2017     */
     double  titsn7;             /*taux d'imposition TSE 2017          */
     double  titgp7;             /*taux d'imposition TSE autre 2017    */
     double  timpe7;             /*taux d'imposition GEMAPI 2017       */
                 }
           s_e8         ;
/*- fin de e8           :entree determin double liquid kta   --*/
/*============================================================
           s_s8;        sortie determin double liquid kta
============================================================*/
typedef struct {
     short   anref;              /*annee ref calculette th (millesime) */
     char    versr;              /*lettre version d'un programme       */
     s_colb          colb_c;          /*01 structure sortie commune                                         */
     s_colb          colb_s;          /*02 structure sortie syndicat                                        */
     s_colb          colb_q;          /*03 structure sortie intercommunalite                                */
     s_colb          colb_n;          /*05 structure sortie TSE                                             */
     s_colb          colb_f;          /*11 structure sortie commune avant ajustement                        */
     s_colb          colb_g;          /*13 structure sortie TSE Autre                                       */
     s_colb          colb_r;          /*15 structure sortie intercommunalite avant ajustement               */
     s_colb          colb_e;          /*17 structure sortie GEMAPI                                          */
     s_colb          colb_h;          /*19 structure sortie commune 2017                                    */
     s_colb          colb_t;          /*20 structure sortie syndicat 2017                                   */
     s_colb          colb_l;          /*21 structure sortie intercommunalite 2017                           */
     s_colb          colb_w;          /*22 structure sortie TSE 2017                                        */
     s_colb          colb_x;          /*23 structure sortie TSE autre 2017                                  */
     s_colb          colb_u;          /*24 structure sortie GEMAPI 2017                                     */

     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_s8         ;
/*- fin de s8           :sortie determin double liquid kta   --*/
/*============================================================
           s_e9;        entree donnees collectives act reval
============================================================*/
typedef struct {
     short   antax;              /*annee campagne taxation (millesime) */
     char    dep[03];            /*code departement                    */
     char    cne[04];            /*code commune de l'aft               */
     char    cxcn1[03];          /*code 1260 exo locaux meubles m1 gite*/
     char    cxcn2[03];          /*code 1260 exo locaux meubles m2     */
     char    cxcn3[03];          /*code 1260 exo locaux meubles m3     */
                 }
           s_e9         ;
/*- fin de e9           :entree donnees collectives act reval--*/



/*============================================================================
   s_ep :  structure entr�e  du recalcul du planchonnement de la VL des LP
  ============================================================================*/
typedef struct {
short  antax        ; /*   Ann�e de taxation                                                                 */
char   indlis       ; /*   Indicateur de lissage                                                             */
char   indrec       ; /*   Indicateur de recalcul du planchonnement et du lissage                            */
long   vlthr        ; /*   VL TH r�vis�e  N                                                                  */
long   vlb70p7      ; /*   VL 70 TH 2017 actualis�e et revaloris�e                                           */
long   vlthr7       ; /*   VL TH 2017 r�vis�e                                                                */
double coefn        ; /*   Coefficient de neutralisation Commune des VL des locaux professionnels            */
double coefnt       ; /*   Coefficient de neutralisation TSE des VL des locaux professionnels                */

}s_ep;

/*-------- fin de la structure entr�e ep du module de recalcul du planchonnement des LP           --------*/

/*============================================================================
   s_sp :  structure sortie  du recalcul du planchonnement de la VL des LP
  ============================================================================*/
typedef struct {
short   anref                  ; /*  Ann�e de r�f�rence                                         */
char    versr                  ; /*  Lettre version de ce module                                */
long    mntpl                  ; /*  Montant du planchonnement appliqu�                         */
char    sgnpl                  ; /*  Signe du planchonnement  appliqu�                          */
long    mntplt                 ; /*  Montant du planchonnement appliqu� TSE                     */
char    sgnplt                 ; /*  Signe du planchonnement appliqu� TSE                       */
long    vlrnp7                 ; /*  VL r�vis�e neutralis�e et planchonn�e 2017  recalcul�e     */
long    vlrnpt7                ; /*  VL r�vis�e neutralis�e et planchonn�e TSE 2017  recalcul�e */
long    vlrnp                  ; /*  VL r�vis�e neutralis�e et planchonn�e appliqu�e            */
long    vlrnpt                 ; /*  VL r�vis�e neutralis�e planchonn�e appliqu�e TSE           */
s_signature    *signature             ; /*  Signature de la calculette                          */
s_liberreur    *libelle               ; /*  Libell� message d�anomalie                          */



}s_sp;

/*-------- fin de la structure entr�e sp du module de recalcul du planchonnement des LP           --------*/
