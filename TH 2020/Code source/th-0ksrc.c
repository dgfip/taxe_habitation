/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/  
  
    /*============================================*/
    /*  MODULE DE CALCUL DES FRAIS, PRELEVEMENTS  */
    /*           ET SOMME A RECOUVRER             */
    /*           programme th-0kSRC.C             */
    /*============================================*/


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif


int th_0ksrc(s_sre * p_sre ,        /* pointeur sur structure entree          */
             s_srs  * p_srs)        /* pointeur sur structure sortie          */
{
    int     ret;
    s_cons  *p_cons            ;    /* pointeur sur struct des constantes     */
    long    fraisDR      = 0   ;    /* frais de role                          */
    long    fraisAR      = 0   ;    /* frais d'assiette et de recouvrement    */
    double  fgest_cq     = 0   ;    /* frais de gestion commune , interco.
    								   & majoration THS                       */
    long    fgest_s      = 0   ;    /* frais de gestion syndicat              */
    long    fgest_ng     = 0   ;    /* frais de gestion TSE TSE Autre         */
    double  fgest_e      = 0   ;    /* frais de gestion GEMAPI         */
    double  far_cq       = 0   ;    /* frais d'assiette commune interco.      */
    long    far_s        = 0   ;    /* frais d'assiette syndicat              */
    long    far_ng       = 0   ;    /* frais d'assiette TSE TSE Autre         */
    double  far_e        = 0   ;    /* frais d'assiette GEMAPI        */
    long    prelt        = 0   ;    /* prelevement                            */
    long    prelp        = 0   ;
    long    preths       = 0   ;    /* prelevement                            */
    long    pre02        = 0   ;
    long    pre12        = 0   ;
    long    pre17        = 0   ;

    long    totcott      = 0   ;
    long    totcotcq     = 0   ;    /* total des cotisations commune majoree THS, interco */
    long    totcotng     = 0   ;    /* total des cotisations TSE et TSE Autre */
    int ret_cons;       /* code retour de cherche_const */
    static s_signature signature;

    p_srs->versr = 'B' ;

    /* controle de la signature */
    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_srs->signature = &signature  ;

    /* CA_ANO_0382 Controle du module KSRC */
    ret = controle_signature(RKSRC,p_srs->versr, &(p_srs->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* recherche des constantes */
    ret_cons=(short)cherche_const(p_sre->antax,&p_cons);
    cherche_Erreur(ret_cons,&(p_srs->libelle)) ;
    if (ret_cons == 1)          /* annee invalide */
    {
        return (1);
    }

    /* CA_TH_0043 Determination de la majoration THS */

    /* 2019MAJR11-01-01 */
    if (p_sre->tax == 'S') /* Articles THS */
    {

        if   (((p_sre->cnat != '5')  || (p_sre->indaths == 'O')) && (p_sre->vlbpc != 0)) /*personne morale ou personne physique assujettie � la majoration*/
        {
            if (p_sre->indmths == 'O') /* si majoration applicable*/
            {
                p_srs->majths = arrondi_euro_voisin((p_sre->cotic * p_sre->timths) * ((float)p_sre->vlbch/(float)p_sre->vlbpc));
                p_srs->limajths = 0 ;
            }
            else
            {
                p_srs->majths = 0 ;
                p_srs->limajths = 0 ;
            }
        }
        else
        {
            p_srs->majths = 0 ;
            p_srs->limajths = 0 ;
        }
    }
    else /* Articles THP et THE */
    {
        p_srs->majths = 0 ;
        p_srs->limajths = 0 ;
    }

    /* FIN  2019MAJR11-01-01 */

    /* CA_TH_0044 Determination cotisation majoree  */
    p_srs->coticm = p_sre->cols_c.coti + p_srs->majths ;

    /* 3 */
    /* CA_TH_0008 Calcul des frais */

    /* Calcul total cotisation commune majoree + interco */
    totcotcq = p_srs->coticm + p_sre->cols_q.coti ;

    /* Calcul total cotisation TSE + TSE Autre */
    totcotng = p_sre->cols_n.coti + p_sre->cols_g.coti;

    /* Calcul total cotisation */

    totcott = p_sre->cols_c.coti + p_sre->cols_s.coti + p_sre->cols_q.coti +
              p_sre->cols_n.coti + p_sre->cols_g.coti + p_sre->cols_e.coti;

    /* initialisation Frais */
    p_srs->fgest_cq = fgest_cq;
    p_srs->fgest_s  = fgest_s;
    p_srs->fgest_ng = fgest_ng;
    p_srs->far_cq = far_cq;
    p_srs->far_s  = far_s;
    p_srs->far_ng = far_ng;
    p_srs->fgest_e  = fgest_e;
    p_srs->far_e  = far_e;
    p_srs->frai4  = 0;
    p_srs->frait  = 0;

    if (p_sre->tyimp == 'H')
    {
        if (p_sre->tax == 'S')
        {  /* frais THS  */
            /* CA_TH_0008 1.a */
            /* syndicat */
            p_srs->fgest_s = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FGEST_THS_s);
            p_srs->far_s   = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FAR_THS_s);

            /* TSE et TSE Autre */
            p_srs->fgest_ng = frais_ar(totcotng, p_cons->coeff_FGEST_THS_t);
            p_srs->far_ng   = frais_ar(totcotng, p_cons->coeff_FAR_THS_t);

            /* CA_TH_0008 1.b */
            /* Commune majoree - Interco  + GEMAPI */
            p_srs->fgest_cq = frais_ar(totcotcq, p_cons->coeff_FGEST_THS_c) + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FGEST_THS_g);
            p_srs->far_cq   = frais_ar(totcotcq, p_cons->coeff_FAR_THS_c)   + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FAR_THS_g);

            /* Frais TH GEMAPI valorises a 0 a voir selon reponse DAF */
            p_srs->fgest_e = 0;
            p_srs->far_e  = 0;

            /* frais de gestion de la cote */
            p_srs->frait = p_srs->fgest_cq + p_srs->fgest_s + p_srs->fgest_ng;

            /* frais d'assiette et de recouvrement de la cote*/
            p_srs->frai4 = p_srs->far_cq + p_srs->far_s + p_srs->far_ng;
        }
        else
        {  /* frais THP ou THE  */
            /* CA_TH_0008 2.a */
            /* syndicat */
            p_srs->fgest_s = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FGEST_THP_THE_s);
            p_srs->far_s   = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FAR_THP_THE_s);

            /* TSE et TSE Autre */
            p_srs->fgest_ng = frais_ar(totcotng, p_cons->coeff_FGEST_THP_THE_t);
            p_srs->far_ng   = frais_ar(totcotng, p_cons->coeff_FAR_THP_THE_t);

            /* CA_TH_0008 2.b */
            /* Commune majoree - Interco  + GEMAPI */

            p_srs->fgest_cq = frais_ar(totcotcq, p_cons->coeff_FGEST_THP_THE_c) + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FGEST_THP_THE_g);
            p_srs->far_cq   = frais_ar(totcotcq, p_cons->coeff_FAR_THP_THE_c)   + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FAR_THP_THE_g);

            /* Frais TH GEMAPI valorises a 0 a voir selon reponse DAF */
            p_srs->fgest_e = 0;
            p_srs->far_e  = 0;

            /* frais de gestion de la cote */
            p_srs->frait = p_srs->fgest_cq + p_srs->fgest_s + p_srs->fgest_ng ;

            /* frais d'assiette et de recouvrement de la cote*/
            p_srs->frai4 = p_srs->far_cq + p_srs->far_s + p_srs->far_ng;
        }
    }

    if (p_sre->tyimp == 'T')  /* frais THLV */
    {
        p_srs->frai4  = 0;
        p_srs->frait  = 0;

        /* En cas de THLV avec GEMAPI, les taux de frais sont diff�rents et il faut
        calculer s�par�ment les frais sur Commune/Syndicat (ou Interco) puis GEMAPI */

        /* toutes les cotisations sauf GEMAPI */

         /*CA_THLV_0010 DETERMINATION COTISATION COMMUNE+SYNDICAT OU INTERCO cf calcul totcott */
        totcott -= p_sre->cols_e.coti;

        /*CA_THLV_0012 DETERMINATION DES FRAIS  COMMUNE+SYNDICAT OU INTERCO */
        fraisDR = frais_ar(totcott, p_cons->coeff_frais_THLV);
        fraisAR = frais_ar(totcott, p_cons->coeff_frais_ass_THLV);

        /*CA_THLV_0013 DETERMINATION DES FRAIS  GEMAPI */
        fgest_e = frais_ar(p_sre->cols_e.coti, p_cons->coeff_FGEST_THP_THE_g);
        far_e   = frais_ar(p_sre->cols_e.coti, p_cons->coeff_FAR_THP_THE_g);

        /* CA_THLV_0003 Determination cotisation totale Toutes les cotisations y compris GEMAPI */
        totcott += p_sre->cols_e.coti;

        p_srs->fgest_e  = fgest_e;
        p_srs->far_e    = far_e;
        p_srs->fgest_cq = fraisDR;
        p_srs->far_cq  = fraisAR;

        /* CA_THLV_0004 DETERMINATION DES FRAIS DE GESTION THLV */
        p_srs->frait = ( fraisDR + fgest_e );
        p_srs->frai4 = ( fraisAR + far_e );
    }

    if (!strcmp(p_sre->cne,"127") && !strcmp(p_sre->dep,"97"))
    {
        p_srs->frait = 0;
        p_srs->frai4 = 0;
    }

    /* 4 */

/*2020MAJR12-03-01 CA_TH_0009  bug 267965*/

    if (p_sre->tyimp == 'H' && p_sre->degex == ' ' && p_sre->imaisf==' ' &&
        (p_sre->abatk == ' ' || (p_sre->abatk =='K' && (p_sre->rgsork != '3' && p_sre->rgsork != '4')))) /* Pour THP, THE et THS */
/*FIN 2020MAJR12-03-01*/


    {
         prelevement( p_sre->cols_c.bnimp, p_sre->tax, p_sre->cnat, p_cons,
                            &prelt, &prelp, &pre02, &pre12, &pre17);
    }

    /* CA_THLV_0005 PRELEVEMENT FORTE VL pour THLV */
    if ( p_sre->tyimp == 'T' && p_sre->indthlv == 'I'  ) /* Pour THLV et Interco */
    {
        prelevementTHLV( p_sre->cols_q.bnimp, p_sre->tax, p_sre->cnat, p_cons,
                        &prelt, &prelp, &pre02, &pre12, &pre17);
    }
    if ( p_sre->tyimp == 'T' && p_sre->indthlv == 'C'  ) /* Pour THLV et Commune */
    {
        prelevementTHLV( p_sre->cols_c.bnimp, p_sre->tax, p_sre->cnat, p_cons,
                        &prelt, &prelp, &pre02, &pre12, &pre17);
    }

    p_srs->prelt = prelt;
    p_srs->pre02 = pre02;
    p_srs->pre12 = pre12;
    p_srs->pre17 = pre17;

    /* 5 */
    /* CA_TH_0011 Determination de la somme a recouvrer */
    p_srs->somrc = 0;
    p_srs->preths = 0;

    /* CA_TH_0010 Determination prelevement additionnel sur les THS */
    if (p_sre->tax == 'S')
    {
    	prelevement_add_ths (totcotcq, p_cons,&preths);
        p_srs->preths = preths;
    }

    /* CA_THLV_0006 DETERMINATION SOMME A RECOUVRER */
    p_srs->somrc = totcott + p_srs->frait + p_srs->prelt + p_srs->preths + p_srs->majths;

    /* 6 */
    /* traitement fin */
    return (0);
}

    /*-----------------------------------*/
    /* fonction de calcul du prelevement */
    /*-----------------------------------*/

void prelevement ( long bnimp,char tax,char cnat,s_cons *pc,
                  long *p_prelt, long *p_prelp, long *p_pre02, long *p_pre12,
                  long *p_pre17)
{

    /* THP et THE */
    if (tax == 'P' || tax =='E')
    {
        if (bnimp > pc->seuil_prel) /* Locaux principaux THP ou THE */
        {
            *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prelp);
            *p_prelp += arrondi_euro_voisin(bnimp * pc->taux_prelp);
            *p_pre02 += arrondi_euro_voisin(bnimp * pc->taux_prelp);
        }
    }

    if (tax == 'S') /* THS */
    {
        if (bnimp > pc->seuil_prel && bnimp <= pc->seuil_prels) {
            if (cnat =='5')
            {
                *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prelp);
                *p_prelp += arrondi_euro_voisin(bnimp * pc->taux_prelp);
                *p_pre02 += arrondi_euro_voisin(bnimp * pc->taux_prelp);
            }
            else
            {
                *p_pre12 = arrondi_euro_voisin(bnimp * pc->taux_prels1);
                *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prels1);
            }
        }
        if (bnimp > pc->seuil_prels)
        {
            if (cnat =='5')
            {
                *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prelp);
                *p_prelp += arrondi_euro_voisin(bnimp * pc->taux_prelp);
                *p_pre02 += arrondi_euro_voisin(bnimp * pc->taux_prelp);
            }
            else
            {
                *p_pre17 = arrondi_euro_voisin(bnimp * pc->taux_prels2);
                *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prels2);
            }
        }
    }
}
void prelevementTHLV ( long bnimp,char tax,char cnat,s_cons *pc,
                  long *p_prelt, long *p_prelp, long *p_pre02, long *p_pre12,
                  long *p_pre17)
{
    /* T H L V  */

    {

        if ((bnimp >  pc->seuil_prel) &&
            (bnimp <= pc->seuil_prels))
        {
            *p_pre12 = arrondi_euro_voisin(bnimp * pc->taux_prels1);
            *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prels1);
        }
        if ((bnimp > pc->seuil_prels))
        {

            *p_pre17 = arrondi_euro_voisin(bnimp * pc->taux_prels2);
            *p_prelt += arrondi_euro_voisin(bnimp * pc->taux_prels2);
        }
    }
}


/*JBI:Le prelevement additionnel residence secondaires est valorise par la multiplication
 du total des cotisations par le taux prelevement additionnel THS */
             /*  CA_TH_0010  */
void prelevement_add_ths(long totcott ,s_cons *pc, long *p_preths)
{
    *p_preths = arrondi_euro_voisin(totcott * pc->taux_preths);
}

/* ----------------------------- */
/* fonction de calcul des frais  */
/* ----------------------------- */

long frais_ar(long totcot, double taux)
{
    return (arrondi_euro_voisin(totcot * taux));
}
