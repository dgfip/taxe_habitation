/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

/*==================================*/
/* MODULE DE CALCUL DES COTISATIONS */
/*          pour la TLV             */
/*     programme th-0kCVC.C         */
/*==================================*/


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif


    /*======*/
    /* MAIN */
    /*======*/

int th_0kcvc(s_e7 * p_e7 ,     /*pointeur sur structure entree*/
             s_s7 * p_s7)      /*pointeur sur structure sortie*/
{


    /* DECLARATIONS ET INITIALISATIONS */
    static s_signature signature;
    int ret_cons=0;       /* code retour de cherche_const */
    int ret=0;            /* code retour de Nap_Ni_Deg_Ni_Exo */
    s_cons *p_cons = NULL;     /* pointeur sur struct des constantes */
    s_dne dne;          /* pointeur sur struct entr�e net � payer */
    s_dne *p_dne;
    s_dns dns;          /* pointeur sur struct sortie net � payer */
    s_dns *p_dns;



    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_s7->signature = &signature;



    p_dne=&dne;
    p_dns=&dns;



    /* initialisation des zones de sortie */
    /* ---------------------------------- */
    init_sortie_kdnc(p_dns);




    /* identification du module */
    p_s7->anref = ANREF ;
    p_s7->versr = 'A' ;

    /* controle de la signature */


    ret = controle_signature(RKCVC,p_s7->versr, &(p_s7->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* 1 */
    /* recherche des constantes */
    ret_cons=(short)cherche_const(p_e7->antax,&p_cons);
    cherche_Erreur(ret_cons,&(p_s7->libelle)) ;
    if (ret_cons == 1)          /* annee invalide */
    {
        return (1);
    }

    /* 3 */
    /* cotisation � partir de la base � taxer au taux 1 */
    p_s7->cotv1 = arrondi_euro_voisin(p_e7->btlv1 * p_cons->taux_TLV_1) ;

    /* cotisation � partir de la base � taxer au taux 2 */
    p_s7->cotv2 = arrondi_euro_voisin(p_e7->btlv2 * p_cons->taux_TLV_2) ;

    /* somme des cotisations */
    p_s7->cotitlv = p_s7->cotv1 + p_s7->cotv2 ;

    /* total des frais de r�le */
    p_s7->frait = frais_ar(p_s7->cotitlv, p_cons->coeff_frais_TLV);
    p_s7->frai5 = frais_ar(p_s7->cotitlv, p_cons->coeff_frais_ass_TLV);
    /* net � payer */
    p_dne->p_cons = p_cons;
    p_dne->degex = ' ' ;
    p_dne->somrp = 0 ;
    p_dne->somrc = p_s7->cotitlv + p_s7->frait ;
    p_dne->vlexo = 'N' ;
    ret=NaP_Ni_Deg_Ni_Exo(p_dne,p_dns);

    if (ret!=0)
    {
        cherche_Erreur(ret,&(p_s7->libelle)) ;
        return ret;
    }
    p_s7->totlv = p_dns->totth ;
    p_s7->netap = p_dns->netth ;
    strncpy(p_s7->codro, p_dns->codro, 3) ;

    /* 4 */
    /* fin de travail : test du code r�le obtenu */
    if ((strcmp(p_s7->codro , "  ") == 0) || (strcmp(p_s7->codro , "NV") == 0))
    {
        return (0) ;
    }
    else
    {
        cherche_Erreur(4206,&(p_s7->libelle)) ;
        return (4206) ;
    }
}

void init_s7( s_s7 *p_s)
{

    p_s->anref  = 0;
    p_s->versr  = ' ';
    p_s->cotv1  = 0;
    p_s->cotv2  = 0;
    p_s->cotitlv= 0;
    p_s->frait=0;
    p_s->frai5=0;
    p_s->totlv=0;
    p_s->netap=0;
    strncpy(p_s->codro,"  ",3);
}
