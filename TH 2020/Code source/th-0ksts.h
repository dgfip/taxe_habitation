/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

/* Fichier de definition des structures                                       */
#if !defined( th_0KSTS )
    #define   th_0KSTS
    #include  <math.h>
    #include  <stdlib.h>
    #include  <stdio.h>
    #include  <ctype.h>
    #include  <string.h>
    #include  <float.h>

#if defined   ILIAD
    #include  <th-0ktos.h>

#else
    #include  "th-0ktos"

#endif



/*----------------------------------------------------------------------------
   d�finition des rangs pour la signature
  ----------------------------------------------------------------------------*/

#define RKTOS   1
#define RKSTS   2
#define RKFOS   3
#define RKARC   4
#define RKSFC   5
#define RKSEC   6
#define RKTAC   7
#define RKVLC   8
#define RKDNC   9
#define RKACC  10
#define RKCOC  11
#define RKRAC  12
#define RKCVC  13
#define RKTVC  14
#define RKCAC  15
#define RKSRC  16
#define RKLPC  17
#define RKRPC  18

#define LGSIGN  19 /* NB DE MODULES + 1 (MILLESIME) */
/*----------------------------------------------------------------------------
   d�finition des identifiants externes pour la signature
  ----------------------------------------------------------------------------*/

#define MILLESIME "0"
#define KTOS_D    "A"
#define KSTS_D    "F"
#define KFOS_D    "A"
#define KARC_D    "A"
#define KSFC_V    "C"
#define KSEC_V    "C"
#define KTAC_V    "A"
#define KVLC_V    "A"
#define KDNC_D    "A"
#define KACC_V    "B"
#define KCOC_V    "A"
#define KRAC_V    "A"
#define KCVC_V    "A"
#define KTVC_V    "A"
#define KCAC_V    "C"
#define KSRC_V    "B"
#define KLPC_V    "A"
#define KRPC_V    "A"


/*----------------------------------------------------------------------------
   MILLESIME DE REFERENCE DE LA CALCULETTE
  ----------------------------------------------------------------------------*/
#define ANREF   2020

/*----------------------------------------------------------------------------
   voici la signature
  ----------------------------------------------------------------------------*/

#define SIGNATURE \
MILLESIME \
KTOS_D \
KSTS_D \
KFOS_D \
KARC_D \
KSFC_V \
KSEC_V \
KTAC_V \
KVLC_V \
KDNC_D \
KACC_V \
KCOC_V \
KRAC_V \
KCVC_V \
KTVC_V \
KCAC_V \
KSRC_V \
KLPC_V \
KRPC_V




/*============================================================================
   s_rev : structure des seuils annuels de revenu en fonction du nombre
   de parts pour les abattements (Art 1417-II) et les RFR(Art 1414-A.I)
  ============================================================================*/

typedef struct {
    long    revenu_unepart        ; /* max de revenu fiscal de reference
                                       pour une part de quotient familial     */
    long    rev_prem_demipart     ; /* montant de revenu fiscal de
                                       reference autorise pour la premiere
                                       demi-part supplementaire               */
    long    rev_deuxieme_demipart ; /* montant de revenu fiscal de
                                       reference autorise pour la deuxieme
                                       demi-part supplementaire               */
    long    rev_troisieme_demipart; /* montant de revenu fiscal de
                                       reference autorise pour la
                                       troisieme demi-part supplementaire     */
    long    rev_quatrieme_demipart; /* montant de revenu fiscal de
                                       reference autorise pour la
                                       quatrieme demi-part supplementaire     */
    long    rev_autre_demipart    ; /* montant de revenu fiscal de
                                       reference autorise a partir de la
                                       troisieme demi-part supplementaire     */
} s_rev;

/*-------- fin de  s_rev : structure des seuils annuels de revenu en
           fonction du nombre de parts pour les plafonnements et RFR  --------*/

/*============================================================================
   s_rev_w : structure des familles de seuils annuels de revenu pour
   code seuil 1417-I Bis (droit W) a partir de 2016
  ============================================================================*/

typedef struct {
    long    rev_w_1part          ; /* Seuil exon�ration droit acquis
                                      W art 1417-I bis jusqu'� 2 parts */
    long    rev_w_autrepart      ; /* Seuil exon�ration droit acquis
                                      W demie part suppl�mentaire      */
} s_rev_w;

/*--------- fin de  s_rev_w : structure des seuils annuels de
            revenu en fonction du nombre de parts */

/*============================================================================
   s_rev_m : structure des familles de seuils annuels de revenu pour
   code seuil 1417-II Bis (droits Macron)
  ============================================================================*/

typedef struct {
    long    rev_macb1_prem_part     ; /* seuil_revenu_une_part art 1417-II Bis 1 */
    long    rev_macb1_prem_demi_part; /* seuil_rev_2_prem_demi_parts art 1417-II Bis 1 */
    long    rev_macb1_autr_demi_part; /* seuil_rev_2_autr_demi_parts art 1417-II Bis 1 */

    long    rev_macb2_prem_part     ; /* seuil_revenu_une_part art 1417-II Bis 2 */
    long    rev_macb2_prem_demi_part; /* seuil_rev_2_prem_demi_parts art 1417-II Bis 2 */
    long    rev_macb2_autr_demi_part; /* seuil_rev_2_autr_demi_parts art 1417-II Bis 2 */
} s_rev_m;

/*--------- fin de  s_rev_m                                                  */


/*============================================================================
   s_typseuil : structure des familles de seuils annuels de revenu en
   fonction du nombre de parts par type d'allegement
  ============================================================================*/

typedef struct {
    s_rev     rev_tousdroits      ; /* donnees seuil non imposition           */
    s_rev_w   rev_w               ; /* seuils relatifs au droit exoneration W */
} s_typseuil;

/*--------- fin de  s_typseuil : structure des seuils annuels de
            revenu en fonction du nombre de parts                     --------*/


/*============================================================================
   s_cons : structure des constantes annuelles
  ============================================================================*/

typedef struct {
    short   antax                 ; /* annee de taxation a laquelle se
                                       referent les differents seuils         */
    /***** Coefficient revalorisation *****/
    double  reval_metro           ; /* taux de revalorisation metropole
                                       amalgame                               */
    double  reval_dom             ; /* taux de revalorisation DOM amalgame    */
    double  reval_mayotte         ; /* taux de revalorisation Mayotte amalgame*/

    double  reval_metro_2         ; /* taux de revalorisation metropole
                                       amalgame 2                             */
    double  reval_dom_2           ; /* taux de revalorisation DOM amalgame 2  */
    double  reval_mayotte_2       ; /* taux de revalorisation Mayotte amalgame 2*/

    /***** Donnees de degrevement TH  *****/
    long    seuil_deg             ; /* minimum de degrevement                 */
    long    seuil_nap             ; /* minimum de net a payer en dessous
                                       c'est une non-valeur                   */
    long  plaf_quotite_gen      ; /* seuil quotit� abattement maximum       */
    long  plaf_quotite_may      ; /* seuil quotit� abattement Mayotte       */

    /***** Donnees de prelevement TH  *****/
    long    seuil_prel            ; /* limite a partir de laquelle
                                       s'applique le prelevement              */
    double  taux_prelp            ; /* taux du prelevement sur principaux     */
    double  taux_prels1           ; /* taux du prelevement sur secondaires    */
    long    seuil_prels           ; /* limite a partir de laquelle
                                       s'applique le deuxieme taux de
                                       prelevement sur secondaires            */
    double  taux_prels2           ; /* taux de prelevement sur secondaires
                                       luxueux                                */
    double  taux_preths            ; /* taux du pr�l�vement additionnel THS   */
    double  taux_TLV_1            ; /* taux de TLV apr�s seulement 2 ans
                                       de vacance                             */
    double  taux_TLV_2            ; /* taux de TLV apr�s 3 ans de vacance     */

    /*****    D�claration des coefficients de FAR et FGEST ****/
    /*****        (frais de gestion TH differencie par collectivite ****/
    double  coeff_FAR_THP_THE_c   ; /* coefficient frais assiette recouvrement (FAR)
                                    THP/THE commune et interco                      */
    double  coeff_FAR_THP_THE_s   ; /* coefficient FAR THP/THE du syndicat          */
    double  coeff_FAR_THP_THE_t   ; /* coefficient FAR THP/THE de la TSE et
                                    TSE Autre                                       */
    double  coeff_FAR_THP_THE_g   ; /* coefficient frais d'assiette et de recouvrement
    								THP/THE GEMAPI                                  */
    double  coeff_FAR_THS_c       ; /* coefficient FAR THS de la commune et
                                    l'interco et Majoration THS                                      */
    double  coeff_FAR_THS_s       ; /* coefficient FAR THS du syndicat              */
    double  coeff_FAR_THS_t       ; /* coefficient DAR THS de la TSE et TSE Autre   */
    double  coeff_FAR_THS_g       ; /* coefficient FAR THS GEMAPI                   */
    double  coeff_FGEST_THP_THE_c ; /* coefficient frais de gestion(FGEST)
                                    THP/THE de la commune et de l'interco.          */
    double  coeff_FGEST_THP_THE_s ; /* coefficient FGEST THP/THE du syndicat        */
    double  coeff_FGEST_THP_THE_t ; /* coefficient FGEST THP/THE de la TSE et
                                    TSE Autre                                       */
    double  coeff_FGEST_THP_THE_g ; /* coefficient FGEST THP/THE GEMAPI             */
    double  coeff_FGEST_THS_c     ; /* coefficient FGEST THS de la commune et de
                                    l'interco et majoration THS                                      */
    double  coeff_FGEST_THS_s     ; /* coefficient FGEST THS du syndicat            */
    double  coeff_FGEST_THS_t     ; /* coefficient FGEST THS de la TSE et TSE Autre */
    double  coeff_FGEST_THS_g     ; /* coefficient FGEST THS GEMAPI                 */
    double  coeff_frais_ass_THLV  ; /* frais assiette recouvrement THLV       */
    double  coeff_frais_THLV      ; /* frais de r�le THLV                     */
    double  coeff_frais_TLV       ; /* frais pour la TLV                      */
    double  coeff_frais_ass_TLV   ; /* frais assiette et de recouvrement TLV  */
    long    seuil_rs              ; /* minimum d'etablissement d'un role
                                       supplementaire                         */

    /* ****    Donnees redevance TV    **** */
    short   frais_redev_metro     ; /* frais_redev_metro                      */
    short   frais_redev_dom       ; /* frais_redev_dom                        */
    short   cotis_redev_metro     ; /* cotis_redev_metro                      */
    short   cotis_redev_dom       ; /* cotis_redev_dom                        */

    s_typseuil   typseuil_metro    ; /* donnees seuil metropole                */
    s_typseuil   typseuil_dom_124  ; /* donnees seuil Guadeloupe, Martinique, R�union */
    s_typseuil   typseuil_973      ; /* donnees seuil Guyane                   */
    s_typseuil   typseuil_976      ; /* donnees seuil Mayotte                  */

    s_rev_m      rev_macron        ; /* seuil de revenus mesures Macron        */

} s_cons;

/*-------- fin de  s_cons : structure des constantes annuelles        --------*/

/*============================================================================
   s_dne : structure des donnees cote entree pour degrevement
  ============================================================================*/

typedef struct {
    s_cons *p_cons                ; /*constantes de l'ann�e                   */
    char    tyimp                 ; /*type d'impot                            */

    char    degex                 ; /* code degrevement                       */
    char    rgsork                ; /*rang de sortie abatt (blc ou de 3 � 6)  */



    long    somrc                 ; /* somme a recouvrer                      */

    long    somrp                 ; /* Somme a recouvrer THP                  */
    char    vlexo                 ; /* presence d'une vl exoneree non
                                       nulle O sinon N                        */
    char    csdi[04]              ; /* direction (code dsf)                   */
    long    revffm                ; /*revenu fiscal de REFERENCE total foyer  */
    long    seuilm1               ; /*seuil revenus 1417 II bis I             */
    long    seuilm2               ; /*seuil revenus 1417 II bis II            */
    char    imaisf                ; /*indic deg Macron apres PeC ISF          */
    double  txdegm                ; /*Taux de d�gmt Macron, effet cliquet     */


} s_dne;

/*-------- fin de  s_dne:donnees cote entree pour degrevement         --------*/

/*============================================================
           s_dns;       structure sortie the dns
============================================================*/
typedef struct {

     long    degpl;              /*mont. degrevement ou plaf  the c    */
     long    totth;              /*mont. du total th        en    c    */
     long    netth;              /*montant du net a payer th           */
     char    codro[03];          /*code role th                        */
     long    tottaham;           /*cotisation avant degrevement reforme*/
     char    imap;               /*' ', M (degrev Macron), D (decote M)*/
     long    degm;               /*Degrevement ou decote Macron        */
     long    degmaso;            /*Degrev/decote Macron avt exam seuil */
     char    codegm[5];          /*Degrevement Macron                  */
     char    codecm;             /*code de la decote reforme Macron    */
     double  txrefm;             /*Taux r�forme Macron                 */
     s_signature * signature;    /*pointeur sur signature              */
     s_liberreur * libelle;      /*pointeur sur erreur                 */
                 }
           s_dns        ;
/*- fin de dns          :structure sortie the dns            --*/

/*===============================================================================
   s_ace : structure donnees entree KACC pour calculs des abattements applicables
  ===============================================================================*/

typedef struct {
     /* Bug 270587 */
     long     vlbr ;             /*VL brute                            */
     /*fin bug 270587 */
     long    vlbri;              /*VL brute imposee (minoree abat K3)  */
     long    vlbrit;              /*VL brute imposee TSE (minoree abat K3)  */
     short   nbpac;              /*nombre total pac th hors era        */
     short   nbera;              /*nombre enfants en residence alternee*/
     char    codni;              /*code non imposable ir pour la th    */
     char    codsh;              /*code abattement handicape applique  */
     double  timpc;              /*taux imp commune/ifp ...            */
     double  timpq;              /*taux imp groupement en c            */
     double  tisyn;              /*taux imp syndicat en c              */
     double  titsn;              /*somme taux imp tse en c             */
     double  titgp;              /*taux d'imposition TSE Autre         */
     double  timpe;				 /*taux d'imposition commune/ifp GEMAPI*/
     /* Bug 270587 */
     /*long    abmos; */         /*quotite minimale alsace-moselle c   */
     /*char    codef; */         /*code 1260 al-mos rap norm           */
     /* fin Bug 270587 */
     char    cocnq;              /* code 1260 mt commune appartenant cu*/
     s_colb  colb_c;             /* donnees communales                 */
     s_colb  colb_s;             /* donnees syndicales                 */
     s_colb  colb_q;             /* donnees groupement de communes     */
     s_colb  colb_n;             /* donnees TSE                        */
     s_colb  colb_g;             /* donnees TSE Autre                  */
     s_colb  colb_f;             /* Commune avant ajustement           */
     s_colb  colb_r;             /* Intercommunalit� avant ajustement  */
     s_colb  colb_e;             /* donnees GEMAPI                     */
     char    tax;                /* code taxation pour abspsa          */

} s_ace;

/*-------- fin de  s_ace : donnees  entree pour KACC                  --------*/


/*===============================================================================
   s_acs : structure donnees sortie KACC pour calculs des abattements applicables
  ===============================================================================*/

typedef struct {
     char            versr;           /*lettre version d'un programme           */
     s_cols          cols_c;          /*structure sortie commune                            */
     s_cols          cols_s;          /*structure sortie syndicat                           */
     s_cols          cols_q;          /*structure sortie intercommunalite                   */
     s_cols          cols_n;          /*structure sortie TSE                                */
     s_cols          cols_g;          /*structure sortie TSE Autre                          */
     s_cols          cols_f;          /*structure sortie commune avant ajustement           */
     s_cols          cols_r;          /*structure sortie intercommunalite avant ajustement  */
     s_cols          cols_e;          /*structure sortie GEMAPI                             */
     s_signature * signature       ; /*pointeur sur signature                  */
     s_liberreur * libelle         ; /*pointeur sur erreur                     */
} s_acs;

/*-------- fin de  s_acs : donnees  sortie pour KACC                  --------*/



/*=================================================================================================
           s_qtpAb;      structure sortie commune quote part d'abattement des locaux d'habitation
                         et cumul des abat des LP avant neutralisation erreur d arrondis
===================================================================================================*/
typedef struct {


     s_cols          qtp_cr;         /*structure sortie commune r�vis�e N                  */
     s_cols          qtp_qr;         /*structure sortie intercommunalite  r�vis�e N        */
     s_cols          qtp_sr;         /*structure sortie syndicat     r�vis�e N             */
     s_cols          qtp_nr;         /*structure sortie TSE          r�vis�e N             */
     s_cols          qtp_gr;         /*structure sortie TSE Autre     r�vis�e N            */
     s_cols          qtp_er;         /*structure sortie GEMAPI        r�vis�e N            */
     s_cols          qtp_fr;         /*structure sortie commune avant ajustement  r�vis�e N*/
     s_cols          qtp_rr;         /*structure sortie interco avant ajustement  r�vis�e N*/

     s_cols          qtp_c7;          /*structure sortie commune 2017 base 70               */
     s_cols          qtp_q7;          /*structure sortie intercommunalite 2017 base 70      */
     s_cols          qtp_s7;          /*structure sortie syndicat 2017 base 70              */
     s_cols          qtp_n7;          /*structure sortie TSE 2017 base 70                   */
     s_cols          qtp_g7;          /*structure sortie TSE autre 2017 base 70             */
     s_cols          qtp_e7;          /*structure sortie GEMAPI 2017 base 70                */

     s_cols          qtp_cr7;         /*structure sortie commune 2017 revisee               */
     s_cols          qtp_qr7;         /*structure sortie intercommunalite 2017 revisee      */
     s_cols          qtp_sr7;         /*structure sortie syndicat 2017 revisee              */
     s_cols          qtp_nr7;         /*structure sortie TSE 2017 revisee                   */
     s_cols          qtp_gr7;         /*structure sortie TSE autre 2017 revisee             */
     s_cols          qtp_er7;         /*structure sortie GEMAPI 2017 revisee                */

    }
           s_qtpAb       ;
/*- fin de qtpAb        : structure sortie quote part d'abattement */


/*============================================================================
   s_bne1 :  structure entree du module de calcul des bases nettes
  ============================================================================*/

typedef struct {
    short   antax                 ; /* annee de taxation                      */
    char    tyimp                 ; /* type d'impot H TH ou T THLV            */
    long    vlbpc                 ; /* Valeur locative brute des locaux
                                       principaux, actualisee et
                                       revalorisee                            */
    long    vlbri                 ; /* VL brute imposee (minoree abat K3)     */
    long    vlbni                 ; /* VL brute non imposee (abattement K3)   */
    char    tax                   ; /* Code taxation P, S, E ou blanc
                                       (THLV)                                 */
    short   nbpac                 ; /* Nombre de PAC hors ERA Total cumule
                                       en cas de taxation conjointe           */
    short   nbera                 ; /* Nombre d'enfants en residence
                                       alternee (ERA) total cumule en cas
                                       de taxation conjointe                  */
    char    degex                 ; /* Code degrevement ou exoneration
                                       F, M, U, I, A, D, V, S, E, P, R, L
                                       ou blanc; code droit a degrevement
                                       ou a exoneration tenant compte de
                                       la condition de cohabitation et de
                                       la situation a l'IR                    */
    char    codni                 ; /* Code NI a l'IR A ou blanc              */
    char    aff                   ; /*code affectation d'un local             */
    char    permo                 ; /* code type de personne morale           */
    char    grrev                 ; /* code groupe revision                   */
    char    codsh                 ; /* code abattement handicape              */
} s_bne1;

/*-------- Fin de la structure entree d'une cote s_bne1               --------*/


/*====================================================================================
   s_vls :  structure intermediaire d'accueil des vl calculees du local pro dans klpc
  ====================================================================================*/
typedef struct {
    long vli;    /* vl imposee du local                                                                */
    long vlit;   /* vl imposee du local servant de  base pour les collectivites TSE, TSE Autre, Gemapi */
    long vlni;   /* vl non imposee du local cas de la commune, de l'interco et du syndicat             */
    long vlnit;  /* vl non imposee du local dans les autres cas                                        */

    long vli7;    /* vl 2017 imposee du local                                                                */
    long vlit7;   /* vl 2017 imposee du local servant de  base pour les collectivites TSE, TSE Autre, Gemapi */
    long vlni7;   /* vl 2017 non imposee du local cas de la commune, de l'interco et du syndicat             */
    long vlnit7;  /* vl 2017 non imposee du local dans les autres cas                                        */

    long vli77;    /* vl 70 imposee du local                                                                */
    long vlni77;    /* vl 70 non imposee du local                                                                */
} s_vls;
/*-------- fin de la structure  des vl imposees du local  s_vls  --------*/


/*============================================================================
   s_coe1 :  structure entree d'une cote pour le calcul des cotisations
  ============================================================================*/

typedef struct {
    short   antax                 ; /* annee de taxation                      */
    char    dep[3]                ; /* code departement                       */
    char    cne[4]                ; /*code commune de l'aft                   */
    char    tyimp                 ; /* type d'impot H TH ou T THLV            */
    char    degex                 ; /* Code degrevement ou exoneration
                                       F, I, A, D, V, S, E, L
                                       ou blanc code droit a degrevement
                                       ou a exoneration tenant compte de
                                       la condition de cohabitation et de
                                       la situation a l'IR */
    char    cnat                  ; /* Code nature FIP du redevable
                                       utilis� pour determiner le taux de
                                       prelevement                            */
    char    tax                   ; /* Code taxation P, S, E ou blanc
                                       (THLV)                                 */
    char    indthlv               ; /* indic. de synthese THLV                */
    char    indmths               ; /* indic. de majoration THS               */
    char    indgem                ;  /*Indicateur d�lib�ration GEMAPI         */
    char    indaths               ;  /*Indicateur assujetissement majo THS    */
    char    aff                   ; /* Code affectation du local              */
    char    permo;              /*code type de personne morale        */
    char    grrev;              /*code groupe revision                */
    char    abatk;              /*code abattement th (K ou blanc)     */
    char    rgsork;             /*rang de sortie abattement (blc ou 3)*/

    long    vl  ;
    long    vlt ;


    s_cols  *p_cols_c                ; /* donnees communales de la cote          */
    s_cols  *p_cols_s                ; /* donnees syndicales de la cote          */
    s_cols  *p_cols_q                ; /* donnees groupement de communes de
                                       la cote                                */
    s_cols  *p_cols_n                ; /* donnees TSE de la cote                 */
    s_cols  *p_cols_g                ; /* donnees TSE Autre                      */
    s_cols  *p_cols_e                ; /* donnees GEMAPI                         */
    s_cols  *p_cols_f                ; /* donnees commune avant ajustement       */
    s_cols  *p_cols_r                ; /* donnees interco avant ajustement       */
    char    cocnq                    ; /*code 1260 mt commune appartenant cu */
} s_coe1;

/*-------- fin de la structure entree d'une cote s_coe1               --------*/


/*============================================================================
   s_coe2 : structure entree des donnees collectives
  ============================================================================*/

typedef struct {
    double  timpc                 ; /* Taux d'imposition communal             */
    double  tisyn                 ; /* Taux d'imposition syndical             */
    double  titsn                 ; /* Taux d'imposition TSE quand
                                       application TSE pour cette commune     */
    double  timpq                 ; /* Taux d'imposition groupement de
                                       communes                               */
    double  titgp                 ; /* Taux d'imposition de la TSE Autre      */
    double  timpe                 ; /* Taux d'imposition GEMAPI               */
    double  timths                ; /* Taux d'imposition majoration THS       */

} s_coe2;

/*-------- fin de la structure entree des donnees collectives s_coe2  --------*/


/*============================================================================
   s_cos :  structure sortie cos du calcul des cotisations
  ============================================================================*/

typedef struct {

    char    versr                 ; /* lettre version d'un programme          */



    s_signature * signature       ; /* pointeur sur signature                 */
    s_liberreur * libelle         ; /* pointeur sur erreur                    */


} s_cos;

/*-------- fin de la structure sortie cos du calcul general           --------*/


/*============================================================================
   s_seuilM;        seuil Macron utilis�s dans le module KSFC
  ============================================================================*/

typedef struct {
   long    seuilm1;                 /*seuil revenus 1417 II bis 1         */
   long    seuilm2;                 /*seuil revenus 1417 II bis 2         */
} s_seuilM;

/*-------- fin de s_seuilM                                            --------*/

/*============================================================================
   s_ese;        entree donnees seuil degrevement
  ============================================================================*/

typedef struct {
    short   antax                 ; /*annee campagne taxation (millesime)     */
    char    csdi[04]              ; /*direction (code dsf)                    */
    char    cnua                  ; /*code nature d'affectation red.th        */
    s_rede  rede_1                ; /*structure entree redevable rede         */
    s_rede  rede_2                ; /*structure entree redevable rede         */
} s_ese;

/*-------- fin de ese :entree donnees seuil degrevement               --------*/


/*============================================================================
   s_sse;        sortie seuil degrevement th
  ============================================================================*/

typedef struct {
    short   anref                 ; /* annee ref calculette th (millesime)    */
    char    versr                 ; /* lettre version d'un programme          */
    s_reds  reds_1                ; /* structure sortie redevable reds        */
    s_reds  reds_2                ; /* structure sortie redevable reds        */
    s_reds  reds_c                ; /* structure sortie redevable reds        */
    s_signature * signature       ; /* pointeur sur signature                 */
    s_liberreur * libelle         ; /* pointeur sur erreur                    */
} s_sse;

/*-------- fin de sse : sortie seuil degrevement th                   --------*/


/*============================================================================
   s_rae : structure entree redevance audiovisuelle
  ============================================================================*/

typedef struct {
    short   antax                 ; /* annee campagne taxation (millesime)    */
    char    dep[3]                ; /* code departement                       */
    char    champ                 ; /* code champ redevance                   */
    char    qtvrt                 ; /* code questionnaire redevance retenu
                                       redevance                              */
    char    rgsor                 ; /*rand de sortie exon�ration droit W      */
    char    degtv                 ; /* code degrevement redevance             */
} s_rae;

/*-------- fin de la structure entree redevance audiovisuelle s_rae  --------*/


/*============================================================================
   s_ras :  structure sortie redevance audiovisuelle
  ============================================================================*/

typedef struct {
    char    versr                 ; /* lettre version d'un programme          */
    long    cottv                 ; /* Cotisation redevance                   */
    long    fratv                 ; /* Frais redevance                        */
    long    somtv                 ; /* Montant redevance en recouvrement      */
    long    mdgtv                 ; /* Degrevement redevance                  */
    long    nettv                 ; /* Net a payer redevance                  */
    char    roltv[4]              ; /* Code role redevance                    */
    s_signature * signature       ; /* pointeur sur signature                 */
    s_liberreur * libelle         ; /* pointeur sur erreur                    */
} s_ras;

/*-------- fin de la structure sortie redevance audiovisuelle         --------*/
/*============================================================================
   s_vle :  structure entr�e des vl pour klpc
  ============================================================================*/
typedef struct {
     long    vlbr;               /*VL brute revisee N                  */
     long    vlbrt;              /*VL brute revisee N TSE              */
     long    vlbr7;              /*VL brute revisee 2017               */
     long    vlbrt7;             /*VL brute revisee 2017 TSE           */
     long    vlb77;              /*VL brute base 70 2017               */
     long    vlbir;              /*VL brute imposee revisee N          */
     long    vlbirt;             /*VL brute imposee revisee N TSE      */
     long    vlbir7;             /*VL brute imposee revisee 2017       */
     long    vlbir7t;            /*VL brute imposee revisee 2017 TSE   */
     long    vlbi77;             /*VL brute imposee base 70 2017       */
     long    vlbnir;             /*VL brute non imposee revisee N      */
     long    vlbnirt;            /*VL brute non imposee revisee N TSE  */
     long    vlbnir7;            /*VL brute non imposee revisee 2017   */
     long    vlbnir7t;           /*VL brute non imposee revisee 2017TSE*/
     long    vlbni77;            /*VL brute non imposee base 70 2017   */
     long    vlbpc;              /* VL70 des locaux H de N             */
     long    vlbp7;              /* VL70 des locaux H de 2017          */
} s_vle;
/*-------- fin de la structure entr�e des vl pour klpc s_vle  --------*/



/*============================================================================
   s_lpe1 :  structure entr�e du module klpc
  ============================================================================*/
typedef struct {
    short antax;
    s_coe1 *p_coe1;
    s_vle *p_vle ;      /* structure contenant les vl n�cessaires aux calculs du module klpc                             */
    s_lopre lopre[DIM_LOCAUX_PRO]; /* tableau des locaux professionnels  en provenance de ea                             */
    s_qtpAb *p_AbCote;  /* structure emportant les abattements appliqu�s � la cote fournis par kacc et consign�s dans s2 */
} s_lpe1;
/*-------- fin de la structure entree s_lpe1  --------*/
/*============================================================================
   s_lpe2 :  structure entr�e du module klpc portant les taux N et 2017
  ============================================================================*/
typedef struct {

    s_coe2 *p_coe2;
    s_coe2 *p_coe2_7;

} s_lpe2;
/*-------- fin de la structure entree s_lpe2  --------*/


/*============================================================================
   s_lps :  structure sortie du module klpc
  ============================================================================*/
typedef struct {
char versr ;
    s_loprs *p_loprs[DIM_LOCAUX_PRO];  /* resultats taxation liquidation pour retour vers Cobol */
    s_qtpAb *p_qtpAbL;              /* quotes-parts d'abattement des locaux pros puis emporte
                                       les abattements calcul�s et somm�s sur LH et LP */
    s_qtpAb *p_qtpAbLh;             /* quotes-parts d'abattement des locaux d'habitation */
    long sommePaLis;                /* somme des pas de lissage */
    s_signature *signature;
    s_liberreur *libelle ;
} s_lps;
/*-------- fin de la structure sortie s_lps  --------*/



/*============================================================================
   s_sre :  structure entree d'une cote pour le calcul des frais
            prelevements et somme a recouvrer
  ============================================================================*/

typedef struct {
    short   antax                 ; /* annee de taxation                      */
    char    dep[03]               ; /*code departement                        */
    char    cne[04]               ; /* code commune de l'aft                  */
    char    degex                 ; /* Code degrevement ou exoneration
                                       F, I, A, D, V, S, E, L
                                       ou blanc code droit a degrevement
                                       ou a exoneration tenant compte de
                                       la condition de cohabitation et de
                                       la situation a l'IR */
    long vlbpc                    ; /* VL actualis�e et revaloris�e des locaux � usage
                                       d�habitation (code affectation H et F) en THP/THE/THS/THLV */
    long vlbch                    ; /* Valeur locative brute des locaux dont le code affectation est H */

    char    aff                   ; /* Code affectation du local              */
    /*Bug 222601 */
    char    abatk                 ; /*si 'K' abatt de VL sortie en sifflet*/
    /*Fin Bug 222601 */
    char    rgsork;             /*rang de sortie abatt (blc ou de 3 � 6)*/

    char    cnat                  ; /* Code nature FIP du redevable
                                       utilis� pour determiner le taux de
                                       prelevement                            */
    char    tax                   ; /* Code taxation P, S, E ou blanc
                                       (THLV)                                 */
    char    indaths               ;  /*Indicateur assujetissement majo THS    */
    char    indmths               ; /* indic. de majoration THS               */
    char    indthlv               ; /* indicateur THLV                        */
    double  timths                ; /* Taux d'imposition majoration THS       */
    char    tyimp                 ; /* type d'impot H TH ou T THLV            */
    char    imaisf                ; /*indic droit degvt Macron apres ISF  */
    long    cotic                 ; /* Bug 215413 cotisation communale des LH */

    s_cols  cols_c                ; /* donnees communales de la cote       */
    s_cols  cols_s                ; /* donnees syndicales de la cote       */
    s_cols  cols_q                ; /* donnees groupement de communes de
                                       la cote                                */
    s_cols  cols_n                ; /* donnees TSE de la cote              */
    s_cols  cols_g                ; /* donnees TSE Autre                   */
    s_cols  cols_e                ; /* donnees GEMAPI                      */

} s_sre;

/*-------- fin de la structure entree d'une cote pour calcul frais s_sre --------*/


/*============================================================================
   s_srs :  structure sortie  du calcul des frais, prelevement
            et somme a recouvrer
  ============================================================================*/

typedef struct {

    char    versr                 ; /* lettre version d'un programme          */
    long    majths				  ; /* majoration THS				          */
    long    limajths              ; /*Partie du lissage sur la majo THS
                                    Bug 212403 amelioration */
    long    coticm                ; /* cotisation communale majoree           */
    long    frait                 ; /* Total des frais de role                */
    long    fgest_cq              ; /* Frais de gestion commune & interco     */
    long    fgest_ng              ; /* Frais de gestion TSE-TSE Autre         */
    long    fgest_e               ; /* Frais de gestion GEMAPI                */
    long    fgest_s               ; /* Frais de gestion syndicat              */
    long    frai4                 ; /* Partie des frais de r�le a 4,4 %       */
    long    far_cq                ; /* Frais d'assiette commune & interco     */
    long    far_s                 ; /* Frais d'assiette syndicat              */
    long    far_ng                ; /* Frais d'assiette TSE-TSE Autre         */
    long    far_e                 ; /* Frais d'assiette GEMAPI                */
    long    prelt                 ; /* Prelevement total                      */
    long    preths                ; /* pr�l�vement additionnel r�sidences secondaires */
    long    somrc                 ; /* Somme a recouvrer, somme des
                                       montants precedents sauf frai4         */
    long    pre02                 ; /* Prelevement a 0,2 %                    */
    long    pre12                 ; /* Prelevement a 1,2 %                    */
    long    pre17                 ; /* Prelevement a 1,7 %                    */

    s_signature * signature       ; /* pointeur sur signature                 */
    s_liberreur * libelle         ; /* pointeur sur erreur                    */

} s_srs;


/*-------- fin de la structure sortie srs du calcul des sommes � recouvrer           --------*/



/*============================================================================
   reservation des variables pour ILIAD
  ============================================================================*/

#if defined(ILIAD)
    s_e1 e1;
    s_s1 s1;
    s_e2 e2;
    s_s2 s2;
    s_e3 e3;
    s_e4 e4;
    s_s4 s4;
    s_e6 e6;
    s_s6 s6;
    s_e7 e7;
    s_s7 s7;
#endif

#endif


