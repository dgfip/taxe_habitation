 /*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/
 
    /*==============================*/
    /* MODULE DE DETERMINATION DU   */
    /*     NET A PAYER              */
    /*==============================*/


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif


    /*======*/
    /* MAIN */
    /*======*/

int th_0kdnc(
             s_dne * p_dne ,   /*pointeur sur structure entree*/
             s_dns * p_dns)    /*pointeur sur structure sortie*/
{

    char versr = 'A';
    s_signature signature;
    int rets;                 /*code retour signature*/
    int ret;                 /* code retour sous-fonctions   */
    int retour;


    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_dns->signature = &signature  ;

    /* controle de la signature */
    /* ------------------------ */
    rets=controle_signature(RKDNC,versr,&(p_dns->libelle));
    if (rets!=0)
    {
        return(rets);
    }

    /* initialisation des zones de sortie */
    /* ---------------------------------- */
    init_sortie_kdnc(p_dns);



    /* Controle des valeurs d entree */
    /* ----------------------------- */

    /* somme a recouvrer de la THP */
    if (p_dne->somrp < 0)
    {
        retour = 1002;
        cherche_Erreur(retour,&(p_dns->libelle));
        return (1002);
    }


    /* revenu imposable */
    /*if (p_dne->revim < 0)
    {
        retour = 1005;
        cherche_Erreur(retour,&(p_dns->libelle));
        return (1005);
    }*/

    /* presence d'une vl exoneree non nulle */
    if ((p_dne->vlexo !='O') && (p_dne->vlexo !='N'))
    {

        retour = 1009;
        cherche_Erreur(retour,&(p_dns->libelle));
        return (1009);
    }




        /* somme a recouvrer */
        if (p_dne->somrc < 0)
        {
            retour = 1003;
            cherche_Erreur(retour,&(p_dns->libelle));
            return (1003);
        }
        /* somme a recouvrer < somme a recouvrer de la THP */
        if (p_dne->somrc < p_dne->somrp)
        {
            retour = 1004;
            cherche_Erreur(retour,&(p_dns->libelle));
            return (1004);
        }


        /* revenu imposable du foyer fiscal mesures Macron */
        if (p_dne->revffm < 0)
        {
            retour = 1015;
            cherche_Erreur(retour,&(p_dns->libelle));
            return (1015);
        }

    /*==============================*/
    /* T  R  A  I  T  E  M  E  N  T */
    /*==============================*/
 if (p_dne->degex!= ' ')
    {


                    /* EXONERATION  degex � F, I, ,A, D, V, S, W, K */
                    /* ----------- */
            if (est_exoneration(p_dne->degex))
            {
                if (p_dne->somrp > 0)
                {
                    cherche_Erreur(1006,&(p_dns->libelle));
                    return (1006);
                }
                else
                {
                    /*  bug 261512  */
                    if ((p_dne->vlexo == 'O') || (p_dne->degex=='D'))
                    /* fin bug 261512 */
                    {
                        ret=NaP_Exo(p_dne,p_dns);
                        if (ret!=0)
                        {
                            return ret;
                        }
                    }else /* cas ou degex est � exo mais vlexo est � 'N': ce cas se produit s'il n'y a pas de base � exon�rer du fait que VL -      abattement vaut 0 */
                    {
                    ret=NaP_Ni_Deg_Ni_Exo(p_dne,p_dns);
                        if (ret!=0)
                        {
                            return ret;
                        }                    }
                }
            }
            else /* DEGEX � G*/
            {
                            /* DEGREVEMENT TOTAL*/
                            /* ----------- */
               /* if (est_degrevement_total(p_dne->degex)) */ /*degex � 'G' */
                /*{*/

                        ret=NaP_Deg_Tot(p_dne,p_dns);
                        if (ret!=0)
                        {
                            return ret;
                        }

               /*}*/

            }
    }
    else
    {
                /* AUCUN ALLEGEMENT DEGEX A BLANC OU THLV*/
                /* ---------------- */

            ret=NaP_Ni_Deg_Ni_Exo(p_dne,p_dns);
            if (ret!=0)
            {
                return ret;
            }

    }



    /*contr�le de coh�rence du bon deroulemnent de KDNC*/


    if ((strchr("ADFGIKSVW",p_dne->degex) != NULL) && (p_dns->netth != 0))
    {
        cherche_Erreur(1007,&(p_dns->libelle));
        return (1007);

    }

    if ((p_dne->degex == ' ') && (p_dns->degpl>0))
    {
        cherche_Erreur(1008,&(p_dns->libelle));
        return (1008);

    }

  /* 2020MAJ17-02-04 */

     if ((p_dns->netth < p_dne->p_cons->seuil_nap) && (p_dns->netth > 0))
    {
        cherche_Erreur(3021,&(p_dns->libelle));
        return (3021);

    }



  /* FIN  2020MAJ17-02-04 */

   /*================*/
   /* FIN TRAITEMENT */
   /*================*/

    return (0);
}



    /* ===================================================== */
    /* fonction de calcul net a payer avec degrevement total */
    /* ===================================================== */

int NaP_Deg_Tot(
                s_dne *p_dne,        /* pointeur sur donnee entree */
                s_dns *p_dns)        /* pointeur sur donnee sortie */
{
    if (p_dne->somrp < 0)
    {
        return (1202);
    }
    if (p_dne->somrc < 0)
    {
        return (1203);
    }
    if (p_dne->somrc < p_dne->somrp) /* somrp est mis � somrc en entrant dans kdnc*/
    {
        return (1204);
    }


            p_dns->degpl = p_dne->somrc;
            /*p_dns->totth = p_dne->somrc ;*/
            p_dns->totth = 0;
            p_dns->netth = 0;
            /*bug 255597 */

                 if ( p_dne->somrc != 0 )
              {
                strncpy(p_dns->codro,"DT",3);
              }
            /*fin bug 255597 */




    return (0);
}
/*-----------------------Fin de la fonction NaP_Deg_Tot-----------------------*/


    /* =============================================== */
    /* fonction de calcul net a payer avec exoneration */
    /* =============================================== */

int NaP_Exo(s_dne *p_dne,    /* pointeur sur donnee entree */
            s_dns *p_dns)    /* pointeur sur donnee sortie */
{
    if (p_dne->somrc != 0)
    {
       return (1303);
    }


    p_dns->degpl = 0;
    p_dns->totth = p_dne->somrc; /* c est � dire 0*/
    p_dns->netth = p_dns->totth; /* c est � dire 0*/
    strncpy(p_dns->codro,"EX",3);

    return (0);
}
/*-----------------------Fin de la fonction NaP_Exo---------------------*/





/*----------------------------------------------------------------------------*/
/*                    Initialisation de la zone de sortie                     */
/*----------------------------------------------------------------------------*/

void  init_sortie_kdnc(s_dns * p_dns)
{

    p_dns->degpl = 0;
    p_dns->totth = 0;
    p_dns->netth = 0;
    strncpy(p_dns->codro, "  ", 3);
    p_dns->tottaham  = 0     ;
    p_dns->imap      = ' '     ;
    p_dns->degm      = 0     ;
    p_dns->degmaso   = 0     ;
    strncpy(p_dns->codegm, "    ", 5);
    p_dns->codecm = ' ';
    p_dns->txrefm = 0;


}

