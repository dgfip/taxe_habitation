/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

/*======================================================================================================================*/
/*                  MODULE DE RECALCUL DU PLANCHONNEMENT DE LA VL DES LOCAUX PROFESSIONNELS (th-0kVLC)                  */
/*======================================================================================================================*/


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif



int th_0krpc ( s_ep *p_ep, s_sp *p_sp)

{

static s_signature signature;
int ret = 0;
long vlrn7 = 0;
long vlrnt7 = 0;
long vlrn = 0;
long vlrnt = 0;

 /* CA_TH_0226 */
    p_sp->mntpl     =  0  ;
    p_sp->sgnpl     =  '+'  ;
    p_sp->mntplt    =  0  ;
    p_sp->sgnplt    =  '+'  ;
    p_sp->vlrnp     =  p_ep->vlthr;
    p_sp->vlrnpt    =  p_ep->vlthr;
    p_sp->vlrnp7    =  p_ep->vlthr7  ;
    p_sp->vlrnpt7   =  p_ep->vlthr7  ;


    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_sp->signature = &signature;


 /*                        controle de la signature                        */

    p_sp->versr='A';
    ret=controle_signature(RKRPC,p_sp->versr,&(p_sp->libelle));


    if (ret!=0)
    {
        cherche_Erreur(ret,&(p_sp->libelle));
        return(ret);
    }


/*                        Detection des anomalies                        */

/*CA_ANO_0397 */

    p_sp->anref = ANREF;
    if (p_ep->antax != ANREF)
    {
        cherche_Erreur(5130,&p_sp->libelle);
        return(5130);
    }

/*CA_ANO_0398 */

    if (!(p_ep->vlb70p7 > 0 ) || !(p_ep->vlthr7 > 0))
    {
       cherche_Erreur(5131,&p_sp->libelle);
       return(5131);
    }

/*CA_ANO_0399 */


    if ((p_ep->coefn == 0 ) || (p_ep->coefnt == 0))
    {
        cherche_Erreur(5132,&p_sp->libelle);
        return(5132);
    }


/*         _______________
          |  TRAITEMENT   |
          |_______________|       */


/* CA_TH_0227 */

    vlrn  = arrondi_euro_voisin((double)(p_ep->vlthr * p_ep->coefn));

    vlrn7 = arrondi_euro_voisin((double)(p_ep->vlthr7 * p_ep->coefn));

/* CA_TH_0228 */

    vlrnt  = arrondi_euro_voisin((double)(p_ep->vlthr * p_ep->coefnt));

    vlrnt7 = arrondi_euro_voisin((double)(p_ep->vlthr7 * p_ep->coefnt));

/* CA_TH_0229 */

    if (p_ep->vlb70p7 >= vlrn7)
    {
        p_sp->mntpl = arrondi_euro_voisin((0.5 * (double)(p_ep->vlb70p7 - vlrn7)));
    }else
    {
        p_sp->mntpl = arrondi_euro_voisin((0.5 * (double)(vlrn7 - p_ep->vlb70p7 )));
    }

/* CA_TH_0230 */

    if (p_ep->vlb70p7 >= vlrn7)
    {
        p_sp->sgnpl = '+';

    }else
    {
        p_sp->sgnpl = '-';
    }

/* CA_TH_0231 */

    if (p_ep->vlb70p7 >= vlrnt7)
    {
        p_sp->mntplt = arrondi_euro_voisin((0.5 * (double)(p_ep->vlb70p7 - vlrnt7)));

    }else
    {
        p_sp->mntplt = arrondi_euro_voisin((0.5 * (double)(vlrnt7 - p_ep->vlb70p7 )));
    }

/* CA_TH_0232 */

    if (p_ep->vlb70p7 >= vlrnt7)
    {
        p_sp->sgnplt = '+';

    }else
    {
        p_sp->sgnplt = '-';
    }

/* CA_TH_0233 */
    if (p_ep->vlb70p7 >= vlrn7)
    {
        p_sp->vlrnp = vlrn + p_sp->mntpl;
    }else
    {
        p_sp->vlrnp = vlrn - p_sp->mntpl;

    }
  /* Bug 246284 - 1/4 */
    if (p_sp->vlrnp < 0)
    {
        p_sp->vlrnp = 0;
    }

  /*FIN Bug 246284 - 1/4 */



/* CA_TH_0234 */

    if (p_ep->vlb70p7 >= vlrnt7)
    {
        p_sp->vlrnpt = vlrnt + p_sp->mntplt;
    }else
    {
        p_sp->vlrnpt = vlrnt - p_sp->mntplt;

    }
  /*  Bug 246284 - 2/4 */
    if (p_sp->vlrnpt < 0)
    {
        p_sp->vlrnpt = 0;
    }
  /* Fin Bug 246284 - 2/4 */

/* CA_TH_0235 */


    if (p_ep->vlb70p7 >= vlrn7)
    {
        p_sp->vlrnp7 = vlrn7 + p_sp->mntpl;
    }else
    {
        p_sp->vlrnp7 = vlrn7 - p_sp->mntpl;

    }
  /*    Bug 246284 - 3/4 */
    if (p_sp->vlrnp7 < 0)
    {
        p_sp->vlrnp7 = 0;
    }
  /* Fin Bug 246284 - 3/4 */

/* CA_TH_0236 */

    if (p_ep->vlb70p7 >= vlrnt7)
    {
        p_sp->vlrnpt7 = vlrnt7 + p_sp->mntplt;
    }else
    {
        p_sp->vlrnpt7 = vlrnt7 - p_sp->mntplt;

    }
  /*    Bug 246284 - 4/4 */
    if (p_sp->vlrnpt7 < 0)
    {
        p_sp->vlrnpt7 = 0;
    }
  /* Fin Bug 246284 - 4/4 */
     return(0);

    /*     ______________________
          |  FIN DE TRAITEMENT   |
          |______________________|       */

}

/***********************************************************************************************************************************************************/
