/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

/*============================================================================*/
/* Module de lissage des cotisations des locaux professionnels                */
/*                                                                            */
/*============================================================================*/

#if defined (ILIAD)
#include <th-0ksts.h>
#include <th-0kfos.h>
#else
#include "th-0ksts"
#include "th-0kfos"
#endif

extern int gbl_Recalcul_Lissage;
extern int gbl_nbLp;

int th_0klpc (s_lpe1 *p_lpe1, s_lpe2 *p_lpe2, s_lps *p_lps)
{
    static s_vls tab[DIM_LOCAUX_PRO];
    int i;
    int ret;
    int rets;   /*zone retour signature */
    s_coe1 coe;
    s_coe1 *p_coe =&coe;
    s_cols cols_null;
    long cotTotR7 = 0;
    long cotTot77 = 0;
    long pasDeLissage =0;
    long delta = 0;
    static s_signature signature;

    p_lps->sommePaLis = 0; /* CA_TH_0119 Initialisation du pas de lissage */
    p_lps->versr='A';
    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_lps->signature=&signature ;

    /*      controle de la signature                        */
    /* CA_ANO_0383 Controle du module KLPC */
    rets=controle_signature(RKLPC,p_lps->versr,&(p_lps->libelle));
    if (rets!=0)
    {
        cherche_Erreur(rets,&(p_lps->libelle));
        return(rets);
    }

    /* CALCUL DES ABATTEMENTS APPLICABLES AUX LOCAUX */

    ret =  traitementAbatLPH(p_lpe1->lopre, p_lps->p_loprs, p_lpe1->p_vle, p_lpe1->p_AbCote, p_lps->p_qtpAbLh, p_lps->p_qtpAbL);


    if(ret != 0) {
        cherche_Erreur(ret,&(p_lps->libelle)) ;
        return(ret);
    }

    /* CALCUL DES COTISATIONS DE CHAQUE LOCAL PROFESSIONNEL */
    /* Donn�es interm�diaires attach�es au local: tableau des vl imposee et non imposee */

    for (i=0; i < gbl_nbLp; i++)
    {
        initialisation_vls(&tab[i]);

        /* CALCUL DES VL IMPOSEES DE CHAQUE LOCAL */
        /* CA_G0148 CA_TH_0083 CA_TH_0084 CA_TH_0085 CA_TH_0086 CA_TH_0087 */
        if ((p_lpe1->p_coe1->tax == 'P') || (p_lpe1->p_coe1->tax == 'E'))    /* Cas des articles THP ou THE */
        {
            if ((p_lpe1->p_coe1->abatk == 'K') && (p_lpe1->p_coe1->rgsork == '3'))
            {

        /* CONSEQUENCE    CA_TH_237     CA_TH_239 */
                /* CA_TH_0083 VL revisee neutralisee et planchonnee N du local */
                tab[i].vlni = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnp * 2)) / 3);
                tab[i].vli = (p_lpe1->lopre[i].vlrnp - tab[i].vlni);

                /* CA_TH_0084 VL revisee neutralisee et planchonnee N TSE du local */
                tab[i].vlnit = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnpt * 2)) / 3);
                tab[i].vlit = (p_lpe1->lopre[i].vlrnpt - tab[i].vlnit);

            /* 2019MAJR13-01-12 2019MAJR13-01-13 2019MAJR13-01-14 */
                if (gbl_Recalcul_Lissage==1)
                {

                    /* CA_TH_0085 VL revisee neutralisee et planchonnee 2017 du local */
                    tab[i].vlni7 = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnp7 * 2)) / 3);
                    tab[i].vli7 = (p_lpe1->lopre[i].vlrnp7 - tab[i].vlni7);

                    /* CA_TH_0086 VL revisee neutralisee et planchonnee TSE 2017 du local */
                    tab[i].vlnit7 = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnpt7 * 2)) / 3);
                    tab[i].vlit7 = (p_lpe1->lopre[i].vlrnpt7 - tab[i].vlnit7);

                    /* CA_TH_0087 VL VL70 actualisee et revalorisee 2017 du local */
                    tab[i].vlni77 = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlb70p7 * 2)) / 3);
                    tab[i].vli77 = (p_lpe1->lopre[i].vlb70p7 - tab[i].vlni77);
                }
            /* FIN 2019MAJR13-01-12 2019MAJR13-01-13 2019MAJR13-01-14 */

            }
            else
            {
                /* Abattement 1/3 */
                if ((p_lpe1->p_coe1->abatk == 'K') && (p_lpe1->p_coe1->rgsork == '4'))
                {
                    /* CA_TH_0083 VL revisee neutralisee et planchonnee N du local */
                    tab[i].vlni = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnp * 1)) / 3);
                    tab[i].vli = (p_lpe1->lopre[i].vlrnp - tab[i].vlni);

                    /* CA_TH_0084 VL revisee neutralisee et planchonnee N TSE du local */
                    tab[i].vlnit = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnpt * 1)) / 3);
                    tab[i].vlit = (p_lpe1->lopre[i].vlrnpt - tab[i].vlnit);

                /* 2019MAJR13-01-12 2019MAJR13-01-13 2019MAJR13-01-14 */
                    if (gbl_Recalcul_Lissage==1)
                    {
                        /* CA_TH_0085 VL revisee neutralisee et planchonnee 2017 du local */
                        tab[i].vlni7 = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnp7 * 1)) / 3);
                        tab[i].vli7 = (p_lpe1->lopre[i].vlrnp7 - tab[i].vlni7);

                        /* CA_TH_0086 VL revisee neutralisee et planchonnee TSE 2017 du local */
                        tab[i].vlnit7 = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlrnpt7 * 1)) / 3);
                        tab[i].vlit7 = (p_lpe1->lopre[i].vlrnpt7 - tab[i].vlnit7);

                        /* CA_TH_0087 VL VL70 actualisee et revalorisee 2017 du local */
                        tab[i].vlni77 = arrondi_euro_voisin(((double)(p_lpe1->lopre[i].vlb70p7 * 1)) / 3);
                        tab[i].vli77 = (p_lpe1->lopre[i].vlb70p7 - tab[i].vlni77);
                    }
                /* FIN 2019MAJR13-01-12 2019MAJR13-01-13 2019MAJR13-01-14 */

                }
                else
                {
                    /* CA_TH_0083 VL revisee neutralisee et planchonnee N du local */
                    tab[i].vlni = 0  ;
                    tab[i].vli = p_lpe1->lopre[i].vlrnp;

                    /* CA_TH_0084 VL revisee neutralisee et planchonnee N TSE du local */
                    tab[i].vlnit = 0  ;
                    tab[i].vlit = p_lpe1->lopre[i].vlrnpt;

                /* 2019MAJR13-01-12 2019MAJR13-01-13 2019MAJR13-01-14 */
                    /* CA_TH_0085 VL revisee neutralisee et planchonnee 2017 du local */
                    if (gbl_Recalcul_Lissage==1)
                    {
                        tab[i].vlni7 = 0  ;
                        tab[i].vli7 = p_lpe1->lopre[i].vlrnp7;

                        /* CA_TH_0086 VL revisee neutralisee et planchonnee TSE 2017 du local */
                        tab[i].vlnit7 = 0  ;
                        tab[i].vlit7 = p_lpe1->lopre[i].vlrnpt7;

                        /* CA_TH_0087 VL VL70 actualisee et revalorisee 2017 du local */
                        tab[i].vlni77 = 0  ;
                        tab[i].vli77 = p_lpe1->lopre[i].vlb70p7;
                    }
                /*FIN 2019MAJR13-01-12 2019MAJR13-01-13 2019MAJR13-01-14 */

                }
            }
        }

        if (p_lpe1->p_coe1->tax == 'S')
        {
            /* CA_TH_0083 VL revisee neutralisee et planchonnee N du local */
            tab[i].vlni = 0  ;
            tab[i].vli = p_lpe1->lopre[i].vlrnp;

            /* CA_TH_0084 VL revisee neutralisee et planchonnee N TSE du local */
            tab[i].vlnit = 0  ;
            tab[i].vlit = p_lpe1->lopre[i].vlrnpt;

            /* CA_TH_0085 VL revisee neutralisee et planchonnee 2017 du local */
            tab[i].vlni7 = 0  ;
            tab[i].vli7 = p_lpe1->lopre[i].vlrnp7;

            /* CA_TH_0086 VL revisee neutralisee et planchonnee TSE 2017 du local */
            tab[i].vlnit7 = 0  ;
            tab[i].vlit7 = p_lpe1->lopre[i].vlrnpt7;

            /* CA_TH_0087 VL VL70 actualisee et revalorisee 2017 du local */
            tab[i].vlni77 = 0  ;
            tab[i].vli77 = p_lpe1->lopre[i].vlb70p7;
        }
    }

    /* CALCUL DES COTISATIONS  PROPRES A CHAQUE LOCAL  */
               /* APPEL A KCOC */
    for (i=0 ; i < gbl_nbLp ; i++)
    {
        s_cos cos;
        s_cos *p_cos = &cos;
        /* cote du local - liquidation N */
        /* CA_TH_0088 CA_TH_0089 CA_TH_0090 CA_TH_0091 */
        p_lpe1->p_coe1->vl = tab[i].vli;
        p_lpe1->p_coe1->vlt = tab[i].vlit;
        p_lpe1->p_coe1->p_cols_c =  &p_lps->p_loprs[i]->cols_cr;
        p_lpe1->p_coe1->p_cols_q =  &p_lps->p_loprs[i]->cols_qr;
        p_lpe1->p_coe1->p_cols_s =  &p_lps->p_loprs[i]->cols_sr;
        p_lpe1->p_coe1->p_cols_n =  &p_lps->p_loprs[i]->cols_nr;
        p_lpe1->p_coe1->p_cols_g =  &p_lps->p_loprs[i]->cols_gr;
        p_lpe1->p_coe1->p_cols_e =  &p_lps->p_loprs[i]->cols_er;
        p_lpe1->p_coe1->p_cols_f =  &p_lps->p_loprs[i]->cols_fr;
        p_lpe1->p_coe1->p_cols_r =  &p_lps->p_loprs[i]->cols_rr;

        ret = th_0kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2, p_cos);

        if (ret !=0)
        {
            cherche_Erreur(ret,&(p_lps->libelle)) ;
            return(ret);
        }

    /* 2019MAJR13-01-15 2019MAJR13-01-16   */
        if (gbl_Recalcul_Lissage==1)
        {
            /* cote du local - liquidation 2017 Base VLNP 2017 */
            /* CA_TH_0092 CA_TH_0093 CA_TH_0094 CA_TH_0095 CA_TH_0096 */
            if (p_lpe1->lopre[i].vlrnp7 >0)
            {
                p_lpe1->p_coe1->vl = tab[i].vli7;
                p_lpe1->p_coe1->vlt = tab[i].vlit7;
                init_cols(&cols_null);
                p_lpe1->p_coe1->p_cols_c =  &p_lps->p_loprs[i]->cols_cr7;
                p_lpe1->p_coe1->p_cols_q =  &p_lps->p_loprs[i]->cols_qr7;
                p_lpe1->p_coe1->p_cols_s =  &p_lps->p_loprs[i]->cols_sr7;
                p_lpe1->p_coe1->p_cols_n =  &p_lps->p_loprs[i]->cols_nr7;
                p_lpe1->p_coe1->p_cols_g =  &p_lps->p_loprs[i]->cols_gr7;
                p_lpe1->p_coe1->p_cols_e =  &p_lps->p_loprs[i]->cols_er7;
                p_lpe1->p_coe1->p_cols_f =  &cols_null;
                p_lpe1->p_coe1->p_cols_r =  &cols_null;


                ret = th_0kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2_7, p_cos);
                if (ret !=0)
                {
                    cherche_Erreur(ret,&(p_lps->libelle)) ;
                    return(ret);
                }
            }

            /* cote du local - liquidation 2017 vl70 taux 2017 */
            /* CA_TH_0097 CA_TH_0098 CA_TH_0099 CA_TH_0100 CA_TH_0101 */
            if (p_lpe1->lopre[i].vlb70p7 >0)
            {
                p_lpe1->p_coe1->vl = tab[i].vli77;
                p_lpe1->p_coe1->vlt = tab[i].vli77;
                p_lpe1->p_coe1->p_cols_c =  &p_lps->p_loprs[i]->cols_c7;
                p_lpe1->p_coe1->p_cols_q =  &p_lps->p_loprs[i]->cols_q7;
                p_lpe1->p_coe1->p_cols_s =  &p_lps->p_loprs[i]->cols_s7;
                p_lpe1->p_coe1->p_cols_n =  &p_lps->p_loprs[i]->cols_n7;
                p_lpe1->p_coe1->p_cols_g =  &p_lps->p_loprs[i]->cols_g7;
                p_lpe1->p_coe1->p_cols_e =  &p_lps->p_loprs[i]->cols_e7;
             /* Bug 217290 */
                p_lpe1->p_coe1->p_cols_f =  &cols_null;
                p_lpe1->p_coe1->p_cols_r =  &cols_null;
             /* Fin Bug 217290 */

                ret = th_0kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2_7, p_cos);
                if (ret !=0)
                {
                    cherche_Erreur(ret,&(p_lps->libelle)) ;
                    return(ret);
                }
            }
        }

    /* FIN 2019MAJR13-01-15 2019MAJR13-01-16   */

    }


    /* CALCUL DU LISSAGE */
    /* Parcours des locaux pros */
    for(i=0 ; i < gbl_nbLp ; i++)
    {
        /* Si lissage a effectuer sur le local de rang i */
        /* CA_TH_0118 Condition d application du lissage */
        /* CA_TH_0119 Initialisation du pas de lissage */
    /* 2019MAJR13-02-01  */
        p_lps->p_loprs[i]->qtprlc = p_lpe1->lopre[i].qtprlc;
        p_lps->p_loprs[i]->qtprlq = p_lpe1->lopre[i].qtprlq;
        p_lps->p_loprs[i]->qtprls = p_lpe1->lopre[i].qtprls;
        p_lps->p_loprs[i]->qtprln = p_lpe1->lopre[i].qtprln;
        p_lps->p_loprs[i]->qtprlg = p_lpe1->lopre[i].qtprlg;
        p_lps->p_loprs[i]->qtprle = p_lpe1->lopre[i].qtprle;
    /* FIN 2019MAJR13-02-01  */
        pasDeLissage = 0;

        if (p_lpe1->lopre[i].indlis == 'O')
        {
    /* 2019MAJR13-01-17 2019MAJR13-01-18 2019MAJR13-01-19*/
            if (gbl_Recalcul_Lissage==1)
            {

                coe.p_cols_c =  &p_lps->p_loprs[i]->cols_cr7;
                coe.p_cols_q =  &p_lps->p_loprs[i]->cols_qr7;
                coe.p_cols_s =  &p_lps->p_loprs[i]->cols_sr7;
                coe.p_cols_n =  &p_lps->p_loprs[i]->cols_nr7;
                coe.p_cols_g =  &p_lps->p_loprs[i]->cols_gr7;
                coe.p_cols_e =  &p_lps->p_loprs[i]->cols_er7;

                /*  CA_TH_0120 Determination de la cotisation revisee totale 2017 */
                cotTotR7 =  cotisationTotale ( p_coe );

                coe.p_cols_c =  &p_lps->p_loprs[i]->cols_c7;
                coe.p_cols_q =  &p_lps->p_loprs[i]->cols_q7;
                coe.p_cols_s =  &p_lps->p_loprs[i]->cols_s7;
                coe.p_cols_n =  &p_lps->p_loprs[i]->cols_n7;
                coe.p_cols_g =  &p_lps->p_loprs[i]->cols_g7;
                coe.p_cols_e =  &p_lps->p_loprs[i]->cols_e7;

                /*  CA_TH_0121 Determination de la cotisation VL70 totale 2017 */
                cotTot77 = cotisationTotale ( p_coe );

                /*  CA_TH_0122 Determination du pas de lissage */
                pasDeLissage = calculPas (cotTotR7, cotTot77);
                p_lps->sommePaLis = p_lps->sommePaLis + pasDeLissage;
            }else
            {
                pasDeLissage =  p_lps->p_loprs[i]->qtprlc + \
                                p_lps->p_loprs[i]->qtprlq + \
                                p_lps->p_loprs[i]->qtprls + \
                                p_lps->p_loprs[i]->qtprln + \
                                p_lps->p_loprs[i]->qtprlg + \
                                p_lps->p_loprs[i]->qtprle;

                p_lps->sommePaLis = p_lps->sommePaLis + pasDeLissage;
            }
        }
    /* FIN 2019MAJR13-01-17 2019MAJR13-01-18 2019MAJR13-01-19*/

        /*  CA_TH_0123 Repartition du pas de lissage */
    /* 2019MAJR13-01-19 */
        if ((p_lpe1->lopre[i].indlis == 'O') && (p_lpe1->lopre[i].indrec == 'O'))
    /* Debut Bugs 244172 amelioration et 244518 amelioration */
        {
            if (pasDeLissage !=0)
/* FIN 2019MAJR13-01-19 */
            {
                majQtpPdL ( &p_lps->p_loprs[i]->qtprlc, pasDeLissage,  p_lps->p_loprs[i]->cols_cr7,  p_lps->p_loprs[i]->cols_c7,cotTotR7, cotTot77 );
                majQtpPdL ( &p_lps->p_loprs[i]->qtprlq, pasDeLissage,  p_lps->p_loprs[i]->cols_qr7,  p_lps->p_loprs[i]->cols_q7,cotTotR7, cotTot77 );
                majQtpPdL ( &p_lps->p_loprs[i]->qtprls, pasDeLissage,  p_lps->p_loprs[i]->cols_sr7,  p_lps->p_loprs[i]->cols_s7,cotTotR7, cotTot77 );
                majQtpPdL ( &p_lps->p_loprs[i]->qtprln, pasDeLissage,  p_lps->p_loprs[i]->cols_nr7,  p_lps->p_loprs[i]->cols_n7,cotTotR7, cotTot77 );
                majQtpPdL ( &p_lps->p_loprs[i]->qtprlg, pasDeLissage,  p_lps->p_loprs[i]->cols_gr7,  p_lps->p_loprs[i]->cols_g7,cotTotR7, cotTot77 );
                majQtpPdL ( &p_lps->p_loprs[i]->qtprle, pasDeLissage,  p_lps->p_loprs[i]->cols_er7,  p_lps->p_loprs[i]->cols_e7,cotTotR7, cotTot77 );
            }
            else
            {
                p_lps->p_loprs[i]->qtprlc = 0;
                p_lps->p_loprs[i]->qtprlq = 0;
                p_lps->p_loprs[i]->qtprls = 0;
                p_lps->p_loprs[i]->qtprln = 0;
                p_lps->p_loprs[i]->qtprlg = 0;
                p_lps->p_loprs[i]->qtprle = 0;
            }

            /* CA_TH_0124 Correction des erreurs liees aux arrondis */
            delta = pasDeLissage - (p_lps->p_loprs[i]->qtprlc + p_lps->p_loprs[i]->qtprlq + p_lps->p_loprs[i]->qtprls +
                                    p_lps->p_loprs[i]->qtprln + p_lps->p_loprs[i]->qtprlg + p_lps->p_loprs[i]->qtprle) ;
            reportDeltaSurQtpSup(delta, qtpSup(p_lps->p_loprs[i]));
        }

        /* CA_TH_0125 Determination de la quote-part modulee N */

        if (p_lpe1->lopre[i].indlis == 'O')

        /* Mise a zero des quotes-parts modulees en cas de taux d'imposition N nul */
        {

                 /* Commune */
                 if (p_lpe2->p_coe2->timpc > 0)
                 {

                     p_lps->p_loprs[i]->qtmc = (p_lps->p_loprs[i]->qtprlc * (2026-p_lpe1->antax));

                     if (p_lps->p_loprs[i]->cols_cr.coti < p_lps->p_loprs[i]->qtmc)
                     {
                           p_lps->p_loprs[i]->qtmc =  p_lps->p_loprs[i]->cols_cr.coti ;
                     }

                 }
                 else
                 {
                     p_lps->p_loprs[i]->qtmc = 0;
                 }
                 /* Intercommunalite */
                 if (p_lpe2->p_coe2->timpq > 0)
                 {

                     p_lps->p_loprs[i]->qtmq = (p_lps->p_loprs[i]->qtprlq * (2026-p_lpe1->antax));
                     if (p_lps->p_loprs[i]->cols_qr.coti < p_lps->p_loprs[i]->qtmq)
                     {
                                p_lps->p_loprs[i]->qtmq =  p_lps->p_loprs[i]->cols_qr.coti ;
                     }
                 }
                 else
                 {
                     p_lps->p_loprs[i]->qtmq = 0;
                 }
                 /* Syndicat */
                 if (p_lpe2->p_coe2->tisyn > 0)
                 {
                     p_lps->p_loprs[i]->qtms = (p_lps->p_loprs[i]->qtprls * (2026-p_lpe1->antax));
                     if (p_lps->p_loprs[i]->cols_sr.coti < p_lps->p_loprs[i]->qtms)
                     {
                         p_lps->p_loprs[i]->qtms =  p_lps->p_loprs[i]->cols_sr.coti;
                     }
                 }
                 else
                 {
                     p_lps->p_loprs[i]->qtms = 0;
                 }
                 /* TSE */
                 if (p_lpe2->p_coe2->titsn > 0)
                 {
                     p_lps->p_loprs[i]->qtmn = (p_lps->p_loprs[i]->qtprln * (2026-p_lpe1->antax));
                     if (p_lps->p_loprs[i]->cols_nr.coti < p_lps->p_loprs[i]->qtmn)
                     {
                         p_lps->p_loprs[i]->qtmn = p_lps->p_loprs[i]->cols_nr.coti;
                     }
                 }
                 else
                 {
                     p_lps->p_loprs[i]->qtmn = 0;
                 }
                 /* TSE Autres */
                 if (p_lpe2->p_coe2->titgp > 0)
                 {
                     p_lps->p_loprs[i]->qtmg = (p_lps->p_loprs[i]->qtprlg * (2026-p_lpe1->antax));
                      if (p_lps->p_loprs[i]->cols_gr.coti < p_lps->p_loprs[i]->qtmg)
                     {
                         p_lps->p_loprs[i]->qtmg = p_lps->p_loprs[i]->cols_gr.coti;
                     }
                 }
                 else
                 {
                     p_lps->p_loprs[i]->qtmg = 0;
                 }
                 /* GEMAPI */
                 if ((p_lpe2->p_coe2->timpe > 0) && (p_lpe1->lopre[i].indqple == ' '))
                 {
                     p_lps->p_loprs[i]->qtme = (p_lps->p_loprs[i]->qtprle * (2026-p_lpe1->antax));
                     if (p_lps->p_loprs[i]->cols_er.coti < p_lps->p_loprs[i]->qtme)
                     {
                         p_lps->p_loprs[i]->qtme = p_lps->p_loprs[i]->cols_er.coti;
                     }
                 }
                 else
                 {
                     p_lps->p_loprs[i]->qtme = 0;
                 }


        }



       if ((p_lpe1->lopre[i].indlis == 'N') || (p_lps->p_loprs[i]->cols_cr.vlnex == p_lps->p_loprs[i]->cols_cr.vlntt))
            {
               p_lps->p_loprs[i]->qtmc = 0;

            }

       if ((p_lpe1->lopre[i].indlis == 'N') || (p_lps->p_loprs[i]->cols_qr.vlnex == p_lps->p_loprs[i]->cols_qr.vlntt ) || (p_lpe1->lopre[i].indqplq == 'N'))
             {
               p_lps->p_loprs[i]->qtmq = 0;

            }
       if (p_lpe1->lopre[i].indlis == 'N' || (p_lps->p_loprs[i]->cols_sr.vlnex == p_lps->p_loprs[i]->cols_sr.vlntt ) || (p_lpe1->lopre[i].indqpls == 'N'))
            {
               p_lps->p_loprs[i]->qtms = 0;

            }
       if (p_lpe1->lopre[i].indlis == 'N' || (p_lps->p_loprs[i]->cols_nr.vlnex == p_lps->p_loprs[i]->cols_nr.vlntt ) || (p_lpe1->lopre[i].indqpln == 'N'))
            {
               p_lps->p_loprs[i]->qtmn = 0;

            }
       if (p_lpe1->lopre[i].indlis == 'N'  || (p_lps->p_loprs[i]->cols_gr.vlnex == p_lps->p_loprs[i]->cols_gr.vlntt ) || (p_lpe1->lopre[i].indqplg == 'N'))
            {
               p_lps->p_loprs[i]->qtmg = 0;

            }
       if (p_lpe1->lopre[i].indlis == 'N'  || (p_lps->p_loprs[i]->cols_er.vlnex == p_lps->p_loprs[i]->cols_er.vlntt ) || (p_lpe1->lopre[i].indqple == 'N'))
            {
               p_lps->p_loprs[i]->qtme = 0;

            }

        /* CA_TH_0126 Determination de la cotisation lissee */

        p_lps->p_loprs[i]->cotilpc = (p_lps->p_loprs[i]->cols_cr.coti - p_lps->p_loprs[i]->qtmc);
        p_lps->p_loprs[i]->cotilpq = (p_lps->p_loprs[i]->cols_qr.coti - p_lps->p_loprs[i]->qtmq);
        p_lps->p_loprs[i]->cotilps = (p_lps->p_loprs[i]->cols_sr.coti - p_lps->p_loprs[i]->qtms);
        p_lps->p_loprs[i]->cotilpn = (p_lps->p_loprs[i]->cols_nr.coti - p_lps->p_loprs[i]->qtmn);
        p_lps->p_loprs[i]->cotilpg = (p_lps->p_loprs[i]->cols_gr.coti - p_lps->p_loprs[i]->qtmg);
        p_lps->p_loprs[i]->cotilpe = (p_lps->p_loprs[i]->cols_er.coti - p_lps->p_loprs[i]->qtme);
    }

    return 0;
}



/*          *********************************************************
            *                                                       *
            *                   FIN DES TRAITEMENTS                 *
            *                                                       *
            ********************************************************* */








long cotisation2003Lissee (long coti2003, long quotePart)
{
    if (coti2003 >= quotePart)
    {
        return (coti2003 - quotePart);
    }
    else
    {
        return 0;
    }
}

long cotisationTotale ( s_coe1 *p_coe)
{
    return (p_coe->p_cols_c->coti + p_coe->p_cols_q->coti + p_coe->p_cols_s->coti +
            p_coe->p_cols_n->coti + p_coe->p_cols_g->coti + p_coe->p_cols_e->coti);
}

long calculPas (double cotR7, long cot77)
{
    return  arrondi_euro_inf(((cotR7 - cot77) / 10 ) + 0.5 );
}

void majQtpPdL (long *p_qtprl, long pasLis, s_cols colsR7, s_cols cols77, double cotR7, long cot77)
{
 	*p_qtprl = arrondi_euro_voisin(pasLis * (( colsR7.coti - cols77.coti)/(cotR7-cot77)));
}

void reportDeltaSurQtpSup (long delta, long *p_qtp)
{
 	*p_qtp = *p_qtp + delta;
}

long * qtpSup(s_loprs *p_loprs)
{
     int i;
     long *p_qtp;
     long *t_qtp[6];
     /* Ordre donn� dans le corpus : commune, syndicat, EPCI, TSE autre, TSE, Gemapi */
     t_qtp[0] =  &p_loprs->qtprlc;
     t_qtp[1] =  &p_loprs->qtprls;
     t_qtp[2] =  &p_loprs->qtprlq;
     t_qtp[3] =  &p_loprs->qtprlg;
     t_qtp[4] =  &p_loprs->qtprln;
     t_qtp[5] =  &p_loprs->qtprle;

     p_qtp = &p_loprs->qtprlc;

     for (i=0 ; i < 6 ; i++)
     {
          if(abs(*p_qtp) < abs(*t_qtp[i]))
          {
              p_qtp = t_qtp[i];
          }
     }
     return p_qtp;
}

int traitementAbatLPH( s_lopre lopre[], s_loprs *p_loprs[] , s_vle *p_vle, s_qtpAb *p_AbCote, s_qtpAb *p_qtpAbLh, s_qtpAb *p_qtpAbL)
{
    int i = 0;
    int rgSup =0;
    s_loprs *p_loprsSup ;
    for(i=0;i < gbl_nbLp;i++) /* CA_TH_0059 */
    {
        if ((lopre[i].indlis != 'O') && ( lopre[i].indlis != 'N' )) /* CA_ANO_0346 */
        {
            return (2508);
        }
        strncpy(p_loprs[i]->indloc, lopre[i].indloc,18);
        /* PRORATISATION : TRAITEMENT DES LOCAUX PROFESSIONNEL */
        if (p_vle->vlbr != 0)
        {
            /* LIQUIDATION N   CA_TH_0061  */
            prorataAb(lopre[i].vlrnp, p_vle->vlbr, &(p_loprs[i]->cols_cr), &p_AbCote->qtp_cr);
            prorataAb(lopre[i].vlrnp, p_vle->vlbr, &(p_loprs[i]->cols_qr), &p_AbCote->qtp_qr);
            prorataAb(lopre[i].vlrnp, p_vle->vlbr, &(p_loprs[i]->cols_sr), &p_AbCote->qtp_sr);
            prorataAb(lopre[i].vlrnp, p_vle->vlbr, &(p_loprs[i]->cols_fr), &p_AbCote->qtp_fr);
            prorataAb(lopre[i].vlrnp, p_vle->vlbr, &(p_loprs[i]->cols_rr), &p_AbCote->qtp_rr);
        }
        if (p_vle->vlbrt != 0)
        {
            /* CA_TH_0062 TSE GEMAPI */
            prorataAb(lopre[i].vlrnpt, p_vle->vlbrt, &(p_loprs[i]->cols_nr), &p_AbCote->qtp_nr);
            prorataAb(lopre[i].vlrnpt, p_vle->vlbrt, &(p_loprs[i]->cols_gr), &p_AbCote->qtp_gr);
            prorataAb(lopre[i].vlrnpt, p_vle->vlbrt, &(p_loprs[i]->cols_er), &p_AbCote->qtp_er);
        }

        /* LIQUIDATION 2017 BASE VLRNP 2017 */

        /* 2019MAJR13-01-05 2019MAJR13-01-06 2019MAJR13-01-07  */

        if (gbl_Recalcul_Lissage == 1)
        {


                if (p_vle->vlbr7 != 0)
                {
                    /* CA_TH_0063 */
                    prorataAb(lopre[i].vlrnp7, p_vle->vlbr7, &(p_loprs[i]->cols_cr7), &p_AbCote->qtp_cr7);
                    prorataAb(lopre[i].vlrnp7, p_vle->vlbr7, &(p_loprs[i]->cols_qr7), &p_AbCote->qtp_qr7);
                    prorataAb(lopre[i].vlrnp7, p_vle->vlbr7, &(p_loprs[i]->cols_sr7), &p_AbCote->qtp_sr7);
                }
                if (p_vle->vlbrt7 != 0)
                {
                    /* CA_TH_0064 TSE GEMAPI */
                    prorataAb(lopre[i].vlrnpt7, p_vle->vlbrt7, &(p_loprs[i]->cols_nr7), &p_AbCote->qtp_nr7);
                    prorataAb(lopre[i].vlrnpt7, p_vle->vlbrt7, &(p_loprs[i]->cols_gr7), &p_AbCote->qtp_gr7);
                    prorataAb(lopre[i].vlrnpt7, p_vle->vlbrt7, &(p_loprs[i]->cols_er7), &p_AbCote->qtp_er7);
                }

                /* LIQUIDATION FICTIVE 2017 BASE VL70 2017 */
                if (p_vle->vlb77 != 0)
                {
                    /* CA_TH_0065 */
                    prorataAb(lopre[i].vlb70p7, p_vle->vlb77, &(p_loprs[i]->cols_c7), &p_AbCote->qtp_c7);
                    prorataAb(lopre[i].vlb70p7, p_vle->vlb77, &(p_loprs[i]->cols_q7), &p_AbCote->qtp_q7);
                    prorataAb(lopre[i].vlb70p7, p_vle->vlb77, &(p_loprs[i]->cols_s7), &p_AbCote->qtp_s7);
                    prorataAb(lopre[i].vlb70p7, p_vle->vlb77, &(p_loprs[i]->cols_n7), &p_AbCote->qtp_n7);
                    prorataAb(lopre[i].vlb70p7, p_vle->vlb77, &(p_loprs[i]->cols_g7), &p_AbCote->qtp_g7);
                    prorataAb(lopre[i].vlb70p7, p_vle->vlb77, &(p_loprs[i]->cols_e7), &p_AbCote->qtp_e7);
                }
        }

        /*FIN 2019MAJR13-01-05 2019MAJR13-01-06 2019MAJR13-01-07  */


    }

    /* PRORATISATION : TRAITEMENT DES LOCAUX D HABITATION */
    /* LIQUIDATION N */
    if (p_vle->vlbr != 0)
    {
        /* CA_TH_0071 */
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_cr),&p_AbCote->qtp_cr);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_qr),&p_AbCote->qtp_qr);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_sr),&p_AbCote->qtp_sr);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_nr),&p_AbCote->qtp_nr);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_gr),&p_AbCote->qtp_gr);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_er),&p_AbCote->qtp_er);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_fr),&p_AbCote->qtp_fr);
        prorataAb(p_vle->vlbpc,p_vle->vlbr,&(p_qtpAbLh->qtp_rr),&p_AbCote->qtp_rr);
    }

    /* LIQUIDATION FICTIVE 2017 BASE VLRNP 2017 */
    /* 2019MAJR13-01-08 2019MAJR13-01-09  */
    if (gbl_Recalcul_Lissage == 1)
    {
            if (p_vle->vlbr7 != 0)
            {
                /* CA_TH_0072 */
                prorataAb(p_vle->vlbp7,p_vle->vlbr7,&(p_qtpAbLh->qtp_cr7),&p_AbCote->qtp_cr7);
                prorataAb(p_vle->vlbp7,p_vle->vlbr7,&(p_qtpAbLh->qtp_qr7),&p_AbCote->qtp_qr7);
                prorataAb(p_vle->vlbp7,p_vle->vlbr7,&(p_qtpAbLh->qtp_sr7),&p_AbCote->qtp_sr7);
                prorataAb(p_vle->vlbp7,p_vle->vlbr7,&(p_qtpAbLh->qtp_nr7),&p_AbCote->qtp_nr7);
                prorataAb(p_vle->vlbp7,p_vle->vlbr7,&(p_qtpAbLh->qtp_gr7),&p_AbCote->qtp_gr7);
                prorataAb(p_vle->vlbp7,p_vle->vlbr7,&(p_qtpAbLh->qtp_er7),&p_AbCote->qtp_er7);
            }

            /* LIQUIDATION FICTIVE 2017 BASE 70 2017 */
            if (p_vle->vlb77 != 0)
            {
                /* CA_TH_0073 */
                prorataAb(p_vle->vlbp7,p_vle->vlb77,&(p_qtpAbLh->qtp_c7),&p_AbCote->qtp_c7);
                prorataAb(p_vle->vlbp7,p_vle->vlb77,&(p_qtpAbLh->qtp_q7),&p_AbCote->qtp_q7);
                prorataAb(p_vle->vlbp7,p_vle->vlb77,&(p_qtpAbLh->qtp_s7),&p_AbCote->qtp_s7);
                prorataAb(p_vle->vlbp7,p_vle->vlb77,&(p_qtpAbLh->qtp_n7),&p_AbCote->qtp_n7);
                prorataAb(p_vle->vlbp7,p_vle->vlb77,&(p_qtpAbLh->qtp_g7),&p_AbCote->qtp_g7);
                prorataAb(p_vle->vlbp7,p_vle->vlb77,&(p_qtpAbLh->qtp_e7),&p_AbCote->qtp_e7);
            }
    }
    /*FIN 2019MAJR13-01-08 2019MAJR13-01-09  */


    /* CA_TH_0077 CA_TH_0078 CA_TH_0079 CA_TH_0082 */
    /* Ces 6 regles sont declinees operation elementaire par operation elementaire */

    /* 2019MAJR13-01-10  2019MAJR13-01-11  */
    /* CUMUL DES ABATTEMENTS DES LOCAUX PRO */

    for(i=0;i < gbl_nbLp;i++)
    {
        /* CA_TH_0077 Etape 1 LIQUIDATION N */
        sommeAb( &p_qtpAbL->qtp_cr,&p_loprs[i]->cols_cr);
        sommeAb( &p_qtpAbL->qtp_qr,&p_loprs[i]->cols_qr);
        sommeAb( &p_qtpAbL->qtp_sr,&p_loprs[i]->cols_sr);
        sommeAb( &p_qtpAbL->qtp_nr,&p_loprs[i]->cols_nr);
        sommeAb( &p_qtpAbL->qtp_gr,&p_loprs[i]->cols_gr);
        sommeAb( &p_qtpAbL->qtp_er,&p_loprs[i]->cols_er);
        sommeAb( &p_qtpAbL->qtp_fr,&p_loprs[i]->cols_fr);
        sommeAb( &p_qtpAbL->qtp_rr,&p_loprs[i]->cols_rr);

    }



    if (gbl_Recalcul_Lissage==1)
    {
             for(i=0;i < gbl_nbLp;i++)
            {
                 /* CA_TH_0078 Etape 1 LIQUIDATION FICTIVE 2017 BASE VLRNP 2017 */
                sommeAb(&(p_qtpAbL->qtp_cr7),&p_loprs[i]->cols_cr7);
                sommeAb(&(p_qtpAbL->qtp_qr7),&p_loprs[i]->cols_qr7);
                sommeAb(&(p_qtpAbL->qtp_sr7),&p_loprs[i]->cols_sr7);
                sommeAb(&(p_qtpAbL->qtp_nr7),&p_loprs[i]->cols_nr7);
                sommeAb(&(p_qtpAbL->qtp_gr7),&p_loprs[i]->cols_gr7);
                sommeAb(&(p_qtpAbL->qtp_er7),&p_loprs[i]->cols_er7);

                /* CA_TH_0079 Etape 1 LIQUIDATION FICTIVE 2017 BASE VL70 2017 */
                sommeAb(&(p_qtpAbL->qtp_c7),&p_loprs[i]->cols_c7);
                sommeAb(&(p_qtpAbL->qtp_q7),&p_loprs[i]->cols_q7);
                sommeAb(&(p_qtpAbL->qtp_s7),&p_loprs[i]->cols_s7);
                sommeAb(&(p_qtpAbL->qtp_n7),&p_loprs[i]->cols_n7);
                sommeAb(&(p_qtpAbL->qtp_g7),&p_loprs[i]->cols_g7);
                sommeAb(&(p_qtpAbL->qtp_e7),&p_loprs[i]->cols_e7);
            }
    }



    /* CUMUL DES ABATTEMENTS DES LOCAUX HABITATION AUX CUMULS DES ABT PRO PRECEDEMMENT CALCULES */

    /* CA_TH_0077 Etape 1 LIQUIDATION N */
    sommeAb( &p_qtpAbL->qtp_cr,&p_qtpAbLh->qtp_cr);
    sommeAb( &p_qtpAbL->qtp_qr,&p_qtpAbLh->qtp_qr);
    sommeAb( &p_qtpAbL->qtp_sr,&p_qtpAbLh->qtp_sr);
    sommeAb( &p_qtpAbL->qtp_nr,&p_qtpAbLh->qtp_nr);
    sommeAb( &p_qtpAbL->qtp_gr,&p_qtpAbLh->qtp_gr);
    sommeAb( &p_qtpAbL->qtp_er,&p_qtpAbLh->qtp_er);
    sommeAb( &p_qtpAbL->qtp_fr,&p_qtpAbLh->qtp_fr);
    sommeAb( &p_qtpAbL->qtp_rr,&p_qtpAbLh->qtp_rr);





     if (gbl_Recalcul_Lissage==1)
    {
              /* CA_TH_0078 Etape 1 LIQUIDATION FICTIVE 2017 BASE VLRNP 2017 */
            sommeAb(&(p_qtpAbL->qtp_cr7),&p_qtpAbLh->qtp_cr7);
            sommeAb(&(p_qtpAbL->qtp_qr7),&p_qtpAbLh->qtp_qr7);
            sommeAb(&(p_qtpAbL->qtp_sr7),&p_qtpAbLh->qtp_sr7);
            sommeAb(&(p_qtpAbL->qtp_nr7),&p_qtpAbLh->qtp_nr7);
            sommeAb(&(p_qtpAbL->qtp_gr7),&p_qtpAbLh->qtp_gr7);
            sommeAb(&(p_qtpAbL->qtp_er7),&p_qtpAbLh->qtp_er7);

            /* CA_TH_0079 Etape 1 LIQUIDATION FICTIVE 2017 BASE VL70 2017 */
            sommeAb(&(p_qtpAbL->qtp_c7),&p_qtpAbLh->qtp_c7);
            sommeAb(&(p_qtpAbL->qtp_q7),&p_qtpAbLh->qtp_q7);
            sommeAb(&(p_qtpAbL->qtp_s7),&p_qtpAbLh->qtp_s7);
            sommeAb(&(p_qtpAbL->qtp_n7),&p_qtpAbLh->qtp_n7);
            sommeAb(&(p_qtpAbL->qtp_g7),&p_qtpAbLh->qtp_g7);
            sommeAb(&(p_qtpAbL->qtp_e7),&p_qtpAbLh->qtp_e7);
       }

    /* FIN 2019MAJR13-01-10  2019MAJR13-01-11  */



    /* D�termination de l'�cart issu des arrondis et report de l'�cart */
    /* sur les abattements correspondants du local de plus grande vlrnp*/
    rgSup = rangLpDeVlSup(lopre);
    p_loprsSup = p_loprs[rgSup];

    /* CA_TH_0077 Etape 2 et 3 LIQUIDATION N */
    reporEcartLP(&p_loprsSup->cols_cr, &p_qtpAbL->qtp_cr, &p_AbCote->qtp_cr);
    reporEcartLP(&p_loprsSup->cols_qr, &p_qtpAbL->qtp_qr, &p_AbCote->qtp_qr);
    reporEcartLP(&p_loprsSup->cols_sr, &p_qtpAbL->qtp_sr, &p_AbCote->qtp_sr);
    reporEcartLP(&p_loprsSup->cols_nr, &p_qtpAbL->qtp_nr, &p_AbCote->qtp_nr);
    reporEcartLP(&p_loprsSup->cols_gr, &p_qtpAbL->qtp_gr, &p_AbCote->qtp_gr);
    reporEcartLP(&p_loprsSup->cols_er, &p_qtpAbL->qtp_er, &p_AbCote->qtp_er);
    reporEcartLP(&p_loprsSup->cols_fr, &p_qtpAbL->qtp_fr, &p_AbCote->qtp_fr);
    reporEcartLP(&p_loprsSup->cols_rr, &p_qtpAbL->qtp_rr, &p_AbCote->qtp_rr);

    /* CA_TH_0078 Etape 2 et 3 LIQUIDATION FICTIVE 2017 BASE VLRNP 2017 */
    reporEcartLP(&p_loprsSup->cols_cr7, &p_qtpAbL->qtp_cr7, &p_AbCote->qtp_cr7);
    reporEcartLP(&p_loprsSup->cols_qr7, &p_qtpAbL->qtp_qr7, &p_AbCote->qtp_qr7);
    reporEcartLP(&p_loprsSup->cols_sr7, &p_qtpAbL->qtp_sr7, &p_AbCote->qtp_sr7);
    reporEcartLP(&p_loprsSup->cols_nr7, &p_qtpAbL->qtp_nr7, &p_AbCote->qtp_nr7);
    reporEcartLP(&p_loprsSup->cols_gr7, &p_qtpAbL->qtp_gr7, &p_AbCote->qtp_gr7);
    reporEcartLP(&p_loprsSup->cols_er7, &p_qtpAbL->qtp_er7, &p_AbCote->qtp_er7);

    /* CA_TH_0079 Etape 2 et 3 LIQUIDATION FICTIVE 2017 BASE VL70 2017 */
    reporEcartLP(&p_loprsSup->cols_c7, &p_qtpAbL->qtp_c7, &p_AbCote->qtp_c7);
    reporEcartLP(&p_loprsSup->cols_q7, &p_qtpAbL->qtp_q7, &p_AbCote->qtp_q7);
    reporEcartLP(&p_loprsSup->cols_s7, &p_qtpAbL->qtp_s7, &p_AbCote->qtp_s7);
    reporEcartLP(&p_loprsSup->cols_n7, &p_qtpAbL->qtp_n7, &p_AbCote->qtp_n7);
    reporEcartLP(&p_loprsSup->cols_g7, &p_qtpAbL->qtp_g7, &p_AbCote->qtp_g7);
    reporEcartLP(&p_loprsSup->cols_e7, &p_qtpAbL->qtp_e7, &p_AbCote->qtp_e7);



    p_loprs[rgSup] = p_loprsSup;

    return (0);
}


void prorataAb ( double vl, long vlTot, s_cols *cols_resultat, s_cols *p_colsDeLaCote)
{
    cols_resultat->abgen = arrondi_euro_voisin (p_colsDeLaCote->abgen * (vl/vlTot));
    cols_resultat->abpac = arrondi_euro_voisin (p_colsDeLaCote->abpac * (vl/vlTot));
    cols_resultat->abp12 = arrondi_euro_voisin (p_colsDeLaCote->abp12 * (vl/vlTot));
    cols_resultat->abspe = arrondi_euro_voisin (p_colsDeLaCote->abspe * (vl/vlTot));
    cols_resultat->abhan = arrondi_euro_voisin (p_colsDeLaCote->abhan * (vl/vlTot));
}

int rangLpDeVlSup (s_lopre lopre[])
{
    int i=0;
    long vlsup =0;
    int rg= 0;

    do
    {
        if (lopre[i].vlrnp > vlsup)
        {
            vlsup = lopre[i].vlrnp;
            rg = i;
        }
        else
        {
            if (lopre[i].vlrnp == vlsup)
            {
                if (strcmp(lopre[i].indloc,lopre[rg].indloc) > 0)
                {
                    vlsup = lopre[i].vlrnp;
                    rg = i;
                }
            }
        }
        i++;
    }
    while (i < gbl_nbLp) ;
    return rg;
}

void sommeAb (s_cols *p_qtp1, s_cols *p_qtp2)
{
    p_qtp1->abgen = p_qtp1->abgen + p_qtp2->abgen;
    p_qtp1->abpac = p_qtp1->abpac + p_qtp2->abpac;
    p_qtp1->abp12 = p_qtp1->abp12 + p_qtp2->abp12;
    p_qtp1->abspe = p_qtp1->abspe + p_qtp2->abspe;
    p_qtp1->abhan = p_qtp1->abhan + p_qtp2->abhan;
}

void reporEcartLP( s_cols *loprs, s_cols *p_qptL, s_cols *p_abDeLaCote)
{
    loprs->abgen = loprs->abgen + (p_abDeLaCote->abgen - p_qptL->abgen);
    loprs->abpac = loprs->abpac + (p_abDeLaCote->abpac - p_qptL->abpac);
    loprs->abp12 = loprs->abp12 + (p_abDeLaCote->abp12 - p_qptL->abp12);
    loprs->abspe = loprs->abspe + (p_abDeLaCote->abspe - p_qptL->abspe);
    loprs->abhan = loprs->abhan + (p_abDeLaCote->abhan - p_qptL->abhan);
}

void initialisation_loprs(s_loprs *p_loprs)  /* CA_TH_0060 */
{
    strncpy(p_loprs->indloc,"                  ",18) ;
    p_loprs->qtprlc=0; /* quote part du pas de lissage commune N */
    p_loprs->qtprls=0; /* quote part du pas de lissage syndicat N */
    p_loprs->qtprlq=0; /* quote part du pas de lissage interco N */
    p_loprs->qtprln=0; /* quote part du pas de lissage tse N */
    p_loprs->qtprlg=0; /* quote part du pas de lissage tse autre N */
    p_loprs->qtprle=0; /* quote part du pas de lissage gemapi N */
    p_loprs->cotilpc=0; /* cotisation liss�e commune N */
    p_loprs->cotilps=0; /* cotisation liss�e syndicat N */
    p_loprs->cotilpq=0; /* cotisation liss�e interco N */
    p_loprs->cotilpn=0; /* cotisation liss�e tse N */
    p_loprs->cotilpg=0; /* cotisation liss�e tse autre N */
    p_loprs->cotilpe=0; /* cotisation liss�e gemapi N */


    init_cols(&p_loprs->cols_cr);          /*structure sortie commune r�vis�e N                           */
    init_cols(&p_loprs->cols_qr);          /*structure sortie intercommunalite  r�vis�e N                 */
    init_cols(&p_loprs->cols_sr);          /*structure sortie syndicat     r�vis�e N                      */
    init_cols(&p_loprs->cols_nr);          /*structure sortie TSE          r�vis�e N                      */
    init_cols(&p_loprs->cols_gr);          /*structure sortie TSE Autre     r�vis�e N                     */
    init_cols(&p_loprs->cols_er);          /*structure sortie GEMAPI        r�vis�e N                     */
    init_cols(&p_loprs->cols_fr);          /*structure sortie commune avant ajustement   r�vis�e N        */
    init_cols(&p_loprs->cols_rr);          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/



    init_cols(&p_loprs->cols_c7);          /*structure sortie commune 2017 base 70               */
    init_cols(&p_loprs->cols_q7);          /*structure sortie intercommunalite 2017 base 70      */
    init_cols(&p_loprs->cols_s7);          /*structure sortie syndicat 2017 base 70              */
    init_cols(&p_loprs->cols_n7);          /*structure sortie TSE 2017 base 70                   */
    init_cols(&p_loprs->cols_g7);          /*structure sortie TSE autre 2017 base 70             */
    init_cols(&p_loprs->cols_e7);          /*structure sortie GEMAPI 2017 base 70                */

    init_cols(&p_loprs->cols_cr7);         /*structure sortie commune 2017 revisee               */
    init_cols(&p_loprs->cols_qr7);         /*structure sortie intercommunalite 2017 revisee      */
    init_cols(&p_loprs->cols_sr7);         /*structure sortie syndicat 2017 revisee              */
    init_cols(&p_loprs->cols_nr7);         /*structure sortie TSE 2017 revisee                   */
    init_cols(&p_loprs->cols_gr7);         /*structure sortie TSE autre 2017 revisee             */
    init_cols(&p_loprs->cols_er7);         /*structure sortie GEMAPI 2017 revisee                */

    p_loprs->mntpl     = 0    ;                /*  Montant du planchonnement appliqu�                 */
    p_loprs->sgnpl     = ' '  ;                /*  Signe du planchonnement appliqu�                   */
    p_loprs->mntplt    = 0    ;                /*  Montant du planchonnement appliqu� TSE             */
    p_loprs->sgnplt    = ' '  ;                /*  Signe du planchonnement appliqu� TSE               */
    p_loprs->vlrnp     = 0    ;                /*  VL  r�vis�e neutralis�e et planchonn�e appliqu�e   */
    p_loprs->vlrnpt    = 0    ;                /*  VL r�vis�e neutralis�e planchonn�e appliqu�e TSE�  */
    /*CA_TH_0237*/
    p_loprs->vlrnp7     = 0    ;               /* VL r�vis�e neutralis�e et planchonn�e 2017 recalcul�e      */
    p_loprs->vlrnpt7   = 0    ; }              /* VL r�vis�e neutralis�e et planchonn�e TSE 2017 recalcul�e  */


void initialisation_vls(s_vls *p_vls)
{
    p_vls->vli    = 0;
    p_vls->vlit   = 0;
    p_vls->vlni   = 0;
    p_vls->vlnit  = 0;
    p_vls->vli7   = 0;
    p_vls->vlit7  = 0;
    p_vls->vlni7  = 0;
    p_vls->vlnit7 = 0;
    p_vls->vli77  = 0;
    p_vls->vlni77 = 0;
}
