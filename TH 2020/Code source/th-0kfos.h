/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

/* ======================================================
=     FICHIER DE DEFINITION DES EN-TETE DE FONCTION     =
========================================================*/
#if defined(ILIAD)
    #include <th-0ksts.h>

#else
    #include "th-0ksts"

#endif


#if !defined(KFOS)
#define KFOS  'A'

/* **************************************************
*     NE RIEN INSERER AVANT CET EN-TETE             *
***************************************************** */

#if !defined(max)
#define max(A, B)  ((A) > (B) ? (A) : (B))
#define min(A, B)  ((A) > (B) ? (B) : (A))
#endif


/* fonctions d'arrondi et math�matiques */

double arrondi_nieme_decimale_voisine(double a_arrondir, short nbdec);

long arrondi_euro_inf(double a_arrondir);

long arrondi_euro_voisin(double a_arrondir);

long arrondi_dizaine_inf(double a_arrondir);

long maximum(long valeur1,long valeur2);



/* fonctions  propres � la calculette */


/* fonctions partag�es */

int controle_signature(int  rang_module , char ident_module, s_liberreur * * p_liberreur);

int cherche_const(short annee,s_cons * * p_cons);

double recherche_tdep(char[], char[], char);

int cherche_Erreur(int erreur,s_liberreur * * p_liberreur);

int recherche_csdi(char[]);

int NaP_Ni_Deg_Ni_Exo( s_dne *p_dne,  s_dns *p_dns);      /*net a payer sans degrevement ni exoneration */

long frais_ar(long totcot, double taux);

void frais(long totcott, long totcotp, double taux_FDR, double taux_AR, long *fraisDR, long *fraisAR, long *fraisDRP);

void determination_parts(short,short*,short*,short*,short*,short*,short*,short*,short*,short*,short*);

short determination_quart_parts_w(short);

char determination_droits(double,char,short,short,short,short,short, short, short, short, short, short, s_typseuil *pc);

char determination_droits_w(double,short, s_typseuil *pc);

void ajout_espace_taux_abattement(s_cole * p_cole);

int recherche_dep_TLV(char[]);


/* fonctions propres au module KACC */


void base_nette(s_colb *p1,s_ace *p2, s_cols *p3, short nb12, short nbsup, short nbera12, short nberasup, long *p_mtvl);

void abat_gen( long , long , long * , long * );

void cal_preal( long, long *, long *, long *, long *);

void detf_abat_pac(long, long, long, long *, long *);

void detg_abat_pac(long, long, long, long, long, short, long *, long *);

void abat_pac(short , short , short, short, long *, long *, long *, long *, long *);

void abat_spe(long, long, short, short, long, long *, long *, char *);

void abat_spespsa(long, short, short, long, long, char *);

void abat_han(long *, char, long *, long *);


/* fonctions propres au module KCAC */

void majBaseDansS2( s_cols source, s_cols *p_cible);

int controles(s_e2 * p_e2, s_e3 * p_e3, s_s2 * p_s2);

void copieCols (s_cols source , s_cols *p_dest);

int isLocPro (char indloc[]);

void regularisation(long,long,long*,char*);



/* fonctions propres au module KCOC */

long totAb(s_cols *p_cols);

void majBaseImpDansColsCQDFR(s_cols *p_cols, long base, long vlnex);

void majBaseImpDansCols(s_cols *p_cols, long base, long vlnex, double taux);

long cotisation (long base, double taux);

void base_nettethlv( s_coe1 *p2, s_cols *p3);

long vlNette(long vl, long totAbat);



/* fonctions propres au module KDNC */



int est_exoneration(char degex);

int NaP_Deg_Tot( s_dne *p_dne, s_dns *p_dns);               /*net a payer avec degrevement total */

int NaP_Exo( s_dne *p_dne, s_dns *p_dns);




/* fonctions propres au module KLPC */


void prorataAb ( double vl, long vlTot, s_cols *cols_resultat, s_cols *p_colsDeLaCote);

void reporEcartLP( s_cols *loprs, s_cols *p_qptL, s_cols *p_abDeLaCote);

void sommeAb (s_cols *p_qtp1, s_cols *p_qtp2);

int rangLpDeVlSup (s_lopre lopre[]);

int traitementAbatLPH( s_lopre lopre[], s_loprs *p_loprs[] , s_vle *p_vle, s_qtpAb *p_AbCote, s_qtpAb *p_qtpAbLh, s_qtpAb *p_qtpAbL);

void initialisation_vls(s_vls *p_vls);

void reportDeltaSurQtpSup (long delta, long *p_qtp);

long calculPas (double cotR7, long cot77);

long cotisationTotale ( s_coe1 *p_coe);

long * qtpSup(s_loprs *p_loprs);

void majQtpPdL (long *p_qtprl, long pasLis, s_cols colsR7, s_cols cols77, double cotR7, long cot77);

long cotisation2003Lissee (long coti2003, long quotePart);



/* fonctions propres au module KSEC */

int controle_ksec( s_ese * p_e, s_sse * p_s);



/* fonctions propres au module KSFC */

int controle_ksfc(s_e4 * p_e4 , s_s4 * p_s4);

char restitution_seuil(s_typseuil * pc, short nbr_part, long revenu, s_rev_m * p_revm);

s_seuilM * determination_seuilM (long parts, s_rev_m *p_revm);




/* fonctions propres au module KSRC */


void prelevement ( long bnimp,char tax,char cnat,s_cons *pc,

long *p_prelt, long *p_prelp, long *p_pre02, long *p_pre12,long *p_pre17);

void prelevementTHLV ( long bnimp,char tax,char cnat,s_cons *pc,
                  long *p_prelt, long *p_prelp, long *p_pre02, long *p_pre12,
                  long *p_pre17);

void prelevement_add_ths(long totcott ,s_cons *pc, long *p_preths);


/* fonctions propres au module KTAC */

int controle_ktac(s_cons *p_cons,s_e8 * p_e8,s_s8 * p_s8);

int A_Son_Propre_Regime(s_cole * p_cole);

int A_Son_Propre_Regime_Taux(s_cole * p_cole);

int Abatt1_Inferieurs_Abatt2( s_cole * p_cole1, s_cole  * p_cole2);

int Controle_Abattements(s_cons *p_cons, s_cole * p_cole, char dep[], char csdi[]);

int Conversion_Taux_Abt (char  p_Taux_Abt_Chaine[]);

int dep_TSE_autre(char * dep);


typedef struct
{
    char taux[03];
    long quotite ;
} s_abatt_unit_cole;

int Abatt1_Unit_Inf_Abatt2_Unit(s_abatt_unit_cole * p_abatt_unit1,s_abatt_unit_cole * p_abatt_unit2);

void Abt_colb_Init(s_colb * p_Abt_Init);

void Abt_colb_Charge(long * p_abmos, char * p_codef, char * p_codeg, s_cole * p_Abt_cole_Source, s_colb * p_Abt_colb);

void Abt_colb_Recopie (s_cole * p_Abt_cole_Cible, s_colb * p_Abt_colb_Source, s_colb * p_Abt_colb_Cible);


/* fonctions propres au module KVLC */

double recherche_tdep_2017(char [] , char [], char );

long calcul_vl(long, double, double); /*calcul vl actualisee*/





/* fonction principale de chaque module */

int th_0kacc (s_ace * p_ace, s_acs * p_acs);

int th_0krpc ( s_ep *p_ep, s_sp *p_sp);

int th_0klpc (s_lpe1 *p_lpe1, s_lpe2 *p_lpe2, s_lps *p_lps);

int th_0ksrc(s_sre * p_sre , s_srs  * p_srs);

int th_0kcoc (s_coe1 * p_coe1, s_coe2 * p_coe2, s_cos * p_cos);

int th_0kdnc ( s_dne * p_dne, s_dns * p_dns);

int th_0kvlc(s_e1 * p_e1, s_e9 * p_e9, s_s1 * p_s1);

int th_0ksec(s_ese * p_e, s_sse * p_s);

int th_0ktac(s_e8 * p_e8, s_s8 * p_s8);

int th_0krac(s_rae * p_rae, s_ras * p_ras);



/* fonctions d'initialisation */

void init_cols(s_cols *p);

void initialisation_sortie(s_s2 * p_s2);

void initialisation_sortieQtp( s_qtpAb *p_qtp);

void initialisation_loprs(s_loprs *p_loprs);

void initialisation_coe2(s_coe2 *p_coe2);

void init_sortie_kdnc(s_dns * p_dns);

void init_s7(s_s7 *p_s);

void init_s4(s_reds *);

void init_colb_s8(s_s8 *p_s);



/*===================== Ne Rien ecrire sous cette ligne ================*/
#endif



