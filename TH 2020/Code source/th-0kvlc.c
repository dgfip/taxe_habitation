/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/

/*============================================================================*/
/*                  MODULE D ACTUALISATION DES VL (th-0kVLC)                  */
/*============================================================================*/


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif


#define MAXDEPVLC 97 /* nombre de departements*/
#define MAXDEP2 8 /* nombre de communes du departement 21 */


    /*    MAIN    */
    /*   ------   */
int th_0kvlc( s_e1 * p_e1, s_e9 * p_e9,  s_s1 * p_s1)
{
    static s_signature signature;
    int ret;
    int retm;
    int retdep=0;
    int retde=0;
    int retcne=0;
    int retan=0;
    int retcons=0;
    int rets;   /*zone retour signature  */
    /* MAJR18-02-02 Coefficients de revalorisation 2017 */
    double  reval_metro_2017    = 3.092  ; /* taux de revalorisation metropole   */
    double  reval_dom_2017      = 2.811  ; /* taux de revalorisation DOM         */
    double  reval_mayotte_2017  = 1.023  ; /* taux de revalorisation Mayotte     */

    s_cons * p_const1; /* pointeur sur struct du taux annuel */
    double taux;    /*   retour du taux departemental */
    double taux_an; /*   retour du taux annuel */
    double val_1; /*valeur locative exoneree zrr */
    static char dep2[]="97";
    char * p_dep2;
    char * p_dep1;
    int ret_cons;
    p_dep2=dep2;   /* initial. pointeur sur char */
    p_dep1=p_e1->dep; /* initial pointeur sur char */
    p_s1->vlblt = 0;
    p_s1->vlxm1 = 0;
    p_s1->vlxm2 = 0;
    p_s1->vlbll = 0;
    p_s1->vlblc = 0;
    /* Fin initialisation */

    /*    identification du module    */
    /*    ------------------------    */
    p_s1->anref=ANREF;   /*   annee de reference  */
    p_s1->versr='A';    /*   version du module   */
    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_s1->signature = &signature  ;

    /*    traitement    */
    /*    ----------    */

    /* controle de la signature */
    rets=controle_signature(RKVLC,p_s1->versr,&(p_s1->libelle));

    if (rets!=0)
    {
        return(rets);
    }

    /* Controle des variables en entree */
    if (p_e1->antax != p_e9->antax)
    {
        retan = 4402;
        cherche_Erreur(retan,&(p_s1->libelle));
        return(4402);
    }

    if (strcmp(p_e1->dep,p_e9->dep)!= 0)
    {
        retde = 4404;
        cherche_Erreur(retde,&(p_s1->libelle));
        return (4404);
    }

    if (strcmp(p_e1->cne,p_e9->cne)!= 0)
    {
        retcne = 4403;
        cherche_Erreur(retcne,&(p_s1->libelle));
        return (4403);
    }

    /*   Fin controle   */
    if (p_e1->antax == 2017)
    {
        taux=recherche_tdep_2017 (p_e1->dep, p_e1->cne,p_e1->aff); /*fonction taux dep. pour 2017 dans KVLC */
    }
    else
    {
        taux=recherche_tdep (p_e1->dep, p_e1->cne,p_e1->aff); /* fonction taux dep. dans KARC pour annee campagne*/
    }

    if (taux==9)        /*  retour de la fonction  */
    {
        retdep = 2;
        cherche_Erreur(retdep,&(p_s1->libelle));
        return (2);       /*  departement absent  */
    }
    else
    {
        if (taux==4401)
        {
            retdep = 4401;
            cherche_Erreur(retdep,&(p_s1->libelle));
            return (4401);
        }
        else
        {
            if (p_e1->antax == 2017)
            {
                ret=strcmp(p_dep1,p_dep2); /* test si DOM */
                if (ret==0)
                {
                    retm=(strcmp(p_e1->csdi,"976")); /* test Direction = Mayotte */
                    if (retm == 0)
                    {
                        taux_an=reval_mayotte_2017; /* taux revalorisation Mayotte */
                    }
                    else
                    {
                        taux_an=reval_dom_2017; /* taux revalorisation DOM */
                    }

                }
                else
                {
                    taux_an=reval_metro_2017; /* taux revalorisation METRO */
                }
            }
            else
            {
                ret_cons=cherche_const(p_e1->antax,&p_const1); /* appel de la fonction taux annuel */
                if (ret_cons==1) /* retour de la fonction */
                {
                    retcons = 1;
                    cherche_Erreur(retcons,&(p_s1->libelle));
                    return(1);  /*  annee  non valide   */
                }
                if (p_e1->tax == 'P' || p_e1->tax == 'E')
                {
                    ret=strcmp(p_dep1,p_dep2); /* test si DOM */
                    if (ret==0)
                    {
                        retm=(strcmp(p_e1->csdi,"976")); /* test Direction = Mayotte */
                        if (retm == 0)
                        {
                            taux_an=p_const1->reval_mayotte_2; /* taux revalorisation Mayotte */
                        }
                        else
                        {
                            taux_an=p_const1->reval_dom_2; /* taux revalorisation DOM */
                        }

                    }
                    else
                    {
                        taux_an=p_const1->reval_metro_2; /* taux revalorisation METRO */
                    }
                }
                else
                {
                    ret=strcmp(p_dep1,p_dep2); /* test si DOM */
                    if (ret==0)
                    {
                        retm=(strcmp(p_e1->csdi,"976")); /* test Direction = Mayotte */
                        if (retm == 0)
                        {
                            taux_an=p_const1->reval_mayotte; /* taux revalorisation Mayotte */
                        }
                        else
                        {
                            taux_an=p_const1->reval_dom; /* taux revalorisation DOM */
                        }

                    }
                    else
                    {
                        taux_an=p_const1->reval_metro; /* taux revalorisation METRO */
                    }
                }
            }

            /* appel fonction calcul vl du local*/
            p_s1->vlbll = calcul_vl( p_e1->vlb7l, taux, taux_an);

            /* appel fonction calcul vl des loc. communs */
            p_s1->vlblc = calcul_vl( p_e1->vlb7c, taux, taux_an);

            /* cumul des vl actualisees */
            p_s1->vlblt = p_s1->vlbll+p_s1->vlblc;

            /* calcul des valeurs locatives exonerees ZRR */
            if ((!strcmp(p_e9->cxcn1,"M1")) && (!strcmp(p_e1->cxloz,"M1")))
            {
                val_1 = (double) p_s1->vlblt * p_e1->txzrr / 100;
                p_s1->vlxm1 = arrondi_euro_voisin(val_1);
            }
            if ((!strcmp(p_e9->cxcn2, "M2")) && (!strcmp(p_e1->cxloz, "M2")))
            {
                val_1 = (double) p_s1->vlblt * p_e1->txzrr / 100;
                p_s1->vlxm2 = arrondi_euro_voisin(val_1);
            }
            p_s1->vlblt = p_s1->vlblt - p_s1->vlxm1 - p_s1->vlxm2;
            return (0);  /* calcul effectue correctement */
        }
    }
}
    /*   fin de traitement   */
    /*   -----------------   */

    /*     CALCUL DES VL   */
    /*     -------------   */

long calcul_vl( long vl,  double taux_d, double taux_a)
{
    double fvl;
    long vl_cal;
    fvl = (double)vl * taux_d; /* vl multipliee par taux departemental */
    vl_cal =arrondi_euro_voisin(fvl); /* arrondi */

    fvl = (double)vl_cal * taux_a; /* vl multipliee par taux annuel */
    vl_cal=arrondi_euro_voisin(fvl); /* arrondi */
    return(vl_cal); /* retour de la vl calculee */
}


/*          RECHERCHE DU TAUX DEPARTEMENTAL  AFFECTATION AUTRE QUE A          */
/*         ----------------------------------------------------------         */
/* MAJR18-02-03 Table des coefficients d actualisation departementaux 2017    */
double recherche_tdep_2017(char di1[], char cne1[], char aff)
{
    int i; /* indice utilise dans les for */
    char * p_tab;
    char * p_dir;
    char * p_di1;
    char * p_cne1;
    char * p_cne2;
    char * p_cne3;
    double res_coef;
    static char di2[]="21";  /*  initialisation char caractere */
    static char di3[]="54";
    char * p_di2;
    char * p_di3;
    static char cne3[]="602";

    /*   structure des taux par departement   */
    static struct coef
    {
        char direction[3];
        double coefficient;
    }c1[MAXDEPVLC]={
        {"01", 1.60},  {"02", 1.75},  {"03", 1.60},  {"04", 1.65},
        {"05", 1.67},  {"06", 1.55},  {"07", 1.59},  {"08", 1.72},
        {"09", 1.60},  {"10", 1.60},  {"11", 1.55},  {"12", 1.73},
        {"13", 1.60},  {"14", 1.66},  {"15", 1.60},  {"16", 1.68},
        {"17", 1.69},  {"18", 1.57},  {"19", 1.67},  {"2A", 1.58},
        {"2B", 1.58},  {"21", 1.72},  {"22", 1.48},  {"23", 1.63},
        {"24", 1.62},  {"25", 1.70},  {"26", 1.70},  {"27", 1.75},
        {"28", 1.57},  {"29", 1.70},  {"30", 1.50},  {"31", 1.49},
        {"32", 1.41},  {"33", 1.65},  {"34", 1.49},  {"35", 1.57},
        {"36", 1.60},  {"37", 1.57},  {"38", 1.63},  {"39", 1.65},
        {"40", 1.70},  {"41", 1.59},  {"42", 1.68},  {"43", 1.65},
        {"44", 1.65},  {"45", 1.64},  {"46", 1.73},  {"47", 1.57},
        {"48", 1.50},  {"49", 1.63},  {"50", 1.70},  {"51", 1.62},
        {"52", 1.59},  {"53", 1.59},  {"54", 1.63},  {"55", 1.70},
        {"56", 1.57},  {"57", 1.76},  {"58", 1.74},  {"59", 1.69},
        {"60", 1.78},  {"61", 1.70},  {"62", 1.77},  {"63", 1.54},
        {"64", 1.70},  {"65", 1.69},  {"66", 1.62},  {"67", 1.67},
        {"68", 1.69},  {"69", 1.63},  {"70", 1.66},  {"71", 1.67},
        {"72", 1.63},  {"73", 1.63},  {"74", 1.60},  {"75", 1.85},
        {"76", 1.73},  {"77", 1.67},  {"78", 1.73},  {"79", 1.59},
        {"80", 1.75},  {"81", 1.54},  {"82", 1.52},  {"83", 1.62},
        {"84", 1.60},  {"85", 1.52},  {"86", 1.55},  {"87", 1.67},
        {"88", 1.67},  {"89", 1.65},  {"90", 1.74},  {"91", 1.71},
        {"92", 1.77},  {"93", 1.68},  {"94", 1.70},  {"95", 1.70},
        {"97", 1.00}

    }, *p_st,
      c1_a[MAXDEPVLC]={
        {"01", 1.99},  {"02", 2.00},  {"03", 1.90},  {"04", 1.74},
        {"05", 2.01},  {"06", 2.03},  {"07", 1.85},  {"08", 2.25},
        {"09", 1.87},  {"10", 1.93},  {"11", 1.73},  {"12", 1.80},
        {"13", 2.00},  {"14", 2.19},  {"15", 1.90},  {"16", 1.83},
        {"17", 1.87},  {"18", 1.99},  {"19", 2.07},  {"2A", 1.83},
        {"2B", 1.77},  {"21", 2.06},  {"22", 1.93},  {"23", 1.81},
        {"24", 1.93},  {"25", 2.28},  {"26", 2.10},  {"27", 2.20},
        {"28", 2.09},  {"29", 1.95},  {"30", 1.90},  {"31", 1.88},
        {"32", 1.75},  {"33", 2.04},  {"34", 1.88},  {"35", 1.87},
        {"36", 1.80},  {"37", 1.92},  {"38", 2.00},  {"39", 1.96},
        {"40", 1.87},  {"41", 1.94},  {"42", 2.10},  {"43", 1.79},
        {"44", 2.06},  {"45", 2.09},  {"46", 1.94},  {"47", 1.84},
        {"48", 1.80},  {"49", 2.13},  {"50", 1.85},  {"51", 2.15},
        {"52", 2.40},  {"53", 1.80},  {"54", 1.96},  {"55", 1.94},
        {"56", 2.03},  {"57", 1.91},  {"58", 1.98},  {"59", 2.02},
        {"60", 2.00},  {"61", 2.18},  {"62", 2.05},  {"63", 1.91},
        {"64", 1.85},  {"65", 2.00},  {"66", 1.90},  {"67", 2.10},
        {"68", 2.08},  {"69", 2.04},  {"70", 2.20},  {"71", 2.15},
        {"72", 2.13},  {"73", 2.04},  {"74", 1.96},  {"75", 2.23},
        {"76", 2.20},  {"77", 2.23},  {"78", 1.93},  {"79", 1.79},
        {"80", 1.90},  {"81", 1.88},  {"82", 1.83},  {"83", 2.06},
        {"84", 2.04},  {"85", 1.91},  {"86", 1.78},  {"87", 2.03},
        {"88", 1.97},  {"89", 2.08},  {"90", 2.10},  {"91", 1.98},
        {"92", 2.13},  {"93", 2.08},  {"94", 2.05},  {"95", 2.25},
        {"97", 1.00}

    }, *p_sta,*p_rech ;   /*   pointeur sur structure  */

    static struct cote_dor {     /*  structure : list des cnes du dep 21 */
    char commune[4];
    }c2[MAXDEP2]={
        {"166"},{"171"},{"231"},{"278"},
        {"355"},{"515"},{"540"},{"617"}
    }, *p_cotedor;

    int retour;
    p_st=c1;    /*  initialisation du pointeur sur structure  */
    p_sta=c1_a;
    p_di1=di1;   /*  initialisation du pointeur sur char  */
    p_di2=di2;
    p_di3=di3;
    p_cne1=cne1;
    p_cne3=cne3;
    res_coef=0;
    p_cotedor=c2;

    /* verification du code affectation */
    if (strchr("HMFSA ",aff)==NULL)
    {
        return(4401);
    }

    /* boucle de recherche de la direction */
    /* ----------------------------------- */
    if (aff == 'A')
    {
        p_rech=p_sta; /* Recherche sur le deuxi�me taux */
    }
    else
    {
        p_rech=p_st; /* Recherche sur le premier taux */
    }

    for(i=0;i<MAXDEPVLC;i++)
    {
        p_dir=di1;  /* init. pointeur  sur tab de char */
        p_tab=p_rech->direction; /* init. ptr pour dir de struct */

        retour=strcmp(p_dir, p_tab); /* passage des adresses */
        if (retour==0)
        {
            res_coef=p_rech->coefficient; /* mise en reserve du coef*/
        }
        p_rech = p_rech+1;  /* incrementation du pointeur sur struct */
    }

    /*   departement non trouve  */
    /*   ----------------------  */
    if (res_coef==0)
    {
        return(9);  /*  retour si dep non trouve  */
    }
    else
    {

        /*  departement trouve */
        /*  ------------------ */
        retour=strcmp(p_di1 , p_di3);   /*comparaison avec dir 540*/
        if (retour==0)                  /* departement 540        */
        {
            retour=strcmp(p_cne1 , p_cne3); /*comparaison avec cne 602*/
            if (retour==0)                /* commune 602            */
            {
                if (aff == 'A')
                {
                    res_coef=1.94;
                }
                else
                {
                    res_coef=1.70;
                }
            }
            return(res_coef);
        }
        else
        {
            retour=strcmp(p_di1 , p_di2); /* comparaison chaine de caracteres */

            /*   trait-ktac autre que dep 21  */
            /*   ---------------------------  */
            if  (retour!=0) /* test de non egalite */
            {
                return(res_coef); /* retour coef si dep # 21 */
            }

            /*   traitement dep 21 */
            /*   ----------------- */
            else
            {
                for(i=0;i<MAXDEP2;i++) /* recherche communes de la cote d or */
                {
                    p_cne1=cne1;  /* init. pointeur  sur tab de char */
                    p_cne2=p_cotedor->commune; /* init. ptr pour cne
                                                    de struct */
                    retour=strcmp(p_cne1, p_cne2); /* passage des adresses */
                    if (retour==0)
                    {
                        if (aff == 'A')
                        {
                            res_coef=2.06;
                        }
                        else
                        {
                            res_coef=1.57; /* retour du coefficient
                                             si commune trouvee */
                        }
                    }
                    p_cotedor = p_cotedor+1;  /* incrementation du
                                                pointeur sur struct */
                }
                return res_coef;
            }
        }
    }
}
