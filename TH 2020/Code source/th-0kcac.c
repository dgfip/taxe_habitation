/*
#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2020]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des
#Finances Publiques pour permettre le calcul de la taxe d'habitation 2018.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
*/
  
            /*==================================
              MODULE GENERAL DE CALCUL DE LA TH
             =================================== */


#if defined (ILIAD)
    #include <th-0ksts.h>
    #include <th-0kfos.h>
#else
    #include "th-0ksts"
    #include "th-0kfos"
#endif


/* La fonction th_0kcac calcule pour une cote les differentes composantes
de la taxation :
    - les bases nettes d'imposition
    - les cotisations par collectivites et les divers frais et prelevements
    - les allegements net a payer et code role resultants

Pour ILIAD, le fichier de definition des structures th-nksts.h devra etre
inclus avant l'appel de la fonction.
Ce fichier reserve
    - une donnee e2 de structure s_e2 pour les entrees de la cote
    - une donnee s2 de structure s_s2 pour les sorties de la cote
    - une donnee e3 de structure s_e3 pour les entrees des donnees
          collectivite (taux, abattements, vl moyenne).



    Avant l'appel de la fonction, tous les champs de la donnee e3
devront avoir ete servis. La fonction ne modifie pas les donnees entree.
Aussi est-il possible de ne modifier le contenu des donnees de e3 que
lorsque l'on change de collectivite. Les champs non utilises doivent etre
reinitialises ( a 0, blanc ou vide)avant l'appel.

    Avant l'appel de la fonction il faudra avoir servi de maniere exhaustive
les differents champs de la variable d'entree e2. La fonction ne modifie pas
les zones d'entree. Tous les champs, y compris ceux non utilises
devront recevoir une valeur adequate.
L'affectation se fait par exemple par : e2.tax='P'

    Apres l'appel de la fonction, si le code retour est egal a 0, les
donnees sortie se recuperent par une simple affectation :
code_role=s2.codro
Si le code retour est different de 0 le contenu des donnees sortie est
imprevisible, a l'exception du libelle d'erreur qui est normalement renseigne.

*/
int gbl_nbLp;
int gbl_Recalcul_Lissage; /* Indicateur de recalcul du lissage des locaux pros de la cote. A 1 si un des LP au moins est elligible au recalcul du planchonnement*/


/*============================================================================
   Main
  ============================================================================*/
int th_0kcac(s_e3 * p_e3, s_e2 * p_e2, s_ea * p_ea, s_sa * p_sa, s_s2 * p_s2)
{
    static s_signature signature;
    int ret_cons = 0;  /* zone retour de recherche des constantes */
    int ret = 0;  /* zone retour strcmp */
    static s_cons * p_const1=NULL;


    static s_lpe1 lpe1, * p_lpe1 = &lpe1;
    static s_lpe2 lpe2, * p_lpe2 = &lpe2;
    static s_lps lps, * p_lps = &lps;
    static s_ace ace, * p_ace=&ace;
    static s_acs acs, * p_acs=&acs;
    static s_sre sre, * p_sre=&sre;
    static s_srs srs, * p_srs=&srs;
    static s_dne dne;
    static s_dns dns;

    static s_e8 e8, * p_e8=&e8;
    static s_s8 s8, * p_s8=&s8;
    static s_rae rae, * p_rae=&rae;
    static s_ras ras, * p_ras=&ras;
    static s_ep ep, *p_ep= &ep;
    static s_sp sp, *p_sp = &sp;

    s_qtpAb qtpAbLh;
    s_qtpAb qtpAbL;
    s_vle vle;
    s_qtpAb AbCote;
    s_coe1 coe1;
    s_coe2 coe2;
    s_coe2 coe2_7;
    s_cos cos;
    s_cols cols_null;

    long napdt=0;
    char signe =' ';
    int i= 0;




    int num_lopre = 0;
    long sVlnc = 0;
    long sVlnq = 0;
    long sVlns = 0;
    long sVlnn = 0;
    long sVlng = 0;
    long sVlne = 0;
    long sVlnf = 0;
    long sVlnr = 0;

    long sVlxc = 0;
    long sVlxq = 0;
    long sVlxs = 0;
    long sVlxn = 0;
    long sVlxg = 0;
    long sVlxe = 0;
    long sVlxf = 0;
    long sVlxr = 0;

    long sBNc = 0;
    long sBNq = 0;
    long sBNs = 0;
    long sBNn = 0;
    long sBNg = 0;
    long sBNe = 0;
    long sBNf = 0;
    long sBNr = 0;





    long totCotLPc          = 0;  /* Total cotisations Commune des LP */
    long totCotLPq          = 0;
    long totCotLPs          = 0;
    long totCotLPn          = 0;
    long totCotLPg          = 0;
    long totCotLPe          = 0;
    long totCotLPlisc       = 0;  /* Total cotisations lissees Commune des LP */
    long totCotLPlisq       = 0;
    long totCotLPliss       = 0;
    long totCotLPlisn       = 0;
    long totCotLPlisg       = 0;
    long totCotLPlise       = 0;




    int ind_liq2017BASE70_2017 = 0;
    int ind_liq2017VLRNP2017 = 0  ;

    /* initialisation variables globales */
    gbl_nbLp = 0;
    gbl_Recalcul_Lissage = 0;


    /* INITIALISATIONS*/

    initialisation_sortie(p_s2);

    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_s2->signature=&signature;


    init_cols(&cols_null);

    /* IDENTIFICATION DU MODULE */
    /* ------------------------ */
    p_s2->anref = ANREF;    /* emission ann�e de r�f�rence */
    p_s2->versr = 'C';     /* lettre de version  */

    /* controle de la signature */
    /* ------------------------ */


    ret = controle_signature(RKCAC,p_s2->versr, &(p_s2->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* ajout des espaces pour les abattements forfaitaires */
    ajout_espace_taux_abattement(&(p_e3->cole_c)); /* structure commune                                        */
    ajout_espace_taux_abattement(&(p_e3->cole_q)); /* structure intercommunalit�                               */
    ajout_espace_taux_abattement(&(p_e3->cole_f)); /* structure commune avant ajustement                       */
    ajout_espace_taux_abattement(&(p_e3->cole_r)); /* structure intercommunalite avant ajustement              */


    ajout_espace_taux_abattement(&(p_e3->cole_h)); /* structure entree commune 2017                            */
    ajout_espace_taux_abattement(&(p_e3->cole_k)); /* structure commune 2017 avant ajustement                  */
    ajout_espace_taux_abattement(&(p_e3->cole_l)); /* structure intercommunalite 2017                          */




    /* Controle code CSDI/DSF */
    if(!recherche_csdi(p_e2->csdi))
    {
        cherche_Erreur(5,&(p_s2->libelle));
        return 5;
    }

    /* Controle de l'indicateur de synthese THLV */
    if ( p_e2->tyimp=='T' )
    {
        if ( !(p_e3->indthlv =='C' || p_e3->indthlv == 'I' ) )
        {
            cherche_Erreur(62,&(p_s2->libelle));
            return(62);
        }
    }

    /* T  R  A  I  T  E  M  E  N  T */
    /* ============================ */
    /*        ==============        */



    ret_cons=(short)cherche_const(p_e2->antax,&p_const1); /* appel de la fonc.taux ann*/
    cherche_Erreur(ret_cons,&(p_s2->libelle)) ;
    if (ret_cons != 0)  /* retour de la fonction*/
    {
        return (ret_cons);
    }


    /* Pour RG25-02 Controle locaux professionnels hors champ THLV */


    while ((num_lopre < DIM_LOCAUX_PRO) && !(strncmp ( p_ea->lopre[num_lopre].indloc, "                  ",18)== 0 ))
    {

        num_lopre = num_lopre +1;

    }
    gbl_nbLp = num_lopre;
    num_lopre = 0;


    ret=controles(p_e2,p_e3,p_s2);
    if (ret !=0)
    {
        cherche_Erreur(ret,&(p_s2->libelle));
        return ret;
    }

    /* Determination des VL brutes (imposee et non imposee */
    /* dans le cadre de l'abattement K3                    */
    if (p_e2->tyimp == 'T')

    /* Traitement de la THLV */
    {
        /* CA_THLV_0014 Determination des VL brutes imposees et non imposees */
        p_s2->vlbni = 0;
        p_s2->vlbri = p_e2->vlbpc;
    }
    else
    {
    /* 2019MAJR18-01-01 2019MAJR18-01-02 2019MAJR18-01-03*/
    /*2020MAJ11-02-01  suppression CA_TH_0170*/
    /*    if ((p_e2->tax == 'P' || p_e2->tax == 'E') && (p_e2->cnat == '3' || p_e2->cnat == '4') &&
            (p_e2->degex == 'H' || p_e2->degex == ' ') && ((p_e2->imaisf == 'M' || p_e2->imaisf == 'D')|| p_e2->txdegm >0))
        {
            gbl_M = 1;
        }*/
    /* fin 2020MAJ11-02-01*/
    /* FIN 2019MAJR18-01-01 2019MAJR18-01-02 2019MAJR18-01-03*/

        for(  i=0; i < DIM_LOCAUX_PRO ; i++)
        {
            initialisation_loprs (&p_sa->loprs[i]); /* CA_TH_0119  et CA_TH_0128 */
            p_sa->loprs[i].vlrnp7 = p_ea->lopre[i].vlrnp7; /* CA_TH_0237 */
            p_sa->loprs[i].vlrnpt7 = p_ea->lopre[i].vlrnpt7;
        }



    /* APPEL DU MODULE KRPC */
    /* ==================== */
    /*       =======        */
    /*         ==           */

    for(i=0;i<gbl_nbLp;i++)
    {

         p_lpe1->lopre[i] = p_ea->lopre[i]; /* PREPARATION KLPC: copie des locaux pros en entr�e lpe1 */


        /* CA_TH_237 */

        p_sa->loprs[i].mntpl =  p_ea->lopre[i].mntpl;
        p_sa->loprs[i].mntplt =  p_ea->lopre[i].mntplt;
        p_sa->loprs[i].sgnpl =  p_ea->lopre[i].sgnpl;
        p_sa->loprs[i].sgnplt =  p_ea->lopre[i].sgnplt;
        p_sa->loprs[i].vlrnp =  p_ea->lopre[i].vlrnp;
        p_sa->loprs[i].vlrnpt =  p_ea->lopre[i].vlrnpt;

     /* CA_TH_238 */
     if (p_ea->lopre[i].indlis == 'O' && p_ea->lopre[i].indrec == 'O')
     {
         if  (gbl_Recalcul_Lissage ==0)
        {
           gbl_Recalcul_Lissage = 1;
        }
         p_ep->antax = p_e2->antax;
         p_ep->indlis = p_ea->lopre[i].indlis;
         p_ep->indrec = p_ea->lopre[i].indrec;
         p_ep->vlthr = p_ea->lopre[i].vlthr;
         p_ep->vlb70p7 = p_ea->lopre[i].vlb70p7;
         p_ep->vlthr7 = p_ea->lopre[i].vlthr7;
         p_ep->coefn = p_ea->lopre[i].coefn;
         p_ep->coefnt = p_ea->lopre[i].coefnt;



       ret = th_0krpc(p_ep,p_sp);
       if (ret!=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }
       /* CA_TH_239 */
        p_sa->loprs[i].mntpl   = p_sp->mntpl          ;
        p_sa->loprs[i].sgnpl   = p_sp->sgnpl          ;
        p_sa->loprs[i].mntplt  = p_sp->mntplt         ;
        p_sa->loprs[i].sgnplt  = p_sp->sgnplt         ;
        p_sa->loprs[i].vlrnp   = p_sp->vlrnp          ;
        p_sa->loprs[i].vlrnpt  = p_sp->vlrnpt         ;
        p_sa->loprs[i].vlrnp7   = p_sp->vlrnp7        ;
        p_sa->loprs[i].vlrnpt7  = p_sp->vlrnpt7       ;

        p_lpe1->lopre[i].vlrnp7  = p_sp->vlrnp7       ;  /* L Entree EA n est pas modifiee, c est le lopre de lpe1 qui transporte les modifs de VL � l'issue de KRPC, qui n'ont pas vocation � �tre fig�es en sortie */
        p_lpe1->lopre[i].vlrnpt7 = p_sp->vlrnpt7      ;
        p_lpe1->lopre[i].vlrnp  = p_sp->vlrnp         ;
        p_lpe1->lopre[i].vlrnpt  = p_sp->vlrnpt       ;

        /* positionnement des indicateurs de calcul des Abattements*/

        if (((p_sa->loprs[i].vlrnp7 > 0 ) || (p_sa->loprs[i].vlrnpt7 > 0 )) && (ind_liq2017VLRNP2017 ==0))
        {
            ind_liq2017VLRNP2017 = 1;
        }
         if ((p_ea->lopre[i].vlb70p7 > 0 ) && (ind_liq2017BASE70_2017 ==0))
        {
            ind_liq2017BASE70_2017 = 1;
        }

     }

    }
        /* Determination des differentes  VL brutes de la cote */

        /* Cumul des VL des locaux professionnels  */



        for(i=0;i<gbl_nbLp;i++)
        {
            p_s2->vlbr   = (p_s2->vlbr + p_lpe1->lopre[i].vlrnp);      /* CA_TH_0048 */
            p_s2->vlbrt  = (p_s2->vlbrt + p_lpe1->lopre[i].vlrnpt);    /* CA_TH_0049 */
            if (gbl_Recalcul_Lissage == 1)
            {
                p_s2->vlbr7  = (p_s2->vlbr7 + p_lpe1->lopre[i].vlrnp7);    /* CA_TH_0050 */
                p_s2->vlbrt7 = (p_s2->vlbrt7 + p_lpe1->lopre[i].vlrnpt7);  /* CA_TH_0051 */
                p_s2->vlb77  = (p_s2->vlb77 + p_lpe1->lopre[i].vlb70p7);   /* CA_TH_0052 */
            }
        }

        /* Ajout de la VL 70 actualisee des locaux d'habitation */
        p_s2->vlbr   = (p_s2->vlbr + p_e2->vlbpc);                       /* CA_TH_0048 */
        p_s2->vlbrt  = (p_s2->vlbrt + p_e2->vlbpc);                      /* CA_TH_0049 */
        p_s2->vlbr7  = (p_s2->vlbr7 + p_e2->vlbp7);                      /* CA_TH_0050 */
        p_s2->vlbrt7 = (p_s2->vlbrt7 + p_e2->vlbp7);                     /* CA_TH_0051 */
        p_s2->vlb77  = (p_s2->vlb77 + p_e2->vlbp7);                      /* CA_TH_0052 */

        /* Determination des differentes VL brutes IMPOSEES et NON IMPOSEES */
        /* CA_TH_0045 Determination des differentes VL brutes imposees de la cote */

        if ((p_e2->tax == 'P') || (p_e2->tax == 'E'))
        {   /* Cas des articles THP ou THE */

            /* Abattement 2/3 */
            if ((p_e2->abatk == 'K') && (p_e2->rgsork == '3'))
            {
                /* vl brute imposee et non imposee revisee N */
                p_s2->vlbnir = arrondi_euro_voisin(((double)(p_s2->vlbr  * 2)) / 3);
                p_s2->vlbir = (p_s2->vlbr - p_s2->vlbnir);

                /* vl brute imposee et non imposee revisee N TSE */
                p_s2->vlbnirt = arrondi_euro_voisin(((double)(p_s2->vlbrt * 2)) / 3);
                p_s2->vlbirt = (p_s2->vlbrt - p_s2->vlbnirt);

                /* vl brute imposee et non imposee revisee 2017 */
                p_s2->vlbnir7 = arrondi_euro_voisin(((double)(p_s2->vlbr7 * 2)) / 3);
                p_s2->vlbir7 = (p_s2->vlbr7 - p_s2->vlbnir7);

                /* vl brute imposee et non imposee revisee 2017 TSE */
                p_s2->vlbnir7t = arrondi_euro_voisin(((double)(p_s2->vlbrt7 * 2)) / 3);
                p_s2->vlbir7t = (p_s2->vlbrt7 - p_s2->vlbnir7t);

                /* vl brute imposee et non imposee base 70 2017 */
                p_s2->vlbni77 = arrondi_euro_voisin(((double)(p_s2->vlb77 * 2)) / 3);
                p_s2->vlbi77 = (p_s2->vlb77 - p_s2->vlbni77);

                /* vl brute imposee et non imposee si que habitation */
                p_s2->vlbni = arrondi_euro_voisin(((double)(p_e2->vlbpc * 2)) / 3);
                p_s2->vlbri = (p_e2->vlbpc - p_s2->vlbni);
            }
            else
            {
                /* Abattement 1/3 */
                if ((p_e2->abatk == 'K') && (p_e2->rgsork == '4'))
                {
                    /* vl brute imposee et non imposee revisee N */
                    p_s2->vlbnir = arrondi_euro_voisin(((double)(p_s2->vlbr * 1)) / 3);
                    p_s2->vlbir = (p_s2->vlbr - p_s2->vlbnir);

                    /* vl brute imposee et non imposee revisee N TSE */
                    p_s2->vlbnirt = arrondi_euro_voisin(((double)(p_s2->vlbrt * 1)) / 3);
                    p_s2->vlbirt = (p_s2->vlbrt - p_s2->vlbnirt);

                    /* vl brute imposee et non imposee revisee 2017 */
                    p_s2->vlbnir7 = arrondi_euro_voisin(((double)(p_s2->vlbr7 * 1)) / 3);
                    p_s2->vlbir7 = (p_s2->vlbr7 - p_s2->vlbnir7);

                    /* vl brute imposee et non imposee revisee 2017 TSE */
                    p_s2->vlbnir7t = arrondi_euro_voisin(((double)(p_s2->vlbrt7 * 1)) / 3);
                    p_s2->vlbir7t = (p_s2->vlbrt7 - p_s2->vlbnir7t);

                    /* vl brute imposee et non imposee base 70 2017 */
                    p_s2->vlbni77 = arrondi_euro_voisin(((double)(p_s2->vlb77 * 1)) / 3);
                    p_s2->vlbi77 = (p_s2->vlb77 - p_s2->vlbni77);

                    /* vl brute imposee et non imposee si que habitation */
                    p_s2->vlbni = arrondi_euro_voisin(((double)(p_e2->vlbpc * 1)) / 3);
                    p_s2->vlbri = (p_e2->vlbpc - p_s2->vlbni);
                }
                else
                {
                    p_s2->vlbnir = 0;
                    p_s2->vlbir = p_s2->vlbr;
                    p_s2->vlbnirt = 0;
                    p_s2->vlbirt = p_s2->vlbrt;
                    p_s2->vlbnir7 = 0;
                    p_s2->vlbir7 = p_s2->vlbr7;
                    p_s2->vlbnir7t = 0;
                    p_s2->vlbir7t = p_s2->vlbrt7;
                    p_s2->vlbni77 = 0;
                    p_s2->vlbi77 = p_s2->vlb77;
                    p_s2->vlbni = 0;
                    p_s2->vlbri = p_e2->vlbpc;
                }
            }
        }
        else if (p_e2->tax == 'S')
        {
                p_s2->vlbnir = 0;
                p_s2->vlbir = p_s2->vlbr;
                p_s2->vlbnirt = 0;
                p_s2->vlbirt = p_s2->vlbrt;
                p_s2->vlbnir7 = 0;
                p_s2->vlbir7 = p_s2->vlbr7;
                p_s2->vlbnir7t = 0;
                p_s2->vlbir7t = p_s2->vlbrt7;
                p_s2->vlbni77 = 0;
                p_s2->vlbi77 = p_s2->vlb77;
                p_s2->vlbni = 0;
                p_s2->vlbri = p_e2->vlbpc;
        }
    }

    /* APPEL DU MODULE KTAC */
    /* ==================== */
    /*       =======        */
    /*         ==           */
    {
        p_e8->antax = p_e3->antax;
        strncpy(p_e8->dep, p_e3->dep,3);
        p_e8->cocnq = p_e3->cocnq;
        p_e8->tisyn = p_e3->tisyn;
        p_e8->titsn = p_e3->titsn;
        p_e8->titgp = p_e3->titgp;
        p_e8->abmos = p_e3->abmos;
        p_e8->codef = p_e3->codef;
        p_e8->codeg = p_e3->codeg;
        p_e8->vmd89 = p_e3->vmd89;

        p_e8->timpe = p_e3->timpe;
        p_e8->indgem = p_e3->indgem;

        p_e8->cole_c = p_e3->cole_c; /* commune annee N*/
        p_e8->cole_q = p_e3->cole_q; /* interco annee N*/
        p_e8->cole_f = p_e3->cole_f; /* commune avt ajustement annee N*/
        p_e8->cole_r = p_e3->cole_r; /* interco avt ajustement annee N*/



        p_e8->cole_h = p_e3->cole_h; /* commune annee 2017*/
        p_e8->cole_l = p_e3->cole_l; /* interco annee 2017*/
        p_e8->cole_k = p_e3->cole_k; /* commune avt ajustement annee 2017*/
        p_e8->tisyn7 = p_e3->tisyn7;
        p_e8->titsn7 = p_e3->titsn7;
        p_e8->titgp7 = p_e3->titgp7;
        p_e8->timpe7 = p_e3->timpe7;
        p_e8->abmos7 = p_e3->abmos7;
        p_e8->codef7 = p_e3->codef7;
        /* Bug 213594 */
        p_e8->timpq7 = p_e3->timpq7;
        p_e8->timpc7 = p_e3->timpc7;
    }

    ret = th_0ktac( p_e8,p_s8);

    if (ret != 0)
    {
        cherche_Erreur(ret,&(p_s2->libelle)) ;
        return (ret);
    }

     /* APPEL DU MODULE KACC */
    /* ==================== */
    /*       =======        */
    /*         ==           */
    /*   APPELE 4 FOIS POUR 4 LIQUIDATIONS */

    /*   LIQUIDATION N POUR LOCAUX HABITATION UNIQUEMENT - PAS DE LOCAL PRO                          - TX N - DONNEES COLLECT N */
    /*   LIQUIDATION N POUR LOCAUX PRO AVEC BASES REVISEES - PRESENCE EVENTUELLE DE LOCAL HABITATION - TX N - DONNEES COLLECT N */

    /* SI UN LP AU MOINS EST ELLIGIBLE AU RECALCUL DU PLANCHONNEMENT */
    /*   LIQUIDATION 2017 POUR LOCAUX PRO AVEC BASES REVISEES 2017          - TX 2017 - DONNEES COLLECT 2017 */
    /*   LIQUIDATION 2017 POUR LOCAUX PRO VL70 AVANT REVISION 2017          - TX 2017 - DONNEES COLLECT 2017 */
    /* FIN SI*/

    /*   -----------------------------------------   */

    if (gbl_nbLp == 0 && p_e2->tyimp== 'H'&& p_e2->tax == 'P') /* Absence de local pro - kacc non jou� en cas de THLV, THS, THE */
    {
        /* pas de local pro, uniquement les locaux d'habitation : ref liquidation 7 */
        /* 1   LIQUIDATION N POUR LOCAUX HABITATION UNIQUEMENT - PAS DE LOCAL PRO                          - TX N - DONNEES COLLECT N */
        /* Bug 270587 */
        p_ace->vlbr  = p_e2->vlbpc;
        /* fin Bug 270587 */
        p_ace->vlbri  = p_s2->vlbir;
        p_ace->vlbrit = p_s2->vlbirt;
        p_ace->nbpac  = p_e2->nbpac;
        p_ace->nbera  = p_e2->nbera;
        p_ace->codni  = p_e2->codni;
        p_ace->codsh  = p_e2->codsh;
        p_ace->timpc  = p_e3->timpc;
        p_ace->timpq  = p_e3->timpq;
        p_ace->tisyn  = p_e3->tisyn;
        p_ace->titsn  = p_e3->titsn;
        p_ace->titgp  = p_e3->titgp;
        p_ace->timpe  = p_e3->timpe;
        /* Bug 270587 */
        /*p_ace->abmos  = p_e3->abmos;
          p_ace->codef  = p_e3->codef;*/
        /* fin Bug 270587 */
        p_ace->colb_c = p_s8->colb_c; /* Commune N */
        p_ace->colb_s = p_s8->colb_s; /* Syndicat N */
        p_ace->colb_q = p_s8->colb_q; /* Intercommunalite  N */
        p_ace->colb_n = p_s8->colb_n; /* TSE N */
        p_ace->colb_g = p_s8->colb_g; /* TSE Autre N */
        p_ace->colb_e = p_s8->colb_e; /* GEMAPI N */
        p_ace->colb_f = p_s8->colb_f; /* Commune avant ajustement N */
        p_ace->colb_r = p_s8->colb_r; /* Interco avant ajustement N */
        p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
        p_ace->cocnq    = p_e3->cocnq;

        ret = th_0kacc( p_ace,p_acs);
        if (ret != 0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }

        p_s2->cols_c = p_acs->cols_c;
        p_s2->cols_s = p_acs->cols_s;
        p_s2->cols_q = p_acs->cols_q;
        p_s2->cols_n = p_acs->cols_n;
        p_s2->cols_g = p_acs->cols_g;
        p_s2->cols_e = p_acs->cols_e;
        p_s2->cols_f = p_acs->cols_f;
        p_s2->cols_r = p_acs->cols_r;

    }
    if ((gbl_nbLp >0) && (p_e2->tax == 'P'))
    {
        /* si local pro existant */
        /* 2  LIQUIDATION N POUR LOCAUX PRO AVEC BASES REVISEES - PRESENCE EVENTUELLE DE LOCAL HABITATION - TX N - DONNEES COLLECT N */
        /* Ref liquidation 1 */
        /* Bug 270587 */
        p_ace->vlbr  = p_s2->vlbr;
        /* fin Bug 270587 */
        p_ace->vlbri  = p_s2->vlbir;
        p_ace->vlbrit = p_s2->vlbirt;
        p_ace->nbpac  = p_e2->nbpac;
        p_ace->nbera  = p_e2->nbera;
        p_ace->codni  = p_e2->codni;
        p_ace->codsh  = p_e2->codsh;
        p_ace->timpc  = p_e3->timpc;
        p_ace->timpq  = p_e3->timpq;
        p_ace->tisyn  = p_e3->tisyn;
        p_ace->titsn  = p_e3->titsn;
        p_ace->titgp  = p_e3->titgp;
        p_ace->timpe  = p_e3->timpe;
        /* Bug 270587 */
        /*p_ace->abmos  = p_e3->abmos;
          p_ace->codef  = p_e3->codef;*/
        /* fin Bug 270587 */
        p_ace->colb_c = p_s8->colb_c; /* Commune N */
        p_ace->colb_s = p_s8->colb_s; /* Syndicat N */
        p_ace->colb_q = p_s8->colb_q; /* Intercommunalite  N */
        p_ace->colb_n = p_s8->colb_n; /* TSE N */
        p_ace->colb_g = p_s8->colb_g; /* TSE Autre N */
        p_ace->colb_e = p_s8->colb_e; /* GEMAPI N */
        p_ace->colb_f = p_s8->colb_f; /* Commune avant ajustement N */
        p_ace->colb_r = p_s8->colb_r; /* Interco avant ajustement N */
        p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
        p_ace->cocnq    = p_e3->cocnq;

        ret = th_0kacc( p_ace,p_acs);
        if (ret != 0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }

        p_s2->cols_cr = p_acs->cols_c;
        p_s2->cols_sr = p_acs->cols_s;
        p_s2->cols_qr = p_acs->cols_q;
        p_s2->cols_nr = p_acs->cols_n;
        p_s2->cols_gr = p_acs->cols_g;
        p_s2->cols_er = p_acs->cols_e;
        p_s2->cols_fr = p_acs->cols_f;
        p_s2->cols_rr = p_acs->cols_r;


        /*2019MAJR13-01-04  2019MAJR13-01-05   */

        if (ind_liq2017VLRNP2017 == 1)
        {
                /* si local pro existant */
                /* 3   LIQUIDATION 2017 POUR LOCAUX PRO AVEC BASES REVISEES 2017          - TX 2017 - DONNEES COLLECT 2017 */
                /* Ref liquidation 2 */

                /* Bug 270587 */
                p_ace->vlbr  = p_s2->vlbr7;
                /* fin  Bug 270587 */
                p_ace->vlbri  = p_s2->vlbir7;
                p_ace->vlbrit = p_s2->vlbir7t;
                p_ace->nbpac  = p_e2->nbpac;
                p_ace->nbera  = p_e2->nbera;
                p_ace->codni  = p_e2->codni;
                p_ace->codsh  = p_e2->codsh;
                p_ace->timpc  = p_e3->timpc7;
                p_ace->timpq  = p_e3->timpq7;
                p_ace->tisyn  = p_e3->tisyn7;
                p_ace->titsn  = p_e3->titsn7;
                p_ace->titgp  = p_e3->titgp7;
                p_ace->timpe  = p_e3->timpe7;
                /* Bug 270587 */
                /*p_ace->abmos  = p_e3->abmos7;
                  p_ace->codef  = p_e3->codef7;*/
                /* fin Bug 270587 */
                p_ace->colb_c = p_s8->colb_h; /* commune 2017 */

                /* Bug 213594 */
                /* p_ace->colb_s = p_s8->colb_l;  interco 2017 */
                /* p_ace->colb_q = p_s8->colb_t;  syndicat 2017 */
                p_ace->colb_s = p_s8->colb_t; /* syndicat 2017 */
                p_ace->colb_q = p_s8->colb_l; /* interco 2017 */

                p_ace->colb_n = p_s8->colb_w; /* TSE 2017 */
                p_ace->colb_g = p_s8->colb_x; /* TSE autre 2017 */
                p_ace->colb_e = p_s8->colb_u; /* gemapi 2017 */
                p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
              /* Bug 250001 */
                p_ace->cocnq    = p_e3->cocnq;
              /* Fin Bug 250001 */


                ret = th_0kacc( p_ace,p_acs);
                if (ret != 0)
                {
                    cherche_Erreur(ret,&(p_s2->libelle)) ;
                    return (ret);
                }

                p_s2->cols_cr7 = p_acs->cols_c; /* commune 2017 base r�vis�e 2017 */
                p_s2->cols_qr7 = p_acs->cols_q; /* interco 2017 base r�vis�e 2017 */
                p_s2->cols_sr7 = p_acs->cols_s; /* syndicat 2017 base r�vis�e 2017 */
                p_s2->cols_nr7 = p_acs->cols_n; /* tse 2017 base r�vis�e 2017 */
                p_s2->cols_gr7 = p_acs->cols_g; /* tse autre 2017 base r�vis�e 2017 */
                p_s2->cols_er7 = p_acs->cols_e; /* gemapi 2017 base r�vis�e 2017 */
            }
        if (ind_liq2017BASE70_2017 == 1)
        {
                /* si local pro existant */
                /* 4  LIQUIDATION 2017 POUR LOCAUX PRO VL70 AVANT REVISION 2017          - TX 2017 - DONNEES COLLECT 2017 */
                /* Ref liquidation 3 */

                /* Bug 270587 */
                p_ace->vlbr  = p_s2->vlb77;
                /* fin Bug 270587 */
                p_ace->vlbri  = p_s2->vlbi77;
                p_ace->vlbrit = p_s2->vlbi77;
                p_ace->nbpac  = p_e2->nbpac;
                p_ace->nbera  = p_e2->nbera;
                p_ace->codni  = p_e2->codni;
                p_ace->codsh  = p_e2->codsh;
                p_ace->timpc  = p_e3->timpc7;
                p_ace->timpq  = p_e3->timpq7;
                p_ace->tisyn  = p_e3->tisyn7;
                p_ace->titsn  = p_e3->titsn7;
                p_ace->titgp  = p_e3->titgp7;
                p_ace->timpe  = p_e3->timpe7;
                /* Bug 270587 */
                /*p_ace->abmos  = p_e3->abmos7;
                  p_ace->codef  = p_e3->codef7;*/
                /* fin Bug 270587 */
                p_ace->colb_c = p_s8->colb_h; /* commune 2017 */
                p_ace->colb_s = p_s8->colb_t; /* syndicat 2017 */
                p_ace->colb_q = p_s8->colb_l; /* interco 2017 */
                p_ace->colb_n = p_s8->colb_w; /* TSE 2017 */
                p_ace->colb_g = p_s8->colb_x; /* TSE autre 2017 */
                p_ace->colb_e = p_s8->colb_u; /* gemapi 2017 */
                p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
                p_ace->cocnq    = p_e3->cocnq;

                ret = th_0kacc( p_ace,p_acs);
                if (ret != 0)
                {
                    cherche_Erreur(ret,&(p_s2->libelle)) ;
                    return (ret);
                }

                p_s2->cols_c7 = p_acs->cols_c; /* commune 2017 base 70 */
                p_s2->cols_q7 = p_acs->cols_q; /* interco 2017 base 70 */
                p_s2->cols_s7 = p_acs->cols_s; /*syndicat 2017 base 70 */
                p_s2->cols_n7 = p_acs->cols_n; /* tse 2017 base 70 */
                p_s2->cols_g7 = p_acs->cols_g; /* tse autre 2017 base 70 */
                p_s2->cols_e7 = p_acs->cols_e; /* gemapi 2017 base 70 */


        }

        /* FIN 2019MAJR13-01-04  2019MAJR13-01-05    */



    } /* fin des liquidations locaux pro */



    /* APPEL DU MODULE KLPC */
    /* ==================== */
    /*       =======        */
    /*         ==           */

    lpe1.antax = p_e2->antax;


    for(i=0; i<DIM_LOCAUX_PRO; i++)
    {
        p_lps->p_loprs[i] = &p_sa->loprs[i];

    }



    initialisation_sortieQtp(&qtpAbLh );

    p_lps->p_qtpAbLh = &qtpAbLh;

    p_lps->p_qtpAbL = &qtpAbL;
    initialisation_sortieQtp(p_lps->p_qtpAbL );
    /* CA_TH_0119 Initialisation du pas de lissage */
    p_lps->sommePaLis = 0;

    p_lpe1->p_vle=&vle;
    vle.vlbr     = p_s2->vlbr;
    vle.vlbrt    = p_s2->vlbrt;
    vle.vlbr7    = p_s2->vlbr7;
    vle.vlbrt7   = p_s2->vlbrt7;
    vle.vlb77    = p_s2->vlb77;
    vle.vlbir    = p_s2->vlbir;
    vle.vlbirt   = p_s2->vlbirt;
    vle.vlbir7   = p_s2->vlbir7;
    vle.vlbir7t  = p_s2->vlbir7t;
    vle.vlbi77   = p_s2->vlbi77;
    vle.vlbnir   = p_s2->vlbnir;
    vle.vlbnirt  = p_s2->vlbnirt;
    vle.vlbnir7  = p_s2->vlbnir7;
    vle.vlbnir7t = p_s2->vlbnir7t;
    vle.vlbni77  = p_s2->vlbni77;
    vle.vlbpc    = p_e2->vlbpc;
    vle.vlbp7    = p_e2->vlbp7;

    p_lpe1->p_AbCote = &AbCote;
    initialisation_sortieQtp( p_lpe1->p_AbCote );

    AbCote.qtp_cr=p_s2->cols_cr;
    AbCote.qtp_qr=p_s2->cols_qr;
    AbCote.qtp_sr=p_s2->cols_sr;
    AbCote.qtp_nr=p_s2->cols_nr;
    AbCote.qtp_gr=p_s2->cols_gr;
    AbCote.qtp_er=p_s2->cols_er;
    AbCote.qtp_fr=p_s2->cols_fr;
    AbCote.qtp_rr=p_s2->cols_rr;


    AbCote.qtp_c7=p_s2->cols_c7;
    AbCote.qtp_q7=p_s2->cols_q7;
    AbCote.qtp_s7=p_s2->cols_s7;
    AbCote.qtp_n7=p_s2->cols_n7;
    AbCote.qtp_g7=p_s2->cols_g7;
    AbCote.qtp_e7=p_s2->cols_e7;

    AbCote.qtp_cr7=p_s2->cols_cr7;
    AbCote.qtp_qr7=p_s2->cols_qr7;
    AbCote.qtp_sr7=p_s2->cols_sr7;
    AbCote.qtp_nr7=p_s2->cols_nr7;
    AbCote.qtp_gr7=p_s2->cols_gr7;
    AbCote.qtp_er7=p_s2->cols_er7;

    p_lpe1->p_coe1 = &coe1;
    coe1.antax  = p_e2->antax;
    coe1.tyimp  = p_e2->tyimp;
    coe1.degex  = p_e2->degex;
    coe1.cnat   = p_e2->cnat;
    coe1.tax    = p_e2->tax;
    coe1.rgsork = p_e2->rgsork;
    coe1.abatk  = p_e2->abatk;
    coe1.grrev  = p_e2->grrev;
    coe1.aff  = p_e2->aff;
    coe1.indthlv = p_e3->indthlv;
    coe1.cocnq = p_e3->cocnq;
    coe1.indgem  = p_e3->indgem;
    coe1.permo  = p_e2->permo;

    initialisation_coe2(&coe2);
    p_lpe2->p_coe2 = &coe2;

    initialisation_coe2(&coe2_7);
    p_lpe2->p_coe2_7 = &coe2_7;

    coe2.timpc     = p_e3->timpc;
    coe2.tisyn     = p_e3->tisyn;
    coe2.titsn     = p_e3->titsn;
    coe2.titgp     = p_e3->titgp;
    coe2.timpq     = p_e3->timpq;
    coe2.timpe     = p_e3->timpe;
    coe2.timths    = p_e3->timths;

    /* Pour liquidation 2017 */
    coe2_7.timpc     = p_e3->timpc7;
    coe2_7.tisyn     = p_e3->tisyn7;
    coe2_7.titsn     = p_e3->titsn7;
    coe2_7.titgp     = p_e3->titgp7;
    coe2_7.timpq     = p_e3->timpq7;
    coe2_7.timpe     = p_e3->timpe7;

    if (gbl_nbLp != 0)
    {
        ret = th_0klpc (p_lpe1, p_lpe2, p_lps);
        if (ret!=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }
    }

    /* CALCUL DE LA COTISATION  PROPRE AUX LOCAUX D HABITATION  */

    /*   APPEL DU MODULE - KCOC  */
    /*   ---------------------   */

    /* Regle 6.1 Conditionnement */
    if ((gbl_nbLp == 0) && (p_s2->vlbri > 0))
    {
        s_cos *p_cos = &cos;

        /* cote des locaux d'habitation - liquidation N */
        /* CA_TH_0136 CA_TH_0137 CA_TH_138 CA_TH_0139 */
        p_lpe1->p_coe1->vl = p_s2->vlbri;
        p_lpe1->p_coe1->vlt = p_s2->vlbri;
        p_lpe1->p_coe1->p_cols_c =  &p_s2->cols_c;
        p_lpe1->p_coe1->p_cols_q =  &p_s2->cols_q;
        p_lpe1->p_coe1->p_cols_s =  &p_s2->cols_s;
        p_lpe1->p_coe1->p_cols_n =  &p_s2->cols_n;
        p_lpe1->p_coe1->p_cols_g =  &p_s2->cols_g;
        p_lpe1->p_coe1->p_cols_e =  &p_s2->cols_e;
        p_lpe1->p_coe1->p_cols_f =  &p_s2->cols_f;
        p_lpe1->p_coe1->p_cols_r =  &p_s2->cols_r;

        ret = th_0kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2, p_cos);
        if (ret !=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return(ret);
        }
    }

    /* Regle 6.11 Conditionnement Liquidation N Hab + Loc Pro */
    if ((gbl_nbLp > 0) && (p_s2->vlbri > 0))
    {
        s_cos *p_cos = &cos;

        /* cote des locaux d'habitation - liquidation N si presence de LP*/
        /* CA_TH_0145 CA_TH_0146 CA_TH_0147 CA_TH_0148 */
        p_lpe1->p_coe1->vl = p_s2->vlbri;
        p_lpe1->p_coe1->vlt = p_s2->vlbri;
        p_lpe1->p_coe1->p_cols_c =  &p_lps->p_qtpAbLh->qtp_cr;
        p_lpe1->p_coe1->p_cols_q =  &p_lps->p_qtpAbLh->qtp_qr;
        p_lpe1->p_coe1->p_cols_s =  &p_lps->p_qtpAbLh->qtp_sr;
        p_lpe1->p_coe1->p_cols_n =  &p_lps->p_qtpAbLh->qtp_nr;
        p_lpe1->p_coe1->p_cols_g =  &p_lps->p_qtpAbLh->qtp_gr;
        p_lpe1->p_coe1->p_cols_e =  &p_lps->p_qtpAbLh->qtp_er;
        p_lpe1->p_coe1->p_cols_f =  &p_lps->p_qtpAbLh->qtp_fr;
        p_lpe1->p_coe1->p_cols_r =  &p_lps->p_qtpAbLh->qtp_rr;

        ret = th_0kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2, p_cos);
        if (ret !=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return(ret);
        }

          majBaseDansS2( p_lps->p_qtpAbLh->qtp_cr, &p_s2->cols_cr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_qr, &p_s2->cols_qr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_sr, &p_s2->cols_sr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_nr, &p_s2->cols_nr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_gr, &p_s2->cols_gr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_er, &p_s2->cols_er);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_fr, &p_s2->cols_fr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_rr, &p_s2->cols_rr);

    }

    /*Somme des VL nettes, bases exon�r�es, bases nettes imposees des locaux pros */

    /* CA_TH_0154 Somme des VL nettes, bases nettes et bases imposees N des locaux professionnels */
    for (i = 0 ; i < gbl_nbLp ; i++)
    {

        sVlnc = sVlnc + p_sa->loprs[i].cols_cr.vlntt;
        sVlnq = sVlnq + p_sa->loprs[i].cols_qr.vlntt;
        sVlns = sVlns + p_sa->loprs[i].cols_sr.vlntt;
        sVlnn = sVlnn + p_sa->loprs[i].cols_nr.vlntt;
        sVlng = sVlng + p_sa->loprs[i].cols_gr.vlntt;
        sVlne = sVlne + p_sa->loprs[i].cols_er.vlntt;
        sVlnf = sVlnf + p_sa->loprs[i].cols_fr.vlntt;
        sVlnr = sVlnr + p_sa->loprs[i].cols_rr.vlntt;

        sVlxc = sVlxc + p_sa->loprs[i].cols_cr.vlnex;
        sVlxq = sVlxq + p_sa->loprs[i].cols_qr.vlnex;
        sVlxs = sVlxs + p_sa->loprs[i].cols_sr.vlnex;
        sVlxn = sVlxn + p_sa->loprs[i].cols_nr.vlnex;
        sVlxg = sVlxg + p_sa->loprs[i].cols_gr.vlnex;
        sVlxe = sVlxe + p_sa->loprs[i].cols_er.vlnex;
        sVlxf = sVlxf + p_sa->loprs[i].cols_fr.vlnex;
        sVlxr = sVlxr + p_sa->loprs[i].cols_rr.vlnex;

        sBNc = sBNc + p_sa->loprs[i].cols_cr.bnimp;
        sBNq = sBNq + p_sa->loprs[i].cols_qr.bnimp;
        sBNs = sBNs + p_sa->loprs[i].cols_sr.bnimp;
        sBNn = sBNn + p_sa->loprs[i].cols_nr.bnimp;
        sBNg = sBNg + p_sa->loprs[i].cols_gr.bnimp;
        sBNe = sBNe + p_sa->loprs[i].cols_er.bnimp;
        sBNf = sBNf + p_sa->loprs[i].cols_fr.bnimp;
        sBNr = sBNr + p_sa->loprs[i].cols_rr.bnimp;

    }


    /* Calculs VLNTT VLNEX et BNIMP de la cote pour annee de campagne   CA_TH_0156 */
    p_s2->cols_cr.vlntt = sVlnc + p_lps->p_qtpAbLh->qtp_cr.vlntt;
    p_s2->cols_qr.vlntt = sVlnq + p_lps->p_qtpAbLh->qtp_qr.vlntt;
    p_s2->cols_sr.vlntt = sVlns + p_lps->p_qtpAbLh->qtp_sr.vlntt;
    p_s2->cols_nr.vlntt = sVlnn + p_lps->p_qtpAbLh->qtp_nr.vlntt;
    p_s2->cols_gr.vlntt = sVlng + p_lps->p_qtpAbLh->qtp_gr.vlntt;
    p_s2->cols_er.vlntt = sVlne + p_lps->p_qtpAbLh->qtp_er.vlntt;
    p_s2->cols_fr.vlntt = sVlnf + p_lps->p_qtpAbLh->qtp_fr.vlntt;
    p_s2->cols_rr.vlntt = sVlnr + p_lps->p_qtpAbLh->qtp_rr.vlntt;

    p_s2->cols_cr.vlnex = sVlxc + p_lps->p_qtpAbLh->qtp_cr.vlnex;
    p_s2->cols_qr.vlnex = sVlxq + p_lps->p_qtpAbLh->qtp_qr.vlnex;
    p_s2->cols_sr.vlnex = sVlxs + p_lps->p_qtpAbLh->qtp_sr.vlnex;
    p_s2->cols_nr.vlnex = sVlxn + p_lps->p_qtpAbLh->qtp_nr.vlnex;
    p_s2->cols_gr.vlnex = sVlxg + p_lps->p_qtpAbLh->qtp_gr.vlnex;
    p_s2->cols_er.vlnex = sVlxe + p_lps->p_qtpAbLh->qtp_er.vlnex;
    p_s2->cols_fr.vlnex = sVlxf + p_lps->p_qtpAbLh->qtp_fr.vlnex;
    p_s2->cols_rr.vlnex = sVlxr + p_lps->p_qtpAbLh->qtp_rr.vlnex;

    p_s2->cols_cr.bnimp = sBNc + p_lps->p_qtpAbLh->qtp_cr.bnimp;
    p_s2->cols_qr.bnimp = sBNq + p_lps->p_qtpAbLh->qtp_qr.bnimp;
    p_s2->cols_sr.bnimp = sBNs + p_lps->p_qtpAbLh->qtp_sr.bnimp;
    p_s2->cols_nr.bnimp = sBNn + p_lps->p_qtpAbLh->qtp_nr.bnimp;
    p_s2->cols_gr.bnimp = sBNg + p_lps->p_qtpAbLh->qtp_gr.bnimp;
    p_s2->cols_er.bnimp = sBNe + p_lps->p_qtpAbLh->qtp_er.bnimp;
    p_s2->cols_fr.bnimp = sBNf + p_lps->p_qtpAbLh->qtp_fr.bnimp;
    p_s2->cols_rr.bnimp = sBNr + p_lps->p_qtpAbLh->qtp_rr.bnimp;

    /* Somme des cotisations des locaux professionnels CA_TH_0158 CA_TH_0159 CA_TH_0160 CA_TH_0161 */

    /* Total pour tous les locaux pros... */
    for ( i=0 ; i < gbl_nbLp ; i++)
    {
        /* cotisations non lissees */
        /* CA_TH_0158 Somme des cotisations N des locaux professionnels */
        totCotLPc = totCotLPc + p_sa->loprs[i].cols_cr.coti;
        totCotLPq = totCotLPq + p_sa->loprs[i].cols_qr.coti;
        totCotLPs = totCotLPs + p_sa->loprs[i].cols_sr.coti;
        totCotLPn = totCotLPn + p_sa->loprs[i].cols_nr.coti;
        totCotLPg = totCotLPg + p_sa->loprs[i].cols_gr.coti;
        totCotLPe = totCotLPe + p_sa->loprs[i].cols_er.coti;

        /* cotisations lissees */
        /* CA_TH_0162 1ere partie somme locaux pros */
        /* CA_TH_0160 Somme des cotisations lissees N des locaux professionnels */
        totCotLPlisc = totCotLPlisc + p_sa->loprs[i].cotilpc;
        totCotLPlisq = totCotLPlisq + p_sa->loprs[i].cotilpq;
        totCotLPliss = totCotLPliss + p_sa->loprs[i].cotilps;
        totCotLPlisn = totCotLPlisn + p_sa->loprs[i].cotilpn;
        totCotLPlisg = totCotLPlisg + p_sa->loprs[i].cotilpg;
        totCotLPlise = totCotLPlise + p_sa->loprs[i].cotilpe;
    }

    /* Somme des cotisations de lq cote CA_TH_0162 CA_TH_0163 CA_TH_0164 CA_TH_0165 */
    if (gbl_nbLp > 0)
    {

        /* CA_TH_0162 2eme partie ajout locaux habitation */
        p_s2->cols_cr.coti  =  totCotLPlisc + p_lps->p_qtpAbLh->qtp_cr.coti;
        p_s2->cols_qr.coti  =  totCotLPlisq + p_lps->p_qtpAbLh->qtp_qr.coti;
        p_s2->cols_sr.coti  =  totCotLPliss + p_lps->p_qtpAbLh->qtp_sr.coti;
        p_s2->cols_nr.coti  =  totCotLPlisn + p_lps->p_qtpAbLh->qtp_nr.coti;
        p_s2->cols_gr.coti  =  totCotLPlisg + p_lps->p_qtpAbLh->qtp_gr.coti;
        p_s2->cols_er.coti  =  totCotLPlise + p_lps->p_qtpAbLh->qtp_er.coti;

    }

    if (gbl_nbLp > 0)
    {
        /* CA_TH_0164 cotisations avant lissage */
        p_s2->cotic  =  totCotLPc + p_lps->p_qtpAbLh->qtp_cr.coti;
        p_s2->cotiq  =  totCotLPq + p_lps->p_qtpAbLh->qtp_qr.coti;
        p_s2->cotis  =  totCotLPs + p_lps->p_qtpAbLh->qtp_sr.coti;
        p_s2->cotin  =  totCotLPn + p_lps->p_qtpAbLh->qtp_nr.coti;
        p_s2->cotig  =  totCotLPg + p_lps->p_qtpAbLh->qtp_gr.coti;
        p_s2->cotie  =  totCotLPe + p_lps->p_qtpAbLh->qtp_er.coti;

        /* CA_TH_0165 cotisations lissees */
        p_s2->cotilc  =  p_s2->cols_cr.coti;
        p_s2->cotilq  =  p_s2->cols_qr.coti;
        p_s2->cotils  =  p_s2->cols_sr.coti;
        p_s2->cotiln  =  p_s2->cols_nr.coti;
        p_s2->cotilg  =  p_s2->cols_gr.coti;
        p_s2->cotile  =  p_s2->cols_er.coti;
    }
    else
    {
        p_s2->cotic  =  p_s2->cols_c.coti;
        p_s2->cotiq  =  p_s2->cols_q.coti;
        p_s2->cotis  =  p_s2->cols_s.coti;
        p_s2->cotin  =  p_s2->cols_n.coti;
        p_s2->cotig  =  p_s2->cols_g.coti;
        p_s2->cotie  =  p_s2->cols_e.coti;

        p_s2->cotilc  =  0;
        p_s2->cotilq  =  0;
        p_s2->cotils  =  0;
        p_s2->cotiln  =  0;
        p_s2->cotilg  =  0;
        p_s2->cotile  =  0;
    }

    /* Calcul du lissage */
    /* CA_TH_0166 Somme des quotes-parts modulees appliquees N des locaux professionnels */

    for(i=0; i<gbl_nbLp; i++)
    {
        p_s2->lissac = p_s2->lissac + p_sa->loprs[i].qtmc;
        p_s2->lissaq = p_s2->lissaq + p_sa->loprs[i].qtmq;
        p_s2->lissas = p_s2->lissas + p_sa->loprs[i].qtms;
        p_s2->lissan = p_s2->lissan + p_sa->loprs[i].qtmn;
        p_s2->lissag = p_s2->lissag + p_sa->loprs[i].qtmg;
        p_s2->lissae = p_s2->lissae + p_sa->loprs[i].qtme;

        /* CA_TH_0167 Somme des quotes-parts des pas de lissage applicables par collectivit�  RG13-06*/
        p_s2->qtprltc = p_s2->qtprltc + p_sa->loprs[i].qtprlc;
        p_s2->qtprltq = p_s2->qtprltq + p_sa->loprs[i].qtprlq;
        p_s2->qtprlts = p_s2->qtprlts + p_sa->loprs[i].qtprls;
        p_s2->qtprltn = p_s2->qtprltn + p_sa->loprs[i].qtprln;
        p_s2->qtprltg = p_s2->qtprltg + p_sa->loprs[i].qtprlg;
        p_s2->qtprlte = p_s2->qtprlte + p_sa->loprs[i].qtprle;
    }

    /* CA_TH_0167 Somme des pas de lissage RG13-05*/
    p_s2->palis = p_lps->sommePaLis;

    /*  APPEL DU MODULE  KSRC  */
    /*  ---------------------  */
    p_sre->antax   = p_e2->antax;
    p_sre->degex   = p_e2->degex;
    p_sre->vlbpc   = p_e2->vlbpc;
    p_sre->vlbch   = p_e2->vlbch;
    p_sre->abatk   = p_e2->abatk;
    p_sre->rgsork   = p_e2->rgsork;
    p_sre->aff     = p_e2->aff;
    p_sre->cnat    = p_e2->cnat;
    p_sre->tax     = p_e2->tax;
    p_sre->indaths = p_e2->indaths;
    strncpy(p_sre->dep, p_e3->dep,3);
    strncpy(p_sre->cne, p_e3->cne,4);
    p_sre->indthlv = p_e3->indthlv;
    p_sre->indmths = p_e3->indmths;
    p_sre->timths  = p_e3->timths;
    p_sre->tyimp   = p_e2->tyimp;
    p_sre->imaisf  = p_e2->imaisf;

    if (gbl_nbLp > 0)
    {


        p_sre->cotic= p_lps->p_qtpAbLh->qtp_cr.coti;
        p_sre->cols_c = p_s2->cols_cr;
        p_sre->cols_s = p_s2->cols_sr;
        p_sre->cols_q = p_s2->cols_qr;
        p_sre->cols_n = p_s2->cols_nr;
        p_sre->cols_g = p_s2->cols_gr;
        p_sre->cols_e = p_s2->cols_er;
    }
    else
    {
        p_sre->cotic= p_s2->cols_c.coti;
        p_sre->cols_c = p_s2->cols_c;
        p_sre->cols_s = p_s2->cols_s;
        p_sre->cols_q = p_s2->cols_q;
        p_sre->cols_n = p_s2->cols_n;
        p_sre->cols_g = p_s2->cols_g;
        p_sre->cols_e = p_s2->cols_e;
    }
    ret = th_0ksrc( p_sre, p_srs);

    if (ret!=0)
    {
        cherche_Erreur(ret,&(p_s2->libelle)) ;
        return (ret);
    }

    p_s2->majths	= p_srs->majths   ;
    p_s2->coticm    = p_srs->coticm   ;
    p_s2->frait     = p_srs->frait    ;
    p_s2->fgest_cq  = p_srs->fgest_cq ;
    p_s2->fgest_ng  = p_srs->fgest_ng ;
    p_s2->fgest_e   = p_srs->fgest_e  ;
    p_s2->fgest_s   = p_srs->fgest_s  ;
    p_s2->frai4     = p_srs->frai4    ;
    p_s2->far_cq    = p_srs->far_cq   ;
    p_s2->far_s     = p_srs->far_s    ;
    p_s2->far_ng    = p_srs->far_ng   ;
    p_s2->far_e     = p_srs->far_e    ;
    p_s2->prelt     = p_srs->prelt    ;
    p_s2->preths    = p_srs->preths   ;
    p_s2->somrc     = p_srs->somrc    ;
    p_s2->pre02     = p_srs->pre02    ;
    p_s2->pre12     = p_srs->pre12    ;
    p_s2->pre17     = p_srs->pre17    ;
    p_s2->limajths	= p_srs->limajths ;


    /*   APPEL DU MODULE KDNC   */
    dne.p_cons = p_const1;
    dne.tyimp = p_e2->tyimp;

    dne.degex = p_e2->degex;
    dne.rgsork = p_e2->rgsork;

    dne.somrc = p_s2->somrc;
    dne.somrp = p_s2->somrc;

    if (gbl_nbLp == 0)
    {
        if (p_s2->cols_c.vlnex + p_s2->cols_q.vlnex + p_s2->cols_s.vlnex   > 0 )

        {
            dne.vlexo = 'O';
        }
        else
        {
           dne.vlexo = 'N';
        }
    }
    else
    {
        if (p_s2->cols_cr.vlnex + p_s2->cols_qr.vlnex + p_s2->cols_sr.vlnex   > 0 )
        {
            dne.vlexo = 'O';
        }
        else
        {
            dne.vlexo = 'N';
        }

    }

    strncpy(dne.csdi,p_e2->csdi, 4);
    dne.revffm   = p_e2->revffm   ;
    dne.seuilm1  = p_e2->seuilm1  ;
    dne.seuilm2  = p_e2->seuilm2  ;
    dne.imaisf   = p_e2->imaisf   ;
    dne.txdegm   = p_e2->txdegm   ;




    ret = th_0kdnc( &dne, &dns);

    if (ret != 0)
    {
        cherche_Erreur(ret,&(p_s2->libelle)) ;
        return (ret);
    }

    {
        p_s2->degpl = dns.degpl;
        p_s2->totth = dns.totth;
        p_s2->netth = dns.netth;
        strncpy(p_s2->codro,dns.codro,3);
        p_s2->tottaham  = dns.tottaham       ;
        p_s2->imap      = dns.imap           ;
        p_s2->degm      = dns.degm           ;
        p_s2->degmaso   = dns.degmaso        ;
        strncpy(p_s2->codegm,dns.codegm,5);
        p_s2->codecm      = dns.codecm           ;
        p_s2->txrefm   = dns.txrefm        ;

    }



    /*    APPEL DU MODULE KRAC    */
    /*    --------------------    */
    if (p_e2->tyimp == 'H')
    {
        {
            p_rae->antax = p_e2->antax;
            strncpy(p_rae->dep,p_e3->dep, 3);
            p_rae->champ = p_e2->champ;
            p_rae->qtvrt = p_e2->qtvrt;
            p_rae->degtv = p_e2->degtv;
            p_rae->rgsor = p_e2->rgsor;
        }

        ret = th_0krac (p_rae ,p_ras);
        if (ret != 0)
        {
            cherche_Erreur(ret,&(p_s2->libelle));
            return (ret);
        }

        {
            p_s2->cottv = p_ras->cottv;
            p_s2->fratv = p_ras->fratv;
            p_s2->somtv = p_ras->somtv;
            p_s2->mdgtv = p_ras->mdgtv;
            p_s2->nettv = p_ras->nettv;
            strncpy(p_s2->roltv,p_ras->roltv, 4);
        }
    }


        /* Cas g�n�ral T ou H : mise � jour du net � payer */

            p_s2->netap = p_s2->netth + p_s2->nettv;


    /* appel fonction regularisation de role CA_TH_0225*/
    if (p_e2->regul=='R')
    {
        regularisation(p_s2->netap ,
                       p_e2->napin,
                       &napdt , &signe);
        p_s2->napdt=napdt;
        p_s2->signe=signe;
    }

    if (p_e2->regul != 'R')
    {
        p_s2->signe = ' ';
        p_s2->napdt = 0;
    }

        /* sortie de THLV */
    if(p_e2->tyimp=='T')
    {
        return (ret) ;
    }


    /* Fusion donnees collectivite N  */
          /* CA_TH_0163 a CA_TH_0169 */
    if (gbl_nbLp > 0)
    {
        /* CA_TH_0168 Copie des resultats de taxation TH N revisee dans les resultats de taxation TH N */
        copieCols(p_s2->cols_cr, &p_s2->cols_c);
        copieCols(p_s2->cols_fr, &p_s2->cols_f);
        copieCols(p_s2->cols_qr, &p_s2->cols_q);
        copieCols(p_s2->cols_rr, &p_s2->cols_r);
        copieCols(p_s2->cols_sr, &p_s2->cols_s);
        copieCols(p_s2->cols_er, &p_s2->cols_e);
        copieCols(p_s2->cols_nr, &p_s2->cols_n);
        copieCols(p_s2->cols_gr, &p_s2->cols_g);




        /* RG17-02 : Fusion des VL brutes CA_TH_0163 */
        p_s2->vlbri = p_s2->vlbir;
        p_s2->vlbni = p_s2->vlbnir;

    }
    else
    {

        /* RG17-02 : Fusion des VL brutes CA_TH_0163 */
        p_s2->vlbirt = p_s2->vlbri;
        p_s2->vlbnirt = p_s2->vlbni;
    }

    /*    SORTIE DU MODULE    */

    return (ret);
}

void majBaseDansS2( s_cols source, s_cols *p_cible)
{
    p_cible->vlntt = source.vlntt;
    p_cible->vlnex = source.vlnex;
    p_cible->bnimp = source.bnimp;
}

/*============================================================================
   Controle generaux
  ============================================================================*/
int controles(s_e2 * p_e2,
              s_e3 * p_e3,
              s_s2 * p_s2)
{
    long quot_abat;  /*  zone traitement quotite  */

    char dep97[]="97";
    char val40[]="40";
    char val50[]="50";

    static char dep21[]="21";  /*initialisation departement 21 */
    double tau1; /* zone retour taux act */
    static char cne1[]="000";  /* initial char sur cne */
    int retour=0;

    retour=strcmp(dep21 , p_e3->dep); /* comparaison si dep 21 */
    if (retour !=0)   /* si dep <> 21 on cherche son taux dans table d'actualisation*/
    {
        tau1=recherche_tdep (p_e3->dep, cne1 , 'H');
        if (tau1==2)  /*retour de la fonction */
        {
            return (2);
        }
    }

    /* traitement code taxation */
    if (((p_e2->tax!='P')&&(p_e2->tax!='S')&&(p_e2->tax!='E')) &&
         (p_e2->tyimp == 'H'))
    {
        return (3);
    }

    /* traitement PAC */
    if (p_e2->nbpac > 99)
    {
        return (4);
    }

    /* traitement code NI a l IR */
    if ((p_e2->codni!='A')&&(p_e2->codni!=' '))
    {
        return (6);
    }

    /* traitement codsh */
    if ((p_e2->codsh!='1')&&(p_e2->codsh!='2')&&(p_e2->codsh!='3')&&
        (p_e2->codsh!='4')&&(p_e2->codsh!='5')&&(p_e2->codsh!=' '))
    {
        return (7);
    }

    /*    traitement tyimp et code nature FIP    */
    if ((p_e2->cnat!='3')&&(p_e2->cnat!='4')&&
        (p_e2->cnat!='5')&&(p_e2->cnat!='6'))
    {
        return (8);
    }

    /*Controle du code affectation du local */
    if ((p_e2->aff!='A')&&(p_e2->aff!='F')&&(p_e2->aff!='H')&&
        (p_e2->aff!='M')&&(p_e2->aff!='S')&&(p_e2->aff!=' '))
    {
        return (10);
    }



    /* nbre pac > 99 */
    if (p_e2->nbera > 99)
    {
        return (16);
    }

    /* code permo diff de 0 1 2 3 4 5 6 7 8 9 ' ' */
    if((p_e2->permo!='0')&&
       (p_e2->permo!='1')&&
       (p_e2->permo!='2')&&
       (p_e2->permo!='3')&&
       (p_e2->permo!='4')&&
       (p_e2->permo!='5')&&
       (p_e2->permo!='6')&&
       (p_e2->permo!='7')&&
       (p_e2->permo!='8')&&
       (p_e2->permo!='9')&&
       (p_e2->permo!=' '))
    {
        return (17);
    }

    /* code grrev diff de  1 2 3 4  ' ' */
    if ((p_e2->grrev!='1')&&(p_e2->grrev!='2')&&
        (p_e2->grrev!='3')&&
        (p_e2->grrev!='4')&&(p_e2->grrev!=' '))
    {
        return (18);
    }

    /* code champ diff de c d e i m s t */
    if ((p_e2->champ!='C')&&(p_e2->champ!='D')&&(p_e2->champ!='E')&&
        (p_e2->champ!='I')&&(p_e2->champ!='M')&&(p_e2->champ!='S')&&
        (p_e2->champ!='R')&&(p_e2->champ!='T')&&(p_e2->tyimp == 'H'))
    {
        return (19);
    }

    /* code qtvrt diff de  0, 1 */
    if ((p_e2->qtvrt!='0')&&(p_e2->qtvrt!='1')&&(p_e2->tyimp == 'H'))
    {
        return (20);
    }

    /* code degtv diff de ejfiadvws ' ' */
    if ((p_e2->degtv!='E')&&(p_e2->degtv!='J')&&(p_e2->degtv!='F')
        &&(p_e2->degtv!='I')&&(p_e2->degtv!='A')&&(p_e2->degtv!='D')
        &&(p_e2->degtv!='V')&&(p_e2->degtv!='S')&&(p_e2->degtv!=' ')
        &&(p_e2->degtv!='Z')&&(p_e2->degtv!='W')&&(p_e2->degtv!='K'))
    {
        return (21);
    }

    /* code degex diff de fgumiadvwsh ' ' */
    if((p_e2->degex!='F')&&(p_e2->degex!='G')&&(p_e2->degex!='I')&&
       (p_e2->degex!='S')&&(p_e2->degex!='A')&&(p_e2->degex!='D')&&
       (p_e2->degex!='V')&&(p_e2->degex!='W')&&
       (p_e2->degex!='K')&&(p_e2->degex!=' '))
    {
        return (22);
    }

    /* code TYIMP diff de h et t   */
    if ((p_e2->tyimp!='H')&&(p_e2->tyimp!='T'))
    {
        return (23);
    }

    /* code TYIMP egal a t , nbpac nbera diff 0 */
    if ((p_e2->tyimp=='T')&&((p_e2->nbpac!=0)||(p_e2->nbera!=0)))
    {
        return (25);
    }

    /* code TYIMP egal a t , degex servi */
    if ((p_e2->tyimp=='T')&&(p_e2->degex!=' '))
    {
        return (26);
    }

     /* code TYIMP egal a t , codni servi */
    if ((p_e2->tyimp=='T')&&(p_e2->codni!=' '))
    {
        return (27);
    }

    /* code TYIMP egal a t , champ TV servi */
    if ((p_e2->tyimp=='T')&&(p_e2->champ!=' '))
    {
        return (28);
    }

    /* code TYIMP egal a t , degtv servi */
    if ((p_e2->tyimp=='T')&&(p_e2->degtv!=' '))
    {
        return (29);
    }

    /* code TYIMP egal a t , qtvrt servi */
    if ((p_e2->tyimp=='T')&&(p_e2->qtvrt!=' '))
    {
        return (30);
    }

    /* code TYIMP egal a t , tax servi */
    if ((p_e2->tyimp=='T')&&(p_e2->tax!=' '))
    {
        return (35);
    }



    /* indicateur majoration THS diff de O et N*/
    if ((p_e3->indmths!='N')&& (p_e3->indmths!='O'))
    {
        return (39);
    }

    /* Controle du p�rim�tre du champ d'application de la majoration THS */
    if(((p_e3->indmths) == 'O') && (recherche_dep_TLV(p_e3->dep) == 0))
    {
        return (40);
    }


    /* Controle taux d'imposition de la majoration THS */
    if ((p_e3->indmths == 'O') && (p_e3->timths > 0.60))
    {
        return (41);
    }

    /* Controle indicateur d'assujetissement a la majoration THS */
    if ((p_e2->indaths != 'O') && (p_e2->indaths != ' '))
    {
        return (42);
    }

    /* regul different de R */
    if ((p_e2->regul!='R')&&(p_e2->regul!= ' '))
    {
        return(5201);
    }

    /* comparaison  annee de reference */
    if (p_e2->antax != p_e3->antax)
    {
        return (51);
    }

    /* code tax  S ou E et pac # zero */
    if ((p_e2->tax == 'S' || p_e2->tax == 'E') && ((p_e2->nbpac != 0) || (p_e2->nbera != 0)))
    {
        return (52);
    }

    /* code tax S et code exoneration servi */
    if ((p_e2->tax == 'S')&&(p_e2->degex != ' '))
    {
        return (54);
    }

    /* code tax S et code ni = "A" */
    if ((p_e2->tax == 'S')&&(p_e2->codni == 'A'))
     {
     return (55);
     }

    /* code deg D ,code DEP # 97 */
    retour=strcmp(p_e3->dep,dep97); /* comparaison si dep 97 */
    if (retour!=0) /* traitement metropole */
    {
        if (p_e2->degex=='D')
        {
            return (58);
        }
    }

    /* 31/01/2012 - DMA */
    /*  code deg A V  , code tax P ,code ni # A , mill.>= 2012 */
    if ((p_e2->degex == 'A' || p_e2->degex == 'V') &&
        (p_e2->tax =='P' && p_e2->codni != 'A'))
    {
        return (60);
    }

    /* code deg G , code nature cnat # 5 */
    if ((p_e2->degex == 'G') && (p_e2->cnat != '5'))
    {
        return (61);
    }

    /* champ # c  qtvrt = 1 */
    if ((p_e2->champ != 'C') && (p_e2->qtvrt == '1'))
    {
        return (65);
    }

    /* champ = c  tax = e */
    if ((p_e2->champ == 'C') && (p_e2->tax == 'E'))
    {
        return (66);
    }

    /* champ = c  cnat = 5 */
    if ((p_e2->champ == 'C') && (p_e2->cnat == '5'))
    {
        return (67);
    }

    /* champ # c  degtv servi */
    if ((p_e2->champ != 'C') && (p_e2->degtv != ' '))
    {
        return (69);
    }

    /* degtv = e j  degth # h z ' ' */
    if (((p_e2->degtv == 'E') || (p_e2->degtv == 'J'))&&
        /*Bug 271322*/
     /*   ((p_e2->degex!='H')&&(p_e2->degex!=' ')))*/
     ((p_e2->degex!='K')&&(p_e2->degex!=' ')))
        /*Fin Bug 271322*/    {
        return (70);
    }

    /* CA_ANO_0384 Anomalie 73 - Locaux professionnels hors champ THLV */
    if ((p_e2->tyimp == 'T') && (gbl_nbLp > 0))
    {
        return(73);
    }


    /*CA_ANO_0396 */
    /*2019MAJR18-03-01 */
    if (((strchr("MD",p_e2->iman1)!=NULL) && p_e2->txdegm ==0) || ((p_e2->iman1 == ' ') && (p_e2->txdegm > 0 )))
    {
      return (74);
    }
     /*FIN 2019MAJR18-03-01 */


    /* 2019MAJR14-05-01 */
    /* CA_ANO_0355 */
    /* Controle du rang de sortie en sifflet exoneration TH */
    if (((p_e2->degex == 'W') && (p_e2->rgsor != ' ')) ||
       ((p_e2->degex == 'K') && ((p_e2->rgsor != '1') && (p_e2->rgsor != '2') && (p_e2->rgsor != '3')
                             &&  (p_e2->rgsor != '4') && (p_e2->rgsor != '5') && (p_e2->rgsor != '6'))) ||
      (((p_e2->degex != 'K') && (p_e2->degex != 'W')) && (p_e2->rgsor != ' ')))
    {
        return(77);
    }
    /* FIN 2019MAJR14-05-01 */

    /* 2019MAJR14-01-01 */
    /* CA_ANO_0358 Controle du code abattement TH et du rang de sortie quand sortie en sifflet */
    if (((p_e2->abatk== 'K') && ((p_e2->rgsork != '3') && (p_e2->rgsork != '4') && (p_e2->rgsork != '5') && (p_e2->rgsork != '6'))) ||
        ((p_e2->abatk== ' ') && (p_e2->rgsork != ' ')))
    {
        return(78);
    }
    /* FIN 2019MAJR14-01-01 */

    /* Codes abattement et exoneration/degrevement sont exclusifs sauf en cas de plafonnement */
    if(((p_e2->degex=='A')||(p_e2->degex=='F')||(p_e2->degex=='I')||(p_e2->degex=='D')||
        (p_e2->degex=='S')||(p_e2->degex=='V')||(p_e2->degex=='W')||
        (p_e2->degex=='K')) && (p_e2->abatk=='K'))
    {
        return(79);
    }

    /* Le code abattement TH est incompatible avec une THS */
    if (p_e2->tax=='S' && p_e2->abatk=='K')
    {
        return(80);
    }

    /* test vl nulle ou > 200000 */
    if ((p_e3->cole_c.vlmoy==0)||(p_e3->cole_c.vlmoy>200000))
    {
        return (101);
    }

    /* quotite abat. pac rang 1 et 2 et plus */
    retour=strcmp(p_e3->dep, dep97);  /* comparaison si dep 97 */
    if (retour !=0) /* trait metropole */
    {
        quot_abat=arrondi_dizaine_inf((p_e3->cole_f.vlmoy * 0.1)-1);
        if ((p_e3->cole_f.apac1 < quot_abat)&& /* trait 1 ou 2 pac */
            (p_e3->codef==' ')&&(p_e3->codeg==' '))
        {
            return (102);
        }
        quot_abat=arrondi_dizaine_inf((p_e3->cole_f.vlmoy * 0.15)-1); /*trait + de 2 pac*/
        if ((p_e3->cole_f.apac3 < quot_abat)&&(p_e3->cole_f.apac3 > 0))
        {
            return (103);
        }
    }
    else /* trait hors metropole */
    {
        quot_abat=arrondi_dizaine_inf((p_e3->cole_f.vlmoy * 0.05)-1); /* trait 1 ou 2 pac */
        if ((p_e3->cole_f.apac1 < quot_abat)&&
            (p_e3->codef==' ')&&(p_e3->codeg==' '))
        {
            return (102);
        }
        quot_abat=arrondi_dizaine_inf((p_e3->cole_f.vlmoy * 0.05)-1); /* trait + de 2 pac */
        if (p_e3->cole_f.apac3 < quot_abat)
        {
            return (103);
        }
    }

    /* quotite abat . cne general a la base */
    retour =strcmp(p_e3->dep,dep97);  /* comparaison si dep 97 */
    if (retour !=0) /* trait metropole */
    {
        if (!( strcmp(p_e3->cole_f.txbas,"F ") == 0))
        {
            quot_abat=arrondi_euro_inf((p_e3->cole_f.vlmoy * 0.15)+1);
            if (p_e3->cole_f.abbas > quot_abat)
            {
                return (104);
            }
        }
    }

    /* quotite abat . cne special a la base */
    quot_abat=arrondi_euro_inf((p_e3->cole_f.vlmoy * 0.15)+1);
    if (p_e3->cole_f.abspe > quot_abat)
    {
        return (105);
    }

    /*  anomalie PAC alsace moselle */
    if (((p_e3->codeg!=' ')||(p_e3->codef!=' '))&&
        ((p_e3->cole_f.apac1>0)||(p_e3->cole_f.apac3 > 0)))
    {
        return (106);
    }

    if ((p_e3->codef == ' ')&&(p_e3->codeg == ' ')&&(p_e3->cole_f.apac1 == 0))
    {
        return (107);
    }

    if ((p_e3->codef == ' ')&&(p_e3->codeg == ' ')&&(p_e3->cole_f.apac3 == 0))
    {
        return (108);
    }

    if (p_e3->codeg != ' ')
    {
        return (109);
    }

    /* indicateur CU # c ou blanc */
    if ((p_e3->cocnq!='C')&&(p_e3->cocnq!=' '))
    {
        return (111);
    }

    /* alsace moselle codef servi , abmos doit etre positif */
    if ((p_e3->codef!=' ')&&(p_e3->abmos <= 0))
    {
        return (114);
    }

    /* a n o m a l i e  115 */
    if (p_e3->vmd89 > 200000)
    {
        return (115);
    }

    /* a n o m a  l i e  116 */
    retour=strcmp(p_e3->dep,dep97);
    /*Modif AMN le 03/09/2008 fiche 33972 if  ((p_e3->vmd89 == 0)*/
    if ((p_e3->vmd89 == 0) && (retour == 0))
    {
        return (116);
    }

    /* a n o m a  l i e  117 */
    retour=strcmp(p_e3->dep,dep97);
    if ((retour != 0) && (p_e3->vmd89 != 0))
    {
        return (117);
    }

    /* a n o m a  l i e  118 */
    /* Autre ecriture de l'ano 118 */
    /* Modif AMN le 03/09/2008 fiche 33972 if (
        ( strcmp(p_e3->dep,dep97)) */
    if (( !strcmp(p_e3->dep,dep97))&&
       ((strcmp(p_e3->cole_c.txbas , val40))&&
        (strcmp(p_e3->cole_c.txbas , val50))))
    {
        return (118);
    }

    /* quotite d'abattements ASH commune */
    if (!(strcmp(p_e3->cole_f.txhan,"  ") == 0) && !(strcmp(p_e3->cole_f.txhan,"00") == 0))
    {
        if ((p_e3->cole_f.abhan > 0.2*p_e3->cole_f.vlmoy + 1) ||
            (p_e3->cole_f.abhan < 0.1*p_e3->cole_f.vlmoy - 1))
        {
            return (119);
        }
    }

    /*   trait vl moyenne groupement de cnes */
    if (p_e3->cole_q.vlmoy > 200000)
    {
        return (201);
    }

    retour=strcmp(p_e3->dep, dep97);  /* comparaison si dep 97 */
    if (retour !=0) /* trait metropole */
    {
        quot_abat=arrondi_dizaine_inf((p_e3->cole_r.vlmoy * 0.1)-1);
        if ((p_e3->cole_r.apac1 < quot_abat) && (p_e3->cole_r.apac1 > 0)) /* trait 1 ou 2 pac */
        {
            return (202);
        }
        quot_abat=arrondi_dizaine_inf((p_e3->cole_r.vlmoy * 0.15)-1); /*trait + de 2 pac*/
        if ((p_e3->cole_r.apac3 < quot_abat)&&(p_e3->cole_r.apac3 > 0))
        {
            return (203);
        }
    }
    else /* trait hors metropole */
    {
        quot_abat=arrondi_dizaine_inf((p_e3->cole_r.vlmoy * 0.05)-1); /* trait 1 ou 2 pac */
        if ((p_e3->cole_r.apac1 < quot_abat) && (p_e3->cole_r.apac1 > 0))
        {
            return (202);
        }
        quot_abat=arrondi_dizaine_inf((p_e3->cole_r.vlmoy * 0.05)-1); /* trait + de 2 pac */
        if ((p_e3->cole_r.apac3 < quot_abat) && (p_e3->cole_r.apac3 > 0))
        {
            return (203);
        }
    }

    /*   quotite abat group cne abat general base  */
    quot_abat=arrondi_euro_inf((p_e3->cole_r.vlmoy * 0.15)+1);
    if (p_e3->cole_r.abbas > quot_abat)
    {
        return (204);
    }

    /*   quotite abat group cne abat special base  */
    quot_abat=arrondi_euro_inf((p_e3->cole_r.vlmoy * 0.15)+1);
    if (p_e3->cole_r.abspe > quot_abat)
    {
        return (205);
    }

    /* quot. abatt. group.comm # zeros */
    if (((p_e3->cole_r.vlmoy != 0) || (p_e3->cole_r.abspe != 0 ) ||
        (p_e3->cole_r.abbas != 0) || (p_e3->cole_r.apac1 != 0) ||
        (p_e3->cole_r.apac3 != 0))&&(p_e3->cocnq == ' '))
    {
        return (206);
    }

    /* taux imp. group cne non nul et CU a blanc */
    if ((p_e3->timpq != 0) && (p_e3->cocnq == ' '))
    {
        return (211);
    }

    /* quotite d'abattements ASH inter-comm */
    if (!(strcmp(p_e3->cole_r.txhan,"  ") == 0) && !(strcmp(p_e3->cole_r.txhan,"00") == 0))
    {
        if ((p_e3->cole_r.abhan > 0.2*p_e3->cole_r.vlmoy + 1) ||
            (p_e3->cole_r.abhan < 0.1*p_e3->cole_r.vlmoy - 1))
        {
            return (212);
        }
    }

    /* Declenchement du message 110 si somme des taux d'imposition anormalement �lev�e */
    if ((p_e3->timpc + p_e3->timpq + p_e3->tisyn + p_e3->titsn + p_e3->titgp+ p_e3->timpe) > 1)/* c'est a dire 100% */
    {
        return (110);
    }
    /* CA_ANO_0378 */
    if ((p_e3->timpc7 + p_e3->timpq7 + p_e3->tisyn7 + p_e3->titsn7 + p_e3->titgp7 + p_e3->timpe7) > 1)/* c'est a dire 100% */
    {
        return (157);
    }

       /* CA_ANO_0402 */
    if (p_e2->nbpac < 0)
    {
        return (3022);
    }

       /* CA_ANO_0403 */
    if (p_e2->nbera < 0)
    {
        return (3023);
    }


    /* tous les controles sont corrects */
    return (0);
}
/*------------------------ fin des controles generaux ------------------------*/

/*----------------------------------------------------------------------------*/
/*                    Initialisation de la zone de sortie                     */
/*----------------------------------------------------------------------------*/

void  initialisation_sortie(s_s2 * p_s2)
{
    p_s2->vlbri = 0;
    p_s2->vlbni = 0;
    p_s2->vlbr = 0;               /*VL brute revisee N                  */
    p_s2->vlbrt = 0;              /*VL brute revisee N TSE              */
    p_s2->vlbr7 = 0;              /*VL brute revisee 2017               */
    p_s2->vlbrt7 = 0;             /*VL brute revisee 2017 TSE           */
    p_s2->vlb77 = 0;              /*VL brute base 70 2017               */
    p_s2->vlbir = 0;              /*VL brute imposee revisee N          */
    p_s2->vlbirt = 0;             /*VL brute imposee revisee N TSE      */
    p_s2->vlbir7 = 0;             /*VL brute imposee revisee 2017       */
    p_s2->vlbir7t = 0;            /*VL brute imposee revisee 2017 TSE   */
    p_s2->vlbi77 = 0;             /*VL brute imposee base 70 2017       */
    p_s2->vlbnir = 0;             /*VL brute non imposee revisee N      */
    p_s2->vlbnirt = 0;            /*VL brute non imposee revisee N TSE  */
    p_s2->vlbnir7 = 0;            /*VL brute non imposee revisee 2017   */
    p_s2->vlbnir7t = 0;           /*VL brute non imposee revisee 2017TSE*/
    p_s2->vlbni77 = 0;            /*VL brute non imposee base 70 2017   */
    p_s2->cotic = 0;
    p_s2->cotiq = 0;
    p_s2->cotis = 0;
    p_s2->cotin = 0;
    p_s2->cotig = 0;
    p_s2->cotie = 0;
    p_s2->cotilc = 0;
    p_s2->cotilq = 0;
    p_s2->cotils = 0;
    p_s2->cotiln = 0;
    p_s2->cotilg = 0;
    p_s2->cotile = 0;
    p_s2->palis  = 0;
    p_s2->lissac = 0;
    p_s2->lissaq = 0;
    p_s2->lissas = 0;
    p_s2->lissan = 0;
    p_s2->lissag = 0;
    p_s2->lissae = 0;
    p_s2->qtprltc = 0;
    p_s2->qtprltq = 0;
    p_s2->qtprlts = 0;
    p_s2->qtprltn = 0;
    p_s2->qtprltg = 0;
    p_s2->qtprlte = 0;
    p_s2->frait = 0;
    p_s2->frai4 = 0;
    p_s2->prelt = 0;
    p_s2->majths = 0;
    p_s2->limajths = 0; /* Bug 212403 amelioration */
    p_s2->coticm = 0;
    p_s2->somrc = 0;
    p_s2->degpl = 0;
    p_s2->totth = 0;
    p_s2->netth = 0;
    p_s2->somrp = 0;
    p_s2->pre02 = 0;
    p_s2->pre12 = 0;
    p_s2->pre17 = 0;
    strncpy(p_s2->codro, "  ",3);
    p_s2->cottv = 0;
    p_s2->fratv = 0;
    p_s2->somtv = 0;
    p_s2->mdgtv = 0;
    p_s2->nettv = 0;
    strncpy(p_s2->roltv, "   ",4);
    p_s2->netap = 0;
    p_s2->signe = ' ';
    p_s2->napdt = 0;
    p_s2->tottaham = 0;
    p_s2->imap  = ' ';
    p_s2->degm  = 0;
    p_s2->degmaso = 0;
    strncpy(p_s2->codegm, "    ",5);
    p_s2->codecm = ' ';
    p_s2->txrefm = 0;
    p_s2->preths = 0;
    p_s2->fgest_cq = 0;
    p_s2->fgest_s  = 0;
    p_s2->fgest_ng = 0;
    p_s2->fgest_e  = 0;
    p_s2->far_cq   = 0;
    p_s2->far_s    = 0;
    p_s2->far_ng   = 0;
    p_s2->far_e    = 0;


    /*Abattements applicables � la c�te*/
    init_cols(&p_s2->cols_c);          /*structure sortie commune                            */
    init_cols(&p_s2->cols_q);          /*structure sortie intercommunalite                   */
    init_cols(&p_s2->cols_s);          /*structure sortie syndicat                           */
    init_cols(&p_s2->cols_n);          /*structure sortie TSE                                */
    init_cols(&p_s2->cols_g);          /*structure sortie TSE Autre                          */
    init_cols(&p_s2->cols_e);          /*structure sortie GEMAPI                             */
    init_cols(&p_s2->cols_f);          /*structure sortie commune avant ajustement           */
    init_cols(&p_s2->cols_r);          /*structure sortie intercommunalite avant ajustement  */

    init_cols(&p_s2->cols_cr);          /*structure sortie commune r�vis�e N                           */
    init_cols(&p_s2->cols_qr);          /*structure sortie intercommunalite  r�vis�e N                 */
    init_cols(&p_s2->cols_sr);          /*structure sortie syndicat     r�vis�e N                      */
    init_cols(&p_s2->cols_nr);          /*structure sortie TSE          r�vis�e N                      */
    init_cols(&p_s2->cols_gr);          /*structure sortie TSE Autre     r�vis�e N                     */
    init_cols(&p_s2->cols_er);          /*structure sortie GEMAPI        r�vis�e N                     */
    init_cols(&p_s2->cols_fr);          /*structure sortie commune avant ajustement   r�vis�e N        */
    init_cols(&p_s2->cols_rr);          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/


    init_cols(&p_s2->cols_c7);          /*structure sortie commune 2017 base 70               */
    init_cols(&p_s2->cols_q7);          /*structure sortie intercommunalite 2017 base 70      */
    init_cols(&p_s2->cols_s7);          /*structure sortie syndicat 2017 base 70              */
    init_cols(&p_s2->cols_n7);          /*structure sortie TSE 2017 base 70                   */
    init_cols(&p_s2->cols_g7);          /*structure sortie TSE autre 2017 base 70             */
    init_cols(&p_s2->cols_e7);          /*structure sortie GEMAPI 2017 base 70                */

    init_cols(&p_s2->cols_cr7);         /*structure sortie commune 2017 revisee               */
    init_cols(&p_s2->cols_qr7);         /*structure sortie intercommunalite 2017 revisee      */
    init_cols(&p_s2->cols_sr7);         /*structure sortie syndicat 2017 revisee              */
    init_cols(&p_s2->cols_nr7);         /*structure sortie TSE 2017 revisee                   */
    init_cols(&p_s2->cols_gr7);         /*structure sortie TSE autre 2017 revisee             */
    init_cols(&p_s2->cols_er7);         /*structure sortie GEMAPI 2017 revisee                */
}

void  initialisation_sortieQtp( s_qtpAb *p_qtp)
{
    init_cols(&p_qtp->qtp_cr);          /*structure sortie commune r�vis�e N                           */
    init_cols(&p_qtp->qtp_qr);          /*structure sortie intercommunalite  r�vis�e N                 */
    init_cols(&p_qtp->qtp_sr);          /*structure sortie syndicat     r�vis�e N                      */
    init_cols(&p_qtp->qtp_nr);          /*structure sortie TSE          r�vis�e N                      */
    init_cols(&p_qtp->qtp_gr);          /*structure sortie TSE Autre     r�vis�e N                     */
    init_cols(&p_qtp->qtp_er);          /*structure sortie GEMAPI        r�vis�e N                     */
    init_cols(&p_qtp->qtp_fr);          /*structure sortie commune avant ajustement   r�vis�e N        */
    init_cols(&p_qtp->qtp_rr);          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/



    init_cols(&p_qtp->qtp_c7);          /*structure sortie commune 2017 base 70               */
    init_cols(&p_qtp->qtp_q7);          /*structure sortie intercommunalite 2017 base 70      */
    init_cols(&p_qtp->qtp_s7);          /*structure sortie syndicat 2017 base 70              */
    init_cols(&p_qtp->qtp_n7);          /*structure sortie TSE 2017 base 70                   */
    init_cols(&p_qtp->qtp_g7);          /*structure sortie TSE autre 2017 base 70             */
    init_cols(&p_qtp->qtp_e7);          /*structure sortie GEMAPI 2017 base 70                */

    init_cols(&p_qtp->qtp_cr7);         /*structure sortie commune 2017 revisee               */
    init_cols(&p_qtp->qtp_qr7);         /*structure sortie intercommunalite 2017 revisee      */
    init_cols(&p_qtp->qtp_sr7);         /*structure sortie syndicat 2017 revisee              */
    init_cols(&p_qtp->qtp_nr7);         /*structure sortie TSE 2017 revisee                   */
    init_cols(&p_qtp->qtp_gr7);         /*structure sortie TSE autre 2017 revisee             */
    init_cols(&p_qtp->qtp_er7);         /*structure sortie GEMAPI 2017 revisee                */
}

void   init_cols(s_cols *  p_c)
{
    p_c->abgen = 0;
    p_c->abpac = 0;
    p_c->abp12 = 0;
    p_c->abspe = 0;
    p_c->abhan = 0;
    p_c->vlntt = 0;
    p_c->vlnex = 0;
    p_c->bnimp = 0;
    p_c->absps = ' '; /* CA_TH_0117 */
    p_c->abspsa = ' ';
    p_c->coti = 0;
}

void initialisation_coe1(s_coe1 *p_coe)
{
    init_cols(p_coe->p_cols_c);
    init_cols(p_coe->p_cols_q);
    init_cols(p_coe->p_cols_s);
    init_cols(p_coe->p_cols_n);
    init_cols(p_coe->p_cols_g);
    init_cols(p_coe->p_cols_e);
    init_cols(p_coe->p_cols_f);
    init_cols(p_coe->p_cols_r);

}

void initialisation_coe2(s_coe2 *p_coe2)
{
     p_coe2->timpc =0;
     p_coe2->tisyn =0;
     p_coe2->titsn =0;
     p_coe2->timpq =0;
     p_coe2->titgp =0;
     p_coe2->timpe =0;
     p_coe2->timths=0;
}

/* retourne 1 si indloc est renseigne */
int isLocPro (char indloc[])
{
    int lg = strlen (indloc);
    int i;
    for(i=0 ; i < lg ; i++)
    {
        if(!(isspace(indloc[i])) && indloc[i] != '\0')
        {
            return (1);
        }
    }
    return (0);
}

void copieCols (s_cols source , s_cols *p_dest)
{
    p_dest->abgen=source.abgen;
    p_dest->abpac=source.abpac;
    p_dest->abp12=source.abp12;
    p_dest->abspe=source.abspe;
    p_dest->abhan=source.abhan;
    p_dest->vlntt=source.vlntt;
    p_dest->vlnex=source.vlnex;
    p_dest->bnimp=source.bnimp;
    p_dest->absps=source.absps;

    /* Bug 214745 ajout ligne*/
    p_dest->abspsa=source.abspsa;

    p_dest->coti =source.coti;
}
