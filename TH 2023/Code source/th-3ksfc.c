

#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>

#else
    #include "th-3ksts"
    #include "th-3kfos"

#endif


/* La fonction th_3ksfc permet de determiner les droits a degrevements/
   exoneration et le revenu fiscal de reference a prendre en compte pour le
   calcul du plafonnement de taxe d'habitation a 3,5% du Revenu Fiscal de
   Reference, en fonction des elements de personnalisation et des revenus
   (informations IR).                                                         */

/*============================================================================
   M A I N
  ============================================================================*/
int th_3ksfc(s_e4 * p_e4, s_s4 * p_s4)
{
    /* DECLARATIONS ET INITIALISATIONS */
    static s_signature signature;
    static char dep97[]="97";
    static char dir973[]="973";
    static char dir976[]="976";


    int ret_cons=0;  /* zone retour de recherche des constantes */
    int ret=0;
    short demipart1=0;
    short demipart2=0;
    short demipart3=0;
    short demipart4=0;
    short autredemipart=0;
    short quartpart1=0;
    short quartpart2=0;
    short quartpart3=0;
    short quartpart4=0;
    short autrequartpart=0;

    long   srevf=0;
    long nbpart_cast = 0;

    s_ese         ese;
    s_sse         sse;
    s_cons * p_cons;
    s_typseuil * pc;
    s_rev_m * p_revm;

    p_s4->versr='A';
    p_s4->anref=ANREF;

    p_s4->revffm = 0;
    p_s4->npaff = 0;
    p_s4->npaffm = 0;
    p_s4->seuilm1 = 0;
    p_s4->seuilm2 = 0;
    p_s4->imhisf = ' ';
    p_s4->imaisf = ' ';

    init_s4(&(p_s4->reds_1));
    init_s4(&(p_s4->reds_2));
    init_s4(&(p_s4->reds_c));
    init_s4(&(p_s4->reds_f));
    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_s4->signature = &signature  ;

    /*============*/
    /* TRAITEMENT */
    /*============*/

    /* controle de la signature */

    ret = controle_signature(RKSFC,p_s4->versr, &(p_s4->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* 1 appel de la fonction taux annuel */

    ret_cons = (short)cherche_const(p_e4->antax,&p_cons);
    cherche_Erreur(ret_cons,&(p_s4->libelle)) ;
    if (ret_cons != 0)               /* retour de la fonction */
    {
        return (ret_cons);
    }

    /* 2 appel de la fonction controle du th_ksfc */
    ret = controle_ksfc(p_e4 , p_s4);
    if ( ret !=0 )
    {
        cherche_Erreur(ret,&(p_s4->libelle)) ;
        return ret;
    }

    p_revm = &(p_cons->rev_macron);

    /* 3 determination du departement pour chargement du pointeur sur typseuil
     */
    if ( strncmp(p_e4->csdi,dep97,2) == 0 )
    {
        pc = &(p_cons->typseuil_dom_124);              /* trait dom */
        if ( strcmp(p_e4->csdi,dir973) == 0 )
        {
            pc = &(p_cons->typseuil_973);         /* trait guyane */
        }
        else
        if ( strcmp(p_e4->csdi,dir976) == 0 )
        {
            pc = &(p_cons->typseuil_976);         /* trait Mayotte */
        }
    }
    else
    {
        pc = &(p_cons->typseuil_metro);                /* trait metropole */
    }


    /* 4    appel du module th_3ksec  */
    ese.antax = p_e4->antax;
    strncpy(ese.csdi , p_e4->csdi, 4);
    ese.cnua = p_e4->cnua;
    ese.rede_1.revfe = p_e4->rede_1.revfe;
    ese.rede_1.nbpar = p_e4->rede_1.nbpar;
    ese.rede_1.front = p_e4->rede_1.front;
    ese.rede_1.annir = p_e4->rede_1.annir;
    ese.rede_2.revfe = p_e4->rede_2.revfe;
    ese.rede_2.nbpar = p_e4->rede_2.nbpar;
    ese.rede_2.front = p_e4->rede_2.front;
    ese.rede_2.annir = p_e4->rede_2.annir;

    ret = th_3ksec(&ese,&sse);         /* appel du th_3ksec */

    if (ret != 0)                      /* code retour du th_3ksec */
    {
        cherche_Erreur(ret,&(sse.libelle));
        p_s4->libelle=sse.libelle;
        return (ret);
    }

    /* recuperation des zones sortie du th_3ksec */
    p_s4->reds_1.seuil = sse.reds_1.seuil;
    p_s4->reds_1.seuilb= sse.reds_1.seuilb;
    p_s4->reds_1.revim = sse.reds_1.revim;
    p_s4->reds_2.seuil = sse.reds_2.seuil;
    p_s4->reds_2.seuilb= sse.reds_2.seuilb;
    p_s4->reds_2.revim = sse.reds_2.revim;
    p_s4->reds_c.seuil = sse.reds_c.seuil;
    p_s4->reds_c.seuilb= sse.reds_c.seuilb;
    p_s4->reds_c.revim = sse.reds_c.revim;

    /* 5    seuil de synthese du foyer fiscal  */
    if (p_e4->cnua == 'R')
    {
        p_s4->reds_f.seuil = ' ';
        p_s4->reds_f.seuilb= ' ';
        p_s4->reds_f.revim = 0;
        p_s4->npaff = 0;
        srevf = 0;
        return 0;
    }
    else
    {
        /* calcul du nb de parts total et du revenu total */
        p_s4->npaff = p_e4->rede_r.nbpar;
        srevf = p_e4->rede_r.revfe;
        if ((p_e4->rede_1.front != 'K' ) || (p_s4->reds_1.revim != 0 ))
        {
            p_s4->npaff = p_s4->npaff + p_e4->rede_1.nbpar;
            srevf = srevf + p_s4->reds_1.revim;
        }
        if ((p_e4->rede_2.front != 'K' ) || (p_s4->reds_2.revim != 0 ))
        {
            p_s4->npaff = p_s4->npaff + p_e4->rede_2.nbpar;
            srevf = srevf + p_s4->reds_2.revim;
        }
        p_s4->reds_f.revim = srevf;
    }

    if (p_s4->npaff != 0) /* il existe un declarant (eventuellement rattache) */
    {
        determination_parts(p_s4->npaff,&demipart1,&demipart2,&demipart3,&demipart4,&autredemipart,
                            &quartpart1,&quartpart2,&quartpart3,&quartpart4,&autrequartpart);

        /* 2020 exigence 6 (CA_SEUIL_0008) */
            if (p_s4->reds_c.seuilb != ' ')
            {
                if ( p_e4->rede_1.front == 'K' && p_e4->rede_1.revfe == 0 ) /* rede_1 frontalier avec revenu nul*/
                {
                   p_s4->reds_f.seuilb='1';
                }
                else
                {
                    if ( p_e4->cnua =='C' &&  p_e4->rede_2.front == 'K' && p_e4->rede_2.revfe == 0 )
                    {
                         p_s4->reds_f.seuilb='1';
                    }
                    else
                    {
                        if(p_e4->rede_r.front == 'K')
                        {
                            p_s4->reds_f.seuilb='1';
                        }
                        else
                        {
                            p_s4->reds_f.seuilb = determination_droits_w(srevf,p_s4->npaff,pc);
                        }
                    }
                }
            }
            else
            {
                p_s4->reds_f.seuilb = ' ';
            }

        /* fin 2020 exigence 6 (CA_SEUIL_0008) */

        /* non frontalier et declarant */
        if ((p_e4->rede_1.front == 'K') || (p_e4->rede_2.front == 'K') || (p_e4->rede_r.front == 'K'))
        {
            p_s4->reds_f.seuil = ' ';
        }
        else
                    /* 2020majr07-03-04 test sur nbpar et non sur revfe */
            if (p_e4->cnua == 'S')
            {
                /* taxation simple avec rattache declarant */
                if (p_e4->rede_1.nbpar != 0) /*taxation simple avec ou sans rattache pas de notion dinconnu ir ou non declarant pour le groupe rattache*/
                    {
                        p_s4->reds_f.seuil = 'A';
                    }
                    else
                    {
                        p_s4->reds_f.seuil = ' ';
                    }
            }
            else  /* taxation conjointe */
            {
                if ((p_e4->rede_1.nbpar != 0) && (p_e4->rede_2.nbpar != 0))  /* taxation conjointe avec ou sans rattache */
                    {
                        p_s4->reds_f.seuil = 'A';
                    }
                    else
                    {
                        p_s4->reds_f.seuil = ' ';
                    }

            /* fin 2020majr07-03-04 test sur nbpar et non sur revfe */
            }
    }
    else                             /* non declarant */
    {
        p_s4->reds_f.seuil = ' ';
        p_s4->reds_f.revim = 0;
    }

    if (p_s4->npaff != 0) /* il existe un declarant (eventuellement rattache) */
    {
        determination_parts(p_s4->npaff,
                            &demipart1,
                            &demipart2,
                            &demipart3,
                            &demipart4,
                            &autredemipart,
                            &quartpart1,
                            &quartpart2,
                            &quartpart3,
                            &quartpart4,
                            &autrequartpart);
    }
    /*2020 MAJ07-04-01 � MAJ07-04-06 modif param�tres par ajout d'un param */
    p_s4->reds_1.serfrc = restitution_seuil(pc, p_e4->rede_1.nbpar, p_s4->reds_1.revim, p_revm);
    p_s4->reds_2.serfrc = restitution_seuil(pc, p_e4->rede_2.nbpar, p_s4->reds_2.revim, p_revm);
    p_s4->reds_c.serfrc = restitution_seuil(pc, p_e4->rede_1.nbpar
                                               +p_e4->rede_2.nbpar, p_s4->reds_c.revim, p_revm);
    /*fin 2020 MAJ07-04-01 � MAJ07-04-06 */

    /*************** CA_SEUIL_0009 et CA_SEUIL_0010  *****************/
    /********************** Mesures Macron **************************/

        p_s4->revffm = p_e4->rede_1.revfe + p_e4->rede_2.revfe + p_e4->rede_r.revfe + p_e4->revfenr; /* CA_SEUIL_0009 */
        p_s4->npaffm = p_e4->rede_1.nbpar + p_e4->rede_2.nbpar + p_e4->rede_r.nbpar + p_e4->nbparnr; /* CA_SEUIL_0010 */

        /* Caster le nombre de parts en long pour les calculs */
        nbpart_cast = (long) p_s4->npaffm;

        /* CA_SEUIL_0011 seuils applicables au foyer */
        if (p_s4->npaffm == 100)
        {
            p_s4->seuilm1 = p_cons->rev_macron.rev_macb1_prem_part;
            p_s4->seuilm2 = p_cons->rev_macron.rev_macb2_prem_part;
        }
        if ((p_s4->npaffm > 100) && (p_s4->npaffm <= 200))
        {
            p_s4->seuilm1 = p_cons->rev_macron.rev_macb1_prem_part
            + arrondi_euro_voisin((double)((p_revm->rev_macb1_prem_demi_part * 2 * ((nbpart_cast - 100)*0.01))));
            p_s4->seuilm2 = p_cons->rev_macron.rev_macb2_prem_part
            + arrondi_euro_voisin((double)((p_revm->rev_macb2_prem_demi_part * 2 * ((nbpart_cast - 100)*0.01))));
        }
        if (p_s4->npaffm > 200)
        {
            p_s4->seuilm1 = p_cons->rev_macron.rev_macb1_prem_part
                            + (p_cons->rev_macron.rev_macb1_prem_demi_part * 2)
                            + arrondi_euro_voisin((double)((p_revm->rev_macb1_autr_demi_part * 2 * ((nbpart_cast - 200)*0.01))));

            p_s4->seuilm2 = p_cons->rev_macron.rev_macb2_prem_part
                            + (p_cons->rev_macron.rev_macb2_prem_demi_part * 2)
                            + arrondi_euro_voisin((double)((p_revm->rev_macb2_autr_demi_part * 2 * ((nbpart_cast - 200)*0.01))));
        }

        p_s4->reds_f.serfrc = restitution_seuil(pc, p_s4->npaffm - p_e4->nbparnr     , p_s4->revffm - p_e4->revfenr , p_revm);

        /* CA_SEUIL_0012  indicateur droit degrevement Macron avant et apres ISF */
        if (p_s4->revffm <= p_s4->seuilm1)
        {
            p_s4->imhisf = 'M';
        }
        if ((p_s4->revffm > p_s4->seuilm1) && (p_s4->revffm <= p_s4->seuilm2))
        {
            p_s4->imhisf = 'D';
        }
        if (p_s4->revffm > p_s4->seuilm2)
        {
            p_s4->imhisf = '3';
        }
        if (p_e4->isfc == 'F')
        {
            p_s4->imaisf = '3';
        }
        else
        {
            p_s4->imaisf = p_s4->imhisf;
        }

    if (p_e4->rede_1.nbpar == 0 ||
        (p_e4->cnua == 'C' &&
        (p_e4->rede_1.nbpar == 0  ||
         p_e4->rede_2.nbpar == 0 ))
        || ((p_e4->rede_1.front == 'K') && (p_e4->rede_1.revfe == 0)) ||
           ((p_e4->rede_2.front == 'K') && (p_e4->rede_2.revfe == 0)) ||
             p_e4->rede_r.front == 'K')


    {
        p_s4->revffm = 0;   /* CA_SEUIL_0009 */
        p_s4->npaffm = 0;   /* CA_SEUIL_0010 */

        /* CA_SEUIL_0011 seuils applicables au foyer */
        p_s4->seuilm1 = 0;
        p_s4->seuilm2 = 0;

        /* CA_SEUIL_0012 indicateur droit degrevement Macron avant et apres ISF */
        p_s4->imhisf = '3';
        p_s4->imaisf = '3';
    }

    /* 6 sortie du module */
    return 0;
}

/*============================================================================*/

    /*----------------------*/
    /*   fonction init_s4   */
    /*----------------------*/

void init_s4(s_reds * p)
{
    p->seuil  = ' ';
    p->revim  = 0  ;
    p->seuilb = ' ';
    p->serfrc = '0';
}

    /*---------------------*/
    /*  controles generaux */
    /*---------------------*/

int controle_ksfc(s_e4 * p_e4 , s_s4 * p_s4)
{
    int part;

    if (recherche_csdi(p_e4->csdi) == 0 )         /* si direction non trouvee */
    {
        cherche_Erreur(4302,&(p_s4->libelle)) ;
        return (4302);
    }

    /* nature d'affectation */
    if ((p_e4->cnua != 'S') && (p_e4->cnua != 'C') && (p_e4->cnua != 'R'))
    {
        cherche_Erreur(4303,&(p_s4->libelle)) ;
        return (4303);
    }

    /* code frontalier */
    if ((p_e4->rede_1.front != ' ') && (p_e4->rede_1.front != 'K'))
    {
        cherche_Erreur(4304,&(p_s4->libelle)) ;
        return (4304);
    }

    if ((p_e4->rede_2.front != ' ') && (p_e4->rede_2.front != 'K'))
    {
        cherche_Erreur(4305,&(p_s4->libelle)) ;
        return (4305);
    }

    if ((p_e4->rede_r.front != ' ') && (p_e4->rede_r.front != 'K'))
    {
        cherche_Erreur(4306,&(p_s4->libelle)) ;
        return (4306);
    }

    if ((p_e4->cnua == 'R') && (p_e4->rede_2.nbpar > 0))   /* nombre de parts */
    {
        cherche_Erreur(4307,&(p_s4->libelle)) ;
        return (4307);
    }

    if ((p_e4->cnua == 'R') && (p_e4->rede_1.nbpar < 100))
    {
        cherche_Erreur(4308,&(p_s4->libelle)) ;
        return (4308);
    }

    if ((p_e4->cnua == 'S') && (p_e4->rede_2.nbpar > 0))
    {
        cherche_Erreur(4309,&(p_s4->libelle)) ;
        return (4309);
    }

    part=(int)(p_e4->rede_1.nbpar*4);
    if (part != arrondi_dizaine_inf(arrondi_dizaine_inf(part) * 0.1) * 10)
    {
        cherche_Erreur(4310,&(p_s4->libelle)) ;
        return (4310);
    }

    part=(int)(p_e4->rede_2.nbpar*4);
    if (part != arrondi_dizaine_inf(arrondi_dizaine_inf(part) * 0.1) * 10)
    {
        cherche_Erreur(4311,&(p_s4->libelle)) ;
        return (4311);
    }

    part=(int)(p_e4->rede_r.nbpar*4);
    if (part != arrondi_dizaine_inf(arrondi_dizaine_inf(part) * 0.1) * 10)
    {
        cherche_Erreur(4312,&(p_s4->libelle)) ;
        return (4312);
    }

    if ((p_e4->rede_1.front == 'K') && (p_e4->rede_1.nbpar < 100))
    {
        cherche_Erreur(4313,&(p_s4->libelle)) ;
        return (4313);
    }

    if ((p_e4->rede_2.front =='K') && (p_e4->rede_2.nbpar < 100))
    {
        cherche_Erreur(4314,&(p_s4->libelle)) ;
        return (4314);
    }

    if ((p_e4->rede_r.front == 'K') && (p_e4->rede_r.nbpar < 100))
    {
        cherche_Erreur(4315,&(p_s4->libelle)) ;
        return (4315);
    }

    if (p_e4->rede_1.nbpar < 0)
    {
        cherche_Erreur(4316,&(p_s4->libelle)) ;
        return (4316);
    }

    if (p_e4->rede_2.nbpar < 0)
    {
        cherche_Erreur(4317,&(p_s4->libelle)) ;
        return (4317);
    }

    if (p_e4->rede_r.nbpar < 0)
    {
        cherche_Erreur(4318,&(p_s4->libelle)) ;
        return (4318);
    }

    if (p_e4->rede_1.revfe < 0)                           /* revenu imposable */
    {
        cherche_Erreur(4319,&(p_s4->libelle)) ;
        return (4319);
    }

    if (p_e4->rede_2.revfe < 0)
    {
        cherche_Erreur(4320,&(p_s4->libelle)) ;
        return (4320);
    }

    if (p_e4->rede_r.revfe < 0)
    {
        cherche_Erreur(4321,&(p_s4->libelle)) ;
        return (4321);
    }

    if ((p_e4->rede_r.nbpar ==0) && ((p_e4->rede_r.front == 'K')
    ||  (p_e4->rede_r.revfe > 0)))
    {
        cherche_Erreur(4322,&(p_s4->libelle)) ;
        return (4322);
    }

    if ((p_e4->rede_r.nbpar == 100) && ((p_e4->rede_r.front == 'K')
     && (p_e4->rede_r.revfe > 0)))
    {
        cherche_Erreur(4323,&(p_s4->libelle)) ;
        return (4323);
    }

    if ((p_e4->rede_r.annir < (p_e4->antax - 1)) && (p_e4->rede_r.nbpar > 0))
    {
        cherche_Erreur(4326,&(p_s4->libelle)) ;
        return (4326);
    }

    if (p_e4->rede_1.annir > p_e4->antax - 1)
    {
        cherche_Erreur(4327,&(p_s4->libelle)) ;
        return (4327);
    }

    if (p_e4->rede_2.annir > p_e4->antax - 1)
    {
        cherche_Erreur(4328,&(p_s4->libelle)) ;
        return (4328);
    }

    if (p_e4->rede_r.annir > p_e4->antax - 1)
    {
        cherche_Erreur(4329,&(p_s4->libelle)) ;
        return (4329);
    }
    return 0;                            /* tous les controles sont corrects */
}

char restitution_seuil(s_typseuil * pc, short nbr_part, long revenu, s_rev_m * p_revm)
{
     s_seuilM *p_seuilM;

    /*2020MAJ07-04-01, 2020MAJ07-04-02, 2020MAJ07-04-03, 2020MAJ07-04-04, 2020MAJ07-04-05, 2020MAJ07-04-06*/
    short demipart1;
    short demipart2;
    short demipart3;
    short demipart4;
    short autredemipart;
    short quartpart1;
    short quartpart2;
    short quartpart3;
    short quartpart4;
    short autrequartpart;
    long seuil_1417I;
    s_rev rev_1417I;
    rev_1417I = pc->rev_tousdroits;



    if (revenu==0)
    {
        return '0';
    }
    else
    {
        determination_parts(nbr_part,
                             &demipart1,
                             &demipart2,
                             &demipart3,
                             &demipart4,
                             &autredemipart,
                             &quartpart1,
                             &quartpart2,
                             &quartpart3,
                             &quartpart4,
                             &autrequartpart);


        seuil_1417I = ((rev_1417I.revenu_unepart                  )
        + ((long) demipart1      * rev_1417I.rev_prem_demipart     )
        + ((long) demipart2      * rev_1417I.rev_deuxieme_demipart )
        + ((long) demipart3      * rev_1417I.rev_troisieme_demipart)
        + ((long) demipart4      * rev_1417I.rev_quatrieme_demipart)
        + ((long) autredemipart  * rev_1417I.rev_autre_demipart    )
        + ((long) quartpart1     * arrondi_euro_voisin((rev_1417I.rev_prem_demipart)      * 0.5))
        + ((long) quartpart2     * arrondi_euro_voisin((rev_1417I.rev_deuxieme_demipart)  * 0.5))
        + ((long) quartpart3     * arrondi_euro_voisin((rev_1417I.rev_troisieme_demipart) * 0.5))
        + ((long) quartpart4     * arrondi_euro_voisin((rev_1417I.rev_quatrieme_demipart) * 0.5))
        + ((long) autrequartpart * arrondi_euro_voisin((rev_1417I.rev_autre_demipart)     * 0.5)));

        /* Determination des deux seuils Macron (1417 II bis)     */
        p_seuilM = determination_seuilM((long) nbr_part, p_revm);


        if (revenu <= seuil_1417I)
        {
            return '1';
        }
            else if (revenu <= p_seuilM->seuilm1)
            {
                return '2';
            }
                 else if (revenu <= p_seuilM->seuilm2)
                {
                    return '3';
                }
                    else
                    {
                        return '4';
                    }
    }
}

s_seuilM * determination_seuilM (long parts, s_rev_m *p_revm)
{

    static s_seuilM seuilMac;
    seuilMac.seuilm1=0;
    seuilMac.seuilm2=0;

    if (parts == 100)
        {
            seuilMac.seuilm1 = p_revm->rev_macb1_prem_part;
            seuilMac.seuilm2 = p_revm->rev_macb2_prem_part;
        }
        if ((parts > 100) && (parts <= 200))
        {
            seuilMac.seuilm1 = p_revm->rev_macb1_prem_part
                            + arrondi_euro_voisin((double)((p_revm->rev_macb1_prem_demi_part * 2 * ((parts - 100)*0.01))));
            seuilMac.seuilm2 = p_revm->rev_macb2_prem_part
                            + arrondi_euro_voisin((double)((p_revm->rev_macb2_prem_demi_part * 2 * ((parts - 100)*0.01))));
        }
        if (parts > 200)
        {
            seuilMac.seuilm1 = p_revm->rev_macb1_prem_part
                            + (p_revm->rev_macb1_prem_demi_part * 2)
                            + arrondi_euro_voisin((double)((p_revm->rev_macb1_autr_demi_part * 2 * ((parts - 200)*0.01))));

            seuilMac.seuilm2 = p_revm->rev_macb2_prem_part
                            + (p_revm->rev_macb2_prem_demi_part * 2)
                            + arrondi_euro_voisin((double)((p_revm->rev_macb2_autr_demi_part * 2 * ((parts - 200)*0.01))));
        }
        return &seuilMac;
}
  /*FIN 2020MAJ07-04-01, 2020MAJ07-04-02, 2020MAJ07-04-03, 2020MAJ07-04-04, 2020MAJ07-04-05, 2020MAJ07-04-06*/
