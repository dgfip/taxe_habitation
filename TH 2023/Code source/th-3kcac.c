            /*==================================
              MODULE GENERAL DE CALCUL DE LA TH
             =================================== */


#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif


int gbl_nbLp;
int gbl_Recalcul_Lissage; /* Indicateur de recalcul du lissage des locaux pros de la cote. A 1 si un des LP au moins est elligible au recalcul du planchonnement*/


/*============================================================================
   Main
  ============================================================================*/
int th_3kcac(s_e3 * p_e3, s_e2 * p_e2, s_ea * p_ea, s_sa * p_sa, s_s2 * p_s2)
{
    static s_signature signature;
    int ret_cons = 0;  /* zone retour de recherche des constantes */
    int ret = 0;  /* zone retour strcmp */
    static s_cons * p_const1=NULL;


    static s_lpe1 lpe1, * p_lpe1 = &lpe1;
    static s_lpe2 lpe2, * p_lpe2 = &lpe2;
    static s_lps lps, * p_lps = &lps;
    static s_ace ace, * p_ace=&ace;
    static s_acs acs, * p_acs=&acs;
    static s_sre sre, * p_sre=&sre;
    static s_srs srs, * p_srs=&srs;
    static s_dne dne;
    static s_dns dns;

    static s_e8 e8, * p_e8=&e8;
    static s_s8 s8, * p_s8=&s8;
    static s_rae rae, * p_rae=&rae;
    static s_ras ras, * p_ras=&ras;
    static s_ep ep, *p_ep= &ep;
    static s_sp sp, *p_sp = &sp;

    s_qtpAb qtpAbLh;
    s_qtpAb qtpAbL;
    s_vle vle;
    s_qtpAb AbCote;
    s_coe1 coe1;
    s_coe2 coe2;
    s_coe2 coe2_7;
    s_cos cos;
    s_cols cols_null;
    s_seuilM seuilM;

    long napdt=0;
    char signe =' ';
    int i= 0;




    int num_lopre = 0;
    long sVlnc = 0;
    long sVlnq = 0;
    long sVlns = 0;
    long sVlnn = 0;
    long sVlng = 0;
    long sVlne = 0;
    long sVlnf = 0;
    long sVlnr = 0;

    long sVlxc = 0;
    long sVlxq = 0;
    long sVlxs = 0;
    long sVlxn = 0;
    long sVlxg = 0;
    long sVlxe = 0;
    long sVlxf = 0;
    long sVlxr = 0;


    long sBNc = 0;
    long sBNq = 0;
    long sBNs = 0;
    long sBNn = 0;
    long sBNg = 0;
    long sBNe = 0;
    long sBNf = 0;
    long sBNr = 0;

    long sVLNNEXc = 0;
    long sVLNNEXq = 0;
    long sVLNNEXs = 0;
    long sVLNNEXn = 0;
    long sVLNNEXg = 0;
    long sVLNNEXe = 0;
    long sVLNNEXf = 0;
    long sVLNNEXr = 0;

    long sVLNABDc = 0;
    long sVLNABDq = 0;
    long sVLNABDs = 0;
    long sVLNABDn = 0;
    long sVLNABDg = 0;
    long sVLNABDe = 0;
    long sVLNABDf = 0;
    long sVLNABDr = 0;

    long sVLNADMc = 0;
    long sVLNADMq = 0;
    long sVLNADMs = 0;
    long sVLNADMn = 0;
    long sVLNADMg = 0;
    long sVLNADMe = 0;
    long sVLNADMf = 0;
    long sVLNADMr = 0;

    long sVLNABFc = 0;
    long sVLNABFq = 0;
    long sVLNABFs = 0;
    long sVLNABFn = 0;
    long sVLNABFg = 0;
    long sVLNABFe = 0;
    long sVLNABFf = 0;
    long sVLNABFr = 0;




    long totCotLPc          = 0;  /* Total cotisations Commune des LP */
    long totCotLPq          = 0;
    long totCotLPs          = 0;
    long totCotLPn          = 0;
    long totCotLPg          = 0;
    long totCotLPe          = 0;
    long totCotLPlisc       = 0;  /* Total cotisations lissees Commune des LP */
    long totCotLPlisq       = 0;
    long totCotLPliss       = 0;
    long totCotLPlisn       = 0;
    long totCotLPlisg       = 0;
    long totCotLPlise       = 0;

    long totCotLPcf          = 0;  /* Total cotisations fictives Commune des LP */
    long totCotLPqf          = 0;
    long totCotLPsf          = 0;
    long totCotLPef          = 0;
    long totCotLPliscf       = 0;  /* Total cotisations lissees fictives Commune des LP */
    long totCotLPlisqf       = 0;
    long totCotLPlissf       = 0;
    long totCotLPlisef       = 0;



    int ind_liq2017BASE70_2017 = 0;
    int ind_liq2017VLRNP2017 = 0  ;

    /* initialisation variables globales */
    gbl_nbLp = 0;
    gbl_Recalcul_Lissage = 0;


    /* INITIALISATIONS*/

    initialisation_sortie(p_s2);

    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_s2->signature=&signature;


    init_cols(&cols_null);

    /* IDENTIFICATION DU MODULE */
    /* ------------------------ */
    p_s2->anref = ANREF;
    p_s2->versr = 'B';

    /* controle de la signature */
    /* ------------------------ */
    ret = controle_signature(RKCAC,p_s2->versr, &(p_s2->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* ajout des espaces pour les abattements forfaitaires */
    ajout_espace_taux_abattement(&(p_e3->cole_c)); /* structure commune                                        */
    ajout_espace_taux_abattement(&(p_e3->cole_q)); /* structure intercommunalit�                               */
    ajout_espace_taux_abattement(&(p_e3->cole_f)); /* structure commune avant ajustement                       */
    ajout_espace_taux_abattement(&(p_e3->cole_r)); /* structure intercommunalite avant ajustement              */


    ajout_espace_taux_abattement(&(p_e3->cole_h)); /* structure entree commune 2017                            */
    ajout_espace_taux_abattement(&(p_e3->cole_k)); /* structure commune 2017 avant ajustement                  */
    ajout_espace_taux_abattement(&(p_e3->cole_l)); /* structure intercommunalite 2017                          */




    /* Controle code CSDI/DSF */
    if(!recherche_csdi(p_e2->csdi))
    {
        cherche_Erreur(5,&(p_s2->libelle));
        return 5;
    }

    /* Controle de l'indicateur de synthese THLV */
    if ( p_e2->tyimp=='T' )
    {
        if ( !(p_e3->indthlv =='C' || p_e3->indthlv == 'I' ) )
        {
            cherche_Erreur(62,&(p_s2->libelle));
            return(62);
        }
    }

    /* T  R  A  I  T  E  M  E  N  T */
    /* ============================ */
    /*        ==============        */



    ret_cons=(short)cherche_const(p_e2->antax,&p_const1); /* appel de la fonc.taux ann*/
    cherche_Erreur(ret_cons,&(p_s2->libelle)) ;
    if (ret_cons != 0)  /* retour de la fonction*/
    {
        return (ret_cons);
    }

    /* Pour RG25-02 Controle locaux professionnels hors champ THLV */

    while ((num_lopre < DIM_LOCAUX_PRO) && !(strncmp ( p_ea->lopre[num_lopre].indloc, "                  ",18)== 0 ))
    {

        num_lopre = num_lopre +1;

    }
    gbl_nbLp = num_lopre;
    num_lopre = 0;


    ret=controles(p_e2,p_e3);
    if (ret !=0)
    {
        cherche_Erreur(ret,&(p_s2->libelle));
        return ret;
    }

    /* Determination des VL brutes (imposee et non imposee */
    /* dans le cadre de l'abattement K3                    */
    if (p_e2->tyimp == 'T')

    /* Traitement de la THLV */
    {
        /* CA_THLV_0014 Determination des VL brutes imposees et non imposees */
        p_s2->vlbr = p_e2->vlbpc;
      /*bug 371914 - MAJ 08-04-01*/
        p_s2->vlbrt = p_e2->vlbpc;
    }
    else
    {
        for(  i=0; i < DIM_LOCAUX_PRO ; i++)
        {
            initialisation_loprs (&p_sa->loprs[i]); /* CA_TH_0119  et CA_TH_0128 */
            p_sa->loprs[i].vlrnp7 = p_ea->lopre[i].vlrnp7; /* CA_TH_0237 */
            p_sa->loprs[i].vlrnpt7 = p_ea->lopre[i].vlrnpt7;
        }



        /* APPEL DU MODULE KRPC */
        /* ==================== */
        /*       =======        */
        /*         ==           */

        for(i=0;i<gbl_nbLp;i++)
        {

             p_lpe1->lopre[i] = p_ea->lopre[i]; /* PREPARATION KLPC: copie des locaux pros en entr�e lpe1 */


            /* CA_TH_237 */

            p_sa->loprs[i].mntpl =  p_ea->lopre[i].mntpl;
            p_sa->loprs[i].mntplt =  p_ea->lopre[i].mntplt;
            p_sa->loprs[i].sgnpl =  p_ea->lopre[i].sgnpl;
            p_sa->loprs[i].sgnplt =  p_ea->lopre[i].sgnplt;
            p_sa->loprs[i].vlrnp =  p_ea->lopre[i].vlrnp;
            p_sa->loprs[i].vlrnpt =  p_ea->lopre[i].vlrnpt;

         /* CA_TH_238 */
         if (p_ea->lopre[i].indlis == 'O' && p_ea->lopre[i].indrec == 'O')
         {
             if  (gbl_Recalcul_Lissage ==0)
            {
               gbl_Recalcul_Lissage = 1;
            }
             p_ep->antax = p_e2->antax;
             p_ep->indlis = p_ea->lopre[i].indlis;
             p_ep->indrec = p_ea->lopre[i].indrec;
             p_ep->vlthr = p_ea->lopre[i].vlthr;
             p_ep->vlb70p7 = p_ea->lopre[i].vlb70p7;
             p_ep->vlthr7 = p_ea->lopre[i].vlthr7;
             p_ep->coefn = p_ea->lopre[i].coefn;
             p_ep->coefnt = p_ea->lopre[i].coefnt;



           ret = th_3krpc(p_ep,p_sp);
           if (ret!=0)
            {
                cherche_Erreur(ret,&(p_s2->libelle)) ;
                return (ret);
            }
           /* CA_TH_239 */
            p_sa->loprs[i].mntpl   = p_sp->mntpl          ;
            p_sa->loprs[i].sgnpl   = p_sp->sgnpl          ;
            p_sa->loprs[i].mntplt  = p_sp->mntplt         ;
            p_sa->loprs[i].sgnplt  = p_sp->sgnplt         ;
            p_sa->loprs[i].vlrnp   = p_sp->vlrnp          ;
            p_sa->loprs[i].vlrnpt  = p_sp->vlrnpt         ;
            p_sa->loprs[i].vlrnp7   = p_sp->vlrnp7        ;
            p_sa->loprs[i].vlrnpt7  = p_sp->vlrnpt7       ;

            p_lpe1->lopre[i].vlrnp7  = p_sp->vlrnp7       ;  /* L Entree EA n est pas modifiee, c est le lopre de lpe1 qui transporte les modifs de VL � l'issue de KRPC, qui n'ont pas vocation � �tre fig�es en sortie */
            p_lpe1->lopre[i].vlrnpt7 = p_sp->vlrnpt7      ;
            p_lpe1->lopre[i].vlrnp  = p_sp->vlrnp         ;
            p_lpe1->lopre[i].vlrnpt  = p_sp->vlrnpt       ;

            /* positionnement des indicateurs de calcul des Abattements*/

            if (((p_sa->loprs[i].vlrnp7 > 0 ) || (p_sa->loprs[i].vlrnpt7 > 0 )) && (ind_liq2017VLRNP2017 ==0))
            {
                ind_liq2017VLRNP2017 = 1;
            }
             if ((p_ea->lopre[i].vlb70p7 > 0 ) && (ind_liq2017BASE70_2017 ==0))
            {
                ind_liq2017BASE70_2017 = 1;
            }

         }

        }
            /* Determination des differentes  VL brutes de la cote */

            /* Cumul des VL des locaux professionnels  */

            for(i=0;i<gbl_nbLp;i++)
            {
                p_s2->vlbr   = (p_s2->vlbr + p_lpe1->lopre[i].vlrnp);      /* CA_TH_0048 */
                p_s2->vlbrt  = (p_s2->vlbrt + p_lpe1->lopre[i].vlrnpt);    /* CA_TH_0049 */
                if (gbl_Recalcul_Lissage == 1)
                {
                    p_s2->vlbr7  = (p_s2->vlbr7 + p_lpe1->lopre[i].vlrnp7);    /* CA_TH_0050 */
                    p_s2->vlbrt7 = (p_s2->vlbrt7 + p_lpe1->lopre[i].vlrnpt7);  /* CA_TH_0051 */
                    p_s2->vlb77  = (p_s2->vlb77 + p_lpe1->lopre[i].vlb70p7);   /* CA_TH_0052 */
                }
            }

            /* Ajout de la VL 70 actualisee des locaux d'habitation */
            p_s2->vlbr   = (p_s2->vlbr + p_e2->vlbpc);                       /* CA_TH_0048 */
            p_s2->vlbrt  = (p_s2->vlbrt + p_e2->vlbpc);                      /* CA_TH_0049 */
            p_s2->vlbr7  = (p_s2->vlbr7 + p_e2->vlbp7);                      /* CA_TH_0050 */
            p_s2->vlbrt7 = (p_s2->vlbrt7 + p_e2->vlbp7);                     /* CA_TH_0051 */
            p_s2->vlb77  = (p_s2->vlb77 + p_e2->vlbp7);                      /* CA_TH_0052 */

    }

    /* APPEL DU MODULE KTAC */
    /* ==================== */
    /*       =======        */
    /*         ==           */
    {
        p_e8->antax = p_e3->antax;
        strncpy(p_e8->dep, p_e3->dep,3);
        p_e8->cocnq = p_e3->cocnq;
        p_e8->tisyn = p_e3->tisyn;
        p_e8->titsn = p_e3->titsn;
        p_e8->titgp = p_e3->titgp;
        p_e8->abmos = p_e3->abmos;
        p_e8->codef = p_e3->codef;
        p_e8->codeg = p_e3->codeg;
        p_e8->vmd89 = p_e3->vmd89;

        p_e8->timpe = p_e3->timpe;
        p_e8->indgem = p_e3->indgem;

        p_e8->cole_c = p_e3->cole_c; /* commune annee N*/
        p_e8->cole_q = p_e3->cole_q; /* interco annee N*/
        p_e8->cole_f = p_e3->cole_f; /* commune avt ajustement annee N*/
        p_e8->cole_r = p_e3->cole_r; /* interco avt ajustement annee N*/



        p_e8->cole_h = p_e3->cole_h; /* commune annee 2017*/
        p_e8->cole_l = p_e3->cole_l; /* interco annee 2017*/
        p_e8->cole_k = p_e3->cole_k; /* commune avt ajustement annee 2017*/
        p_e8->tisyn7 = p_e3->tisyn7;
        p_e8->titsn7 = p_e3->titsn7;
        p_e8->titgp7 = p_e3->titgp7;
        p_e8->timpe7 = p_e3->timpe7;
        p_e8->abmos7 = p_e3->abmos7;
        p_e8->codef7 = p_e3->codef7;
        p_e8->timpq7 = p_e3->timpq7;
        p_e8->timpc7 = p_e3->timpc7;
    }

    ret = th_3ktac( p_e8,p_s8);

    if (ret != 0)
    {
        cherche_Erreur(ret,&(p_s2->libelle)) ;
        return (ret);
    }

     /* APPEL DU MODULE KACC */
    /* ==================== */
    /*       =======        */
    /*         ==           */
    /*   APPELE 4 FOIS POUR 4 LIQUIDATIONS */

    /*   LIQUIDATION N POUR LOCAUX HABITATION UNIQUEMENT - PAS DE LOCAL PRO                          - TX N - DONNEES COLLECT N */
    /*   LIQUIDATION N POUR LOCAUX PRO AVEC BASES REVISEES - PRESENCE EVENTUELLE DE LOCAL HABITATION - TX N - DONNEES COLLECT N */

    /* SI UN LP AU MOINS EST ELLIGIBLE AU RECALCUL DU PLANCHONNEMENT */
    /*   LIQUIDATION 2017 POUR LOCAUX PRO AVEC BASES REVISEES 2017          - TX 2017 - DONNEES COLLECT 2017 */
    /*   LIQUIDATION 2017 POUR LOCAUX PRO VL70 AVANT REVISION 2017          - TX 2017 - DONNEES COLLECT 2017 */
    /* FIN SI*/

    /*   -----------------------------------------   */

    if (gbl_nbLp == 0 && p_e2->tyimp== 'H'&& p_e2->tax == 'P') /* Absence de local pro - kacc non jou� en cas de THLV, THS, THE */
    {
        /* pas de local pro, uniquement les locaux d'habitation : ref liquidation 7 */
        /* 1   LIQUIDATION N POUR LOCAUX HABITATION UNIQUEMENT - PAS DE LOCAL PRO                          - TX N - DONNEES COLLECT N */
        p_ace->vlbr   = p_e2->vlbpc;
        p_ace->nbpac  = p_e2->nbpac;
        p_ace->nbera  = p_e2->nbera;
        p_ace->codni  = p_e2->codni;
        p_ace->codsh  = p_e2->codsh;
        p_ace->timpc  = p_e3->timpc;
        p_ace->timpq  = p_e3->timpq;
        p_ace->tisyn  = p_e3->tisyn;
        p_ace->titsn  = p_e3->titsn;
        p_ace->titgp  = p_e3->titgp;
        p_ace->timpe  = p_e3->timpe;
        p_ace->colb_c = p_s8->colb_c; /* Commune N */
        p_ace->colb_s = p_s8->colb_s; /* Syndicat N */
        p_ace->colb_q = p_s8->colb_q; /* Intercommunalite  N */
        p_ace->colb_n = p_s8->colb_n; /* TSE N */
        p_ace->colb_g = p_s8->colb_g; /* TSE Autre N */
        p_ace->colb_e = p_s8->colb_e; /* GEMAPI N */
        p_ace->colb_f = p_s8->colb_f; /* Commune avant ajustement N */
        p_ace->colb_r = p_s8->colb_r; /* Interco avant ajustement N */
        p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
        p_ace->cocnq    = p_e3->cocnq;

        ret = th_3kacc( p_ace,p_acs);
        if (ret != 0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }

        p_s2->cols_c = p_acs->cols_c;
        p_s2->cols_s = p_acs->cols_s;
        p_s2->cols_q = p_acs->cols_q;

        p_s2->cols_n = p_acs->cols_n;
        p_s2->cols_g = p_acs->cols_g;
        p_s2->cols_e = p_acs->cols_e;
        p_s2->cols_f = p_acs->cols_f;
        p_s2->cols_r = p_acs->cols_r;

    }
    if ((gbl_nbLp >0) && (p_e2->tax == 'P'))
    {
        /* si local pro existant */
        /* 2  LIQUIDATION N POUR LOCAUX PRO AVEC BASES REVISEES - PRESENCE EVENTUELLE DE LOCAL HABITATION - TX N - DONNEES COLLECT N */
        /* Ref liquidation 1 */
        p_ace->vlbr   = p_s2->vlbr;
        p_ace->nbpac  = p_e2->nbpac;
        p_ace->nbera  = p_e2->nbera;
        p_ace->codni  = p_e2->codni;
        p_ace->codsh  = p_e2->codsh;
        p_ace->timpc  = p_e3->timpc;
        p_ace->timpq  = p_e3->timpq;
        p_ace->tisyn  = p_e3->tisyn;
        p_ace->titsn  = p_e3->titsn;
        p_ace->titgp  = p_e3->titgp;
        p_ace->timpe  = p_e3->timpe;
        p_ace->colb_c = p_s8->colb_c; /* Commune N */
        p_ace->colb_s = p_s8->colb_s; /* Syndicat N */
        p_ace->colb_q = p_s8->colb_q; /* Intercommunalite  N */
        p_ace->colb_n = p_s8->colb_n; /* TSE N */
        p_ace->colb_g = p_s8->colb_g; /* TSE Autre N */
        p_ace->colb_e = p_s8->colb_e; /* GEMAPI N */
        p_ace->colb_f = p_s8->colb_f; /* Commune avant ajustement N */
        p_ace->colb_r = p_s8->colb_r; /* Interco avant ajustement N */
        p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
        p_ace->cocnq    = p_e3->cocnq;

        ret = th_3kacc( p_ace,p_acs);
        if (ret != 0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }

        p_s2->cols_cr = p_acs->cols_c;
        p_s2->cols_sr = p_acs->cols_s;
        p_s2->cols_qr = p_acs->cols_q;
        p_s2->cols_er = p_acs->cols_e;
        p_s2->cols_fr = p_acs->cols_f;
        p_s2->cols_rr = p_acs->cols_r;


        /*2019MAJR13-01-04  2019MAJR13-01-05   */

        if (ind_liq2017VLRNP2017 == 1)
        {
                /* si local pro existant */
                /* 3   LIQUIDATION 2017 POUR LOCAUX PRO AVEC BASES REVISEES 2017          - TX 2017 - DONNEES COLLECT 2017 */
                /* Ref liquidation 2 */
                p_ace->vlbr   = p_s2->vlbr7;
                p_ace->nbpac  = p_e2->nbpac;
                p_ace->nbera  = p_e2->nbera;
                p_ace->codni  = p_e2->codni;
                p_ace->codsh  = p_e2->codsh;
                p_ace->timpc  = p_e3->timpc7;
                p_ace->timpq  = p_e3->timpq7;
                p_ace->tisyn  = p_e3->tisyn7;
                p_ace->titsn  = p_e3->titsn7;
                p_ace->titgp  = p_e3->titgp7;
                p_ace->timpe  = p_e3->timpe7;
                p_ace->colb_c = p_s8->colb_h; /* commune 2017 */
                p_ace->colb_s = p_s8->colb_t; /* syndicat 2017 */
                p_ace->colb_q = p_s8->colb_l; /* interco 2017 */
                p_ace->colb_n = p_s8->colb_w; /* TSE 2017 */
                p_ace->colb_g = p_s8->colb_x; /* TSE autre 2017 */
                p_ace->colb_e = p_s8->colb_u; /* gemapi 2017 */
                p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
                p_ace->cocnq    = p_e3->cocnq;


                ret = th_3kacc( p_ace,p_acs);
                if (ret != 0)
                {
                    cherche_Erreur(ret,&(p_s2->libelle)) ;
                    return (ret);
                }

                p_s2->cols_cr7 = p_acs->cols_c; /* commune 2017 base r�vis�e 2017 */
                p_s2->cols_qr7 = p_acs->cols_q; /* interco 2017 base r�vis�e 2017 */
                p_s2->cols_sr7 = p_acs->cols_s; /* syndicat 2017 base r�vis�e 2017 */
                p_s2->cols_nr7 = p_acs->cols_n; /* tse 2017 base r�vis�e 2017 */
                p_s2->cols_gr7 = p_acs->cols_g; /* tse autre 2017 base r�vis�e 2017 */
                p_s2->cols_er7 = p_acs->cols_e; /* gemapi 2017 base r�vis�e 2017 */
            }
        if (ind_liq2017BASE70_2017 == 1)
        {
                /* si local pro existant */
                /* 4  LIQUIDATION 2017 POUR LOCAUX PRO VL70 AVANT REVISION 2017          - TX 2017 - DONNEES COLLECT 2017 */
                /* Ref liquidation 3 */
                p_ace->vlbr   = p_s2->vlb77;
                p_ace->nbpac  = p_e2->nbpac;
                p_ace->nbera  = p_e2->nbera;
                p_ace->codni  = p_e2->codni;
                p_ace->codsh  = p_e2->codsh;
                p_ace->timpc  = p_e3->timpc7;
                p_ace->timpq  = p_e3->timpq7;
                p_ace->tisyn  = p_e3->tisyn7;
                p_ace->titsn  = p_e3->titsn7;
                p_ace->titgp  = p_e3->titgp7;
                p_ace->timpe  = p_e3->timpe7;
                p_ace->colb_c = p_s8->colb_h; /* commune 2017 */
                p_ace->colb_s = p_s8->colb_t; /* syndicat 2017 */
                p_ace->colb_q = p_s8->colb_l; /* interco 2017 */
                p_ace->colb_n = p_s8->colb_w; /* TSE 2017 */
                p_ace->colb_g = p_s8->colb_x; /* TSE autre 2017 */
                p_ace->colb_e = p_s8->colb_u; /* gemapi 2017 */
                p_ace->tax    = p_e2->tax; /* pour CA_G0071 calcul abspsa */
                p_ace->cocnq    = p_e3->cocnq;

                ret = th_3kacc( p_ace,p_acs);
                if (ret != 0)
                {
                    cherche_Erreur(ret,&(p_s2->libelle)) ;
                    return (ret);
                }

                p_s2->cols_c7 = p_acs->cols_c; /* commune 2017 base 70 */
                p_s2->cols_q7 = p_acs->cols_q; /* interco 2017 base 70 */
                p_s2->cols_s7 = p_acs->cols_s; /*syndicat 2017 base 70 */
                p_s2->cols_n7 = p_acs->cols_n; /* tse 2017 base 70 */
                p_s2->cols_g7 = p_acs->cols_g; /* tse autre 2017 base 70 */
                p_s2->cols_e7 = p_acs->cols_e; /* gemapi 2017 base 70 */


        }

        /* FIN 2019MAJR13-01-04  2019MAJR13-01-05    */



    } /* fin des liquidations locaux pro */



    /* APPEL DU MODULE KLPC */
    /* ==================== */
    /*       =======        */
    /*         ==           */

    lpe1.antax = p_e2->antax;


    for(i=0; i<DIM_LOCAUX_PRO; i++)
    {
        p_lps->p_loprs[i] = &p_sa->loprs[i];

    }



    initialisation_sortieQtp(&qtpAbLh );

    p_lps->p_qtpAbLh = &qtpAbLh;

    p_lps->p_qtpAbL = &qtpAbL;
    initialisation_sortieQtp(p_lps->p_qtpAbL );
    /* CA_TH_0119 Initialisation du pas de lissage */
    p_lps->sommePaLis = 0;

    p_lpe1->p_vle=&vle;
    vle.vlbr     = p_s2->vlbr;
    vle.vlbrt    = p_s2->vlbrt;
    vle.vlbr7    = p_s2->vlbr7;
    vle.vlbrt7   = p_s2->vlbrt7;
    vle.vlb77    = p_s2->vlb77;
    vle.vlbpc    = p_e2->vlbpc;
    vle.vlbp7    = p_e2->vlbp7;

    p_lpe1->p_AbCote = &AbCote;
    initialisation_sortieQtp( p_lpe1->p_AbCote );

    AbCote.qtp_cr=p_s2->cols_cr;
    AbCote.qtp_qr=p_s2->cols_qr;
    AbCote.qtp_sr=p_s2->cols_sr;
    AbCote.qtp_nr=p_s2->cols_nr;
    AbCote.qtp_gr=p_s2->cols_gr;
    AbCote.qtp_er=p_s2->cols_er;
    AbCote.qtp_fr=p_s2->cols_fr;
    AbCote.qtp_rr=p_s2->cols_rr;


    AbCote.qtp_c7=p_s2->cols_c7;
    AbCote.qtp_q7=p_s2->cols_q7;
    AbCote.qtp_s7=p_s2->cols_s7;
    AbCote.qtp_n7=p_s2->cols_n7;
    AbCote.qtp_g7=p_s2->cols_g7;
    AbCote.qtp_e7=p_s2->cols_e7;

    AbCote.qtp_cr7=p_s2->cols_cr7;
    AbCote.qtp_qr7=p_s2->cols_qr7;
    AbCote.qtp_sr7=p_s2->cols_sr7;
    AbCote.qtp_nr7=p_s2->cols_nr7;
    AbCote.qtp_gr7=p_s2->cols_gr7;
    AbCote.qtp_er7=p_s2->cols_er7;

    p_lpe1->p_coe1 = &coe1;
    coe1.antax  = p_e2->antax;
    coe1.tyimp  = p_e2->tyimp;
    coe1.degex  = p_e2->degex;
    coe1.cnat   = p_e2->cnat;
    coe1.tax    = p_e2->tax;
    seuilM.seuilm1    = p_e2->seuilm1;
    seuilM.seuilm2    = p_e2->seuilm2;
    coe1.p_seuilM = &seuilM;
    coe1.imaisf    = p_e2->imaisf;
    coe1.revffm    = p_e2->revffm;
    coe1.grrev  = p_e2->grrev;
    coe1.aff  = p_e2->aff;
    coe1.indthlv = p_e3->indthlv;
    coe1.cocnq = p_e3->cocnq;
    coe1.indgem  = p_e3->indgem;
    coe1.permo  = p_e2->permo;
    strncpy(coe1.dep, p_e3->dep,3);
    strncpy(coe1.cne, p_e3->cne,4);

    initialisation_coe2(&coe2);
    p_lpe2->p_coe2 = &coe2;

    initialisation_coe2(&coe2_7);
    p_lpe2->p_coe2_7 = &coe2_7;

    coe2.timpc     = p_e3->timpc;
    coe2.tisyn     = p_e3->tisyn;
    coe2.titsn     = p_e3->titsn;
    coe2.titgp     = p_e3->titgp;
    coe2.timpq     = p_e3->timpq;
    coe2.timpe     = p_e3->timpe;
    coe2.timths    = p_e3->timths;

    /* Pour liquidation 2017 */
    coe2_7.timpc     = p_e3->timpc7;
    coe2_7.tisyn     = p_e3->tisyn7;
    coe2_7.titsn     = p_e3->titsn7;
    coe2_7.titgp     = p_e3->titgp7;
    coe2_7.timpq     = p_e3->timpq7;
    coe2_7.timpe     = p_e3->timpe7;

    if (gbl_nbLp != 0)
    {
        ret = th_3klpc (p_lpe1, p_lpe2, p_lps);
        if (ret!=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return (ret);
        }
    }

    /* CALCUL DE LA COTISATION  PROPRE AUX LOCAUX D HABITATION  */

    /*   APPEL DU MODULE - KCOC  */
    /*   ---------------------   */

    /* Regle 6.1 Conditionnement */
    if ((gbl_nbLp == 0) && (p_s2->vlbr > 0))
    {
        s_cos *p_cos = &cos;

        /* cote des locaux d'habitation - liquidation N */
        /* CA_TH_0136 CA_TH_0137 CA_TH_0138 CA_TH_0139 */
        p_lpe1->p_coe1->vl = p_s2->vlbr;
        p_lpe1->p_coe1->vlt = p_s2->vlbr;
        p_lpe1->p_coe1->workimaisf = p_lpe1->p_coe1->imaisf;
        p_lpe1->p_coe1->ind_origine_kcoc = 1;
        if (strchr( "PE",p_lpe1->p_coe1->tax) != NULL )
        {
              p_lpe1->p_coe1->ind_absence_TSE = 1;
        }
        else
        {
              p_lpe1->p_coe1->ind_absence_TSE = 0;
        }
        p_lpe1->p_coe1->p_cols_c =  &p_s2->cols_c;
        p_lpe1->p_coe1->p_cols_q =  &p_s2->cols_q;
        p_lpe1->p_coe1->p_cols_s =  &p_s2->cols_s;
        /*CA_TH_0240 */
        if(strchr( "S ",p_lpe1->p_coe1->tax) != NULL )
        {
            p_lpe1->p_coe1->p_cols_n =  &p_s2->cols_n;
            p_lpe1->p_coe1->p_cols_g =  &p_s2->cols_g;
        }
        else /* TAX VAUT P OU E */
        {
            init_cols(&p_s2->cols_n);
            init_cols(&p_s2->cols_g);
            p_lpe1->p_coe1->p_cols_n =  &cols_null;
            p_lpe1->p_coe1->p_cols_g =  &cols_null;
        }

        p_lpe1->p_coe1->p_cols_e =  &p_s2->cols_e;
        p_lpe1->p_coe1->p_cols_f =  &p_s2->cols_f;
        p_lpe1->p_coe1->p_cols_r =  &p_s2->cols_r;

        ret = th_3kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2, p_cos);
        if (ret !=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return(ret);
        }
    }

    /* Regle 6.11 Conditionnement Liquidation N Hab + Loc Pro */
    if ((gbl_nbLp > 0) && (p_s2->vlbr > 0))
    {
        s_cos *p_cos = &cos;

        /* cote des locaux d'habitation - liquidation N si presence de LP*/
        /* CA_TH_0145 CA_TH_0146 CA_TH_0147 CA_TH_0148 CA_TH_0148-1*/
        p_lpe1->p_coe1->vl = p_e2->vlbpc;
        p_lpe1->p_coe1->vlt = p_e2->vlbpc;
        p_lpe1->p_coe1->workimaisf = p_lpe1->p_coe1->imaisf;
        p_lpe1->p_coe1->ind_origine_kcoc = 1;
        if (strchr( "PE",p_lpe1->p_coe1->tax) != NULL )
        {
              p_lpe1->p_coe1->ind_absence_TSE = 1;
        }
        else
        {
              p_lpe1->p_coe1->ind_absence_TSE = 0;
        }
        p_lpe1->p_coe1->p_cols_c =  &p_lps->p_qtpAbLh->qtp_cr;
        p_lpe1->p_coe1->p_cols_q =  &p_lps->p_qtpAbLh->qtp_qr;
        p_lpe1->p_coe1->p_cols_s =  &p_lps->p_qtpAbLh->qtp_sr;
          /*CA_TH_0240 */
        if(strchr( "S ",p_lpe1->p_coe1->tax) != NULL )
        {
            p_lpe1->p_coe1->p_cols_n =  &p_lps->p_qtpAbLh->qtp_nr;
            p_lpe1->p_coe1->p_cols_g =  &p_lps->p_qtpAbLh->qtp_gr;
        }
        else /* TAX VAUT P OU E */
        {
            p_lpe1->p_coe1->p_cols_n =  &cols_null;
            p_lpe1->p_coe1->p_cols_g =  &cols_null;
        }

        p_lpe1->p_coe1->p_cols_e =  &p_lps->p_qtpAbLh->qtp_er;
        p_lpe1->p_coe1->p_cols_f =  &p_lps->p_qtpAbLh->qtp_fr;
        p_lpe1->p_coe1->p_cols_r =  &p_lps->p_qtpAbLh->qtp_rr;

        ret = th_3kcoc ( p_lpe1->p_coe1,p_lpe2->p_coe2, p_cos);
        if (ret !=0)
        {
            cherche_Erreur(ret,&(p_s2->libelle)) ;
            return(ret);
        }
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_cr, &p_s2->cols_cr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_qr, &p_s2->cols_qr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_sr, &p_s2->cols_sr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_nr, &p_s2->cols_nr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_gr, &p_s2->cols_gr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_er, &p_s2->cols_er);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_fr, &p_s2->cols_fr);
          majBaseDansS2( p_lps->p_qtpAbLh->qtp_rr, &p_s2->cols_rr);
    }

    /*Somme des VL nettes, bases exon�r�es, bases nettes imposees des locaux pros */

    /* CA_TH_0154 Somme des VL nettes, bases nettes et bases imposees N des locaux professionnels */
    for (i = 0 ; i < gbl_nbLp ; i++)
    {

        sVlnc = sVlnc + p_sa->loprs[i].cols_cr.vlntt;
        sVlnq = sVlnq + p_sa->loprs[i].cols_qr.vlntt;
        sVlns = sVlns + p_sa->loprs[i].cols_sr.vlntt;
        sVlnn = sVlnn + p_sa->loprs[i].cols_nr.vlntt;
        sVlng = sVlng + p_sa->loprs[i].cols_gr.vlntt;
        sVlne = sVlne + p_sa->loprs[i].cols_er.vlntt;
        sVlnf = sVlnf + p_sa->loprs[i].cols_fr.vlntt;
        sVlnr = sVlnr + p_sa->loprs[i].cols_rr.vlntt;

        sVlxc = sVlxc + p_sa->loprs[i].cols_cr.vlnex;
        sVlxq = sVlxq + p_sa->loprs[i].cols_qr.vlnex;
        sVlxs = sVlxs + p_sa->loprs[i].cols_sr.vlnex;
        sVlxn = sVlxn + p_sa->loprs[i].cols_nr.vlnex;
        sVlxg = sVlxg + p_sa->loprs[i].cols_gr.vlnex;
        sVlxe = sVlxe + p_sa->loprs[i].cols_er.vlnex;
        sVlxf = sVlxf + p_sa->loprs[i].cols_fr.vlnex;
        sVlxr = sVlxr + p_sa->loprs[i].cols_rr.vlnex;

        sBNc = sBNc + p_sa->loprs[i].cols_cr.bnimp;
        sBNq = sBNq + p_sa->loprs[i].cols_qr.bnimp;
        sBNs = sBNs + p_sa->loprs[i].cols_sr.bnimp;
        sBNn = sBNn + p_sa->loprs[i].cols_nr.bnimp;
        sBNg = sBNg + p_sa->loprs[i].cols_gr.bnimp;
        sBNe = sBNe + p_sa->loprs[i].cols_er.bnimp;
        sBNf = sBNf + p_sa->loprs[i].cols_fr.bnimp;
        sBNr = sBNr + p_sa->loprs[i].cols_rr.bnimp;

        sVLNNEXc = sVLNNEXc + p_sa->loprs[i].cols_cr.vlnnex;
        sVLNNEXq = sVLNNEXq + p_sa->loprs[i].cols_qr.vlnnex;
        sVLNNEXs = sVLNNEXs + p_sa->loprs[i].cols_sr.vlnnex;
        sVLNNEXn = sVLNNEXn + p_sa->loprs[i].cols_nr.vlnnex;
        sVLNNEXg = sVLNNEXg + p_sa->loprs[i].cols_gr.vlnnex;
        sVLNNEXe = sVLNNEXe + p_sa->loprs[i].cols_er.vlnnex;
        sVLNNEXf = sVLNNEXf + p_sa->loprs[i].cols_fr.vlnnex;
        sVLNNEXr = sVLNNEXr + p_sa->loprs[i].cols_rr.vlnnex;

        sVLNABDc = sVLNABDc + p_sa->loprs[i].cols_cr.vlnabd;
        sVLNABDq = sVLNABDq + p_sa->loprs[i].cols_qr.vlnabd;
        sVLNABDs = sVLNABDs + p_sa->loprs[i].cols_sr.vlnabd;
        sVLNABDn = sVLNABDn + p_sa->loprs[i].cols_nr.vlnabd;
        sVLNABDg = sVLNABDg + p_sa->loprs[i].cols_gr.vlnabd;
        sVLNABDe = sVLNABDe + p_sa->loprs[i].cols_er.vlnabd;
        sVLNABDf = sVLNABDf + p_sa->loprs[i].cols_fr.vlnabd;
        sVLNABDr = sVLNABDr + p_sa->loprs[i].cols_rr.vlnabd;

        sVLNADMc = sVLNADMc + p_sa->loprs[i].cols_cr.vlnadm;
        sVLNADMq = sVLNADMq + p_sa->loprs[i].cols_qr.vlnadm;
        sVLNADMs = sVLNADMs + p_sa->loprs[i].cols_sr.vlnadm;
        sVLNADMn = sVLNADMn + p_sa->loprs[i].cols_nr.vlnadm;
        sVLNADMg = sVLNADMg + p_sa->loprs[i].cols_gr.vlnadm;
        sVLNADMe = sVLNADMe + p_sa->loprs[i].cols_er.vlnadm;
        sVLNADMf = sVLNADMf + p_sa->loprs[i].cols_fr.vlnadm;
        sVLNADMr = sVLNADMr + p_sa->loprs[i].cols_rr.vlnadm;

        sVLNABFc = sVLNABFc + p_sa->loprs[i].cols_cr.vlnabf;
        sVLNABFq = sVLNABFq + p_sa->loprs[i].cols_qr.vlnabf;
        sVLNABFs = sVLNABFs + p_sa->loprs[i].cols_sr.vlnabf;
        sVLNABFn = sVLNABFn + p_sa->loprs[i].cols_nr.vlnabf;
        sVLNABFg = sVLNABFg + p_sa->loprs[i].cols_gr.vlnabf;
        sVLNABFe = sVLNABFe + p_sa->loprs[i].cols_er.vlnabf;
        sVLNABFf = sVLNABFf + p_sa->loprs[i].cols_fr.vlnabf;
        sVLNABFr = sVLNABFr + p_sa->loprs[i].cols_rr.vlnabf;

    }

    /* Calculs VLNTT VLNEX et BNIMP de la cote pour annee de campagne   CA_TH_0156 */
    p_s2->cols_cr.vlntt = sVlnc + p_lps->p_qtpAbLh->qtp_cr.vlntt;
    p_s2->cols_qr.vlntt = sVlnq + p_lps->p_qtpAbLh->qtp_qr.vlntt;
    p_s2->cols_sr.vlntt = sVlns + p_lps->p_qtpAbLh->qtp_sr.vlntt;
    p_s2->cols_nr.vlntt = sVlnn + p_lps->p_qtpAbLh->qtp_nr.vlntt;
    p_s2->cols_gr.vlntt = sVlng + p_lps->p_qtpAbLh->qtp_gr.vlntt;
    p_s2->cols_er.vlntt = sVlne + p_lps->p_qtpAbLh->qtp_er.vlntt;
    p_s2->cols_fr.vlntt = sVlnf + p_lps->p_qtpAbLh->qtp_fr.vlntt;
    p_s2->cols_rr.vlntt = sVlnr + p_lps->p_qtpAbLh->qtp_rr.vlntt;

    p_s2->cols_cr.vlnex = sVlxc + p_lps->p_qtpAbLh->qtp_cr.vlnex;
    p_s2->cols_qr.vlnex = sVlxq + p_lps->p_qtpAbLh->qtp_qr.vlnex;
    p_s2->cols_sr.vlnex = sVlxs + p_lps->p_qtpAbLh->qtp_sr.vlnex;
    p_s2->cols_nr.vlnex = sVlxn + p_lps->p_qtpAbLh->qtp_nr.vlnex;
    p_s2->cols_gr.vlnex = sVlxg + p_lps->p_qtpAbLh->qtp_gr.vlnex;
    p_s2->cols_er.vlnex = sVlxe + p_lps->p_qtpAbLh->qtp_er.vlnex;
    p_s2->cols_fr.vlnex = sVlxf + p_lps->p_qtpAbLh->qtp_fr.vlnex;
    p_s2->cols_rr.vlnex = sVlxr + p_lps->p_qtpAbLh->qtp_rr.vlnex;

    p_s2->cols_cr.bnimp = sBNc + p_lps->p_qtpAbLh->qtp_cr.bnimp;
    p_s2->cols_qr.bnimp = sBNq + p_lps->p_qtpAbLh->qtp_qr.bnimp;
    p_s2->cols_sr.bnimp = sBNs + p_lps->p_qtpAbLh->qtp_sr.bnimp;
    p_s2->cols_nr.bnimp = sBNn + p_lps->p_qtpAbLh->qtp_nr.bnimp;
    p_s2->cols_gr.bnimp = sBNg + p_lps->p_qtpAbLh->qtp_gr.bnimp;
    p_s2->cols_er.bnimp = sBNe + p_lps->p_qtpAbLh->qtp_er.bnimp;
    p_s2->cols_fr.bnimp = sBNf + p_lps->p_qtpAbLh->qtp_fr.bnimp;
    p_s2->cols_rr.bnimp = sBNr + p_lps->p_qtpAbLh->qtp_rr.bnimp;

    p_s2->cols_cr.vlnnex = sVLNNEXc + p_lps->p_qtpAbLh->qtp_cr.vlnnex;
    p_s2->cols_qr.vlnnex = sVLNNEXq + p_lps->p_qtpAbLh->qtp_qr.vlnnex;
    p_s2->cols_sr.vlnnex = sVLNNEXs + p_lps->p_qtpAbLh->qtp_sr.vlnnex;
    p_s2->cols_nr.vlnnex = sVLNNEXn + p_lps->p_qtpAbLh->qtp_nr.vlnnex;
    p_s2->cols_gr.vlnnex = sVLNNEXg + p_lps->p_qtpAbLh->qtp_gr.vlnnex;
    p_s2->cols_er.vlnnex = sVLNNEXe + p_lps->p_qtpAbLh->qtp_er.vlnnex;
    p_s2->cols_fr.vlnnex = sVLNNEXf + p_lps->p_qtpAbLh->qtp_fr.vlnnex;
    p_s2->cols_rr.vlnnex = sVLNNEXr + p_lps->p_qtpAbLh->qtp_rr.vlnnex;

    p_s2->cols_cr.vlnabd = sVLNABDc + p_lps->p_qtpAbLh->qtp_cr.vlnabd;
    p_s2->cols_qr.vlnabd = sVLNABDq + p_lps->p_qtpAbLh->qtp_qr.vlnabd;
    p_s2->cols_sr.vlnabd = sVLNABDs + p_lps->p_qtpAbLh->qtp_sr.vlnabd;
    p_s2->cols_nr.vlnabd = sVLNABDn + p_lps->p_qtpAbLh->qtp_nr.vlnabd;
    p_s2->cols_gr.vlnabd = sVLNABDg + p_lps->p_qtpAbLh->qtp_gr.vlnabd;
    p_s2->cols_er.vlnabd = sVLNABDe + p_lps->p_qtpAbLh->qtp_er.vlnabd;
    p_s2->cols_fr.vlnabd = sVLNABDf + p_lps->p_qtpAbLh->qtp_fr.vlnabd;
    p_s2->cols_rr.vlnabd = sVLNABDr + p_lps->p_qtpAbLh->qtp_rr.vlnabd;

    p_s2->cols_cr.vlnadm = sVLNADMc + p_lps->p_qtpAbLh->qtp_cr.vlnadm;
    p_s2->cols_qr.vlnadm = sVLNADMq + p_lps->p_qtpAbLh->qtp_qr.vlnadm;
    p_s2->cols_sr.vlnadm = sVLNADMs + p_lps->p_qtpAbLh->qtp_sr.vlnadm;
    p_s2->cols_nr.vlnadm = sVLNADMn + p_lps->p_qtpAbLh->qtp_nr.vlnadm;
    p_s2->cols_gr.vlnadm = sVLNADMg + p_lps->p_qtpAbLh->qtp_gr.vlnadm;
    p_s2->cols_er.vlnadm = sVLNADMe + p_lps->p_qtpAbLh->qtp_er.vlnadm;
    p_s2->cols_fr.vlnadm = sVLNADMf + p_lps->p_qtpAbLh->qtp_fr.vlnadm;
    p_s2->cols_rr.vlnadm = sVLNADMr + p_lps->p_qtpAbLh->qtp_rr.vlnadm;

    p_s2->cols_cr.vlnabf = sVLNABFc + p_lps->p_qtpAbLh->qtp_cr.vlnabf;
    p_s2->cols_qr.vlnabf = sVLNABFq + p_lps->p_qtpAbLh->qtp_qr.vlnabf;
    p_s2->cols_sr.vlnabf = sVLNABFs + p_lps->p_qtpAbLh->qtp_sr.vlnabf;
    p_s2->cols_nr.vlnabf = sVLNABFn + p_lps->p_qtpAbLh->qtp_nr.vlnabf;
    p_s2->cols_gr.vlnabf = sVLNABFg + p_lps->p_qtpAbLh->qtp_gr.vlnabf;
    p_s2->cols_er.vlnabf = sVLNABFe + p_lps->p_qtpAbLh->qtp_er.vlnabf;
    p_s2->cols_fr.vlnabf = sVLNABFf + p_lps->p_qtpAbLh->qtp_fr.vlnabf;
    p_s2->cols_rr.vlnabf = sVLNABFr + p_lps->p_qtpAbLh->qtp_rr.vlnabf;

    /* Somme des cotisations des locaux professionnels CA_TH_0158 CA_TH_0159 CA_TH_0160 CA_TH_0161 */

    /* Total pour tous les locaux pros... */

    for ( i=0 ; i < gbl_nbLp ; i++)
    {
         /* cotisations non lissees */
        /* CA_TH_0158 Somme des cotisations N des locaux professionnels */
        totCotLPc = totCotLPc + p_sa->loprs[i].cols_cr.coti;
        totCotLPq = totCotLPq + p_sa->loprs[i].cols_qr.coti;
        totCotLPs = totCotLPs + p_sa->loprs[i].cols_sr.coti;
        totCotLPn = totCotLPn + p_sa->loprs[i].cols_nr.coti;
        totCotLPg = totCotLPg + p_sa->loprs[i].cols_gr.coti;
        totCotLPe = totCotLPe + p_sa->loprs[i].cols_er.coti;

        /* CA_TH_0158-1 Somme des cotisations fictive N des locaux professionnels */

        totCotLPcf = totCotLPcf + p_sa->loprs[i].cols_cr.cotif;
        totCotLPqf = totCotLPqf + p_sa->loprs[i].cols_qr.cotif;
        totCotLPsf = totCotLPsf + p_sa->loprs[i].cols_sr.cotif;
        totCotLPef = totCotLPef + p_sa->loprs[i].cols_er.cotif;

        /* cotisations lissees */
        /* CA_TH_0162 1ere partie somme locaux pros */
        /* CA_TH_0160 Somme des cotisations lissees N des locaux professionnels */
        totCotLPlisc = totCotLPlisc + p_sa->loprs[i].cotilpc;
        totCotLPlisq = totCotLPlisq + p_sa->loprs[i].cotilpq;
        totCotLPliss = totCotLPliss + p_sa->loprs[i].cotilps;
        totCotLPlisn = totCotLPlisn + p_sa->loprs[i].cotilpn;
        totCotLPlisg = totCotLPlisg + p_sa->loprs[i].cotilpg;
        totCotLPlise = totCotLPlise + p_sa->loprs[i].cotilpe;

        /* CA_TH_0162-1 1ere partie somme locaux pros */
        /* CA_TH_0160-1 Somme des cotisations fictive lissees N des locaux professionnels */
        totCotLPliscf = totCotLPliscf + p_sa->loprs[i].cotilpcf;
        totCotLPlisqf = totCotLPlisqf + p_sa->loprs[i].cotilpqf;
        totCotLPlissf = totCotLPlissf + p_sa->loprs[i].cotilpsf;
        totCotLPlisef = totCotLPlisef + p_sa->loprs[i].cotilpef;
    }

    /* Somme des cotisations de la cote CA_TH_0162 CA_TH_0163 CA_TH_0164 CA_TH_0165 */
    if (gbl_nbLp > 0)
    {

        /* CA_TH_0162 2eme partie ajout locaux habitation */
        p_s2->cols_cr.coti  =  totCotLPlisc + p_lps->p_qtpAbLh->qtp_cr.coti;
        p_s2->cols_qr.coti  =  totCotLPlisq + p_lps->p_qtpAbLh->qtp_qr.coti;
        p_s2->cols_sr.coti  =  totCotLPliss + p_lps->p_qtpAbLh->qtp_sr.coti;
        p_s2->cols_nr.coti  =  totCotLPlisn + p_lps->p_qtpAbLh->qtp_nr.coti;
        p_s2->cols_gr.coti  =  totCotLPlisg + p_lps->p_qtpAbLh->qtp_gr.coti;
        p_s2->cols_er.coti  =  totCotLPlise + p_lps->p_qtpAbLh->qtp_er.coti;


        /* CA_TH_0162-1 2eme partie ajout locaux habitation */
        p_s2->cols_cr.cotif  =  totCotLPliscf + p_lps->p_qtpAbLh->qtp_cr.cotif;
        p_s2->cols_qr.cotif  =  totCotLPlisqf + p_lps->p_qtpAbLh->qtp_qr.cotif;
        p_s2->cols_sr.cotif  =  totCotLPlissf + p_lps->p_qtpAbLh->qtp_sr.cotif;
        p_s2->cols_er.cotif  =  totCotLPlisef + p_lps->p_qtpAbLh->qtp_er.cotif;


    }

    if (gbl_nbLp > 0)
    {
        /* CA_TH_0164 cotisations avant lissage */
        p_s2->cotic  =  totCotLPc + p_lps->p_qtpAbLh->qtp_cr.coti;
        p_s2->cotiq  =  totCotLPq + p_lps->p_qtpAbLh->qtp_qr.coti;
        p_s2->cotis  =  totCotLPs + p_lps->p_qtpAbLh->qtp_sr.coti;
        p_s2->cotin  =  totCotLPn + p_lps->p_qtpAbLh->qtp_nr.coti;
        p_s2->cotig  =  totCotLPg + p_lps->p_qtpAbLh->qtp_gr.coti;
        p_s2->cotie  =  totCotLPe + p_lps->p_qtpAbLh->qtp_er.coti;

        /* CA_TH_0165 cotisations lissees */
        p_s2->cotilc  =  p_s2->cols_cr.coti;
        p_s2->cotilq  =  p_s2->cols_qr.coti;
        p_s2->cotils  =  p_s2->cols_sr.coti;
        p_s2->cotiln  =  p_s2->cols_nr.coti;
        p_s2->cotilg  =  p_s2->cols_gr.coti;
        p_s2->cotile  =  p_s2->cols_er.coti;

        /* CA_TH_0165-1 cotisations fictives avant lissage */
        p_s2->coticf  =  totCotLPcf + p_lps->p_qtpAbLh->qtp_cr.cotif;
        p_s2->cotiqf  =  totCotLPqf + p_lps->p_qtpAbLh->qtp_qr.cotif;
        p_s2->cotisf  =  totCotLPsf + p_lps->p_qtpAbLh->qtp_sr.cotif;
        p_s2->cotief  =  totCotLPef + p_lps->p_qtpAbLh->qtp_er.cotif;

        /* CA_TH_0165-2 cotisations lissees fictives*/
        p_s2->cotilcf  =  p_s2->cols_cr.cotif;
        p_s2->cotilqf  =  p_s2->cols_qr.cotif;
        p_s2->cotilsf  =  p_s2->cols_sr.cotif;
        p_s2->cotilef  =  p_s2->cols_er.cotif;


    }
    else
    {
        p_s2->cotic  =  p_s2->cols_c.coti;
        p_s2->cotiq  =  p_s2->cols_q.coti;
        p_s2->cotis  =  p_s2->cols_s.coti;
        p_s2->cotin  =  p_s2->cols_n.coti;
        p_s2->cotig  =  p_s2->cols_g.coti;
        p_s2->cotie  =  p_s2->cols_e.coti;

        p_s2->cotilc  =  0;
        p_s2->cotilq  =  0;
        p_s2->cotils  =  0;
        p_s2->cotiln  =  0;
        p_s2->cotilg  =  0;
        p_s2->cotile  =  0;

        p_s2->coticf  =  p_s2->cols_c.cotif;
        p_s2->cotiqf  =  p_s2->cols_q.cotif;
        p_s2->cotisf  =  p_s2->cols_s.cotif;
        p_s2->cotief  =  p_s2->cols_e.cotif;

        p_s2->cotilcf  =  0;
        p_s2->cotilqf  =  0;
        p_s2->cotilsf  =  0;
        p_s2->cotilef  =  0;
    }

    /* Calcul du lissage */
    /* CA_TH_0166 Somme des quotes-parts modulees appliquees N des locaux professionnels */

    for(i=0; i<gbl_nbLp; i++)
    {
        p_s2->lissac = p_s2->lissac + p_sa->loprs[i].qtmc;
        p_s2->lissaq = p_s2->lissaq + p_sa->loprs[i].qtmq;
        p_s2->lissas = p_s2->lissas + p_sa->loprs[i].qtms;
        p_s2->lissan = p_s2->lissan + p_sa->loprs[i].qtmn;
        p_s2->lissag = p_s2->lissag + p_sa->loprs[i].qtmg;
        p_s2->lissae = p_s2->lissae + p_sa->loprs[i].qtme;

        /* CA_TH_0167 Somme des quotes-parts des pas de lissage applicables par collectivit�  RG13-06*/
        p_s2->qtprltc = p_s2->qtprltc + p_sa->loprs[i].qtprlc;
        p_s2->qtprltq = p_s2->qtprltq + p_sa->loprs[i].qtprlq;
        p_s2->qtprlts = p_s2->qtprlts + p_sa->loprs[i].qtprls;
        p_s2->qtprltn = p_s2->qtprltn + p_sa->loprs[i].qtprln;
        p_s2->qtprltg = p_s2->qtprltg + p_sa->loprs[i].qtprlg;
        p_s2->qtprlte = p_s2->qtprlte + p_sa->loprs[i].qtprle;
    }

    /* CA_TH_0167 Somme des pas de lissage RG13-05*/
    p_s2->palis = p_lps->sommePaLis;

    /*  APPEL DU MODULE  KSRC  */
    /*  ---------------------  */
    p_sre->antax   = p_e2->antax;
    p_sre->degex   = p_e2->degex;
    p_sre->vlbpc   = p_e2->vlbpc;
    p_sre->vlbch   = p_e2->vlbch;
    p_sre->aff     = p_e2->aff;
    p_sre->cnat    = p_e2->cnat;
    p_sre->tax     = p_e2->tax;
    p_sre->indaths = p_e2->indaths;
    strncpy(p_sre->dep, p_e3->dep,3);
    strncpy(p_sre->cne, p_e3->cne,4);
    p_sre->indmths = p_e3->indmths;
    p_sre->timths  = p_e3->timths;
    p_sre->tyimp   = p_e2->tyimp;
    if (gbl_nbLp > 0)
    {


        p_sre->cotic= p_lps->p_qtpAbLh->qtp_cr.coti;
        p_sre->cols_c = p_s2->cols_cr;
        p_sre->cols_s = p_s2->cols_sr;
        p_sre->cols_q = p_s2->cols_qr;
        p_sre->cols_n = p_s2->cols_nr;
        p_sre->cols_g = p_s2->cols_gr;
        p_sre->cols_e = p_s2->cols_er;
    }
    else
    {
        p_sre->cotic= p_s2->cols_c.coti;
        p_sre->cols_c = p_s2->cols_c;
        p_sre->cols_s = p_s2->cols_s;
        p_sre->cols_q = p_s2->cols_q;
        p_sre->cols_n = p_s2->cols_n;
        p_sre->cols_g = p_s2->cols_g;
        p_sre->cols_e = p_s2->cols_e;
    }
    ret = th_3ksrc( p_sre, p_srs);

    if (ret!=0)
    {
        cherche_Erreur(ret,&(p_s2->libelle)) ;
        return (ret);
    }

    p_s2->majths	= p_srs->majths   ;
    p_s2->coticm    = p_srs->coticm   ;
    p_s2->frait     = p_srs->frait    ;
    p_s2->fraitf    = p_srs->fraitf   ;
    p_s2->fgest_cq  = p_srs->fgest_cq ;
    p_s2->fgest_ng  = p_srs->fgest_ng ;
    p_s2->fgest_e   = p_srs->fgest_e  ;
    p_s2->fgest_s   = p_srs->fgest_s  ;
    p_s2->frai4     = p_srs->frai4    ;
    p_s2->far_cq    = p_srs->far_cq   ;
    p_s2->far_s     = p_srs->far_s    ;
    p_s2->far_ng    = p_srs->far_ng   ;
    p_s2->far_e     = p_srs->far_e    ;
    p_s2->somrc     = p_srs->somrc    ;
    p_s2->limajths	= p_srs->limajths ;

    /*   APPEL DU MODULE KDNC   */
    dne.p_cons = p_const1;
    dne.tyimp = p_e2->tyimp;

    dne.degex = p_e2->degex;

    dne.somrc = p_s2->somrc;
    dne.somrp = p_s2->somrc;
    dne.somrcf   = p_srs->somrcf  ;



    if (gbl_nbLp == 0)
    {
        if (p_s2->cols_c.vlnex + p_s2->cols_q.vlnex + p_s2->cols_s.vlnex   > 0 )

        {
            dne.vlexo = 'O';
        }
        else
        {
           dne.vlexo = 'N';
        }
        /* CA_TH_0224 Determination des indicateurs reforme TH */
        /* bug 299398 */
        if (p_s2->cols_c.vlnnex ==  p_s2->cols_c.bnimp &&
            p_s2->cols_q.vlnnex ==  p_s2->cols_q.bnimp &&
            p_s2->cols_s.vlnnex ==  p_s2->cols_s.bnimp &&
            p_s2->cols_n.vlnnex ==  p_s2->cols_n.bnimp &&
            p_s2->cols_g.vlnnex ==  p_s2->cols_g.bnimp &&
            p_s2->cols_e.vlnnex ==  p_s2->cols_e.bnimp
            /* bug 306359*/
            /*
            && p_s2->cols_f.vlnnex ==  p_s2->cols_f.bnimp
            && p_s2->cols_r.vlnnex ==  p_s2->cols_r.bnimp   */
            )

        {
            dne.MAJindicateur = 'N';
        }
        else
        {
            dne.MAJindicateur = 'O';

        }
    }
    else
    {
        if (p_s2->cols_cr.vlnex + p_s2->cols_qr.vlnex + p_s2->cols_sr.vlnex   > 0 )
        {
            dne.vlexo = 'O';
        }
        else
        {
            dne.vlexo = 'N';
        }
        if (p_s2->cols_cr.vlnnex ==  p_s2->cols_cr.bnimp &&
            p_s2->cols_qr.vlnnex ==  p_s2->cols_sr.bnimp &&
            p_s2->cols_sr.vlnnex ==  p_s2->cols_qr.bnimp &&
            p_s2->cols_nr.vlnnex ==  p_s2->cols_nr.bnimp &&
            p_s2->cols_gr.vlnnex ==  p_s2->cols_gr.bnimp &&
            p_s2->cols_er.vlnnex ==  p_s2->cols_er.bnimp &&
            p_s2->cols_fr.vlnnex ==  p_s2->cols_er.bnimp &&
            p_s2->cols_rr.vlnnex ==  p_s2->cols_er.bnimp  )

        {
            dne.MAJindicateur = 'N';
        }
        else
        {
            dne.MAJindicateur = 'O';
        }
    }

    strncpy(dne.csdi,p_e2->csdi, 4);
    dne.revffm   = p_e2->revffm   ;
    dne.p_sM     = &seuilM        ;
    dne.imaisf   = p_e2->imaisf   ;



    ret = th_3kdnc( &dne, &dns);

    if (ret != 0)
    {
        cherche_Erreur(ret,&(p_s2->libelle)) ;
        return (ret);
    }

    {
        p_s2->degpl = dns.degpl;
        p_s2->totth = dns.totth;
        p_s2->netth = dns.netth;
        strncpy(p_s2->codro,dns.codro,3);
        p_s2->imap      = dns.imap           ;
        strncpy(p_s2->codegm,dns.codegm,5);
        p_s2->txrefm   = dns.txrefm        ;
        p_s2->totthf = dns.totthf;
        p_s2->netthf = dns.netthf;

            /* CA_TH_0042-2 DETERMINATION DU GAIN MACRON */
        if (( p_e2->tax == 'P' || p_e2->tax == 'E') && strchr("34",p_e2->cnat) != NULL  && p_e2->degex == ' ' &&               strchr("M3D",p_e2->imaisf)!= NULL)
       {
           p_s2->mtexo = dns.netthf - dns.netth;
       }

    }



    /*    APPEL DU MODULE KRAC    */
    /*    --------------------    */
    if (p_e2->tyimp == 'H')
    {
        {
            p_rae->antax = p_e2->antax;
            strncpy(p_rae->dep,p_e3->dep, 3);
            p_rae->champ = p_e2->champ;
            p_rae->qtvrt = p_e2->qtvrt;
            p_rae->degtv = p_e2->degtv;
            p_rae->rgstv = p_e2->rgstv;
        }

        ret = th_3krac (p_rae ,p_ras);
        if (ret != 0)
        {
            cherche_Erreur(ret,&(p_s2->libelle));
            return (ret);
        }

        {
            p_s2->cottv = p_ras->cottv;
            p_s2->fratv = p_ras->fratv;
            p_s2->somtv = p_ras->somtv;
            p_s2->mdgtv = p_ras->mdgtv;
            p_s2->nettv = p_ras->nettv;
            strncpy(p_s2->roltv,p_ras->roltv, 4);
        }
    }


        /* Cas g�n�ral T ou H : mise � jour du net � payer */

            p_s2->netap = p_s2->netth + p_s2->nettv;


    /* appel fonction regularisation de role CA_TH_0225*/
    if (p_e2->regul=='R')
    {
        regularisation(p_s2->netap ,
                       p_e2->napin,
                       &napdt , &signe);
        p_s2->napdt=napdt;
        p_s2->signe=signe;
    }

    if (p_e2->regul != 'R')
    {
        p_s2->signe = ' ';
        p_s2->napdt = 0;
    }

        /* sortie de THLV */
    if(p_e2->tyimp=='T')
    {
        return (ret) ;
    }


    /* Fusion donnees collectivite N  */
          /* CA_TH_0163 a CA_TH_0169 */
    if (gbl_nbLp > 0)
    {
        /* CA_TH_0168 Copie des resultats de taxation TH N revisee dans les resultats de taxation TH N */
        copieCols(p_s2->cols_cr, &p_s2->cols_c);
        copieCols(p_s2->cols_fr, &p_s2->cols_f);
        copieCols(p_s2->cols_qr, &p_s2->cols_q);
        copieCols(p_s2->cols_rr, &p_s2->cols_r);
        copieCols(p_s2->cols_sr, &p_s2->cols_s);
        copieCols(p_s2->cols_er, &p_s2->cols_e);
        copieCols(p_s2->cols_nr, &p_s2->cols_n);
        copieCols(p_s2->cols_gr, &p_s2->cols_g);

    }



    /*    SORTIE DU MODULE    */

    return (ret);
}

void majBaseDansS2( s_cols source, s_cols *p_cible)
{
    p_cible->vlntt = source.vlntt;
    p_cible->vlnex = source.vlnex;
    p_cible->bnimp = source.bnimp;
    p_cible->vlnnex = source.vlnnex;
    p_cible->vlnadm = source.vlnadm;
    p_cible->vlnabd = source.vlnabd;
    p_cible->vlnabf = source.vlnabf;
}

/*============================================================================
   Controle generaux
  ============================================================================*/
int controles(s_e2 * p_e2,
              s_e3 * p_e3)
{
    long quot_abat;  /*  zone traitement quotite  */

    char dep97[]="97";
 /*   char val40[]="40";
    char val50[]="50";*/

    static char dep21[]="21";  /*initialisation departement 21 */
    double tau1; /* zone retour taux act */
    static char cne1[]="000";  /* initial char sur cne */
    int retour=0;

    retour=strcmp(dep21 , p_e3->dep); /* comparaison si dep 21 */
    if (retour !=0)   /* si dep <> 21 on cherche son taux dans table d'actualisation*/
    {
        tau1=recherche_tdep (p_e3->dep, cne1 , 'H');
        if (tau1==2)  /*retour de la fonction */
        {
            return (2);
        }
    }

    /* traitement code taxation */
    if ((strchr("S",p_e2->tax) == NULL) && p_e2->tyimp == 'H')
    {
        return (3);
    }

    /* traitement PAC */
    if (p_e2->nbpac > 99)
    {
        return (4);
    }

    /* traitement code NI a l IR */
    if (strchr("A ",p_e2->codni) == NULL)
    {
        return (6);
    }

    /* traitement codsh */
    if (strchr("12345 ",p_e2->codsh) == NULL)
    {
        return (7);
    }

    /*    traitement tyimp et code nature FIP    */
    if (strchr("3456",p_e2->cnat) == NULL)
    {
        return (8);
    }

    /*Controle du code affectation du local */
    if (strchr("AFHMS ",p_e2->aff) == NULL)
    {
        return (10);
    }



    /* nbre pac > 99 */
    if (p_e2->nbera > 99)
    {
        return (16);
    }

    /* code permo diff de 0 1 2 3 4 5 6 7 8 9 ' ' */
    if (strchr("0123456789 ",p_e2->permo) == NULL)
    {
        return (17);
    }

    /* code grrev diff de  1 2 3 4  ' ' */
    if (strchr("1234 ",p_e2->grrev) == NULL)
    {
        return (18);
    }

    /* code champ diff de c d e i m s t */
    if ((strchr("CDEILMSRT",p_e2->champ) == NULL) && (p_e2->tyimp == 'H'))
    {
        return (19);
    }

    /* code qtvrt diff de  0, 1 */
    if ((strchr("01",p_e2->qtvrt) == NULL) && (p_e2->tyimp == 'H'))
    {
        return (20);
    }

    /* code degtv diff de ejfiadvzkws ' ' */
    if (strchr("EJFIADVS ZWK",p_e2->degtv) == NULL)
    {
        return (21);
    }

    /* code degex diff de gdk ' ' */
    if (strchr("GDK ",p_e2->degex) == NULL)
    {
        return (22);
    }

    /* code TYIMP diff de h et t   */
    if (strchr("HT",p_e2->tyimp) == NULL)
    {
        return (23);
    }

    /* code TYIMP egal a t , nbpac nbera diff 0 */
    if ((p_e2->tyimp=='T')&&((p_e2->nbpac!=0)||(p_e2->nbera!=0)))
    {
        return (25);
    }

    /* code TYIMP egal a t , degex servi */
    if ((p_e2->tyimp=='T')&&(p_e2->degex!=' '))
    {
        return (26);
    }

     /* code TYIMP egal a t , codni servi */
    if ((p_e2->tyimp=='T')&&(p_e2->codni!=' '))
    {
        return (27);
    }

    /* code TYIMP egal a t , champ TV servi */
    if ((p_e2->tyimp=='T')&&(p_e2->champ!=' '))
    {
        return (28);
    }

    /* code TYIMP egal a t , degtv servi */
    if ((p_e2->tyimp=='T')&&(p_e2->degtv!=' '))
    {
        return (29);
    }

    /* code TYIMP egal a t , qtvrt servi */
    if ((p_e2->tyimp=='T')&&(p_e2->qtvrt!=' '))
    {
        return (30);
    }

    /* code TYIMP egal a t , tax servi */
    if ((p_e2->tyimp=='T')&&(p_e2->tax!=' '))
    {
        return (35);
    }



    /* indicateur majoration THS diff de O et N*/
    if (strchr("NO",p_e3->indmths) == NULL)
    {
        return (39);
    }

    /* Controle du p�rim�tre du champ d'application de la majoration THS */
    if(((p_e3->indmths) == 'O') && (recherche_dep_TLV(p_e3->dep) == 0))
    {
        return (40);
    }


    /* Controle taux d'imposition de la majoration THS */
    if ((p_e3->indmths == 'O') && (p_e3->timths > 0.60))
    {
        return (41);
    }

    /* Controle indicateur d'assujetissement a la majoration THS */
    if (strchr("O ",p_e2->indaths) == NULL)
    {
        return (42);
    }

    /* regul different de R */
    if (strchr("R ",p_e2->regul) == NULL)
    {
        return(5201);
    }

    /* comparaison  annee de reference */
    if (p_e2->antax != p_e3->antax)
    {
        return (51);
    }

    /* code tax  S ou E et pac # zero */
    if ((p_e2->tax == 'S' || p_e2->tax == 'E') && ((p_e2->nbpac != 0) || (p_e2->nbera != 0)))
    {
        return (52);
    }

    /* code tax S et code exoneration servi autre que G */
    if ((p_e2->tax == 'S')&&(p_e2->degex != ' ')&&(p_e2->degex != 'G'))
    {
        return (54);
    }

    /* code tax S et code ni = "A" */
    if ((p_e2->tax == 'S')&&(p_e2->codni == 'A'))
     {
     return (55);
     }

    /* code deg D ,code DEP # 97 */
    retour=strcmp(p_e3->dep,dep97); /* comparaison si dep 97 */
    if (retour!=0) /* traitement metropole */
    {
        if (p_e2->degex=='D')
        {
            return (58);
        }
    }

    /* code deg G , code nature cnat # 5 */
    if ((p_e2->degex == 'G') && (p_e2->cnat != '5'))
    {
        return (61);
    }

    /* champ # c  qtvrt = 1 */
    if ((p_e2->champ != 'C') && (p_e2->qtvrt == '1'))
    {
        return (65);
    }

    /* champ = c  tax = e */
    if ((p_e2->champ == 'C') && (p_e2->tax == 'E'))
    {
        return (66);
    }

    /* champ = c  cnat = 5 */
    if ((p_e2->champ == 'C') && (p_e2->cnat == '5'))
    {
        return (67);
    }

    /* champ # c  degtv servi */
    if ((p_e2->champ != 'C') && (p_e2->degtv != ' '))
    {
        return (69);
    }

    /* degtv = e j  degth # h z ' ' */
    if ((strchr("EJ",p_e2->degtv) != NULL) && (strchr("K ",p_e2->degex) == NULL))
    {
        return (70);
    }

  /* bug 305545 */
    /* suppression de CA_ANO_0379 Anomalie 72 - VL locaux habitation nulle */



    /* CA_ANO_0384 Anomalie 73 - Locaux professionnels hors champ THLV */
    if ((p_e2->tyimp == 'T') && (gbl_nbLp > 0))
    {
        return(73);
    }

    /* 2021MAJ07-03-04 */
    /* CA_ANO_0404 */
    /* Controle du code degrevement TV et du rang de sortie en sifflet exoneration TV */
    if (((p_e2->degtv == 'K') && (strchr("12345678",p_e2->rgstv) == NULL)) ||
       ((p_e2->degtv != 'K') && (p_e2->rgstv != ' ')))
    {
        return(75);
    }
    /* CA_ANO_0355 */
    /* Controle du rang de sortie en sifflet exoneration TH */
    if ((p_e2->degex == 'K' && strchr("12345678",p_e2->rgsor) == NULL) ||
        (p_e2->degex != 'K' && p_e2->rgsor != ' '))
    {
        return(77);
    }
/* 2023MAJ07-02-04*/
    /* test  > 200000 */
    if (p_e3->cole_c.vlmoy>200000)

    {
        return (101);
    }
/* FIN 2023MAJ07-02-04*/



    /* quotite abat . cne general a la base */
    retour =strcmp(p_e3->dep,dep97);  /* comparaison si dep 97 */
    if (retour !=0) /* trait metropole */
    {
        if (!( strcmp(p_e3->cole_f.txbas,"F ") == 0))
        {
            quot_abat=arrondi_euro_inf((p_e3->cole_f.vlmoy * 0.15)+1);
            if (p_e3->cole_f.abbas > quot_abat)
            {
                return (104);
            }
        }
    }

    /* quotite abat . cne special a la base */
    quot_abat=arrondi_euro_inf((p_e3->cole_f.vlmoy * 0.15)+1);
    if (p_e3->cole_f.abspe > quot_abat)
    {
        return (105);
    }

    /*  anomalie PAC alsace moselle */
    if (((p_e3->codeg!=' ')||(p_e3->codef!=' '))&&
        ((p_e3->cole_f.apac1>0)||(p_e3->cole_f.apac3 > 0)))
    {
        return (106);
    }

    if (p_e3->codeg != ' ')
    {
        return (109);
    }

    /* indicateur CU # c ou blanc */
    if ((p_e3->cocnq!='C')&&(p_e3->cocnq!=' '))
    {
        return (111);
    }

    /* alsace moselle codef servi , abmos doit etre positif */
    if ((p_e3->codef!=' ')&&(p_e3->abmos <= 0))
    {
        return (114);
    }

    /* a n o m a l i e  115 */
    if (p_e3->vmd89 > 200000)
    {
        return (115);
    }


    /* a n o m a  l i e  117 */
    retour=strcmp(p_e3->dep,dep97);
    if ((retour != 0) && (p_e3->vmd89 != 0))
    {
        return (117);
    }



    /* quotite d'abattements ASH commune */
    if (!(strcmp(p_e3->cole_f.txhan,"  ") == 0) && !(strcmp(p_e3->cole_f.txhan,"00") == 0))
    {
        if ((p_e3->cole_f.abhan > 0.2*p_e3->cole_f.vlmoy + 1) ||
            (p_e3->cole_f.abhan < 0.1*p_e3->cole_f.vlmoy - 1))
        {
            return (119);
        }
    }

    /*   trait vl moyenne groupement de cnes */
    if (p_e3->cole_q.vlmoy > 200000)
    {
        return (201);
    }



    /*   quotite abat group cne abat general base  */
    quot_abat=arrondi_euro_inf((p_e3->cole_r.vlmoy * 0.15)+1);
    if (p_e3->cole_r.abbas > quot_abat)
    {
        return (204);
    }

    /*   quotite abat group cne abat special base  */
    quot_abat=arrondi_euro_inf((p_e3->cole_r.vlmoy * 0.15)+1);
    if (p_e3->cole_r.abspe > quot_abat)
    {
        return (205);
    }

    /* quot. abatt. group.comm # zeros */
    if (((p_e3->cole_r.vlmoy != 0) || (p_e3->cole_r.abspe != 0 ) ||
        (p_e3->cole_r.abbas != 0) || (p_e3->cole_r.apac1 != 0) ||
        (p_e3->cole_r.apac3 != 0))&&(p_e3->cocnq == ' '))
    {
        return (206);
    }

    /* taux imp. group cne non nul et CU a blanc */
    if ((p_e3->timpq != 0) && (p_e3->cocnq == ' '))
    {
        return (211);
    }

    /* quotite d'abattements ASH inter-comm */
    if (!(strcmp(p_e3->cole_r.txhan,"  ") == 0) && !(strcmp(p_e3->cole_r.txhan,"00") == 0))
    {
        if ((p_e3->cole_r.abhan > 0.2*p_e3->cole_r.vlmoy + 1) ||
            (p_e3->cole_r.abhan < 0.1*p_e3->cole_r.vlmoy - 1))
        {
            return (212);
        }
    }

    /* Declenchement du message 110 si somme des taux d'imposition anormalement �lev�e */
    if ((p_e3->timpc + p_e3->timpq + p_e3->tisyn + p_e3->titsn + p_e3->titgp+ p_e3->timpe) > 1)/* c'est a dire 100% */
    {
        return (110);
    }
    /* CA_ANO_0378 */
    if ((p_e3->timpc7 + p_e3->timpq7 + p_e3->tisyn7 + p_e3->titsn7 + p_e3->titgp7 + p_e3->timpe7) > 1)/* c'est a dire 100% */
    {
        return (157);
    }

       /* CA_ANO_0402 */
    if (p_e2->nbpac < 0)
    {
        return (3022);
    }

       /* CA_ANO_0403 */
    if (p_e2->nbera < 0)
    {
        return (3023);
    }


    /* tous les controles sont corrects */
    return (0);
}
/*------------------------ fin des controles generaux ------------------------*/

/*----------------------------------------------------------------------------*/
/*                    Initialisation de la zone de sortie                     */
/*----------------------------------------------------------------------------*/

void  initialisation_sortie(s_s2 * p_s2)
{

    p_s2->vlbr = 0;               /*VL brute revisee N                  */
    p_s2->vlbrt = 0;              /*VL brute revisee N TSE              */
    p_s2->vlbr7 = 0;              /*VL brute revisee 2017               */
    p_s2->vlbrt7 = 0;             /*VL brute revisee 2017 TSE           */
    p_s2->vlb77 = 0;              /*VL brute base 70 2017               */
    p_s2->cotic = 0;
    p_s2->cotiq = 0;
    p_s2->cotis = 0;
    p_s2->cotin = 0;
    p_s2->cotig = 0;
    p_s2->cotie = 0;
    p_s2->coticf = 0;
    p_s2->cotiqf = 0;
    p_s2->cotisf = 0;
    p_s2->cotief = 0;
    p_s2->cotilcf = 0;
    p_s2->cotilqf = 0;
    p_s2->cotilsf = 0;
    p_s2->cotilef = 0;
    p_s2->cotilc = 0;
    p_s2->cotilq = 0;
    p_s2->cotils = 0;
    p_s2->cotiln = 0;
    p_s2->cotilg = 0;
    p_s2->cotile = 0;
    p_s2->palis  = 0;
    p_s2->lissac = 0;
    p_s2->lissaq = 0;
    p_s2->lissas = 0;
    p_s2->lissan = 0;
    p_s2->lissag = 0;
    p_s2->lissae = 0;
    p_s2->qtprltc = 0;
    p_s2->qtprltq = 0;
    p_s2->qtprlts = 0;
    p_s2->qtprltn = 0;
    p_s2->qtprltg = 0;
    p_s2->qtprlte = 0;
    p_s2->frait = 0;
    p_s2->fraitf = 0;
    p_s2->frai4 = 0;
    p_s2->majths = 0;
    p_s2->limajths = 0; /* Bug 212403 amelioration */
    p_s2->coticm = 0;
    p_s2->somrc = 0;
    p_s2->degpl = 0;
    p_s2->totth = 0;
    p_s2->totthf = 0;
    p_s2->netth = 0;
    p_s2->netthf = 0;
    p_s2->mtexo = 0;
    p_s2->somrp = 0;
    strncpy(p_s2->codro, "  ",3);
    p_s2->cottv = 0;
    p_s2->fratv = 0;
    p_s2->somtv = 0;
    p_s2->mdgtv = 0;
    p_s2->nettv = 0;
    strncpy(p_s2->roltv, "   ",4);
    p_s2->netap = 0;
    p_s2->signe = ' ';
    p_s2->napdt = 0;
    p_s2->imap  = ' ';
    strncpy(p_s2->codegm, "    ",5);
    p_s2->txrefm = 0;
    p_s2->fgest_cq = 0;
    p_s2->fgest_s  = 0;
    p_s2->fgest_ng = 0;
    p_s2->fgest_e  = 0;
    p_s2->far_cq   = 0;
    p_s2->far_s    = 0;
    p_s2->far_ng   = 0;
    p_s2->far_e    = 0;


    /*Abattements applicables � la c�te*/
    init_cols(&p_s2->cols_c);          /*structure sortie commune                            */
    init_cols(&p_s2->cols_q);          /*structure sortie intercommunalite                   */
    init_cols(&p_s2->cols_s);          /*structure sortie syndicat                           */
    init_cols(&p_s2->cols_n);          /*structure sortie TSE                                */
    init_cols(&p_s2->cols_g);          /*structure sortie TSE Autre                          */
    init_cols(&p_s2->cols_e);          /*structure sortie GEMAPI                             */
    init_cols(&p_s2->cols_f);          /*structure sortie commune avant ajustement           */
    init_cols(&p_s2->cols_r);          /*structure sortie intercommunalite avant ajustement  */

    init_cols(&p_s2->cols_cr);          /*structure sortie commune r�vis�e N                           */
    init_cols(&p_s2->cols_qr);          /*structure sortie intercommunalite  r�vis�e N                 */
    init_cols(&p_s2->cols_sr);          /*structure sortie syndicat     r�vis�e N                      */
    init_cols(&p_s2->cols_nr);          /*structure sortie TSE          r�vis�e N                      */
    init_cols(&p_s2->cols_gr);          /*structure sortie TSE Autre     r�vis�e N                     */
    init_cols(&p_s2->cols_er);          /*structure sortie GEMAPI        r�vis�e N                     */
    init_cols(&p_s2->cols_fr);          /*structure sortie commune avant ajustement   r�vis�e N        */
    init_cols(&p_s2->cols_rr);          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/


    init_cols(&p_s2->cols_c7);          /*structure sortie commune 2017 base 70               */
    init_cols(&p_s2->cols_q7);          /*structure sortie intercommunalite 2017 base 70      */
    init_cols(&p_s2->cols_s7);          /*structure sortie syndicat 2017 base 70              */
    init_cols(&p_s2->cols_n7);          /*structure sortie TSE 2017 base 70                   */
    init_cols(&p_s2->cols_g7);          /*structure sortie TSE autre 2017 base 70             */
    init_cols(&p_s2->cols_e7);          /*structure sortie GEMAPI 2017 base 70                */

    init_cols(&p_s2->cols_cr7);         /*structure sortie commune 2017 revisee               */
    init_cols(&p_s2->cols_qr7);         /*structure sortie intercommunalite 2017 revisee      */
    init_cols(&p_s2->cols_sr7);         /*structure sortie syndicat 2017 revisee              */
    init_cols(&p_s2->cols_nr7);         /*structure sortie TSE 2017 revisee                   */
    init_cols(&p_s2->cols_gr7);         /*structure sortie TSE autre 2017 revisee             */
    init_cols(&p_s2->cols_er7);         /*structure sortie GEMAPI 2017 revisee                */
}

void  initialisation_sortieQtp( s_qtpAb *p_qtp)
{
    init_cols(&p_qtp->qtp_cr);          /*structure sortie commune r�vis�e N                           */
    init_cols(&p_qtp->qtp_qr);          /*structure sortie intercommunalite  r�vis�e N                 */
    init_cols(&p_qtp->qtp_sr);          /*structure sortie syndicat     r�vis�e N                      */
    init_cols(&p_qtp->qtp_nr);          /*structure sortie TSE          r�vis�e N                      */
    init_cols(&p_qtp->qtp_gr);          /*structure sortie TSE Autre     r�vis�e N                     */
    init_cols(&p_qtp->qtp_er);          /*structure sortie GEMAPI        r�vis�e N                     */
    init_cols(&p_qtp->qtp_fr);          /*structure sortie commune avant ajustement   r�vis�e N        */
    init_cols(&p_qtp->qtp_rr);          /*structure sortie intercommunalite avant ajustement  r�vis�e N*/



    init_cols(&p_qtp->qtp_c7);          /*structure sortie commune 2017 base 70               */
    init_cols(&p_qtp->qtp_q7);          /*structure sortie intercommunalite 2017 base 70      */
    init_cols(&p_qtp->qtp_s7);          /*structure sortie syndicat 2017 base 70              */
    init_cols(&p_qtp->qtp_n7);          /*structure sortie TSE 2017 base 70                   */
    init_cols(&p_qtp->qtp_g7);          /*structure sortie TSE autre 2017 base 70             */
    init_cols(&p_qtp->qtp_e7);          /*structure sortie GEMAPI 2017 base 70                */

    init_cols(&p_qtp->qtp_cr7);         /*structure sortie commune 2017 revisee               */
    init_cols(&p_qtp->qtp_qr7);         /*structure sortie intercommunalite 2017 revisee      */
    init_cols(&p_qtp->qtp_sr7);         /*structure sortie syndicat 2017 revisee              */
    init_cols(&p_qtp->qtp_nr7);         /*structure sortie TSE 2017 revisee                   */
    init_cols(&p_qtp->qtp_gr7);         /*structure sortie TSE autre 2017 revisee             */
    init_cols(&p_qtp->qtp_er7);         /*structure sortie GEMAPI 2017 revisee                */
}

void   init_cols(s_cols *  p_c)
{
    p_c->abgen = 0;
    p_c->abpac = 0;
    p_c->abp12 = 0;
    p_c->abspe = 0;
    p_c->abhan = 0;
    p_c->vlntt = 0;
    p_c->vlnex = 0;
    p_c->vlnnex = 0;
    p_c->vlnadm = 0;
    p_c->vlnabd = 0;
    p_c->vlnabf = 0;
    p_c->bnimp = 0;
    p_c->absps = ' '; /* CA_TH_0117 */
    p_c->abspsa = ' ';
    p_c->coti = 0;
    p_c->cotif = 0;
}

void initialisation_coe1(s_coe1 *p_coe)
{
    p_coe->antax  = 0;
    strncpy(p_coe->dep,"  ",3);
    strncpy (p_coe->cne,"   ",4);
    p_coe->tyimp  = ' ' ;
    p_coe->degex  = ' ' ;
    p_coe->cnat   = ' ' ;
    p_coe->tax        = ' ';
    p_coe->indthlv    = ' ';
    p_coe->indmths    = ' ';
    p_coe->indgem     = ' ';
    p_coe->indaths    = ' ';
    p_coe->aff        = ' ';
    p_coe->permo      = ' ';
    p_coe->grrev      = ' ';
    p_coe->imaisf     = ' ';
    p_coe->revffm  =0;
    p_coe->workimaisf = ' ';
    p_coe->ind_origine_kcoc  = 0;
    p_coe->ind_absence_TSE   = 0;
    p_coe-> vl = 0 ;
    p_coe-> vlt = 0 ;
    p_coe->cocnq     = ' ';

}

void initialisation_coe2(s_coe2 *p_coe2)
{
     p_coe2->timpc =0;
     p_coe2->tisyn =0;
     p_coe2->titsn =0;
     p_coe2->timpq =0;
     p_coe2->titgp =0;
     p_coe2->timpe =0;
     p_coe2->timths=0;
}

/* retourne 1 si indloc est renseigne */
int isLocPro (char indloc[])
{
    int lg = strlen (indloc);
    int i;
    for(i=0 ; i < lg ; i++)
    {
        if(!(isspace(indloc[i])) && indloc[i] != '\0')
        {
            return (1);
        }
    }
    return (0);
}

void copieCols (s_cols source , s_cols *p_dest)
{
    p_dest->abgen=source.abgen;
    p_dest->abpac=source.abpac;
    p_dest->abp12=source.abp12;
    p_dest->abspe=source.abspe;
    p_dest->abhan=source.abhan;
    p_dest->vlntt=source.vlntt;
    p_dest->vlnex=source.vlnex;
    p_dest->vlnnex=source.vlnnex;
    p_dest->vlnadm=source.vlnadm;
    p_dest->vlnabd=source.vlnabd;
    p_dest->vlnabf=source.vlnabf;
    p_dest->bnimp=source.bnimp;
    p_dest->absps=source.absps;
    p_dest->abspsa=source.abspsa;
    p_dest->coti =source.coti;
    p_dest->cotif=source.cotif;
}
