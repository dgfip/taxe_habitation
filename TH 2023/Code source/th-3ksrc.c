    /*============================================*/
    /*  MODULE DE CALCUL DES FRAIS, PRELEVEMENTS  */
    /*           ET SOMME A RECOUVRER             */
    /*           programme th-3kSRC.C             */
    /*============================================*/


#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif


int th_3ksrc(s_sre * p_sre ,        /* pointeur sur structure entree          */
             s_srs  * p_srs)        /* pointeur sur structure sortie          */
{
    int     ret;
    s_cons  *p_cons            ;    /* pointeur sur struct des constantes     */
    long    fraisDR      = 0   ;    /* frais de role                          */
    long    fraisAR      = 0   ;    /* frais d'assiette et de recouvrement    */
    double  fgest_cq     = 0   ;    /* frais de gestion commune , interco.
    								   & majoration THS                       */
    long    fgest_s      = 0   ;    /* frais de gestion syndicat              */
    long    fgest_ng     = 0   ;    /* frais de gestion TSE TSE Autre         */
    double  fgest_e      = 0   ;    /* frais de gestion GEMAPI         */
    double  far_cq       = 0   ;    /* frais d'assiette commune interco.      */
    long    far_s        = 0   ;    /* frais d'assiette syndicat              */
    long    far_ng       = 0   ;    /* frais d'assiette TSE TSE Autre         */
    double  far_e        = 0   ;    /* frais d'assiette GEMAPI        */

    double  fgest_cqf     = 0   ;    /* frais de gestion fictifs commune , interco.*/
    double  fgest_sf      = 0   ;    /* frais de gestion fictifs syndicat              */
    double  fgest_ef      = 0   ;    /* frais de gestion fictifs GEMAPI         */

    long    totcott      = 0   ;
    long    totcottf     = 0   ;
    long    totcotcq     = 0   ;    /* total des cotisations commune majoree THS, interco */
    long    totcotng     = 0   ;    /* total des cotisations TSE et TSE Autre */
    int ret_cons;       /* code retour de cherche_const */
    static s_signature signature;

    p_srs->versr = 'A' ;

    /* controle de la signature */
    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_srs->signature = &signature  ;

    /* CA_ANO_0382 Controle du module KSRC */
    ret = controle_signature(RKSRC,p_srs->versr, &(p_srs->libelle));
    if (ret != 0 )
    {
        return (ret);
    }

    /* recherche des constantes */
    ret_cons=(short)cherche_const(p_sre->antax,&p_cons);
    cherche_Erreur(ret_cons,&(p_srs->libelle)) ;
    if (ret_cons == 1)          /* annee invalide */
    {
        return (1);
    }

    /* CA_TH_0043 Determination de la majoration THS */

    /* 2019MAJR11-01-01 */
    if (p_sre->tax == 'S') /* Articles THS */
    {

        if   (((p_sre->cnat != '5')  || (p_sre->indaths == 'O')) && (p_sre->vlbpc != 0)) /*personne morale ou personne physique assujettie � la majoration*/
        {
            if (p_sre->indmths == 'O') /* si majoration applicable*/
            {
                p_srs->majths = arrondi_euro_voisin((p_sre->cotic * p_sre->timths) * ((float)p_sre->vlbch/(float)p_sre->vlbpc));
                p_srs->limajths = 0 ;
            }
            else
            {
                p_srs->majths = 0 ;
                p_srs->limajths = 0 ;
            }
        }
        else
        {
            p_srs->majths = 0 ;
            p_srs->limajths = 0 ;
        }
    }
    else /* Articles THP et THE */
    {
        p_srs->majths = 0 ;
        p_srs->limajths = 0 ;
    }

    /* FIN  2019MAJR11-01-01 */

    /* CA_TH_0044 Determination cotisation majoree  */
    p_srs->coticm = p_sre->cols_c.coti + p_srs->majths ;

    /* 3 */
    /* CA_TH_0008 Calcul des frais */

    /* Calcul total cotisation commune majoree + interco */
    totcotcq = p_srs->coticm + p_sre->cols_q.coti ;

    /* Calcul total cotisation TSE + TSE Autre */
    totcotng = p_sre->cols_n.coti + p_sre->cols_g.coti;

    /* Calcul total cotisation y compris THLV_0007 */

    totcott = p_sre->cols_c.coti + p_sre->cols_s.coti + p_sre->cols_q.coti +
              p_sre->cols_n.coti + p_sre->cols_g.coti + p_sre->cols_e.coti;

    /* CA_TH_0007-1 Calcul total cotisation fictive */

    totcottf = p_sre->cols_c.cotif + p_sre->cols_s.cotif + p_sre->cols_q.cotif + p_sre->cols_e.cotif;

    /* initialisation Frais */
    p_srs->fgest_cq = fgest_cq;
    p_srs->fgest_s  = fgest_s;
    p_srs->fgest_ng = fgest_ng;
    p_srs->far_cq = far_cq;
    p_srs->far_s  = far_s;
    p_srs->far_ng = far_ng;
    p_srs->fgest_e  = fgest_e;
    p_srs->far_e  = far_e;
    p_srs->frai4  = 0;
    p_srs->frait  = 0;
    p_srs->fraitf  = 0;
    p_srs->somrc  = 0;
    p_srs->somrcf  = 0;

    if (p_sre->tyimp == 'H')
    {
        if (p_sre->tax == 'S')
        {  /* frais THS  */
            /* CA_TH_0008 1.a */
            /* syndicat */
            p_srs->fgest_s = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FGEST_THS_s);
            p_srs->far_s   = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FAR_THS_s);

            /* TSE et TSE Autre */
            p_srs->fgest_ng = frais_ar(totcotng, p_cons->coeff_FGEST_THS_t);
            p_srs->far_ng   = frais_ar(totcotng, p_cons->coeff_FAR_THS_t);

            /* CA_TH_0008 1.b */
            /* Commune majoree - Interco  + GEMAPI */
            p_srs->fgest_cq = frais_ar(totcotcq, p_cons->coeff_FGEST_THS_c) + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FGEST_THS_g);
            p_srs->far_cq   = frais_ar(totcotcq, p_cons->coeff_FAR_THS_c)   + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FAR_THS_g);

            /* Frais TH GEMAPI valorises a 0 a voir selon reponse DAF */
            p_srs->fgest_e = 0;
            p_srs->far_e  = 0;

            /* frais de gestion de la cote */
            p_srs->frait = p_srs->fgest_cq + p_srs->fgest_s + p_srs->fgest_ng;

            /* frais d'assiette et de recouvrement de la cote*/
            p_srs->frai4 = p_srs->far_cq + p_srs->far_s + p_srs->far_ng;
        }
        else
        {  /* frais THP ou THE  */
            /* CA_TH_0008 2.a */
            /* syndicat */
            p_srs->fgest_s = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FGEST_THP_THE_s);
            p_srs->far_s   = frais_ar(p_sre->cols_s.coti, p_cons->coeff_FAR_THP_THE_s);

            /* TSE et TSE Autre */
            p_srs->fgest_ng = frais_ar(totcotng, p_cons->coeff_FGEST_THP_THE_t);
            p_srs->far_ng   = frais_ar(totcotng, p_cons->coeff_FAR_THP_THE_t);

            /* CA_TH_0008 2.b */
            /* Commune majoree - Interco  + GEMAPI */

            p_srs->fgest_cq = frais_ar(totcotcq, p_cons->coeff_FGEST_THP_THE_c) + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FGEST_THP_THE_g);
            p_srs->far_cq   = frais_ar(totcotcq, p_cons->coeff_FAR_THP_THE_c)   + frais_ar(p_sre->cols_e.coti, p_cons->coeff_FAR_THP_THE_g);

            /* Frais TH GEMAPI valorises a 0 a voir selon reponse DAF */
            /* Ils sont integres a fgest_cq et far_cq et on remet les zones dediees a zero pour raisons comptables */
            p_srs->fgest_e = 0;
            p_srs->far_e  = 0;

            /* frais de gestion de la cote */
            p_srs->frait = p_srs->fgest_cq + p_srs->fgest_s + p_srs->fgest_ng ;

            /* frais d'assiette et de recouvrement de la cote*/
            p_srs->frai4 = p_srs->far_cq + p_srs->far_s + p_srs->far_ng;

            /* CA_TH_0008-1 DETERMINATION DES FRAIS DE GESTION FICTIFS */
            fgest_sf = frais_ar(p_sre->cols_s.cotif, p_cons->coeff_FGEST_THP_THE_s);
            fgest_ef = frais_ar(p_sre->cols_e.cotif, p_cons->coeff_FGEST_THP_THE_g);
            fgest_cqf = frais_ar(p_sre->cols_c.cotif + p_sre->cols_q.cotif , p_cons->coeff_FGEST_THP_THE_c) ;

            /* frais de gestion fictifs de la cote */
            p_srs->fraitf = fgest_cqf + fgest_sf + fgest_ef ;
        }
    }

    if (p_sre->tyimp == 'T')  /* frais THLV */
    {
        p_srs->frai4  = 0;
        p_srs->frait  = 0;
        p_srs->majths = 0;

        /* En cas de THLV avec GEMAPI, les taux de frais sont diff�rents et il faut
        calculer s�par�ment les frais sur Commune/Syndicat (ou Interco) puis GEMAPI */

        /* toutes les cotisations sauf GEMAPI */

         /*CA_THLV_0005 DETERMINATION COTISATION COMMUNE+SYNDICAT OU INTERCO cf calcul totcott */
        totcotcq = p_sre->cols_c.coti + p_sre->cols_s.coti + p_sre->cols_q.coti;

        /*CA_THLV_0008 DETERMINATION DES FRAIS  COMMUNE+SYNDICAT OU INTERCO */
        fraisDR = frais_ar(totcotcq, p_cons->coeff_frais_THLV);
        fraisAR = frais_ar(totcotcq, p_cons->coeff_frais_ass_THLV);

        /*CA_THLV_0009 DETERMINATION DES FRAIS  GEMAPI */
        fgest_e = frais_ar(p_sre->cols_e.coti, p_cons->coeff_FGEST_THP_THE_g);
        far_e   = frais_ar(p_sre->cols_e.coti, p_cons->coeff_FAR_THP_THE_g);

        /* CA_THLV_0009-1 DETERMINATION DES FRAIS TSE et TSE Autre (utilise les constantes THS)*/
        p_srs->fgest_ng = frais_ar(totcotng, p_cons->coeff_FGEST_THS_t);
        p_srs->far_ng   = frais_ar(totcotng, p_cons->coeff_FAR_THS_t);


        p_srs->fgest_e  = fgest_e;
        p_srs->far_e    = far_e;
        p_srs->fgest_cq = fraisDR;
        p_srs->far_cq  = fraisAR;

        /* CA_THLV_00010 DETERMINATION DES FRAIS DE GESTION THLV */
        p_srs->frait = ( fraisDR + fgest_e + p_srs->fgest_ng);
        p_srs->frai4 = ( fraisAR + far_e + p_srs->far_ng );
    }

    /* evolution 2008 : mise a zero des frais pour la commune de Saint-Martin CA_G0052*/
    if (!strcmp(p_sre->cne,"127") && !strcmp(p_sre->dep,"97"))
    {
        p_srs->frait = 0;
        p_srs->fraitf= 0;
        p_srs->frai4 = 0;
    }

    /* 4 */

    /* CA_TH_0011 + CA_THLV_0012 DETERMINATION SOMME A RECOUVRER */
    p_srs->somrc = totcott + p_srs->frait + p_srs->majths;

    /* CA_TH_0011-1 DETERMINATION SOMME A RECOUVRER FICTIVE */
    p_srs->somrcf = totcottf + p_srs->fraitf;


    /* 5 */
    /* traitement fin */

    return (0);
}

/* ----------------------------- */
/* fonction de calcul des frais  */
/* ----------------------------- */

long frais_ar(long totcot, double taux)
{
    return (arrondi_euro_voisin(totcot * taux));
}
