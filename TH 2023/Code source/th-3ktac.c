    /*=====================================*/
    /*    MODULE DE DETERMINATION DE LA    */
    /*  DOUBLE LIQUIDATION ET DES DONNEES  */
    /*     DES COLLECTIVITES N ET 2017     */
    /*                                     */
    /*        programme th-3kTAC.C         */
    /*=====================================*/


#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif

/*============================================================================
   M A I N
  ============================================================================*/

int th_3ktac ( s_e8 * p_e8, s_s8 * p_s8)
{
    int rets=0;       /* code retour signature */
    char versr='B'; /* version du programme */
    int retour=0 ;    /* code retour controle */
    int retp=0;     /* code retour final */

    int retcons=0;  /*code retour cherche_const  */
    static s_signature signature;
    static s_cons * p_cons=NULL;

    /* long sv_vmd89; */
    long Zero = 0;
    char blanc = ' ';
    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_s8->signature = &signature  ;
    p_s8->versr = versr;
    p_s8->anref = ANREF;

    init_colb_s8(p_s8);

    /* controle signature */
    rets=controle_signature(RKTAC,p_s8->versr,&(p_s8->libelle));

    if (rets!=0)
    {
        return(rets);
    }

    /* ajout des espaces pour les abattements forfaitaires */
    ajout_espace_taux_abattement(&(p_e8->cole_c));
    ajout_espace_taux_abattement(&(p_e8->cole_q));
    ajout_espace_taux_abattement(&(p_e8->cole_f));
    ajout_espace_taux_abattement(&(p_e8->cole_r));

    ajout_espace_taux_abattement(&(p_e8->cole_h)); /* commune annee 2017*/
    ajout_espace_taux_abattement(&(p_e8->cole_l)); /* interco annee 2017*/
    ajout_espace_taux_abattement(&(p_e8->cole_k)); /* commune avt ajustement annee 2017*/

    retcons=(short)cherche_const(p_e8->antax,&p_cons);
    cherche_Erreur(retcons,&(p_s8->libelle)) ;
    if (retcons == 1)          /* annee invalide */
    {
        return (1);
    }

    /* controle  des  donnees en entree  */
    retour = controle_ktac(p_cons,p_e8,p_s8);

    if (retour != 0)
    {
        cherche_Erreur(retour,&(p_s8->libelle));
        return (retour);
    }

    /* determination des abattements annee N */

    /* Donn�es commune */
    Abt_colb_Charge (&(p_e8->abmos),&(p_e8->codef),&(p_e8->codeg),
                     &(p_e8->cole_c),&(p_s8->colb_c));

    /* Donn�es commune avant ajustement */
    Abt_colb_Charge (&(p_e8->abmos),&(p_e8->codef),&(p_e8->codeg),
                     &(p_e8->cole_f),&(p_s8->colb_f));

    /* Donn�es syndicat */
    if (p_e8->tisyn != 0)
    {
        /* commune avant ajustement */
        /*Abt_colb_Recopie (&(p_s8->colb_f), &(p_s8->colb_s));*/
        Abt_colb_Recopie (&(p_e8->cole_f),&(p_s8->colb_f), &(p_s8->colb_s));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_s));
    }

    /* Donn�es intercommunalit� */
    if (p_e8->cocnq == 'C')
    {
        if (A_Son_Propre_Regime_Taux(&(p_e8->cole_q)))
        {
            Abt_colb_Charge (&Zero, &blanc, &blanc, &(p_e8->cole_q),
                             &(p_s8->colb_q));
        }
        else
        {
            /* commune avant ajustement */
            /*Abt_colb_Recopie (&(p_s8->colb_f), &(p_s8->colb_q));*/
            Abt_colb_Recopie (&(p_e8->cole_q),&(p_s8->colb_f), &(p_s8->colb_q));

            /* Si l'interco a des quotit�s ajust�es
            de la VLM interco � partir de la VLM communale*/
            if(A_Son_Propre_Regime(&(p_e8->cole_q)))
            {
                p_s8->colb_q.abbas = p_e8->cole_q.abbas;
                p_s8->colb_q.apac1 = p_e8->cole_q.apac1;
                p_s8->colb_q.apac3 = p_e8->cole_q.apac3;
                p_s8->colb_q.abspe = p_e8->cole_q.abspe;
                p_s8->colb_q.abhan = p_e8->cole_q.abhan;
            }
        }
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_q));
    }

    /* Donn�es intercommunalit� avant ajustement */
    if (p_e8->cocnq == 'C')
    {
        if (A_Son_Propre_Regime(&(p_e8->cole_r)))
        {
            Abt_colb_Charge (&Zero, &blanc, &blanc, &(p_e8->cole_r),
                             &(p_s8->colb_r));
        }
        else
        {
            /*Abt_colb_Recopie (&(p_s8->colb_f), &(p_s8->colb_r));*/
            Abt_colb_Recopie (&(p_e8->cole_r),&(p_s8->colb_f), &(p_s8->colb_r));
        }
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_r));
    }

    /* Donn�es TSE */
    if (p_e8->titsn != 0)
    {
        /*Abt_colb_Recopie (&(p_s8->colb_f), &(p_s8->colb_n));*/
        Abt_colb_Recopie (&(p_e8->cole_f), &(p_s8->colb_f), &(p_s8->colb_n));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_n));
    }

    /* Donn�es TSE Autre */
    if (p_e8->titgp != 0)
    {
        /*Abt_colb_Recopie (&(p_s8->colb_f), &(p_s8->colb_g));*/
        Abt_colb_Recopie (&(p_e8->cole_f),&(p_s8->colb_f), &(p_s8->colb_g));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_g));
    }

    /* Donn�es GEMAPI */
    if (p_e8->timpe > 0)
    {
        /*Abt_colb_Recopie (&(p_s8->colb_c), &(p_s8->colb_e));*/
        Abt_colb_Recopie (&(p_e8->cole_c),&(p_s8->colb_c), &(p_s8->colb_e));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_e));
    }

    /* Ajout traitement annee 2017 si la collectivite existe en 2017 */

    /* Donn�es commune 2017 */
    /* CA_DCOL_0025 */
    Abt_colb_Charge (&(p_e8->abmos7),&(p_e8->codef7),&(p_e8->codeg),
                     &(p_e8->cole_h),&(p_s8->colb_h));

    /* Donn�es syndicat 2017 */
    /* CA_DCOL_0026 */
    if (p_e8->tisyn7 != 0)
    {
        /* si taux syndicat non nul, donnees commune 2017 dans syndicat */
        Abt_colb_Charge (&(p_e8->abmos7),&(p_e8->codef7),&(p_e8->codeg),
                         &(p_e8->cole_k),&(p_s8->colb_t));
    }
    else
    {
        /*si taux nul pour syndicat, alors initialisation donnees syndicat � 0 */
        Abt_colb_Init (&(p_s8->colb_t));
    }

    /* Donn�es intercommunalit� 2017 */
    /* CA_DCOL_0027 */
    if (p_e8->timpq7 > 0) /* si commune appartient a un EPCI */
    {
        if (A_Son_Propre_Regime_Taux(&(p_e8->cole_l)))
        {
            Abt_colb_Charge (&Zero, &blanc, &blanc, &(p_e8->cole_l),
                             &(p_s8->colb_l));
        }
        else
        {
            /* commune avant ajustement 2017 */
            Abt_colb_Charge (&(p_e8->abmos7),&(p_e8->codef7),&(p_e8->codeg),
                             &(p_e8->cole_k),&(p_s8->colb_l));

            /* Si l'interco 2017 a des quotit�s ajust�es
            de la VLM interco � partir de la VLM communale*/
            if(A_Son_Propre_Regime(&(p_e8->cole_l)))
            {
                p_s8->colb_l.abbas = p_e8->cole_l.abbas;
                p_s8->colb_l.apac1 = p_e8->cole_l.apac1;
                p_s8->colb_l.apac3 = p_e8->cole_l.apac3;
                p_s8->colb_l.abspe = p_e8->cole_l.abspe;
                p_s8->colb_l.abhan = p_e8->cole_l.abhan;
            }
        }
    }
    else
    {
        /* initialise les donnees interco 2017 � nul */
        Abt_colb_Init (&(p_s8->colb_l));
    }

    /* Donn�es TSE 2017 dans commune 2017 */
    /* CA_DCOL_0028 */
    if (p_e8->titsn7 != 0)
    {
         Abt_colb_Charge (&(p_e8->abmos7),&(p_e8->codef7),&(p_e8->codeg),
                         &(p_e8->cole_k),&(p_s8->colb_w));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_w));
    }

    /* Donn�es TSE Autre 2017 */
    /* CA_DCOL_0029 */
    if (p_e8->titgp7 != 0)
    {
         Abt_colb_Charge (&(p_e8->abmos7),&(p_e8->codef7),&(p_e8->codeg),
                          &(p_e8->cole_k),&(p_s8->colb_x));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_x));
    }
    /* Donn�es GEMAPI 2017 */
    /* CA_DCOL_0030 */
    if (p_e8->timpe7 > 0)
    {
        Abt_colb_Charge (&(p_e8->abmos7),&(p_e8->codef7),&(p_e8->codeg),
                           &(p_e8->cole_h),&(p_s8->colb_u));
    }
    else
    {
        Abt_colb_Init (&(p_s8->colb_u));
    }
    /* Fin ajout traitement annee 2017 */

    return (retp);
}

/* controle des donnees recues */

int controle_ktac(s_cons *p_cons,s_e8 * p_e8, s_s8 * p_s8)
{
    int retabatt=0;
    int retour;
    s_cole * pt;

    /* Traitement annee taxation */
    if ((p_e8->antax != p_s8->anref ))
    {
        return (1);
    }

    /* Commune */
    retabatt=Controle_Abattements(p_cons,&(p_e8->cole_f), p_e8->csdi);

    if (retabatt != 0)
    {
        retour= retabatt + 2300;
        return (retour);
    }

    /* Intercommunalit� */
    retabatt=Controle_Abattements(p_cons,&(p_e8->cole_r), p_e8->csdi);

    if (retabatt != 0)
    {
        retour= retabatt + 2320;
        return (retour);
    }

    if ((strchr("C ",p_e8->cocnq) == NULL) || (strchr("F ",p_e8->codef) == NULL)  ||  (strchr("12345 ",p_e8->codeg) == NULL))
    {
        return (2422);
    }

    if (((p_e8->abmos > 0) && (p_e8->codef != 'F' || p_e8->codeg != ' '))  ||
         (p_e8->abmos == 0 && (p_e8->codef == 'F' || (strchr("12345 ",p_e8->codeg) == NULL))))
    {
        return (2423);
    }

    pt = &p_e8->cole_q;

    if (p_e8->cocnq == ' ' && pt->apac1 > 0)
    {
        return (2421);
    }

    /* a n o  2475 GEMAPI  CA_ANO_0352 */
    if ((p_e8->indgem != ' ' ) && (p_e8->indgem != 'I' ) && (p_e8->indgem != 'M' ) && (p_e8->indgem != 'C' ))
    {
        return(2475);
    }

    if (((p_e8->indgem == ' ' ) && (p_e8->timpe != 0 ))
        || (p_e8->timpe > 1))
    {
        return(2476);
    }

    /* Controle de la valorisation du taux d�imposition de la TSE Autre */
    if ((p_e8->titgp != 0 )  && (!dep_TSE_autre(p_e8->dep)))
    {
        return(2425);
    }

    /* A partir d'ici modifs 2018 */
    /* CA_ANO_0361 */
    if (p_e8->cole_k.vlmoy < 0)
    {
        return(2480);
    }

    /* CA_ANO_0362 */
    if (p_e8->cole_k.abspe < 0)
    {
        return(2481);
    }

    /* CA_ANO_0363 */
    if (p_e8->cole_k.abbas < 0)
    {
        return(2482);
    }

    /* CA_ANO_0364 */
    if (p_e8->cole_k.apac1 < 0)
    {
        return(2483);
    }

    /* CA_ANO_0365 */
    if (p_e8->cole_k.apac3 < 0)
    {
        return(2484);
    }

    /* CA_ANO_0366 */
    if (p_e8->cole_k.abhan < 0)
    {
        return(2485);
    }

    /* CA_ANO_0367 */
    if (p_e8->cole_k.vlmoy > 15000)
    {
        return(2486);
    }

    if (strcmp(p_e8->csdi,"976") == 0)
    {
		/* CA_ANO_0368 */
		if (p_e8->cole_k.abspe > p_cons->plaf_quotite_may)
		{
			return (2487);
		}

		/* CA_ANO_0369 */
		if (p_e8->cole_k.abbas > p_cons->plaf_quotite_may)
		{
			return (2488);
		}

		/* CA_ANO_0370 */
		if (p_e8->cole_k.apac1 > p_cons->plaf_quotite_may)
		{
			return (2489);
		}

		/* CA_ANO_0371 */
		if (p_e8->cole_k.apac3 > p_cons->plaf_quotite_may)
		{
			return (2490);
		}

		/* CA_ANO_0372 */
		if (p_e8->cole_k.abhan > p_cons->plaf_quotite_may)
		{
			return (2491);
		}
    }
    else
    {
		/* CA_ANO_0368 */
		if (p_e8->cole_k.abspe > p_cons->plaf_quotite_gen)
		{
			return (2487);
		}

		/* CA_ANO_0369 */
		if (p_e8->cole_k.abbas > p_cons->plaf_quotite_gen)
		{
			return (2488);
		}

		/* CA_ANO_0370 */
		if (p_e8->cole_k.apac1 > p_cons->plaf_quotite_gen)
		{
			return (2489);
		}

		/* CA_ANO_0371 */
		if (p_e8->cole_k.apac3 > p_cons->plaf_quotite_gen)
		{
			return (2490);
		}

		/* CA_ANO_0372 */
		if (p_e8->cole_k.abhan > p_cons->plaf_quotite_gen)
		{
			return (2491);
		}
    }

    /* CA_ANO_0373 */
    if (p_e8->cole_k.apac3 == 0 && p_e8->cole_k.apac1 > 0)
    {
        return (2492);
    }

    /* CA_ANO_0374 */
    if (p_e8->cole_k.apac1 == 0 && p_e8->cole_k.apac3 > 0)
    {
        return (2493);
    }



    /* Anomalies specifiques au regime d'Alsace Moselle  */

    /* CA_ANO_0377 */
    if ((p_e8->abmos7 > 0 && p_e8->codef7 != 'O') ||
        (p_e8->abmos7 == 0 && p_e8->codef7 == 'O'))
    {
        return (2478);
    }

    /* CA_ANO_0376 */
    if (p_e8->codef7 != 'N' && p_e8->codef7 != 'O')
    {
        return (2477);
    }
    /* Sortie correcte: Tous les controles sont satisfaits */
    else
    {
        return (0);
    }
}

/* controles des abattements */
int Controle_Abattements (s_cons *p_cons,s_cole * p_cole, char csdi[])
{
    if (p_cole->vlmoy < 0)
    {
        return (1);
    }

    if (p_cole->abspe < 0)
    {
        return (2);
    }

    if (p_cole->abbas < 0)
    {
        return (3);
    }

    if (p_cole->apac1 < 0)
    {
        return (4);
    }

    if (p_cole->apac3 < 0)
    {
        return (5);
    }

    if (p_cole->vlmoy > 15000)
    {
        return (6);
    }

    if (strcmp(csdi,"976") == 0)
    {
		if (p_cole->abspe > p_cons->plaf_quotite_may)
		{
			return (7);
		}

		if (p_cole->abbas > p_cons->plaf_quotite_may)
		{
			return (8);
		}

		if (p_cole->apac1 > p_cons->plaf_quotite_may)
		{
			return (9);
		}

		if (p_cole->apac3 > p_cons->plaf_quotite_may)
		{
			return (10);
		}
    }
    else
    {
		if (p_cole->abspe > p_cons->plaf_quotite_gen)
		{
			return (7);
		}

		if (p_cole->abbas > p_cons->plaf_quotite_gen)
		{
			return (8);
		}

		if (p_cole->apac1 > p_cons->plaf_quotite_gen)
		{
			return (9);
		}

		if (p_cole->apac3 > p_cons->plaf_quotite_gen)
		{
			return (10);
		}
    }

    if (p_cole->apac3 == 0 && p_cole->apac1 > 0)
    {
        return (11);
    }

    if (p_cole->apac1 == 0 && p_cole->apac3 > 0)
    {
        return (12);
    }



    if (p_cole->abhan < 0)
    {
        return (15);
    }

    if (strcmp(csdi,"976") == 0)
    {
		if (p_cole->abhan > p_cons->plaf_quotite_may)
		{
			return (16);
		}
		else
		{
			return (0);
		}
    }
    else
    {
		if (p_cole->abhan > p_cons->plaf_quotite_gen)
		{
			return (16);
		}
		else
		{
			return (0);
		}
    }
}

/* comparaison des abattements */

int Abatt1_Inferieurs_Abatt2 ( s_cole * p_cole1, s_cole  * p_cole2)
{
    s_abatt_unit_cole col1_abspe, col1_abbas, col1_apac1, col1_apac3;
    s_abatt_unit_cole col2_abspe, col2_abbas, col2_apac1, col2_apac3;

    strncpy(col1_abspe.taux , p_cole1->txspe, 3);
    col1_abspe.quotite = p_cole1->abspe;
    strncpy(col1_abbas.taux , p_cole1->txbas, 3);
    col1_abbas.quotite = p_cole1->abbas;
    strncpy(col1_apac1.taux , p_cole1->tpac1, 3);
    col1_apac1.quotite = p_cole1->apac1;
    strncpy(col1_apac3.taux , p_cole1->tpac3, 3);
    col1_apac3.quotite = p_cole1->apac3;
    strncpy(col2_abspe.taux , p_cole2->txspe, 3);
    col2_abspe.quotite = p_cole2->abspe;
    strncpy(col2_abbas.taux , p_cole2->txbas, 3);
    col2_abbas.quotite = p_cole2->abbas;
    strncpy(col2_apac1.taux , p_cole2->tpac1, 3);
    col2_apac1.quotite = p_cole2->apac1;
    strncpy(col2_apac3.taux , p_cole2->tpac3, 3);
    col2_apac3.quotite = p_cole2->apac3;

    if (Abatt1_Unit_Inf_Abatt2_Unit(&(col1_abspe),&(col2_abspe))
    ||  Abatt1_Unit_Inf_Abatt2_Unit(&(col1_abbas),&(col2_abbas))
    ||  Abatt1_Unit_Inf_Abatt2_Unit(&(col1_apac1),&(col2_apac1))
    ||  Abatt1_Unit_Inf_Abatt2_Unit(&(col1_apac3),&(col2_apac3)))
    {
        return (1);
    }
    else
    {
        return(0);
    }
}

/* vote regime quotit� d'abattements */

int A_Son_Propre_Regime (s_cole * p_cole) /* si les quotites sont servies */
{
    if (p_cole->abspe == 0 && p_cole->abbas == 0
    &&  p_cole->apac1 == 0 && p_cole->apac3 == 0)
    {
        return (0);
    }
    else
    {
        return (1);
    }
}

/* vote regime taux d'abattements */

int A_Son_Propre_Regime_Taux(s_cole * p_cole) /* si les taux sont servis */
{
    if (  ((strcmp(p_cole->txspe,"  ") == 0) || (strcmp(p_cole->txspe,"F ") == 0) || (strcmp(p_cole->txspe,"00") == 0))
        &&((strcmp(p_cole->txbas,"  ") == 0) || (strcmp(p_cole->txbas,"F ") == 0) || (strcmp(p_cole->txbas,"00") == 0))
        &&((strcmp(p_cole->tpac1,"  ") == 0) || (strcmp(p_cole->tpac1,"F ") == 0) || (strcmp(p_cole->tpac1,"00") == 0))
        &&((strcmp(p_cole->tpac3,"  ") == 0) || (strcmp(p_cole->tpac3,"F ") == 0) || (strcmp(p_cole->tpac3,"00") == 0)))
    {
        return (0);
    }
    else
    {
        return (1);
    }
}

/* conversion des taux (chaine)  en integer */

int Conversion_Taux_Abt (char  p_Taux_Abt_Chaine[])
{
    if (strcmp(p_Taux_Abt_Chaine,"F ") == 0)
    {
        return (-1);
    }
    else if ((strcmp(p_Taux_Abt_Chaine,"00") == 0
    ||       (strcmp(p_Taux_Abt_Chaine,"  ") == 0)))
    {
        return (0);
    }
    else if ( atoi(p_Taux_Abt_Chaine) )
    {
        return atoi(p_Taux_Abt_Chaine);
    }
    else
    {
        return (-2);
    }
}

/* comparaison des taux d'abattements */

int Abatt1_Unit_Inf_Abatt2_Unit(s_abatt_unit_cole * p_abatt_unit1,
                                s_abatt_unit_cole * p_abatt_unit2)
{
    if(Conversion_Taux_Abt(p_abatt_unit1->taux) != -1
    && Conversion_Taux_Abt(p_abatt_unit2->taux) != -1)
    {
        if (Conversion_Taux_Abt(p_abatt_unit1->taux) < Conversion_Taux_Abt(p_abatt_unit2->taux))
        {
            return (1);
        }
        else
        {
            return (0);
        }
    }
    else
    {
        return (p_abatt_unit1->quotite < p_abatt_unit2->quotite);
    }
}

/* initialisation des abattements */
void Abt_colb_Init (s_colb * p_Abt_Init)
{
    p_Abt_Init->vlmrf = 0;
	p_Abt_Init->vlmoy = 0;
    p_Abt_Init->abspe = 0;
    p_Abt_Init->abbas = 0;
    p_Abt_Init->apac1 = 0;
    p_Abt_Init->apac3 = 0;
    p_Abt_Init->abhan = 0;
    p_Abt_Init->abmos = 0;
    p_Abt_Init->codef = ' ';
    p_Abt_Init->codeg = ' ';
    strncpy(p_Abt_Init->txbas,"00", 3);
    strncpy(p_Abt_Init->txspe,"00", 3);
    strncpy(p_Abt_Init->tpac1,"00", 3);
    strncpy(p_Abt_Init->tpac3,"00", 3);
    strncpy(p_Abt_Init->txhan,"00", 3);
}

void Abt_colb_Charge (long * p_abmos, char * p_codef, char * p_codeg, s_cole * p_Abt_cole_Source, s_colb * p_Abt_colb)
{
	p_Abt_colb->vlmrf = p_Abt_cole_Source->vlmoy;
	p_Abt_colb->vlmoy = p_Abt_cole_Source->vlmoy;
    p_Abt_colb->abspe = p_Abt_cole_Source->abspe;
    p_Abt_colb->abbas = p_Abt_cole_Source->abbas;
    p_Abt_colb->apac1 = p_Abt_cole_Source->apac1;
    p_Abt_colb->apac3 = p_Abt_cole_Source->apac3;
    p_Abt_colb->abhan = p_Abt_cole_Source->abhan;
    strncpy(p_Abt_colb->txbas , p_Abt_cole_Source->txbas, 3);
    strncpy(p_Abt_colb->txspe , p_Abt_cole_Source->txspe, 3);
    strncpy(p_Abt_colb->tpac1 , p_Abt_cole_Source->tpac1, 3);
    strncpy(p_Abt_colb->tpac3 , p_Abt_cole_Source->tpac3, 3);
    strncpy(p_Abt_colb->txhan , p_Abt_cole_Source->txhan, 3);
    p_Abt_colb->abmos = *p_abmos;
    p_Abt_colb->codef = *p_codef;
    p_Abt_colb->codeg = *p_codeg;
}

/* recopie des abattements d'une collectivite pour N */
/* CA_G0071 */
void Abt_colb_Recopie (s_cole * p_Abt_cole_Cible, s_colb * p_Abt_colb_Source, s_colb * p_Abt_colb_Cible)
{
    p_Abt_colb_Cible->vlmrf = p_Abt_cole_Cible->vlmoy;
    p_Abt_colb_Cible->vlmoy = p_Abt_colb_Source->vlmoy;
    p_Abt_colb_Cible->abspe = p_Abt_colb_Source->abspe;
    p_Abt_colb_Cible->abbas = p_Abt_colb_Source->abbas;
    p_Abt_colb_Cible->apac1 = p_Abt_colb_Source->apac1;
    p_Abt_colb_Cible->apac3 = p_Abt_colb_Source->apac3;
    p_Abt_colb_Cible->abhan = p_Abt_colb_Source->abhan;
    strncpy(p_Abt_colb_Cible->txbas , p_Abt_colb_Source->txbas, 3);
    strncpy(p_Abt_colb_Cible->txspe , p_Abt_colb_Source->txspe, 3);
    strncpy(p_Abt_colb_Cible->tpac1 , p_Abt_colb_Source->tpac1, 3);
    strncpy(p_Abt_colb_Cible->tpac3 , p_Abt_colb_Source->tpac3, 3);
    strncpy(p_Abt_colb_Cible->txhan , p_Abt_colb_Source->txhan, 3);
    p_Abt_colb_Cible->abmos = p_Abt_colb_Source->abmos;
    p_Abt_colb_Cible->codef = p_Abt_colb_Source->codef;
    p_Abt_colb_Cible->codeg = p_Abt_colb_Source->codeg;
}

void init_colb_s8(s_s8 *p_s)
{
    static s_colb init_colb =
    {

      "  ",        /*txbas    */
      "  ",        /*tpac1    */
      "  ",        /*tpac3    */
      "  ",        /*txspe    */
      "  ",        /*txhan    */

      0,       /*vlmoy*/
      0,       /*abspe*/
      0,       /*abhan*/
      0,       /*abbas*/
      0,       /*apac1*/
      0,       /*apac3*/
      0,       /*abmos*/
      ' ',     /*codef*/
      ' ',     /*codeg*/
      0,       /*vlmrf*/

    };
      p_s->colb_c = init_colb;
      p_s->colb_s  = init_colb;
      p_s->colb_q  = init_colb;
      p_s->colb_n  = init_colb;
      p_s->colb_f  = init_colb;
      p_s->colb_g  = init_colb;
      p_s->colb_r  = init_colb;
      p_s->colb_e  = init_colb;
      p_s->colb_h  = init_colb;
      p_s->colb_t  = init_colb;
      p_s->colb_l  = init_colb;
      p_s->colb_w  = init_colb;
      p_s->colb_x  = init_colb;
      p_s->colb_u  = init_colb;
}

