    /*====================================*/
    /*  MODULE DE CALCUL DES COTISATIONS  */
    /*        programme th-3kCOC.C        */
    /*====================================*/


#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif


    /*======*/
    /* MAIN */
    /*======*/

int th_3kcoc(s_coe1 * p_coe1 ,
             s_coe2 * p_coe2 ,
             s_cos  * p_cos)

{
    static s_signature signature;


    /* Bases nettes avant abattement Macron */
    long bnc = 0;
    long bnq = 0;
    long bns = 0;
    long bnn = 0;
    long bng = 0;
    long bne = 0;
    long bnf = 0;
    long bnr = 0;

    int     ret=0;
    double  work_titsn ;
    double work_titgp ;


    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_cos->signature = &signature;
    p_cos->versr = 'A' ;

    /* controle de la signature */



    ret = controle_signature(RKCOC,p_cos->versr, &(p_cos->libelle));
    if (ret != 0 )
    {
        return (ret);
    }


    /* THLV */
    /* Mise � jour des bases nettes */
    if (p_coe1->tyimp == 'T')
    {
        /* CA_THLV_0001 DETERMINATION BASE NETTE IMPOSEE */
        /*          base nette commune                           */

		if( p_coe1->indthlv == 'C' )
        {

			base_nettethlv(p_coe1,p_coe1->p_cols_c);
			if(p_coe2->tisyn != 0)
			{
			    /* base nette  syndicat */
				base_nettethlv(p_coe1,p_coe1->p_cols_s);
            }
		}

		/* base nette  intercommunalit� */
		if ( p_coe1->indthlv == 'I' )
        {
			base_nettethlv(p_coe1,p_coe1->p_cols_q);
		}

        /* CA_THLV_0003 DETERMINATION BASE NETTE IMPOSEE GEMAPI */
        /* Bases nettes GEMAPI, TSE, TSE autre peuvent se d�terminer comme celles de Commune/syndicat ou Interco en THLV. En effet en THLV vl = vlt , la thlv n est pas concern�e par l exo HLM/SEM */
        if ( p_coe2->timpe > 0  && p_coe1->indgem != ' ')
        {
            base_nettethlv(p_coe1,p_coe1->p_cols_e);
        }

        /* CA_THLV_0003-1 DETERMINATION BASE NETTE IMPOSEE TSE */
        /* 2023MAJ08-01-01*/
        if ( p_coe2->titsn > 0 )
        {
            base_nettethlv(p_coe1,p_coe1->p_cols_n);
        }

        /* CA_THLV_0003-2 DETERMINATION BASE NETTE IMPOSEE TSE AUTRE */
        /* 2023MAJ08-01-01 */
        if ( p_coe2->titgp > 0 )
        {
            base_nettethlv(p_coe1,p_coe1->p_cols_g);
        }
        /* FIN 2023MAJ08-01-01 */

        /* CA_THLV_0002 DETERMINATION DES COTISATIONS */
        p_coe1->p_cols_c->coti = cotisation(p_coe1->p_cols_c->bnimp , p_coe2->timpc);
        p_coe1->p_cols_q->coti = cotisation(p_coe1->p_cols_q->bnimp , p_coe2->timpq);
        p_coe1->p_cols_s->coti = cotisation(p_coe1->p_cols_s->bnimp , p_coe2->tisyn);

        /* CA_THLV_0011 DETERMINATION COTISATION GEMAPI */
        p_coe1->p_cols_e->coti = cotisation(p_coe1->p_cols_e->bnimp , p_coe2->timpe);

        /* 2023MAJ08-02-01*/
        p_coe1->p_cols_n->coti = cotisation(p_coe1->p_cols_n->bnimp , p_coe2->titsn);
        p_coe1->p_cols_g->coti = cotisation(p_coe1->p_cols_g->bnimp , p_coe2->titgp);
        /* FIN 2023MAJ08-02-01 */

    }
    else
    {


   /* Bug 299395 - ca_th_0240 absence de la TSE et TSE autre en THP ou THE on force les taux correspondants � 0 pour les calculs */

       if(p_coe1->ind_absence_TSE == 1)
       {
           work_titgp = 0;
           work_titsn = 0;
       }
       else
       {
            work_titgp = p_coe2->titgp;
            work_titsn = p_coe2->titsn;
       }

        /* 1 */
        /* base nette des locaux principaux au niveau commune */

        /* DETERMINATION DE LA VL NETTE  CA_TH_0088 ou CA_TH_0136 ou CA_TH_0141*/
        if (p_coe1->tax == 'P')    /* Cas des articles THP  */
        {
            bnc = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_c));

            bnq = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_q));
            bns = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_s));
            bnf = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_f));
            bnr = vlNette (p_coe1->vl,totAb(p_coe1->p_cols_r));

            bnn = vlNette (p_coe1->vlt,totAb(p_coe1->p_cols_n));
            bng = vlNette (p_coe1->vlt,totAb(p_coe1->p_cols_g));
            bne = vlNette (p_coe1->vlt,totAb(p_coe1->p_cols_e));

            if (bnc < 0){bnc =0;}
            if (bnq < 0){bnq =0;}
            if (bns < 0){bns =0;}
            if (bnf < 0){bnf =0;}
            if (bnr < 0){bnr =0;}

            if (bnn < 0){bnn =0;}
            if (bng < 0){bng =0;}
            if (bne < 0){bne =0;}
        }

        if ((p_coe1->tax == 'S') || (p_coe1->tax == 'E'))    /* Cas des articles THS ou THE  */
        {
            bnc = p_coe1->vl;
            bnq = p_coe1->vl;
            bns = p_coe1->vl;
            bnf = p_coe1->vl;
            bnr = p_coe1->vl;
            bnn = p_coe1->vlt;
            bng = p_coe1->vlt;
            bne = p_coe1->vlt;
        }

        /* DETERMINATION DE LA BASE NETTE EXONEREE  ET DE LA BASE NETTE IMPOSEE */
        /*  CA_TH_0089 ET CA_TH_0090 ou CA_TH_0137 et CA_TH_0138
        ou CA_TH_0142 et CA_TH_0143*/

        if (p_coe1->degex == 'D' || p_coe1->degex == 'K')
        {
            /* 2019MAJR08-01-01  */
            majVlnnexDansColsCQFR(p_coe1->p_cols_c,0,bnc);
            majVlnnexDansColsCQFR(p_coe1->p_cols_f,0,bnf);
            if (p_coe1->cocnq == 'C')
            {
                majVlnnexDansColsCQFR(p_coe1->p_cols_q,0,bnq);
                majVlnnexDansColsCQFR(p_coe1->p_cols_r,0,bnr);

            }


            majVlnnexDansCols(p_coe1->p_cols_s,0,bns, p_coe2->tisyn);
            majVlnnexDansCols(p_coe1->p_cols_n,0,bnn, work_titsn);
            majVlnnexDansCols(p_coe1->p_cols_g,0,bng, work_titgp);
            majVlnnexDansCols(p_coe1->p_cols_e,0,bne, p_coe2->timpe);
        }
        else
        {
            /* CA_G0085 exonerations HLM SEM */

            if ((strchr("56",p_coe1->permo) != NULL) && (strchr("HMF",p_coe1->aff) != NULL)
                 && (strchr("2 ",p_coe1->grrev) != NULL))
            {
                majVlnnexDansColsCQFR(p_coe1->p_cols_c,bnc,0);
                majVlnnexDansColsCQFR(p_coe1->p_cols_f,bnf,0);
                if (p_coe1->cocnq == 'C')
                {
                    majVlnnexDansColsCQFR(p_coe1->p_cols_q,bnq,0);
                    majVlnnexDansColsCQFR(p_coe1->p_cols_r,bnr,0);
                }
                majVlnnexDansCols(p_coe1->p_cols_s,bns,0, p_coe2->tisyn);
                majVlnnexDansCols(p_coe1->p_cols_n,0,bnn, work_titsn);
                majVlnnexDansCols(p_coe1->p_cols_g,0,bng, work_titgp);
                majVlnnexDansCols(p_coe1->p_cols_e,0,bne, p_coe2->timpe);
             }
             else
             {

                majVlnnexDansColsCQFR(p_coe1->p_cols_c,bnc,0);
                majVlnnexDansColsCQFR(p_coe1->p_cols_f,bnf,0);

                if (p_coe1->cocnq == 'C')
                {
                        majVlnnexDansColsCQFR(p_coe1->p_cols_q,bnq,0);
                        majVlnnexDansColsCQFR(p_coe1->p_cols_r,bnr,0);
                }
                majVlnnexDansCols(p_coe1->p_cols_s,bns,0, p_coe2->tisyn);
                majVlnnexDansCols(p_coe1->p_cols_n,bnn,0, work_titsn);
                majVlnnexDansCols(p_coe1->p_cols_g,bng,0, work_titgp);
                majVlnnexDansCols(p_coe1->p_cols_e,bne,0, p_coe2->timpe);

             }
        }
        /* DETERMINATION DE LA BASE NETTE ABATTUE DEGRESSIVE CA_TH_0090-1, CA_TH_0095-1, CA_TH_0100-1,
           CA_TH_0147-1, CA_TH_0138-1  */

        p_coe1->p_cols_c->vlnabd = 0;
        p_coe1->p_cols_q->vlnabd = 0;
        p_coe1->p_cols_s->vlnabd = 0;
        p_coe1->p_cols_f->vlnabd = 0;
        p_coe1->p_cols_r->vlnabd = 0;
        p_coe1->p_cols_n->vlnabd = 0;
        p_coe1->p_cols_g->vlnabd = 0;
        p_coe1->p_cols_e->vlnabd = 0;

        if (( p_coe1->tax == 'P' || p_coe1->tax == 'E') && strchr("34",p_coe1->cnat) != NULL  && p_coe1->degex == ' ')
        {
            if (p_coe1->workimaisf == 'M')
            {
                p_coe1->p_cols_c->vlnabd =  p_coe1->p_cols_c->vlnnex;
                p_coe1->p_cols_q->vlnabd =  p_coe1->p_cols_q->vlnnex;
                p_coe1->p_cols_f->vlnabd =  p_coe1->p_cols_f->vlnnex;
                p_coe1->p_cols_r->vlnabd =  p_coe1->p_cols_r->vlnnex;
                p_coe1->p_cols_s->vlnabd =  Resultat_A_Afficher(p_coe1->p_cols_s->vlnnex,p_coe2->tisyn);
                p_coe1->p_cols_n->vlnabd =  Resultat_A_Afficher(p_coe1->p_cols_n->vlnnex,work_titsn);
                p_coe1->p_cols_g->vlnabd =  Resultat_A_Afficher(p_coe1->p_cols_g->vlnnex,work_titgp);
                p_coe1->p_cols_e->vlnabd =  Resultat_A_Afficher(p_coe1->p_cols_e->vlnnex,p_coe2->timpe);
            }

            if (p_coe1->workimaisf == 'D')
            {
                p_coe1->p_cols_c->vlnabd = KalVlnabdSiImaifD(p_coe1->p_cols_c->vlnnex, p_coe1->p_seuilM, p_coe1->revffm);
                p_coe1->p_cols_q->vlnabd = KalVlnabdSiImaifD(p_coe1->p_cols_q->vlnnex, p_coe1->p_seuilM, p_coe1->revffm);
                p_coe1->p_cols_f->vlnabd = KalVlnabdSiImaifD(p_coe1->p_cols_f->vlnnex, p_coe1->p_seuilM, p_coe1->revffm);
                p_coe1->p_cols_r->vlnabd = KalVlnabdSiImaifD(p_coe1->p_cols_r->vlnnex, p_coe1->p_seuilM, p_coe1->revffm);
                p_coe1->p_cols_s->vlnabd = Resultat_A_Afficher(KalVlnabdSiImaifD(p_coe1->p_cols_s->vlnnex, p_coe1->p_seuilM, p_coe1->revffm),p_coe2->tisyn);
                p_coe1->p_cols_n->vlnabd = Resultat_A_Afficher(KalVlnabdSiImaifD(p_coe1->p_cols_n->vlnnex, p_coe1->p_seuilM, p_coe1->revffm),work_titsn);
                p_coe1->p_cols_g->vlnabd = Resultat_A_Afficher(KalVlnabdSiImaifD(p_coe1->p_cols_g->vlnnex, p_coe1->p_seuilM, p_coe1->revffm),work_titgp);
                p_coe1->p_cols_e->vlnabd = Resultat_A_Afficher(KalVlnabdSiImaifD(p_coe1->p_cols_e->vlnnex, p_coe1->p_seuilM, p_coe1->revffm),p_coe2->timpe);

            }
        }

        /* DETERMINATION DE LA BASE NETTE APRES ABATTEMENT DEGRESSIF  CA_TH_0090-3, CA_TH_0095-2,
           CA_TH_0100-2, CA_TH_0147-2, CA_TH_0138-2  */

      p_coe1->p_cols_c->vlnadm =  p_coe1->p_cols_c->vlnnex - p_coe1->p_cols_c->vlnabd;
      p_coe1->p_cols_q->vlnadm =  p_coe1->p_cols_q->vlnnex - p_coe1->p_cols_q->vlnabd;
      p_coe1->p_cols_f->vlnadm =  p_coe1->p_cols_f->vlnnex - p_coe1->p_cols_f->vlnabd;
      p_coe1->p_cols_r->vlnadm =  p_coe1->p_cols_r->vlnnex - p_coe1->p_cols_r->vlnabd;
      p_coe1->p_cols_s->vlnadm =  Resultat_A_Afficher(p_coe1->p_cols_s->vlnnex - p_coe1->p_cols_s->vlnabd ,p_coe2->tisyn);
      p_coe1->p_cols_n->vlnadm =  Resultat_A_Afficher(p_coe1->p_cols_n->vlnnex - p_coe1->p_cols_n->vlnabd ,work_titsn);
      p_coe1->p_cols_g->vlnadm =  Resultat_A_Afficher(p_coe1->p_cols_g->vlnnex - p_coe1->p_cols_g->vlnabd ,work_titgp);
      p_coe1->p_cols_e->vlnadm =  Resultat_A_Afficher(p_coe1->p_cols_e->vlnnex - p_coe1->p_cols_e->vlnabd ,p_coe2->timpe);

        /* DETERMINATION DE LA BASE NETTE ABATTUE FIXE  CA_TH_0090-4, CA_TH_0095-3,
           CA_TH_0100-3, CA_TH_0147-3, CA_TH_0138-3  */

     p_coe1->p_cols_c->vlnabf = 0;
     p_coe1->p_cols_q->vlnabf = 0;
     p_coe1->p_cols_f->vlnabf = 0;
     p_coe1->p_cols_r->vlnabf = 0;
     p_coe1->p_cols_s->vlnabf = 0;
     p_coe1->p_cols_n->vlnabf = 0;
     p_coe1->p_cols_g->vlnabf = 0;
     p_coe1->p_cols_e->vlnabf = 0;

        if (( p_coe1->tax == 'P' || p_coe1->tax == 'E') && strchr("34",p_coe1->cnat) != NULL  && p_coe1->degex == ' ' && (p_coe1->workimaisf == 'M'||
            p_coe1->workimaisf == 'D' || p_coe1->workimaisf == '3'))
        {
                          p_coe1->p_cols_c->vlnabf = KalVlnabfSiImaifMD3(p_coe1->p_cols_c->vlnadm);
                          p_coe1->p_cols_q->vlnabf = KalVlnabfSiImaifMD3(p_coe1->p_cols_q->vlnadm);
                          p_coe1->p_cols_f->vlnabf = KalVlnabfSiImaifMD3(p_coe1->p_cols_f->vlnadm);
                          p_coe1->p_cols_r->vlnabf = KalVlnabfSiImaifMD3(p_coe1->p_cols_r->vlnadm);
                          p_coe1->p_cols_s->vlnabf = Resultat_A_Afficher(KalVlnabfSiImaifMD3(p_coe1->p_cols_s->vlnadm),p_coe2->tisyn);
                          p_coe1->p_cols_n->vlnabf = Resultat_A_Afficher(KalVlnabfSiImaifMD3(p_coe1->p_cols_n->vlnadm),work_titsn);
                          p_coe1->p_cols_g->vlnabf = Resultat_A_Afficher(KalVlnabfSiImaifMD3(p_coe1->p_cols_g->vlnadm),work_titgp);
                          p_coe1->p_cols_e->vlnabf = Resultat_A_Afficher(KalVlnabfSiImaifMD3(p_coe1->p_cols_e->vlnadm),p_coe2->timpe);
        }

         /* DETERMINATION DE LA BASE NETTE IMPOSEE  CA_TH_0090-5, CA_TH_0095-4, CA_TH_0100-4,
            CA_TH_0147-4, CA_TH_0138-5  */

      p_coe1->p_cols_c->bnimp =  p_coe1->p_cols_c->vlnadm - p_coe1->p_cols_c->vlnabf;
      p_coe1->p_cols_q->bnimp =  p_coe1->p_cols_q->vlnadm - p_coe1->p_cols_q->vlnabf;
      p_coe1->p_cols_f->bnimp =  p_coe1->p_cols_f->vlnadm - p_coe1->p_cols_f->vlnabf;
      p_coe1->p_cols_r->bnimp =  p_coe1->p_cols_r->vlnadm - p_coe1->p_cols_r->vlnabf;
      p_coe1->p_cols_s->bnimp =  Resultat_A_Afficher(p_coe1->p_cols_s->vlnadm - p_coe1->p_cols_s->vlnabf ,p_coe2->tisyn);
      p_coe1->p_cols_n->bnimp =  Resultat_A_Afficher(p_coe1->p_cols_n->vlnadm - p_coe1->p_cols_n->vlnabf ,work_titsn);
      p_coe1->p_cols_g->bnimp =  Resultat_A_Afficher(p_coe1->p_cols_g->vlnadm - p_coe1->p_cols_g->vlnabf ,work_titgp);
      p_coe1->p_cols_e->bnimp =  Resultat_A_Afficher(p_coe1->p_cols_e->vlnadm - p_coe1->p_cols_e->vlnabf ,p_coe2->timpe);

        /* DETERMINATION DE LA COTISATION CA_TH_0091 ou CA_TH_0139 ou CA_TH_0144 */

        p_coe1->p_cols_c->coti = cotisation(p_coe1->p_cols_c->bnimp , p_coe2->timpc);
        p_coe1->p_cols_q->coti = cotisation(p_coe1->p_cols_q->bnimp , p_coe2->timpq);
        p_coe1->p_cols_s->coti = cotisation(p_coe1->p_cols_s->bnimp , p_coe2->tisyn);
        p_coe1->p_cols_f->coti = cotisation(p_coe1->p_cols_f->bnimp , p_coe2->timpc);
        p_coe1->p_cols_r->coti = cotisation(p_coe1->p_cols_r->bnimp , p_coe2->timpc);

        p_coe1->p_cols_n->coti = cotisation(p_coe1->p_cols_n->bnimp , work_titsn);
        p_coe1->p_cols_g->coti = cotisation(p_coe1->p_cols_g->bnimp , work_titgp);
        p_coe1->p_cols_e->coti = cotisation(p_coe1->p_cols_e->bnimp , p_coe2->timpe);
    }

    /* DETERMINATION DE LA COTISATION FICTIVE (CONCERNE LIQUIDATION N AVEC OU SANS LP) A COMPTER DE 2021 CA_TH_0091-1 CA_TH_0139-1 */

    if ((p_coe1->tax == 'P' || p_coe1->tax == 'E') &&  strchr("34",p_coe1->cnat) != NULL  && p_coe1->degex == ' '
         && strchr("MD3",p_coe1->imaisf) != NULL && p_coe1->ind_origine_kcoc == 1)
    {
        /*Bug 299399*/
        p_coe1->p_cols_c->cotif = cotisation(p_coe1->p_cols_c->vlnnex , p_coe2->timpc);
        p_coe1->p_cols_q->cotif = cotisation(p_coe1->p_cols_q->vlnnex , p_coe2->timpq);
        p_coe1->p_cols_s->cotif = cotisation(p_coe1->p_cols_s->vlnnex , p_coe2->tisyn);
        p_coe1->p_cols_e->cotif = cotisation(p_coe1->p_cols_e->vlnnex , p_coe2->timpe);
    }
    else
    {
        p_coe1->p_cols_c->cotif = 0;
        p_coe1->p_cols_q->cotif = 0;
        p_coe1->p_cols_s->cotif = 0;
        p_coe1->p_cols_e->cotif = 0;
    }
    /* Appel de la fonction controle du th_kcoc             */
    /* pour verification des zones calculees dans le module */

    ret = controle_kcoc(p_coe1);
    if ( ret !=0 )
    {
        cherche_Erreur(ret,&(p_cos->libelle)) ;
        return ret;
    }
    return (0);
}

int controle_kcoc(s_coe1 * p_coe1)
{
    int ret = 0;

    /* controles bases nettes non exonerees */
    if (p_coe1->p_cols_c->vlnnex < 0)
    {
        ret = (911);
    }
    if (p_coe1->p_cols_q->vlnnex < 0)
    {
        ret = (913);
    }
    if (p_coe1->p_cols_s->vlnnex < 0)
    {
        ret = (915);
    }
    if (p_coe1->p_cols_n->vlnnex < 0)
    {
        ret = (916);
    }
    if (p_coe1->p_cols_g->vlnnex < 0)
    {
        ret = (917);
    }
    if (p_coe1->p_cols_e->vlnnex < 0)
    {
        ret = (918);
    }

    /* controles bases nettes apres abattement degressif */

    if (p_coe1->p_cols_c->vlnadm < 0)
    {
        ret = (921);
    }
    if (p_coe1->p_cols_q->vlnadm < 0)
    {
        ret = (923);
    }
    if (p_coe1->p_cols_s->vlnadm < 0)
    {
        ret = (925);
    }
    if (p_coe1->p_cols_n->vlnadm < 0)
    {
        ret = (926);
    }
    if (p_coe1->p_cols_g->vlnadm < 0)
    {
        ret = (927);
    }
    if (p_coe1->p_cols_e->vlnadm < 0)
    {
        ret = (928);
    }

    /* Controles de coherence sur suppression TSE en taxation principale ou etendue */

    if ((strchr("PE",p_coe1->tax) != NULL) && p_coe1->p_cols_n->bnimp > 0 && p_coe1->ind_absence_TSE == 1)
    {
        ret = (929);
    }
    if ((strchr("PE",p_coe1->tax) != NULL) && p_coe1->p_cols_g->bnimp > 0 && p_coe1->ind_absence_TSE == 1)
    {
        ret = (930);
    }
    if ((strchr("PE",p_coe1->tax) != NULL) && p_coe1->p_cols_n->coti > 0 && p_coe1->ind_absence_TSE == 1)
    {
        ret = (931);
    }
    if ((strchr("PE",p_coe1->tax) != NULL) && p_coe1->p_cols_g->coti > 0 && p_coe1->ind_absence_TSE == 1)
    {
        ret = (932);
    }

    return ret;
}

long totAb(s_cols *p_cols)
{
    return (p_cols->abgen + p_cols->abpac + p_cols->abspe + p_cols->abhan);
}

long vlNette(long vl, long totAbat)
{
    return (vl - totAbat);
}

void majVlnnexDansColsCQFR(s_cols *p_cols, long base, long vlnex)
{
    p_cols->vlnnex = base;
    p_cols->vlnex = vlnex;
    p_cols->vlntt = base + vlnex; /* CA_TH_0089 l'une des deux est � 0 */
}

long KalVlnabdSiImaifD( long vlnnex,s_seuilM *p_M, double revffm)
{
    return arrondi_euro_voisin( vlnnex  * ((double)(p_M->seuilm2 - revffm )/(  double)( p_M->seuilm2 - p_M->seuilm1)));
}

long KalVlnabfSiImaifMD3(long vlnadm)
{
       return arrondi_euro_voisin( vlnadm  * ((double)(65)/(  double)(100)));

}


 long Resultat_A_Afficher(long resultat, double taux)
 {
     if (taux > 0)
     {
         return resultat;
     }
     else
     {
         return 0;
     }
 }

void majVlnnexDansCols(s_cols *p_cols, long base, long vlnex, double taux)
{
    p_cols->vlnnex = 0;
    p_cols->vlnex = 0;
    p_cols->vlntt = 0; /* CA_G0077 */

    /* CA_G0077 Donnees renseignees ssi il existe un taux positif */
    if (taux > 0)
    {
        p_cols->vlnnex = base;
        p_cols->vlnex = vlnex;
        p_cols->vlntt = base + vlnex; /* CA_TH_0089 l'une des deux est � 0 */
    }
}

void base_nettethlv( s_coe1 *p2, s_cols *p3)
{
    p3->vlntt=p2->vl;
    p3->bnimp=p2->vl;
}

long cotisation (long base, double taux)
{
    return (arrondi_euro_inf(base * taux + 0.5));
}
