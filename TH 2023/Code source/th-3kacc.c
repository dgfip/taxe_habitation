/*============================================================================*/
/* Determination des abattements a appliquer a la cote                        */
/*============================================================================*/


#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif

                         /*=========*/
                         /*   main  */
                         /*=========*/

int th_3kacc(s_ace * p_ace, s_acs * p_acs)
{
    static s_signature signature;
    int ret;    /*zone retour programme th_nkacc */
    int rets;   /*zone retour signature */

    short nb12;
    short nbsup;
    short nbpac;
    short nbera;
    short nbera12;
    short nberasup;
    long mtvl;

     /* INITIALISATIONS */

    ret=0;
    rets=0;
    nb12=0;
    nbsup=0;
    nbera12=0;
    nberasup=0;
    nbpac=p_ace->nbpac;
    nbera=p_ace->nbera;





     /*                    initialisation des zones sorties                    */
    init_cols(&(p_acs->cols_c));
    init_cols(&(p_acs->cols_s));
    init_cols(&(p_acs->cols_q));
    init_cols(&(p_acs->cols_n));
    init_cols(&(p_acs->cols_g));
    init_cols(&(p_acs->cols_e));
    init_cols(&(p_acs->cols_f));
    init_cols(&(p_acs->cols_r));


    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_acs->signature = &signature;
    p_acs->versr='A';
    rets=controle_signature(RKACC,p_acs->versr,&(p_acs->libelle));

    if (rets!=0)
    {
        cherche_Erreur(rets,&(p_acs->libelle));
        return(rets);
    }

    /*                             traitement  TH                             */
    /*                determination du nombre de pac par rang                 */
    if (nbera<2)
    {
        nbera12=nbera;
        nberasup=0;

        if (nbpac<(2-nbera12))
        {
            nb12=nbpac;
            nbsup=0;
        }
        else
        {
            nb12=2-nbera12;
            nbsup=nbpac - nb12;
        }
    }
    else
    {
        nbera12=2;
        nberasup=nbera-(short)2;
        nb12=0;
        nbsup=nbpac;
    }


    /*                appel de la fonction base nette commune                 */
    base_nette(&(p_ace->colb_c),
               p_ace,
               &(p_acs->cols_c),
               nb12, nbsup, nbera12, nberasup, &mtvl);

    /*                appel de la fonction base nette syndicat                */
    if (p_ace->tisyn!=0)
    {
        base_nette(&(p_ace->colb_s),
               p_ace,
                   &(p_acs->cols_s),
                   nb12, nbsup, nbera12, nberasup, &mtvl);
    }

    if (p_ace->cocnq == 'C')

    {
        base_nette(&(p_ace->colb_q), p_ace,
                   &(p_acs->cols_q),
                   nb12, nbsup, nbera12, nberasup, &mtvl);
    }



    /*              appel de la fonction base nette tse                       */
    if (p_ace->titsn!=0)
    {
        base_nette(&(p_ace->colb_n), p_ace,
                   &(p_acs->cols_n),
                   nb12, nbsup, nbera12, nberasup, &mtvl);
    }

        /*  appel de la fonction base nette TSE Autre  */
    if (p_ace->titgp != 0)
    {
        base_nette(&(p_ace->colb_g), p_ace,
                   &(p_acs->cols_g),
                   nb12, nbsup, nbera12, nberasup, &mtvl);
    }

    /*  appel de la fonction base nette GEMAPI  */
    if (p_ace->timpe > 0)
    {
        base_nette(&(p_ace->colb_e), p_ace,
                       &(p_acs->cols_e),
                   nb12, nbsup, nbera12, nberasup, &mtvl);
    }

    /* appel de la fonction base nette commune avant ajustement  */
    base_nette(&(p_ace->colb_f),
               p_ace,
               &(p_acs->cols_f),
               nb12, nbsup, nbera12, nberasup, &mtvl);

    if (p_ace->cocnq == 'C')
    {
        base_nette(&(p_ace->colb_r),
               p_ace,
                   &(p_acs->cols_r),
                   nb12, nbsup, nbera12, nberasup, &mtvl);
    }

    return(ret);
}



    /*                          fonction base nette                           */
/* CA_TH_0053 CA_TH_0054 CA_TH_0055 CA_TH_0058 sont faites ici selon appels a KACC */
void base_nette(s_colb *p1,s_ace *p2, s_cols *p3, short nb12,
                short nbsup, short nbera12, short nberasup, long *p_mtvl)
{
    long vlcol;
    long vlbp10;
    long vlmc10;
    long vlmc15;
    long vlmc20;
    long vlmc25;
    short numg;

    *p_mtvl=0;

    vlcol=p1->vlmrf;
    vlbp10=0;
    vlmc10=0;
    vlmc15=0;
    vlmc20=0;
    vlmc25=0;
    numg=0;

    /*                   appel de la fonction abat_gen                        */
    abat_gen(p2->vlbr, p1->abbas, &(p3->abgen), p_mtvl);

    /*                   appel de la fonction cal_preal                       */
    switch (p1->codeg)
    {
        case '1':numg=1;
        break;
        case '2':numg=2;
        break;
        case '3':numg=3;
        break;
        case '4':numg=4;
        break;
        case '5':numg=5;
    }

    if ((p1->codef=='F')|| (p1->codef=='O')||((numg>0)&&(numg<6)))

    {
        vlbp10=arrondi_euro_voisin((double)(p2->vlbr) / 10);

        if (vlbp10<p1->abmos)
        {
            vlbp10=p1->abmos;
        }
        cal_preal( p1->vlmoy,&vlmc10, &vlmc15, &vlmc20, &vlmc25);
    }

    /*                 appel de la fonction detf_abat_pac                     */

    if ((p1->codef=='F') || (p1->codef=='O'))

    {
        detf_abat_pac(vlbp10, vlmc10, vlmc15, &(p1->apac1), &(p1->apac3));
    }

    /*                   appel de la fonction detg_abat_pac                   */
    if ((numg>0)&&(numg<6))
    {
        detg_abat_pac(vlbp10, vlmc10, vlmc15, vlmc20, vlmc25,
                          numg, &(p1->apac1), &(p1->apac3));
    }

    /*                   appel de la fonction abat_pac                        */
    abat_pac(nb12,
            nbera12,
            nbsup,
            nberasup,
            &(p1->apac1),
            &(p1->apac3),
            p_mtvl,
            &(p3->abpac),
            &(p3->abp12));

    /*            appel de la fonction abat_spe pour garnir ABSPS             */
    /*              et  de la fonction abat_spespsa pour garnir ABSPSA        */
    if (p2->codni=='A')
    {
        abat_spe(p1->abspe,
                 p1->vlmoy,
                 p2->nbpac,
                 p2->nbera,
                 p2->vlbr,
                 p_mtvl,
                 &(p3->abspe),
                 &(p3->absps));

        /* CA_G0071 vlbpc des locaux principaux ssi tax = P */
        if (p2->tax=='P')
        {
           abat_spespsa(p1->vlmoy,
                        p2->nbpac,
                        p2->nbera,
                        p2->vlbr,
                        vlcol,
                        &(p3->abspsa));
        }
    }
    else
    {
        p3->absps='N';
        p3->abspsa='N';
    }

    /*                     appel de la fonction abat_han                      */
    abat_han(&(p1->abhan), p2->codsh, p_mtvl, &(p3->abhan));

}

    /*                           fonction abat_gen                            */
void abat_gen( long vlbr, long abbas, long * p_abgen, long * p_mtvl)
{
    if ((abbas!=0)&&(abbas<vlbr))
    {
        *p_mtvl=vlbr - abbas;
        *p_abgen=abbas;
    }
    if ((abbas!=0)&&(abbas>=vlbr))
    {
        *p_mtvl=0;
        *p_abgen=vlbr;
    }
    if (abbas==0)
    {
        *p_mtvl=vlbr;
        *p_abgen=abbas;
    }
}

    /*                           fonction cal_preal                           */
void cal_preal(long vlmoy, long *p_vlmc10, long *p_vlmc15,
               long *p_vlmc20, long *p_vlmc25)
{
    *p_vlmc10=arrondi_euro_voisin((double)(vlmoy) / 10);
    *p_vlmc15=vlmoy * 15;
    *p_vlmc15=arrondi_euro_voisin((double)(*p_vlmc15) / 100);
    *p_vlmc20=arrondi_euro_voisin((double)(vlmoy) / 5);
    *p_vlmc25=arrondi_euro_voisin((double)(vlmoy) / 4);
}

    /*                         fonction detf_abat_pac                         */
void detf_abat_pac(long vlbp10, long vlmc10, long vlmc15,
                   long *p_apac1, long *p_apac3)
{
    if (vlbp10<vlmc10)
    {
        *p_apac1=vlmc10;
    }
    else
    {
        *p_apac1=vlbp10;
    }
    if (vlbp10<vlmc15)
    {
        *p_apac3=vlmc15;
    }
    else
    {
        *p_apac3=vlbp10;
    }
}

    /*                         fonction detg_abat_pac                         */
void detg_abat_pac(long vlbp10, long vlmc10, long vlmc15,
                   long vlmc20, long vlmc25,
                   short numg,
                   long *p_apac1, long *p_apac3)
{
    if (vlmc10>=vlbp10)
    {
        *p_apac1=vlmc10;
        *p_apac3=vlmc15;
    }
    if ((vlmc10<vlbp10)&&(vlbp10<=vlmc15))
    {
        *p_apac1=vlbp10;
        *p_apac3=vlmc15;
    }
    if (vlbp10>vlmc25)
    {
        *p_apac1=(vlbp10 - ((vlbp10 - vlmc20) * numg) / 5);
        *p_apac3=(vlbp10 - ((vlbp10 - vlmc25) * numg) / 5);

    }
    if ((vlbp10>vlmc20)&&(vlbp10<=vlmc25))
    {
        *p_apac1=(vlbp10 - ((vlbp10 - vlmc20) * numg) / 5);
        *p_apac3=vlbp10;
    }
    if ((vlbp10>vlmc15)&&(vlbp10<=vlmc20))
    {
        *p_apac1=vlbp10;
        *p_apac3=vlbp10;
    }
}

    /*                           fonction abat_pac                            */
void abat_pac(short nb12, short nbera12, short nbsup, short nberasup,
              long *p_apac1, long *p_apac3,
              long *p_mtvl, long *p_abpac, long *p_abp12)
{
    long valaba;
    long aera1;
    long aera3;

    valaba=0;
    aera1=0;
    aera3=0;

    aera1=arrondi_euro_voisin(((double)*p_apac1)/ 2);
    aera3=arrondi_euro_voisin(((double)*p_apac3)/ 2);

    valaba=(nb12 * *p_apac1) + (nbsup * *p_apac3) + (nbera12 * aera1) + (nberasup * aera3);
    if (*p_mtvl>valaba)
    {
        *p_mtvl=*p_mtvl - valaba;
        *p_abpac=valaba;
        *p_abp12=(nb12 * *p_apac1) + (nbera12 * aera1);
    }
    else
    {
        *p_abpac=*p_mtvl;
        if (*p_mtvl-((nb12 * *p_apac1) + (nbera12 * aera1))<0)
        {
            *p_abp12=*p_mtvl;
        }
        else
        {
            *p_abp12=(nb12 * *p_apac1) + (nbera12 * aera1);
        }
        *p_mtvl=0;
    }
}

    /*  CA_G0071                     fonction abat_spe                            */
void abat_spe(long abspe, long vlmoy, short nbpac, short nbera, long vlbpc,
              long *p_mtvl, long *p_abspe, char *p_absps)
{
    long sasp;
    double nb13;
    double vl130;

    sasp=0;
    nb13=0;
    vl130=0;

    nb13=nbpac + ((double)nbera / 2) + (short)13;
    vl130=vlmoy * nb13;
    sasp=arrondi_euro_voisin(((double)vl130) / 10);

    if (abspe>0)
    {
        if (vlbpc<sasp)
        {
            *p_absps = 'O';
            if ((*p_mtvl - abspe)>0)
            {
                *p_mtvl=*p_mtvl - abspe;
                *p_abspe=abspe;
            }
            else
            {
                *p_abspe=*p_mtvl;
                *p_mtvl=0;
            }
        }
        else
        {
           *p_absps = 'N';
        }
    }
}

    /*                           fonction abat_han                            */
void abat_han(long *pe_abhan, char codsh, long *p_mtvl, long *ps_abhan)
{
    long mtvl = *p_mtvl;

    if (codsh=='1' || codsh=='2' || codsh=='3' || codsh=='4' || codsh=='5')
    {
        if ((*pe_abhan!= 0) && (*pe_abhan < *p_mtvl))
        {
            mtvl = *p_mtvl - *pe_abhan;
            *ps_abhan = *pe_abhan;
        }

        if ((*pe_abhan!= 0) && (*pe_abhan >= *p_mtvl))
        {
            *ps_abhan = *p_mtvl;
            mtvl = 0;
        }

        if (*pe_abhan== 0)
        {
            *ps_abhan = *pe_abhan;
        }

        *p_mtvl = mtvl;
    }
}


    /* CA_G0071            fonction abat_spespsa  pour garnir ABSPSA                      */

void abat_spespsa(long vlmoy, short nbpac, short nbera, long vlbpc,
              long vlcol, char *p_abspsa)
{
    long sasp;
    long sass;
    double nb13;
    double vl130;
    double vc130;

    sasp=0;
    sass=0;
    nb13=0;
    vl130=0;
    vc130=0;

    nb13=nbpac + ((double)nbera / 2) + (short)13;
    vl130=vlmoy * nb13;
    vc130=vlcol * nb13;
    sasp=arrondi_euro_voisin(((double)vl130) / 10);
    sass=arrondi_euro_voisin(((double)vc130) / 10);

    /* traitement pour calcul abspsa idem campagne 2016 */
    if (vlmoy==vlcol)
    {
        if (vlbpc<sasp)
        {
            *p_abspsa = 'O';
        }
        else
        {
            *p_abspsa = 'N';
        }
    }
    else
    {
        if (vlbpc<sass)
        {
            *p_abspsa = 'O';
        }
        else
        {
            *p_abspsa = 'N';
        }
    }
}
