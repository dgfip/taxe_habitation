
                /*-------------------------------------*/
                /* MODULE DE CALCUL DE LA CONTRIBUTION */
                /*        AUDIOVISUELLE PUBLIQUE       */
                /*-------------------------------------*/

#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif


int controle_krac (s_rae * p_rae) ;

/*============================================================================
   M A I N
  ============================================================================*/
int th_3krac ( s_rae * p_rae, s_ras * p_ras)
{
    int rets;                 /*code retour signature*/
    int ret;                  /*code retour contr�le_krac*/
    int ret_const;

    int retp=0;             /* code retour du programme th-krac*/
    long cotis;
    long frais;
    s_cons * p_cons;
    char dep97[]="97";
    int retour=0;
    static s_signature signature;


    /* TRAITEMENT */
    /* ---------- */
    p_ras->versr = 'A';

    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_ras->signature = &signature  ;

    /* controle signature */
    rets=controle_signature(RKRAC,p_ras->versr,&(p_ras->libelle));

    if (rets!=0)
    {
        return(rets);
    }

    /* controle des zones d'entree */
    ret=controle_krac(p_rae);
    if (ret != 0)
    {
        cherche_Erreur(ret,&(p_ras->libelle));
        return(ret);
    }
    ret_const=cherche_const(p_rae->antax,&p_cons);
    if (ret_const != 0)
    {
        cherche_Erreur(ret_const,&(p_ras->libelle)); /* mvo*/
        return (ret_const);
    }

    /*recuperation des constantes */
    /*----------------------------*/
    retour=strcmp(p_rae->dep,dep97);
    if (retour!=0)

    /* recuperation des constantes pour la metropole */
    {
        cotis=p_cons->cotis_redev_metro;
        frais=p_cons->frais_redev_metro;
    }
    else    /* recuperation des constantes pour les DOM */
    {
        cotis=p_cons->cotis_redev_dom;
        frais=p_cons->frais_redev_dom;
    }
    /* CA_CAP_0002 - CONSTITUTION DU CODE ROLE TV */
        p_ras->roltv[0] = p_rae->qtvrt;
        p_ras->roltv[1] = p_rae->degtv;
        p_ras->roltv[2] = p_rae->rgstv;

    /*CA_CAP_0003*/
    if ((p_rae->qtvrt == '0') && (p_rae->champ != 'C'))
    {
        strncpy(p_ras->roltv,"0N ", 4);
    }
    p_ras->cottv=0;
    p_ras->fratv=0;
    p_ras->somtv=0;
    p_ras->mdgtv=0;
    p_ras->nettv=0;

    if ((p_rae->champ=='C')&&(p_rae->qtvrt == '1'))
    {
        p_ras->cottv=cotis;
        p_ras->fratv=frais;
        p_ras->somtv = cotis + frais;
    }

    if (p_rae->degtv == ' ')
    {
        p_ras->nettv = p_ras->somtv;
    }
    else
    {
        p_ras->mdgtv = p_ras->somtv;
    }
    return(retp);
}

    /* controle entree */

int controle_krac(s_rae * p_rae)
{
    if ((strcmp(p_rae->dep,"01")<0) ||
        (strchr("0123456789",p_rae->dep[0])==NULL) ||
       ((strchr("0123456789AB",p_rae->dep[1])==NULL)&&
        (strchr("2",p_rae->dep[0])!=NULL)) ||
       ((strchr("0123456789",p_rae->dep[1])==NULL)&&
        (strchr("2",p_rae->dep[0])==NULL)))
    {
        return (2502);
    }

    if (strchr("CDEILMRST",p_rae->champ)==NULL)
    {
        return (2503);
    }

    if (strchr("01",p_rae->qtvrt) == NULL)
    {
        return (2504);
    }

    if (strchr("EJFIADVSZWK ",p_rae->degtv) == NULL)
    {
        return (2505);
    }

    if (p_rae->champ != 'C' && p_rae->qtvrt == '1')
    {
        return (2506);
    }

    if (p_rae->champ != 'C' && p_rae->degtv != ' ')
    {
        return (2507);
    }
    else
    {
        return (0);
    }
}
