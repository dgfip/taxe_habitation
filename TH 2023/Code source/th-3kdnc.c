    /*==============================*/
    /* MODULE DE DETERMINATION DU   */
    /*     NET A PAYER              */
    /*==============================*/


#if defined (ILIAD)
    #include <th-3ksts.h>
    #include <th-3kfos.h>
#else
    #include "th-3ksts"
    #include "th-3kfos"
#endif


    /*======*/
    /* MAIN */
    /*======*/

int th_3kdnc(
             s_dne * p_dne ,
             s_dns * p_dns)
{
    char versr = 'A';
    s_signature signature;
    int rets;                 /*code retour signature*/
    int ret;                 /* code retour sous-fonctions   */
    int retour;


    strncpy (signature.signature, SIGNATURE, LGSIGN);
    p_dns->signature = &signature  ;

    /* controle de la signature */
    /* ------------------------ */
    rets=controle_signature(RKDNC,versr,&(p_dns->libelle));
    if (rets!=0)
    {
        return(rets);
    }

    /* initialisation des zones de sortie */
    /* ---------------------------------- */
    init_sortie_kdnc(p_dns);
    /* Controle des valeurs d entree */
    /* ----------------------------- */

    /* somme a recouvrer de la THP */
    if (p_dne->somrp < 0)
    {
        retour = 1002;
        cherche_Erreur(retour,&(p_dns->libelle));
        return (1002);
    }

    /* presence d'une vl exoneree non nulle */
    if ((p_dne->vlexo !='O') && (p_dne->vlexo !='N'))
    {

        retour = 1009;
        cherche_Erreur(retour,&(p_dns->libelle));
        return (1009);
    }

        /* somme a recouvrer */
        if (p_dne->somrc < 0)
        {
            retour = 1003;
            cherche_Erreur(retour,&(p_dns->libelle));
            return (1003);
        }
        /* somme a recouvrer < somme a recouvrer de la THP */
        if (p_dne->somrc < p_dne->somrp)
        {
            retour = 1004;
            cherche_Erreur(retour,&(p_dns->libelle));
            return (1004);
        }

        /* revenu imposable du foyer fiscal mesures Macron */
        if (p_dne->revffm < 0)
        {
            retour = 1015;
            cherche_Erreur(retour,&(p_dns->libelle));
            return (1015);
        }


    /*==============================*/
    /* T  R  A  I  T  E  M  E  N  T */
    /*==============================*/
     if (p_dne->degex!= ' ')
        {
                        /* EXONERATION  degex � D, K */
                        /* ----------- */
                if (est_exoneration(p_dne->degex))
                {
                    if (p_dne->somrp > 0)
                    {
                        cherche_Erreur(1006,&(p_dns->libelle));
                        return (1006);
                    }
                    else
                    {
                        /*  bug 261512  */
                        if ((p_dne->vlexo == 'O') || (p_dne->degex=='D'))
                        /* fin bug 261512 */
                        {
                            ret=NaP_Exo(p_dne,p_dns);
                            if (ret!=0)
                            {
                                return ret;
                            }
                        }else /* cas ou degex est � exo mais vlexo est � 'N': ce cas se produit s'il n'y a pas de base � exon�rer du fait que VL -      abattement vaut 0 */
                        {
                        ret=NaP_Ni_Deg_Ni_Exo(p_dne,p_dns);
                            if (ret!=0)
                            {
                                return ret;
                            }                    }
                    }
                }
                else /* DEGEX � G*/
                {
                                /* DEGREVEMENT TOTAL*/
                                /* ----------- */
                   /* if (est_degrevement_total(p_dne->degex)) */ /*degex � 'G' */
                    /*{*/

                            ret=NaP_Deg_Tot(p_dne,p_dns);
                            if (ret!=0)
                            {
                                return ret;
                            }
                   /*}*/
                }
        }
        else
        {
                    /* AUCUN ALLEGEMENT DEGEX A BLANC OU THLV*/
                    /* ---------------- */

                ret=NaP_Ni_Deg_Ni_Exo(p_dne,p_dns);
                if (ret!=0)
                {
                    return ret;
                }

        }

    /*contr�le de coh�rence du bon deroulemnent de KDNC*/

    if ((strchr("DGK",p_dne->degex) != NULL) && (p_dns->netth != 0))
    {
        cherche_Erreur(1007,&(p_dns->libelle));
        return (1007);
    }

    if ((p_dne->degex == ' ') && (p_dns->degpl>0))
    {
        cherche_Erreur(1008,&(p_dns->libelle));
        return (1008);
    }

     if ((p_dns->netth < p_dne->p_cons->seuil_nap) && (p_dns->netth > 0))
    {
        cherche_Erreur(3021,&(p_dns->libelle));
        return (3021);
    }

    /* Controle coherence IMAP et SOMRP pour les mesures Macron */
        /*Bug 299400*/
    if (p_dne->somrp > 0 && p_dns->imap == 'M')
    {
        retour = 1016;
        cherche_Erreur(retour,&(p_dns->libelle));
        return (1016);
    }

   /*================*/
   /* FIN TRAITEMENT */
   /*================*/

    return (0);
}



    /* ===================================================== */
    /* fonction de calcul net a payer avec degrevement total */
    /* ===================================================== */

    int NaP_Deg_Tot(
                    s_dne *p_dne,        /* pointeur sur donnee entree */
                    s_dns *p_dns)        /* pointeur sur donnee sortie */
    {
        if (p_dne->somrp < 0)
        {
            return (1202);
        }
        if (p_dne->somrc < 0)
        {
            return (1203);
        }
        if (p_dne->somrc < p_dne->somrp) /* somrp est mis � somrc en entrant dans kdnc*/
        {
            return (1204);
        }

        p_dns->degpl = p_dne->somrc;
        p_dns->totth = 0;
        p_dns->netth = 0;
        if ( p_dne->somrc != 0 )
        {
            strncpy(p_dns->codro,"DT",3);
        }

        return (0);
    }
    /*-----------------------Fin de la fonction NaP_Deg_Tot-----------------------*/


    /* =============================================== */
    /* fonction de calcul net a payer avec exoneration */
    /* =============================================== */

    int NaP_Exo(s_dne *p_dne,    /* pointeur sur donnee entree */
                s_dns *p_dns)    /* pointeur sur donnee sortie */
    {
        if (p_dne->somrc != 0)
        {
           return (1303);
        }


        p_dns->degpl = 0;
        p_dns->totth = p_dne->somrc; /* c est � dire 0*/
        p_dns->netth = p_dns->totth; /* c est � dire 0*/
        strncpy(p_dns->codro,"EX",3);

        return (0);
    }
    /*-----------------------Fin de la fonction NaP_Exo---------------------*/





/*----------------------------------------------------------------------------*/
/*                    Initialisation de la zone de sortie                     */
/*----------------------------------------------------------------------------*/

    void  init_sortie_kdnc(s_dns * p_dns)
    {

        p_dns->degpl = 0;
        p_dns->totth = 0;
        p_dns->totthf = 0;
        p_dns->netth = 0;
        p_dns->netthf = 0;
        strncpy(p_dns->codro, "  ", 3);
        p_dns->imap      = ' '     ;
        strncpy(p_dns->codegm, "    ", 5);
        p_dns->txrefm = 0;


    }

