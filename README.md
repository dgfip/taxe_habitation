La calculette TH codée par la Direction Générale des Finances Publiques est mono-annuelle.

Elle permet de calculer pour une année de taxation donnée (AAAN) la TH, la THLV, la TLV et la contribution à l’audiovisuel public (CAP). 

Elle est composée des 18 modules suivants codés en langage C.


Modules communs  
TH-NKARC (constantes de taxation, fonctions communes, table des libellés des messages d’anomalies)
TH-NKSTS (structures internes)
TH-NKTOS (structures externes)
TH-NKFOS (entêtes de procédures)

Modules seuil du revenu fiscal de référence : 
TH-NKSFC (récupération des constantes de taxation)
TH-NKSEC (détermination droits à dégrèvement/exonération et condition de cohabitation)

Module revalorisation des VL : TH-NKCVL

Modules calculs généraux : 
TH-NKCAC
TH-NKRPC (recalcul de la valeur locative revalorisée neutralisée planchonnée et du planchonnement des locaux professionnels)
TH-NKTAC (prise en compte des délibérations des collectivités)
TH-NKACC (détermination des abattements à appliquer à la côte)
TH-NKLPC (calcul des cotisations des locaux professionnels)
TH-NKCOC (calcul des bases nettes imposables et des cotisations des locaux d’habitation)
TH-NKSRC (calcul des frais, prélèvements et sommes à recouvrer)
TH-NKDNC (détermination net à payer et code rôle TH)
TH-NKRAC (détermination CAP : code rôle, dégrèvement, cotisation)

Modules TLV
TH-NKTVC
TH-NKCVC


Le dossier documentation contient 3 fichiers : 
* PRO.TH.AAAN.calent - données en entrée.pdf 
* PRO.TH.AAAN.calsor – données en sortie.pdf 
* PRO.TH.AAAN.calano - liste des anomalies.pdf
